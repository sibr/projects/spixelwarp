# Depth Synthesis and Local Warps for plausible image-based navigation - Bayesian approach for selective image-based rendering using superpixels {#spixelwarpPage}

## Introduction

This *Project* contains the implementations of two papers:

[Chaurasia 13] *Depth synthesis and local warps for plausible image-based navigation*, (http://www-sop.inria.fr/reves/Basilic/2013/CDSD13/) and 

[Ortiz-Cayon 15] *A Bayesian approach for selective image-based rendering using superpixels* (http://www-sop.inria.fr/reves/Basilic/2015/ODD15/).

If you use the code, we would greatly appreciate it if you could cite the corresponding papers:
```
@Article{CDSD13,
  author       = "Chaurasia, Gaurav and Duch\^ene, Sylvain and Sorkine-Hornung, Olga and Drettakis, George",
  title        = "Depth Synthesis and Local Warps for Plausible Image-based Navigation",
  journal      = "ACM Transactions on Graphics",
  volume       = "32",
  year         = "2013",
  note         = "to be presented at SIGGRAPH 2013",
  keywords     = "Image-based rendering, image warp, superpixels, variational warp, wide baseline, multi-view stereo",
  url          = "http://www-sop.inria.fr/reves/Basilic/2013/CDSD13"
}
```

and/or:

```
@InProceedings{ODD15,
  author       = "Ortiz-Cayon, Rodrigo and Djelouah, Abdelaziz and Drettakis, George",
  title        = "A Bayesian Approach for Selective Image-Based Rendering using Superpixels",
  booktitle    = "International Conference on 3D Vision (3DV)",
  year         = "2015",
  publisher    = "IEEE",
  key          = "Image-based rendering, image warp, superpixels, variational warp, wide baseline, multi-view stereo",
  url          = "http://www-sop.inria.fr/reves/Basilic/2015/ODD15"
}
```

and of course the actual *sibr* system:

```
@misc{sibr2020,
   author       = "Bonopera, Sebastien and Hedman, Peter and Esnault, Jerome and Prakash, Siddhant and Rodriguez, Simon and Thonat, Theo and Benadel, Mehdi and Chaurasia, Gaurav and Philip, Julien and Drettakis, George",
   title        = "sibr: A System for Image Based Rendering",
   year         = "2020",
   url          = "https://sibr.gitlabpages.inria.fr/"
}
```

The code is based on the original implementations, but has gone through multiple versions. Most notably, the preprocessing tools have been completely rewritten, and thus the result of preprocessing is not exactly the same as the original codebase. As a result the quality is not exactly the same as that in the original paper.
 

---

## Authors
 
The original code for [Chaurasia 13] was developed by Gaurav Chaurasia, and for [Ortiz-Cayon 15] by Rodrigo Ortiz-Cayon and Abdelaziz Djelouah. Rewrites of the preprocessing tools are by Sai Praveen Bungaru and Uditha Kasthuriarachchi. Moves to newer versions by Sebastien Bonopera, Jerome Esnault, Siddhant Prakash and George Drettakis, who also supervised the entire *Project*.
 
 
---

## How to use

The code has been developed for Windows (10), and we currently only support this platform. A Linux version will follow shortly.

### Use the binary distribution

The easiest way to use *SIBR* to run [Chaurasia 13] and [Ortiz-Cayon 15] is to download the binary distribution. All steps described below, including all preprocessing for your datasets will work using this code.
Download the distribution from the page: https://sibr.gitlabpages.inria.fr/download.html (Spixelwarp, 150Mb); unzip the file and rename the directory "install". The ULR and texturedMesh viewers are included as part of sibr-core.

 
### Checkout the code
 
You will need to checkout SIBR Core. For this use the following commands:

```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/sibr_core.git
## through SSH
git clone git@gitlab.inria.fr:sibr/sibr_core.git
```

Then go to *src/projects* and clone the spixelwarp project:
```bash
## through HTTPS
git clone https://gitlab.inria.fr/sibr/projects/spixelwarp.git
## through SSH
git clone git@gitlab.inria.fr:sibr/projects/spixelwarp.git
```
 
### Configuration
 As for most of the projects, spixelwarp can be configured through SIBR Core CMake configuration by selecting `SIBR_IBR_SPIXELWARP` variable before running the configuration (see \ref sibr_configure_cmake)  [Configuring the solution](https://sibr.gitlabpages.inria.fr/docs/nightly/index.html#sibr_configure_cmake).
 
### Build & Install
 
You can build and install *spixelwarp* via running ALL_BUILD and/or INSTALL in sibr_projects.sln solution (as mentioned in \ref sibr_compile [compile page in the documentation](https://sibr.gitlabpages.inria.fr/docs/nightly/index.html#sibr_compile) or through `*sibr_swarp*` specific targets in sibr_projects.sln solution.
 Dont forget to build INSTALL if you use ALL_BUILD.

---
 
## Running the renderer
 
After installing *spixelwarp*, several apps should be available in `install\bin`, notably the swarp_app that runs the original [Chaurasia et al. 13] algorithm:
 
```cmd	
	SIBR_swarp_app.exe --path PATH_TO_DATASET
 	SIBR_swarp_rwdi_app.exe --path PATH_TO_DATASET
```

If the dataset has been preprocessed for the Selective IBR algorithm [Ortiz-Cayon et al. 15] (please see below), you can run the selection_app:
 
```cmd	
	sibr_selection_app.exe --path PATH_TO_DATASET
 	sibr_selection_rwdi_app.exe --path PATH_TO_DATASET
```

The quality is similar, but the selection_app is faster since it does not perform the warps for every superpixel.
Example datasets are given below.

Our interactive viewer has a main view running the algorithm and a top view to visualize the position of the calibrated cameras. By default you are in WASD mode, and can toggle to trackball using the "y" key. Please see the page [Interface](https://sibr.gitlabpages.inria.fr/docs/nightly/howto_sibr_useful_objects.html) for more details on the interface.


## Playing paths from the command line

Paths can be played by the renderers by running the renderer in offscreen mode:
```
SIBR_selection_app.exe --path PATH_TO_DATASET --offscreen --pathFile path.(out|lookat|tst|path) [--outPath optionalOutputPath --noExit]
SIBR_swarp_app.exe --path PATH_TO_DATASET --offscreen --pathFile path.(out|lookat|tst|path) [--outPath optionalOutputPath --noExit]
```
By default, the application exits when this operation is performed. This is the easiest way to compare algorithms, although interactive options exist for some *Projects*.

#### Bugs and Issues

We will track bugs and issues through the Issues interface on gitlab. Inria gitlab does not allow creation of external accounts, so if you have an issue/bug please email <code>sibr@inria.fr</code> and we will either create a guest account or create the issue on our side.
 
---
 
## Datasets
 
In this section, we explain how to create SuperpixelWarp datasets from your SfM/MVS data, the structure of the dataset and provide some example datasets.
 
### Dataset structure
 
To generate an SuperpixelWarp dataset from SfM/MVS data, use the process described in the [documentation page](https://sibr.gitlabpages.inria.fr/docs/nightly/howto_generate_dataset.html).

The SuperpixelWarp datasets have an additional directory,*spixelwarp* that contains additional information. In particular it contains the per-view depth information (in the *depth* directory), the superpixel information (in *superpixels*) with a few additional directories that are mainly for code legacy purposes. More information about these datastructures is given [below](https://sibr.gitlabpages.inria.fr/docs/nightly/spixelwarpDetails.html).
 
 
### Example Datasets
 
Some example datasets can be found here:
 		https://repo-sam.inria.fr/fungraph/sibr-datasets/

These datasets have been generated by colmap, and preprocessed with the current version of our tools you can find here.
You can download the Museum-Front-27 dataset here:
```
wget https://repo-sam.inria.fr/fungraph/sibr-datasets/museum_front27_sibr_swarp_selection_cm.zip
```
and the run the [Chaurasia 13] by going to <code>install\bin</code> and running:
```
	SIBR_swarp_app.exe --path DATASETS_PATH\museum_front27\sibr_cm
 	SIBR_swarp_rwdi_app.exe --path DATASETS_PATH\museum_front27\sibr_cm
```
Several other datasets are available on the same page, as well as for other algorithms.
 
---

## Preprocessing to create a new dataset from your images

---

### <a name="Swarp_RC"></a>Creating from Reality Capture

For preprocessing you will need COLMAP (*https://colmap.github.io/* version 3.6), Meshlab (*https://www.meshlab.net/*) (ATTN: Version 2020.07 *only*) and ImageMagick (https://imagemagick.org/script/download.php) installed on your system.

Follow the steps to create a SIBR dataset compatible with SPixelWarp application:

   - Create a SIBR Dataset by following instructions from: \ref howto_generate_dataset.
   - Go to *install/scripts*, and run the python script using:
  ```
  python ibr_swarp_preprocess_only.py -i dataset_path (typically Dataset\SibrData ) -d (typically Dataset\SwarpData ) [ --bin path_to_sibr_bin ]
  ```
   - Specifying the binaries directory is optional. While compiling cmake automatically generates settings file which is parsed by the script to set bin directory.
   - If the preprocessing apps are compiled in RelWithDeb mode, provide -r parameter while running the script.
   - The script calls the Swarp_scene2patch, Swarp_depth, Swarp_superpixel and Swarp_depthNormalSynthesis app executables from the dataset_tools project; make sure they are up to date.
       - The apps compute the MVS Patch points, superpixels, and depth data required by the superpixel algorithm and stores them in the spixelwarp directory.
       - Additionally, the clipping planes are recomputed to create superpixels and the new computed clipping planes are stored in separate file clipping_planes.txt in spixelwarp directory.
       - A new scene_metadata.txt file is also generated with revised clipping planes in the spixelwarp directory.
   - *[Recommended]* If you do not want to create a copy of the dataset, you can only specify the input directory with -i option. The dataset will be generated within the input directory itself.
  
  
### <a name="SwarpColmap"></a>Creating from Colmap

Run colmap using the instructions provided in the @ref HowToColmap ([Colmap Preprocessing](https://sibr.gitlabpages.inria.fr/docs/nightly/index.html)) section of the documentation.

Go to *install/scripts* and run
```
python colmap2swarp.py --path PATH_TO_DATASET
```
where *PATH_TO_DATASET* is the dataset directory containing the the *colmap* folder. This script first converts the colmap reconstruction to a native *sibr* datasets, notably cropping the images so they all have the same size. The converted dataset is put in the *PATH_TO_DATASET/sibr* folder. The script then runs the *ibr_swarp_preprocess_only.py* script described above to run the preprocessing specific to spixelwarp.

**Note:** Preprocessing large datasets or datasets with large images can be slow and require a lot of memory. We recommend using images with width < 2500 resolution. For datasets with more than ~50 images, 64Gb of RAM may be required.

### <a name="Selection"></a>Preprocessing for Selective IBR [Ortiz-Cayon et al. 15]

You need to run an additional step to run the selective rendering app.
Go to *install/scripts* and run
```
python ibr_selection_preprocess.py -d PATH_TO_DATASET
```

This will run the plane estimation and the quality evaluation step. Quality evaluation is *very* slow. Time to get a coffee. This will create two directories "planes" and "densities" that contain the informatio for the selective IBR algorithm.

---
@subpage spixelwarpDetails
