/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <fstream>
#include <fstream>
#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>
#include <core/scene/ParseData.hpp>
#include <core/scene/BasicIBRScene.hpp>

#include "projects/spixelwarp/renderer/SpixelWarpScene.hpp"
#include "projects/ulr/renderer/ULRV3View.hpp"

#define PROGRAM_NAME "sibr_swarp_ulr_compare"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path""\n";

struct CompareAppArgs :
	virtual BasicIBRAppArgs {
};

int main(int ac, char** av)
{
	{

		// Parse Commad-line Args
		CommandLineArgs::parseMainArgs(ac, av);
		CompareAppArgs myArgs;
		SwarpAppArgs swarpArgs;
		ULRAppArgs ulrArgs;
		

		// [Option 1] To create the cameras with swarp clipping planes
		//myArgs.scene_metadata_filename = "spixelwarp/scene_metadata.txt";

		const bool doVSync = !myArgs.vsync;
		// rendering size
		uint rendering_width = myArgs.rendering_size.get()[0];
		uint rendering_height = myArgs.rendering_size.get()[1];
		// window size
		uint win_width = myArgs.win_width;
		uint win_height = myArgs.win_height;

		// Window setup
		sibr::Window        window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs);

		// Setup the scene: load the proxy, create the texture arrays.
		const uint flags = SIBR_GPU_LINEAR_SAMPLING | SIBR_FLIP_TEXTURE;
		// Setup ULR IBR Scene
		BasicIBRScene::Ptr		ulr_scene(new BasicIBRScene(ulrArgs));
		ulr_scene->renderTargets()->initRGBandDepthTextureArrays(ulr_scene->cameras(), ulr_scene->images(), ulr_scene->proxies(), flags);
		

		// check rendering size
		rendering_width = (rendering_width <= 0) ? ulr_scene->cameras()->inputCameras()[0]->w() : rendering_width;
		rendering_height = (rendering_height <= 0) ? ulr_scene->cameras()->inputCameras()[0]->h() : rendering_height;

		// check rendering size
		Vector2u usedResolution(rendering_width, rendering_height);
		std::cerr << "Used Resolution " << rendering_width << " x " << rendering_height << std::endl;

		const unsigned int sceneResWidth = usedResolution.x();
		const unsigned int sceneResHeight = usedResolution.y();

		// Create the ULR view which passes data required to the renderer for each rendered image
		ULRV3View::Ptr	ulrView(new ULRV3View(ulr_scene, sceneResWidth, sceneResHeight));

		// Setup Swarp IBR Scene
		SpixelWarpScene::Ptr		swarp_scene(new SpixelWarpScene(ulr_scene, swarpArgs));
		// Create the SpixelWarp view which passes data required to the renderer for each rendered image
		SpixelWarpView::Ptr		swarpView(new SpixelWarpView(swarp_scene, 4, myArgs.dataset_path, sceneResWidth, sceneResHeight));
		
		// Raycaster.
		std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
		raycaster->init();
		raycaster->addMesh(ulr_scene->proxies()->proxy());

		// Camera handler for main view.
		sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());

		if (!ulr_scene->cameras()->inputCameras().empty()) {
			generalCamera->setup(ulr_scene->cameras()->inputCameras(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);
		}
		else {
			generalCamera->setup(ulr_scene->proxies()->proxy().getBoundingBox(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);
		}

		// Add views to mvm.
		MultiViewManager        multiViewManager(window, false);
		multiViewManager.addIBRSubView("Compare view", ulrView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
		multiViewManager.addCameraForView("Compare view", generalCamera);

		// Top view
		const std::shared_ptr<sibr::SceneDebugView>    topView(new sibr::SceneDebugView(ulr_scene, multiViewManager.getViewport(), generalCamera, myArgs));
		multiViewManager.addSubView("Top view", topView);

		enum Algo : int {
			ULR = 0, SWARP
		};
		Algo currentAlgo = ULR;

		std::vector<sibr::ViewBase::Ptr> views;
		std::vector<sibr::BasicIBRScene::Ptr> scenes;
		scenes.push_back(ulr_scene);
		views.push_back(ulrView);
		scenes.push_back(swarp_scene);
		views.push_back(swarpView);

		while (window.isOpened())
		{
			sibr::Input::poll();
			window.makeContextCurrent();

			if (sibr::Input::global().key().isActivated(sibr::Key::Escape))
				window.close();

			multiViewManager.onUpdate(sibr::Input::global());
			if (ImGui::Begin("Comparison")) {
				if (ImGui::Combo("Algo", (int*)&currentAlgo, "ULR\0SWARP\0\0")) {
					multiViewManager.getIBRSubView("Compare view") = views[int(currentAlgo)];
				}
			}
			ImGui::End();

			if (sibr::Input::global().key().isReleased(sibr::Key::N)) {
				if (currentAlgo == ULR) {
					currentAlgo = SWARP;
				}
				else {
					currentAlgo = static_cast<Algo>(currentAlgo - 1);
				}
				std::cout << "Switch to previous algorithm" << static_cast<Algo>(currentAlgo) << std::endl;
				multiViewManager.getIBRSubView("Compare view") = views[int(currentAlgo)];
				topView->setScene(scenes[int(currentAlgo)], true);				
			}

			if (sibr::Input::global().key().isReleased(sibr::Key::M)) {
				if (currentAlgo == SWARP) {
					currentAlgo = ULR;
				}
				else {
					currentAlgo = static_cast<Algo>(currentAlgo + 1);
				}
				std::cout << "Switch to next algorithm: " << static_cast<Algo>(currentAlgo) << std::endl;
				multiViewManager.getIBRSubView("Compare view") = views[int(currentAlgo)];
				topView->setScene(scenes[int(currentAlgo)], true);				
			}

			multiViewManager.onRender(window);

			window.swapBuffer();
		}
	}


	return EXIT_SUCCESS;
}
