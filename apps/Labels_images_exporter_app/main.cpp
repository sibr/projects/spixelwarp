/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include <fstream>
#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>
#include <core/scene/ParseData.hpp>
#include <projects/spixelwarp/renderer/SpixelWarpScene.hpp>
#include <projects/spixelwarp/renderer/SelectionView.hpp>



#define PROGRAM_NAME "sibr_labels_images_exporter_app"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;



int main(int ac, char** av)
{
	{
		// Parse Commad-line Args
		CommandLineArgs::parseMainArgs(ac, av);
		SwarpAppArgs myArgs;

		// rendering size
		uint rendering_width = myArgs.rendering_size.get()[0];
		uint rendering_height = myArgs.rendering_size.get()[1];

		sibr::Window			window(100, 100, PROGRAM_NAME, myArgs);
		SpixelWarpScene::Ptr		scene(new SpixelWarpScene(myArgs));

		// check rendering size
		rendering_width = (rendering_width <= 0) ? scene->cameras()->inputCameras()[0]->w() : rendering_width;
		rendering_height = (rendering_height <= 0) ? scene->cameras()->inputCameras()[0]->h() : rendering_height;
				
		// check rendering size
		Vector2u usedResolution(rendering_width, rendering_height);
		std::cerr << "Used Resolution " << rendering_width << " x " 
			<< rendering_height << std::endl;


		const unsigned int sceneResWidth = usedResolution.x();
		const unsigned int sceneResHeight = usedResolution.y();

		// Raycaster.
		std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
		raycaster->init();
		raycaster->addMesh(scene->proxies()->proxy());

		// Camera handler for main view.
		sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
		generalCamera->setup(scene->cameras()->inputCameras(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);


		SelectionView	selectionView(scene, 4, myArgs.dataset_path, sceneResWidth, sceneResHeight );
		
		std::string outputFolder = myArgs.dataset_path.get() + "/spixelwarp/labels/";

		std::vector<sibr::InputCameraPreprocess> camlist;
		camlist.resize(scene->cameras()->inputCameras().size());

		std::string image_dir = myArgs.dataset_path.get();

		int i = 0;
		for (int i = 0; i < scene->cameras()->inputCameras().size(); i++) {
			camlist[i] = *scene->cameras()->inputCameras()[i];
			camlist[i].loadSuperpixelsAndReconstruction(image_dir); 	/** For each camera, load superpixels color info and reconstruction info. */
		}
		

		for (uint i = 0; i < camlist.size(); ++i) {
			auto image = scene->images()->inputImages().at(i);
			sibr::ImageRGB imageToExport( image->w()/2, image->h()/2);
			auto camera = camlist.at(i);
			auto spixelsData = selectionView.getSelectionLabel()->getLabels(i);

			auto camPreprocess = camlist[i];
			auto spixelImage = camPreprocess.getSpixelImage();
			auto spixels = spixelImage->getSpixels();
			for (uint j = 0; j < spixels.size(); j++) {
				auto labels = spixelsData.at(j);
				sibr::ColorRGBA color;
				if (labels.at(3)) { // If the spixel is a swarp
					color = { 0.f, 0.f, 1.f, 0.5f };
				}
				else if (labels.at(1)) { // if the spixel is a plan
					color = { 1.f, 0.f, 0.f, 0.5f };
				}
				else if (labels.at(2)) { // if the spixel is a plan
					color = { 1.f, 0.5f, 0.f, 0.5f };
				}
				for (uint k = 0; k < camPreprocess.getSpixelImage()->getSpixel(j).getPixels().size(); k++)
				{
					auto pixel = camPreprocess.getSpixelImage()->getSpixel(j).getPixels().at(k);
					//spixelsData
					sibr::ColorRGBA basedColor = image->color(
						pixel.x()*2,
						image->h()-1-(pixel.y())*2);
					sibr::ColorRGBA mergedColor = color * 0.5f + basedColor * 0.5f;
					imageToExport.color(pixel.x(), imageToExport.h()-1-pixel.y(), mergedColor);
				}
			}
			imageToExport.save(outputFolder + "labels" + std::to_string(i) + ".png");
			
		}


		//ibrView.addSubView(selectionView);

	}
	//catch (std::exception& e)
	//{
	//	SIBR_ERR << e.what() << std::endl;
	//	return EXIT_FAILURE;
	//}

	return EXIT_SUCCESS;
}
