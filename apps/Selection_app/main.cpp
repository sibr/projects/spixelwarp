/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once

#include <fstream>
#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>
#include <core/scene/ParseData.hpp>
#include <projects/spixelwarp/renderer/SpixelWarpScene.hpp>
#include <projects/spixelwarp/renderer/SelectionView.hpp>



#define PROGRAM_NAME "sibr_selection_app"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;



int main(int ac, char** av)
{
	{
		// Parse Commad-line Args
		CommandLineArgs::parseMainArgs(ac, av);
		SwarpAppArgs myArgs;

		// rendering size
		uint rendering_width = myArgs.rendering_size.get()[0];
		uint rendering_height = myArgs.rendering_size.get()[1];

		sibr::Window        window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs, getResourcesDirectory() + "/spixelwarp/" + PROGRAM_NAME + ".ini");
		SpixelWarpScene::Ptr		scene(new SpixelWarpScene(myArgs));

		// check rendering size
		rendering_width = (rendering_width <= 0) ? scene->cameras()->inputCameras()[0]->w() : rendering_width;
		rendering_height = (rendering_height <= 0) ? scene->cameras()->inputCameras()[0]->h() : rendering_height;
				
		// check rendering size
		Vector2u usedResolution(rendering_width, rendering_height);
		std::cerr << "Used Resolution " << rendering_width << " x " 
			<< rendering_height << std::endl;


		const unsigned int sceneResWidth = usedResolution.x();
		const unsigned int sceneResHeight = usedResolution.y();

		// Raycaster.
		std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
		raycaster->init();
		raycaster->addMesh(scene->proxies()->proxy());

		// Camera handler for main view.
		sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
		generalCamera->setup(scene->cameras()->inputCameras(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);

		//IBRView				ibrView(window, scene);

		ViewBase::Ptr	selectionView(new SelectionView(scene, 4, myArgs.dataset_path, sceneResWidth, sceneResHeight ));
		//ibrView.addSubView(selectionView);

		MultiViewManager        multiViewManager(window, false);
		multiViewManager.addIBRSubView("Selection view", selectionView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
		multiViewManager.addCameraForView("Selection view", generalCamera);

		if (myArgs.pathFile.get() !=  "" ) {
			generalCamera->getCameraRecorder().loadPath(myArgs.pathFile.get(), usedResolution.x(), usedResolution.y());
			generalCamera->getCameraRecorder().recordOfflinePath(myArgs.outPath, multiViewManager.getIBRSubView("Selection view"), "selection");
			if( !myArgs.noExit )
				exit(0);
		}

		while (window.isOpened())
		{
			sibr::Input::poll();
			window.makeContextCurrent();

			if (sibr::Input::global().key().isPressed(sibr::Key::Escape))
				window.close();

			multiViewManager.onUpdate(sibr::Input::global());

			window.viewport().bind();
			multiViewManager.onRender(window);

			window.swapBuffer();
		}
	}
	//catch (std::exception& e)
	//{
	//	SIBR_ERR << e.what() << std::endl;
	//	return EXIT_FAILURE;
	//}

	return EXIT_SUCCESS;
}

