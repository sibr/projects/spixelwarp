# Superpixel Warp Rendering CLI options, Data Structures and Rendering Details {#spixelwarpDetails}

@ingroup projects

@tableofcontents

## CLI options

| name                 | type      | Required  | default value             | description                                   |
| -------------------- | --------- | --------- | ------------------------- | --------------------------------------------- |
| **Basic app options**            |||||
| appPath              | string    | false     |   "./"                    | define a custom app path                      |
| help                 | bool      | false     |   false                   | display this help message                     |
| **Basic window options**         |||||
| width                | int       | false     |   720                     | initial window width                          |
| height               | int       | false     |   480                     | initial window height                         |
| vsync                | int       | false     |   1                       | enable vertical sync                          |
| fullscreen           | bool      | false     |   false                   | set the window to fullscreen                  |
| hd                   | bool      | false     |   false                   | rescale UI elements for high-density screens  |
| nogui                | bool      | false     |   false                   | do not use ImGui                              |
| gldebug              | bool      | false     |   false                   | enable OpenGL error callback                  |
| **Basic rendering options**      |||||
| scene                | string    | false     |   "scene_metadata.txt"    | scene metadata file                           |
| rendering-size       | Vector2i  | false     |   { 0, 0 }                | size at which rendering is performed          |
| texture-width        | int       | false     |   0                       | size of the input data in memory              |
| texture-ratio        | float     | false     |   1.0f                    |                                               |
| rendering-mode       | int       | false     |   RENDERMODE_MONO         | select mono (0) or stereo (1) rendering mode  |
| focal-pt             | Vector3f  | false     |   { 0.0f, 0.0f, 0.0f }    |                                               |
| colmap_fovXfovY_flag | Switch    | false     |   false                   |                                               |
| **Basic dataset options**        |||||
| path                 | string    | true      |                           | path to the dataset root                      |
| dataset_type         | string    | false     |   ""                      | type of dataset                               |
| **SuperpixelWarp specific options**         |||||
| poisson-blend        | bool      | false     |   false                   | apply Poisson-filling to the ULR result       |


## Details on Data structures and Render passes

### SpixelWarp input dataset formats

|  			files									| 		comeFrom (if not from dataset generation process)		 						  | format (Text/Binary) |
| :-----------------------------------------------: | :---------------------------------------------: | :------------------: |
| images (undistorted)		| computed from [RadialUndistort](http://www.cs.cornell.edu/~snavely/bundler/bundler-v0.4-manual.html::S3) or equivalent [VSFM] (http://ccwu.me/vsfm/)... | Jpg |
| bundle.out		| computed from [bundler](http://www.cs.cornell.edu/~snavely/bundler/) or equivalent [VSFM] (http://ccwu.me/vsfm/)... | Txt |
| list_images.txt	| computed from hand | Txt |
| [clipping_planes.txt](#clipping_planes)	| computed from the depth maps (see @ref pageReprojection) | Txt |
| [active_images.txt](#active_images)		| create by hand to list/select/filter all input images we want to process| Txt (optional)|
| [superpixel/%8d.sp (superpixel/00000000.sp)](#superpixel)			| computed from superpixel project (SLIC algo) (see [main page](../README.md)) | Bin |
| [superpixel/spixel_graph.txt](#spixel_graph)		| computed from superpixel project (see [main page](../README.md)) | Txt |
| [depth/%8d.mvs (depth/00000000.mvs)](#depth_mvs)		| computed from matlab stuff propagation depth synthesis (see @ref pagePropagation)| Txt(ascii) |
| [depth/%8d.png (depth/00000000.png)](#depth)	| computed from matlab stuff propagation depth synthesis (see @ref pagePropagation)| Png|

### <a name="clipping_planes"></a>clipping_planes.txt
Content: The near and the far clipping planes informations compute from the nearest and the farest 3D reconstructed points

Description: We cumulate all the depth maps files and extract the near and far clipping plane for the virtual camera.
```
<near> <far>	[two floats]
```


### <a name="active_images"></a>active_images.txt
Content: If we want to select which image we want to process, we create this file and put all input images indexes

Description:
```
<ActiveImageID> <ActiveImageID> <ActiveImageID> <...>
```

### <a name="superpixel"></a>superpixel/%8d.sp (superpixel/00000000.sp)
Content: This is the oversegmented image where each pixel of a superpixel (segmented from SLIC superpixel project [main page](../README.md)) have a unique ID

Description: **Be careful, this is a binary file, so the description here is just to have a representation of its content.**
```
<width> <height> [resolution as tow intergers]
<pixel(0,height)=spixelID> <pixel(1,height)=spixelID> <pixel(2,height)=spixelID> <...> <pixel(width,height)=spixelID> [ID as an interger]
 ...
<pixel(0,0)=spixelID> <pixel(1,0)=spixelID> <pixel(2,0)=spixelID> <...> <pixel(width,0)=spixelID> [ID as an interger]
```

### <a name="spixel_graph"></a>superpixel/spixel_graph.txt
Content: A graph of neighbors for each superpixel overall segmented images 

Description: Since it's an ascii file, line breaks are clearly marked. 
```
<totalSuperpixelsNumber>  [over all images]
<numberOfNeighbors> <<idCamNeighbor1> <idSuperpixelNeighbor1>> ... <<idCamNeighborN> <idSuperpixelNeighborN>>  [for first superpixel]
<numberOfNeighbors> <<idCamNeighbor1> <idSuperpixelNeighbor1>> ... <<idCamNeighborN> <idSuperpixelNeighborN>>  [for second superpixel]
...
<numberOfNeighbors> <<idCamNeighbor1> <idSuperpixelNeighbor1>> ... <<idCamNeighborN> <idSuperpixelNeighborN>>  [for last superpixel]
```

### <a name="depth_mvs"></a>depth/%8d.mvs (depth/00000000.mvs)
Content: This file contain all informations about back projected reconstructed points cloud and additional points depth synthesis 

Description: Since it's an ascii file, line breaks are clearly marked. 
```
<width> <height> <numOfSuperpixels>
<superpixel1> 
<superpixel2> 
...
<superpixelN> 
```
Where \<superpixelI\> contains (space delimited) :
```
<idSuperpixel> <numberOfPoints> <medianDepth> <point1> <point2> ... <pointN>
```
Where \<pointI\> contain :
```
<x> <y> <depth> [in range -1;1]
```

### <a name="depth"></a>depth/depth_%8d.png (depth/depth_00000000.png)
Content: This file is only used for the render top view (see Reproject::Reproject). It load it to re-projected 3D points cloud according to depth (gray scale) information in the image

Description: 

### <a name="data-structure"></a>Superpixels data structure
* SPixelImage class load the result of the oversegmentation (one instance by input image)
* Each pixel within the oversegmented image with the same value **id** (SPixelImage::_id) is a part of a new SPixel with a **global_id** SPixel::_gid (over all images)
* SPixelImage::map compute a data texture which contain for each pixel of the oversegmented image the current global id of the pixel and its 3 closest neighbors : 
  in canal R: the current SPixel::_gid associated to the pixel 
  in canal G: the neighbor SPixel::_gid if depth diff > 0.08 (otherwise repeat the current SPixel::_gid of the superpixel) 
  in canal B: the neighbor SPixel::_gid if depth diff > 0.08 (otherwise repeat the current SPixel::_gid of the superpixel) 
  in canal A: the neighbor SPixel::_gid if depth diff > 0.08 (otherwise repeat the current SPixel::_gid of the superpixel) 
![Fig.1 : depth superpixels proximities data image](superpixel_cmap_data_structure.png)
**From the Fig.1 :**We have a texture where each pixel have 4 components which correspond to the best closest depth proximities for a pixel coord.
This will be useful at render time for [the first render pass](#first-render-pass) in `warp.fp`.

## Render step (for each novel view)

### <a name="first-render-pass"></a>first render pass (camera selection and shape-preserving warp):
 * We select the 4 closest input cameras of the new point of view (novel camera we want to render) => See RendererGL::select_warp_images 

**Then for each closest cameras (RendererGL::render_warped):**

 * We solve sparse linear system for each local warps (each superpixel transformations vertices computation) => ImageWarper::warp 
![Fig.2 : mesh for shape preserving warp](warpMesh.png)
In fact, from the initialisation (before render loop), we overlay a 2D boundingBox on each superpixel of each images => see ImageWarper::init. 
Little trick: we take a little wider (about 1%) Bbox to avoid depth sample lying on boundary.
We took some random points inside our superpixels. 
This will give us vertices we used (in addition of 4 corner BBox) to triangulate a 2D mesh over our superpixels. 
Note that vertices are not necessary a point with depth information. Triangles may contain samples depth or not.
Then, each superpixel of an image have a mesh associate to the input view matrix. 
With the novel view matrix we can find the vertices transformations to preserve shape in the novel view. 
Then we can assign a unique index number for each vertices of each mesh of superpixels to get one big single Vertex Buffer Object to process. 
We filled the vertex array (coord between [-1;1]). 
We filled the coord text array [u;v] between [0;1] **AND** median depth of mesh corresponding superpixel **AND** the superpixel global_id 
Texture coord had 4 components [u;v;d;gid].

 * We render the warped image of the novel view (ImageWarper::renderWarp) using shader (`ibr.vp` and `warp.fp`)
With prepared data structures, on glDrawElements call, our vertex shader and fragment shader can works : 
`ibr.vp` : take vertex from vertices and coordTexture of this vertex (which contain extra infos [u;v;d;gid]).
It disable the ModelViewMatrix projection, put the tranformed vertex and pass the coordtext to the `warp.fp`.
Note that the vec4 color will be used for another purpose (see RendererGL::render_top_view).
`warp.fp` : take the input image and the oversegmented image (both resized to the window size) and the vec4 coordtext from the `ibr.vp`.
Here we test if the pixel we have to render is a part of the current superpixel. 
Indeed, we try to render pixel from the superpixel mesh based on a Bbox, but all pixels of the Bbox are not necessarly a part of the superpixel.
So we need to check the global_id of the pixel and for those pixel outside the superpixel, we need to test if the depth of one of the 3 best neighbors superpixel match the depth of the current superpixel. 
Depth evaluations has already been done at the begining and can be retrieved from the [Superpixel Data Structure](#data-structure). 
So, with textCoord we can access a pixel of the texture data and recover the 3 closest superpixel global_id. 
**The objective here is to fill cracks between warped superpixel due to the transformation.** 
So, only for pixels on "out-border" of the superpixel and in a "same depth" neighbors superpixel, we put the depth information and take the RGB value of the input image to expose as output.
Note that we also expose the Warped texture coords, the current superpixel id and the depth info in a vec4.
Otherwise, we discard the pixel render.
![Fig.3 : warp pixel shader](warp_pixel_shader.png)
**From the Fig.3 :** We evaluate the mesh of the red target superpixel to be rendered.
We need to see if pixel (inside the mesh but outside the superpixel area) have to be drawn or not.
We take the depth superpixels proximities data image from Fig.1 and extract eligible global_id neighbors for that pixel in order to compare with the current global_id red target superpixel we are processing.
This is possible thanks to our current coordText we took from our previous `ibr.vp`.
We found correspondance ? yes => we continue the draw process for that pixel; no otherwise => we cancel the process for that pixel.

### second render pass (blending from warped images):
Here the main objective is, for each pixel of superpixel warped (from 4 previous render target images), we want to select only the 2 best superpixel candidate to blend together with a weight.
Selection and weight criteras are :
1- Computing angle penality to keep only 2 best candidates [Buehler et al. 2001]
2- Check the superpixel graph correspondance (across all images)
  2.1- If the pixels to be blended have a correspondence edge (neighbor), we use the weights computed above to blend both superpixel
  2.2- If superpixels do not have correspondence and 1 superpixel contains "true" depth samples (obtained from PMVS/VSFM) while the other contains depth samples added by our synthesis, we increase the weight of the former by a factor of 2.0
  2.3- In all other cases, we increase the blending weight of the pixel with the higher depth value by a factor of 2.0

### third render pass (hole filling):
Here, with a "ping-pong" shader we implement the POISSON hole filling.
We solve the Poisson equation [Perez et al. 2003] with zero gradient values to create blurred color in such holes.