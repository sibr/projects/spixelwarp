# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


set(PROJECT_PAGE "spixelwarpPage")
set(PROJECT_LINK "https://gitlab.inria.fr/sprakash/spixelwarp")
set(PROJECT_DESCRIPTION "Depth Synthesis and Local Warps for plausible image-based navigation, paper reference: http://www-sop.inria.fr/reves/Basilic/2013/CDSD13/ ; Bayesian approach for selective image-based rendering using superpixels, paper reference: http://www-sop.inria.fr/reves/Basilic/2015/ODD15/ )")
set(PROJECT_TYPE "OURS")
