/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>
#include <iostream>
#include <core/system/CommandLineArgs.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/raycaster/CameraRaycaster.hpp>
#include <core/assets/ImageListFile.hpp>
#include <core/system/Utils.hpp>

#include <core/imgproc/DistordCropUtility.hpp>
#include <core/imgproc/CropScaleImageUtility.hpp>

#define PROCESSING_BATCH_SIZE 150

#define PROGRAM_NAME "sibr_colmap_to_swarp"
typedef boost::filesystem::path Path;


using namespace boost::filesystem;
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path " "\n"
;

struct ColmapPreprocessArgs :
	virtual BasicIBRAppArgs {
};

int main(const int argc, const char** argv)
{

	CommandLineArgs::parseMainArgs(argc, argv);
	ColmapPreprocessArgs myArgs;

	DistordCropUtility distordCrop;
	CropScaleImageUtility cropImg;

	std::string pathScene = myArgs.dataset_path;
	std::string outputFolder = myArgs.dataset_path.get() + "/spixelwarp/images";
	std::string scaledDownOutputFolder = myArgs.dataset_path.get() + "/spixelwarp/half_size";
	std::vector<std::string> dirs = { "spixelwarp/cameras", "spixelwarp/half_size", "spixelwarp/meshes", "spixelwarp/images" };


	for (auto dir : dirs) {
		std::cout << dir << std::endl;
		if (!boost::filesystem::exists(pathScene + "/" + dir.c_str())) {
			sibr::makeDirectory(pathScene + "/" + dir.c_str());
		}
	}


	std::cout << "Generating SIBR Spixelwarp Scene." << std::endl;

	BasicIBRScene scene(myArgs, true);

	Path root(myArgs.dataset_path.get());
	Path imgs;
	std::string meshPath;

	if (scene.data()->datasetType() == sibr::IParseData::Type::MESHROOM) {
		imgs = scene.data()->basePathName() + "/PrepareDenseScene/" + sibr::listSubdirectories(scene.data()->basePathName() + "/PrepareDenseScene/")[0];
		meshPath = scene.data()->basePathName() + "/Texturing/" + sibr::listSubdirectories(scene.data()->basePathName() + "/Texturing/")[0] + "/texturedMesh.obj";
		sibr::copyFile(meshPath, pathScene + "/spixelwarp/meshes/recon.obj", true);
		meshPath = scene.data()->basePathName() + "/Texturing/" + sibr::listSubdirectories(scene.data()->basePathName() + "/Texturing/")[0] + "/texturedMesh.mtl";
		sibr::copyFile(meshPath, pathScene + "/spixelwarp/meshes/texturedMesh.mtl", true);
		std::string meshTexturePath = scene.data()->basePathName() + "/Texturing/" + sibr::listSubdirectories(scene.data()->basePathName() + "/Texturing/")[0] + "/texture_1001.png";
		sibr::copyFile(meshTexturePath, pathScene + "/spixelwarp/meshes/texture_1001.png", true);
	}
	else {
		imgs = scene.data()->basePathName() + "/images/";
		meshPath = scene.data()->meshPath();
		sibr::copyFile(meshPath, pathScene + "/spixelwarp/meshes/recon.ply", true);
	}

	std::vector< Path > imagePaths;
	directory_iterator it(imgs), eod;
	std::vector<sibr::Vector2i> resolutions;

	// load the cams
	std::vector<InputCamera::Ptr>	cams = scene.cameras()->inputCameras();
	std::vector<InputCamera::Ptr>	newCams;
	const int maxCam = cams.size();
	const int minCam = 0;

	
	std::ofstream outputBundleCam;
	std::ofstream outputListIm;

	outputBundleCam.open(pathScene + "/spixelwarp/cameras/bundle.out");
	outputListIm.open(pathScene + "/spixelwarp/images/list_images.txt");
	outputBundleCam << "# Bundle file v0.3" << std::endl;
	outputBundleCam << maxCam << " " << 0 << std::endl;

	Path p;
	for (int i = 0; i < cams.size(); i++) {
		p = scene.data()->basePathName() + "/images/" + cams[i]->name();
		imagePaths.push_back(p);
	}


	sibr::Vector2i avgResolution = distordCrop.calculateAvgResolution(imagePaths, resolutions);

	std::cout << "Average camera resolution: (" << avgResolution.x() << "," << avgResolution.y() << ")" << std::endl;
	sibr::Vector2i minSize = distordCrop.findBiggestImageCenteredBox(root, imagePaths, resolutions, avgResolution.x(), avgResolution.y());
	std::cout << "[distordCrop] minSize " << minSize[0] << "x" << minSize[1] << std::endl;
	int minWidth = minSize[0];
	int minHeight = minSize[1];

	int new_half_w = (minWidth % 2 == 0) ? (minWidth / 2) : (--minWidth / 2);
	int new_half_h = (minHeight % 2 == 0) ? (minHeight / 2) : (--minHeight / 2);

	while ((new_half_w % 4) != 0) { --new_half_w; }
	while ((new_half_h % 4) != 0) { --new_half_h; }

	sibr::Vector2i cropResolution(2 * new_half_w, 2 * new_half_h);
	std::cout << "Crop resolution: (" << cropResolution.x() << "," << cropResolution.y() << ")" << std::endl;
	/**
	* \todo TODO:[SP] Handle exclude_images.txt
	*/

	/* Crop Images and Scale down to get Half Size Images*/
	float scaleDownFactor = 0.5f;
	bool scaleDown = (scaleDownFactor > 0);
	cv::Size resizedSize(cropResolution[0] * scaleDownFactor, cropResolution[1] * scaleDownFactor);



	// calculate nr batches
	unsigned nrBatches = static_cast<int>(ceil((float)(imagePaths.size()) / PROCESSING_BATCH_SIZE));

	std::chrono::time_point <std::chrono::system_clock> start, end;
	start = std::chrono::system_clock::now();

	// run batches sequentially
	for (unsigned batchId = 0; batchId < nrBatches; batchId++) {

		unsigned nrItems = (batchId != nrBatches - 1) ? PROCESSING_BATCH_SIZE : ((nrBatches * PROCESSING_BATCH_SIZE != imagePaths.size()) ? (imagePaths.size() - (PROCESSING_BATCH_SIZE * batchId)) : PROCESSING_BATCH_SIZE);

#pragma omp parallel for
		for (int localImgIndex = 0; localImgIndex < nrItems; localImgIndex++) {

			unsigned globalImgIndex = (batchId * PROCESSING_BATCH_SIZE) + localImgIndex;

			// using next code will keep filename in output directory
			Path boostPath = imagePaths[globalImgIndex];
			try
			{
				std::stringstream ss, ssc;
				ss << std::setfill('0') << std::setw(8) << cams[globalImgIndex]->id() << boostPath.extension().string();
				ssc << std::setfill('0') << std::setw(8) << cams[globalImgIndex]->id() << ".jpg";
				std::string outputFileName = (outputFolder / ss.str()).string();
				std::string scaledDownOutputFileName = (scaledDownOutputFolder / ss.str()).string();
				std::string command = "convert " + outputFileName + " " + (outputFolder / ssc.str()).string();
				std::string scaledDownCommand = "convert " + scaledDownOutputFileName + " " + (scaledDownOutputFolder / ssc.str()).string();

				cv::Mat img = cv::imread(imagePaths[globalImgIndex].string(), cv::IMREAD_ANYCOLOR | cv::IMREAD_ANYDEPTH);
				cv::Rect areOfIntererst = cv::Rect((img.cols - cropResolution[0]) / 2, (img.rows - cropResolution[1]) / 2, cropResolution[0], cropResolution[1]);
				cv::Mat croppedImg = img(areOfIntererst);
				cv::imwrite(outputFileName, croppedImg);
				if (boostPath.extension().string() != ".jpg") {
					system(command.c_str());
				}

				if (scaleDown) {
					cv::Mat resizedImg;
					cv::resize(croppedImg, resizedImg, resizedSize, 0, 0, cv::INTER_LINEAR);
					cv::imwrite(scaledDownOutputFileName, resizedImg);
					if (boostPath.extension().string() != ".jpg") {
						system(scaledDownCommand.c_str());
					}
				}
			}
			catch (const std::exception&)
			{
				continue;
			}
		}
	}



	// [IMPORTANT]: Sort Cameras by ID before writing renamed images to maintain image order!!!!
	newCams.resize(cams.size());
	newCams = scene.data()->cameras();
	//for (int c = minCam; c < maxCam; c++) {
	//	//newCams[c] = InputCamera(cams[c].id(), cropResolution.x(), cropResolution.y(), scene.data()->outputCamsMatrix()[c], true);
	//	newCams[c] = scene.data()->cameras()[c];
	//	newCams[c]->name(cams[c]->name());
	//}


	std::sort(newCams.begin(), newCams.end(), [](const InputCamera::Ptr & a, const InputCamera::Ptr & b) {
		return a->id() < b->id();
	});

	
	for (int c = minCam; c < maxCam; c++) {
		
		auto & camIm = *newCams[c];

		std::string extensionFile = ".jpg";
		std::ostringstream ssZeroPad;
		if (scene.data()->datasetType() == sibr::IParseData::Type::COLMAP) {
			ssZeroPad << std::setw(8) << std::setfill('0') << camIm.id();
		}
		else {
			ssZeroPad << std::setw(8) << std::setfill('0') << c;
		}
		std::string newFileName = ssZeroPad.str() + extensionFile;
		outputBundleCam << camIm.toBundleString();
		outputListIm << newFileName << " " << camIm.w() << " " << camIm.h() << std::endl;
	}

	outputBundleCam.close();
	outputListIm.close();

	return EXIT_SUCCESS;
}