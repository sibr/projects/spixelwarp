/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <core/system/CommandLineArgs.hpp>
#include <core/graphics/Camera.hpp>
#include <core/graphics/Image.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/system/String.hpp>

#include "core/scene/CalibratedCameras.hpp"

using namespace sibr;

#include "../../renderer/SpixelImage.hpp"

#include "../../renderer/PatchFile.hpp"
#include "../../renderer/PatchPoint.hpp"
#include "../../renderer/InputCameraPreprocess.hpp"

/* Global definitions */
float min_mvs_threshold = 0.8f;
BasicIBRScene::Ptr		scene;

// get rid of set_scale completely
//int set_scale = 1;

struct DepthArgs :
	virtual BasicIBRAppArgs {
	Arg<float> mvs = { "mvs", 0.8f };
	Arg<bool> big_dataset = { "big_dataset" }; 
	Arg<std::string> help = { "help", ""};
	Arg<std::string> ext = { "ext", ".jpg" };
};

static	std::string imagePath; 
static	std::string swarpDatasetPath;


// Main at the end 

// Save depths from MVS patch points

static void saveDepthImage(const sibr::ImageRGB32F& img, std::string str, uint w, uint h) {
  std::ofstream sdepth(str.c_str(), std::ios::binary);
  SIBR_ASSERT(sdepth.is_open());
  sdepth.write((char*)&w, sizeof(int));
  sdepth.write((char*)&h, sizeof(int));
  for (uint j=0; j<uint(h); j++) {
    for (uint i=0; i<uint(w); i++) {
      float r = img(i,j)[0];
      sdepth.write((char*)&r, sizeof(float));
    }
  }
}


void saveSparseDepth(uint id, std::vector<sibr::InputCameraPreprocess>& camlist, const DepthArgs & myArgs) {
	std::cerr << "saving depth image " << id << std::endl;
	uint max_dim = std::max(scene->images()->image(id).w(),scene->images()->image(id).h());
	uint w = scene->images()->image(id).w()/2;
	uint h = scene->images()->image(id).h()/2;

	std::string half_name = swarpDatasetPath + sibr::sprint("/half_size/%08d", id) + myArgs.ext.get().c_str();
	sibr::ImageRGB half_img;
	if(half_img.load(half_name)) {
		w = half_img.w();
		h = half_img.h();
		std::cerr << "Setting depth dimensions to " << w << " x " << h << std::endl;
	}
	else {
		SIBR_ERR <<"************ [SIBR depth] Can't find half image, probably wont work" << std::endl;
	}

	if (( w % 2 ) || ( h % 2 ) ) {
		std::cerr << "Images not divisible by two, exiting." << std::endl;
		exit(1);
	}

	float scale = 1.0f;
	uint wi = scene->images()->image(id).w() ;
	uint hi = scene->images()->image(id).h() ;
	uint x;
	uint y;
	std::cout << "wi " << wi << ", w " << w << std::endl;
	scale = wi*1.0f/w;

	{
		// 
		sibr::ImageRGB32F img(w,h,0.f);
		std::cerr << "Camera " << id << " has " <<   camlist[id].numMVSPnts() << " MVS points. " << std::endl;

		float v=0;
		for (uint i=0; i<camlist[id].numMVSPnts(); i++) {
			sibr::Vector2i  p  = camlist[id].mvs2DPos(i);
			x  = std::min(uint(p[0]/scale),w-1);
			y  = std::min(uint(p[1]/scale),h-1);
			float d= float(camlist[id].mvsPntDepth(i));
			img(x,y)[0]=d;
			img(x,y)[1]=d;
			img(x,y)[2]=d;
			v+=d;
		}

		std::cerr <<"Average value " << v/camlist[id].numMVSPnts() << std::endl;

		std::string d_str   = sibr::sprint("%s/depth/%08d.sdepth",swarpDatasetPath.c_str(),id);
		std::string png_str = sibr::sprint("%s/depth/sdepth_%08d.png",swarpDatasetPath.c_str(),id);
		saveDepthImage(img,d_str, w, h);
		img(0,0)[0] = 1.0; // hack to avoid seg fault if depth image is all black
		sibr::ImageRGB imcopy(w,h);


		sibr::Vector3f avg(0.f, 0.f, 0.f);
		int cnt =0;
		for(unsigned int x1=0;x1<w;x1++)
			for(unsigned int y1=0;y1<h;y1++)
			{
				sibr::Vector3f cf = img(x1,y1);
				sibr::Vector3ub c = imcopy(x1,y1);

				if( cf.x() > 0 ) { 
					avg+=cf;
					cnt++;
					// remap from 0, 1 to 0 .. 255
					c = sibr::Vector3ub((unsigned char)(cf.x()*255), (unsigned char)(cf.y()*255), (unsigned char)(cf.z()*255));
				}
				else // Seb: images are not init using black color, you have to specify it
					c = sibr::Vector3ub(0,0,0);

				imcopy(x1,y1)= c;
			}

		std::cerr <<"Avg " << avg.x()/cnt << " on " << cnt << " points compared to " << camlist[id].numMVSPnts() << std::endl;

		imcopy.save(png_str);
	}
	std::cout << " [done]" << std::endl;
}


int main(int argc, char **argv) 
{
	CommandLineArgs::parseMainArgs(argc, argv);
	DepthArgs myArgs;


	BasicIBRScene::SceneOptions so;

	// no opengl context cant load texture
	so.texture = false;
	so.renderTargets = false;

	scene.reset( new BasicIBRScene(myArgs, so));


	if (sibr::directoryExists(myArgs.dataset_path.get() + "/half_size/")) {
		swarpDatasetPath = myArgs.dataset_path.get();
		imagePath = swarpDatasetPath + "/images/";
	}
	else {
		swarpDatasetPath = myArgs.dataset_path.get() + "/spixelwarp/";
		imagePath = scene->data()->basePathName() + "/images/";
	}

	if (scene->data()->datasetType() == sibr::IParseData::Type::EMPTY) {
		SIBR_ERR << "Dataset type not recongnized" << std::endl;
	}

	sibr::Vector2f swarpNearFar;

	std::ifstream clipping_planes(swarpDatasetPath + "/clipping_planes.txt");
	if (!clipping_planes.is_open()) {
		SIBR_WRG << swarpDatasetPath + "/clipping_planes.txt: doesnt exist, cannot perform depth preprocessing" << std::endl;
		return false;
	}

	clipping_planes >> swarpNearFar[0] >> swarpNearFar[1];
	std::vector<sibr::Vector2f> nearsFars;
	std::vector<InputCamera::Ptr> inCams = scene->cameras()->inputCameras(); 
	nearsFars.resize(inCams.size());

	for (int i = 0; i < inCams.size(); i++)
		nearsFars[i] = swarpNearFar;
	scene->cameras()->updateNearsFars(nearsFars);

	min_mvs_threshold = myArgs.mvs;

    std::cout << min_mvs_threshold << std::endl;

    sibr::makeDirectory(swarpDatasetPath + "/depth/");

	if( myArgs.big_dataset ) 
		std::cerr << "Doing big scene" << std::endl;

	// load PatchFile
	sibr::PatchFile patchFile;
	std::string fname = patchFile.search(swarpDatasetPath);
	patchFile.load(fname);

	// add PMVS points to InputCameras
	sibr::InputCameraPreprocess ep;
	std::vector<sibr::InputCameraPreprocess> camlist;

	camlist.resize(scene->cameras()->inputCameras().size()); 
	int i = 0;
    for (int i=0;i<scene->cameras()->inputCameras().size(); i++)  {
		camlist[i] = *scene->cameras()->inputCameras()[i];
	}

	// the following approach uses too much memory for large datasets
	if( !myArgs.big_dataset ) {
		ep.loadPatchMVSPoints(scene, patchFile, camlist);

		std::cerr << "Done loading... starting save" << std::endl;
   		 //for (uint i=0; i<scene->images().size(); i++)
		for (uint i=0; i<camlist.size(); i++)
			if (camlist[i].isActive())
				saveSparseDepth(i, camlist, myArgs);
			else
				std::cerr << "Cam " << i << " inactive " << std::endl;
	}
	// this is (relatively) inefficient, but at least doesnt explode in memory usage
	else {
		int i = 0;
#define	STRIDE 	10 // variable with scene and memory availability should be tuned automatically
		do {
				int j = i+STRIDE; // batch 
				if( j >= camlist.size()) 
					j = (int)camlist.size(); 
				ep.loadPatchMVSPoints4CameraRange(scene, patchFile, camlist, i, j);
				for(int k=i;k<j;k++ ) {
					if( camlist[k].isActive() ) {
						std::cerr << "Process Camera " << k << std::endl;
						saveSparseDepth(k, camlist, myArgs);
					}
					else
						std::cerr << "Cam " << k << " inactive " << std::endl;
					camlist[k].deleteMVSPntPreprocess();
				}
				i = i+STRIDE; // may go over to stop loop
		}
		while ( i < camlist.size() ) ;
	}
	
    return EXIT_SUCCESS;
}

