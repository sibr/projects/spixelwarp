/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "ds.h"

#define N_HIST_BINS     20
#define N_DHIST_BINS    20
#define NN_final        3
#define NN              50

#define MIN_PIXELS_PER_SP       100
#define MIN_MVS_POINTS_PER_SP   20
#define MIN_MVS_DENSITY_PER_SP  0.010

#define MAX_ITERATIONS 7

using namespace cv;

using std::cout;
using std::endl;
using std::vector;
using std::string;

static int depth_histogram_bin(float d) {
//    assert(d>=0.0f && d<=1.0f);
// hack for google data
if( !(d>=0.0f && d<=1.0f) ) {
static int cnt =0;
cnt++;
if( cnt < 100 )
std::cerr <<"ERRROR IN DATA **** " << d << " Setting to 0 " << __FILE__ << ":" << __LINE__ << std::endl;
	if( d < 0 )
		d= 0;
	else
		d= 0.9f;
}
    float x = std::min(d, 0.99999f);
    int bin = (int)std::floor(x*float(N_DHIST_BINS));
    return bin;
}

static int color_histogram_bin(float c) {
    assert(c>=0.0f && c<=1.0f);
    float x = std::min(c, 0.99999f);
    int bin = (int)std::floor(x*float(N_HIST_BINS));
    return bin;
}

static bool sortNormal( const cv::Vec3f& A, const cv::Vec3f& B ) {
	// sort on Z (test)
	return A[2] < B[2];
}

DepthSynthesizer::DepthSynthesizer(SpMat2<int> sp, SpMat2<float> sd, cv::Mat im, const cv::Mat& normal ) :
    spixel(sp),
    sdepth(sd),
	image (im),
	normal(normal) {}

bool DepthSynthesizer::initNoProcess() {
    initialize();
	std::cerr << "Return from init "<< std::endl;
	return true;
}

bool DepthSynthesizer::process() {
    initialize();
    bool some_superpixels_updated = true;
    for (int i=0; i<MAX_ITERATIONS && some_superpixels_updated; i++) {
        some_superpixels_updated = updateDepthPixels(i);
    }
    floodFillDepth(MAX_ITERATIONS);
    return true;
}


bool DepthSynthesizer::initialize() {
    // number of superpixels
    num_spixel = spixel.getMax() + 1;

    hist_sp = SpMat2<float>(num_spixel, N_HIST_BINS*3, 0);
    hist_d  = SpMat2<float>(num_spixel, N_DHIST_BINS,  0);

    pixels_in_sp.resize(num_spixel);        // list of pixels
    depths_in_sp.resize(num_spixel);        // list of depth samples
    iter_sp     .resize(num_spixel,0);      // iteration id when spixel was filled

	snormal.clear();
	snormal.resize(num_spixel);

    vector<vector<float> > depthvals(num_spixel);

    // convert image to L*a*b
    Mat lab;
    cvtColor(image, lab, cv::COLOR_BGR2Lab);

    // image size
    int w = lab.cols;
    int h = lab.rows;

#define USE_ADPATIVE_HISTOGRAM 1
#if USE_ADPATIVE_HISTOGRAM
    float max_l = FLT_MIN, min_l = FLT_MAX;
    float max_a = FLT_MIN, min_a = FLT_MAX;
    float max_b = FLT_MIN, min_b = FLT_MAX;

    // find the max and min LAB values in image
    for (int y=0; y<h; y++) {
        for (int x=0; x<w; x++) {
            cv::Vec3f pixel_color = lab.at<cv::Vec3f>(y,x);
            max_l = std::max(max_l, pixel_color[0]);
            max_a = std::max(max_a, pixel_color[1]);
            max_b = std::max(max_b, pixel_color[2]);
            min_l = std::min(min_l, pixel_color[0]);
            min_a = std::min(min_a, pixel_color[1]);
            min_b = std::min(min_b, pixel_color[2]);
        }
    }
#else
    float min_l = 0.0f,    max_l = 100.0f;
    float min_a = -128.0f, max_a = 127.0f;
    float min_b = -128.0f, max_b = 127.0f;
#endif

    assert(max_l > min_l);
    assert(max_a > min_a);
    assert(max_b > min_b);

	// will store normal of each pixel in each superpixel
	vector<vector<cv::Vec3f>> normals_sp(num_spixel);

    // loop over all pixels of the image
    for (int y=0; y<h; y++) {
        for (int x=0; x<w; x++) {

            // get spixel id
            int spid = spixel.getVal(y,x);

			// Add normal if not null
			if( normal.data ) {
	            cv::Vec3f pixel_normal = normal.at<cv::Vec3f>(y,x);
				if (pixel_normal[0] != 0 || pixel_normal[1] != 0 || pixel_normal[2] != 0)
					normals_sp[spid].push_back(pixel_normal);
			}

            cv::Vec3f pixel_color = lab.at<cv::Vec3f>(y,x);

            // extract LAB values and convert to [0,1] range
            float l = (pixel_color[0]-min_l) / (max_l-min_l);
            float a = (pixel_color[1]-min_a) / (max_a-min_a);
            float b = (pixel_color[2]-min_b) / (max_b-min_b);

            // determine bin positions for lab values
            // multiply [0,1] LAB values by number of bins and downcast to int
            int lbinpos = 0*N_HIST_BINS + color_histogram_bin(l);
            int abinpos = 1*N_HIST_BINS + color_histogram_bin(a);
            int bbinpos = 2*N_HIST_BINS + color_histogram_bin(b);

            // add LAB to spixel histogram
            hist_sp.incVal(spid, lbinpos, 1);
            hist_sp.incVal(spid, abinpos, 1);
            hist_sp.incVal(spid, bbinpos, 1);

            // if the pixel has a depth sample, then add it to the
            // superpixel depth histogram and to list of depth samples
            // for this superpixel
            float d = sdepth.getVal(y,x);
            if (d > 0.0f) {
                int dbinpos = depth_histogram_bin(d);
                hist_d.incVal(spid, dbinpos, 1);
                depthvals[spid].push_back(d);
                depths_in_sp[spid].push_back(std::tuple<int,int,float>(y,x,d));
            }

            // add pixel to superpixel
            pixels_in_sp[spid].push_back(std::pair<int,int>(y,x));
        }
    }

    // get totals of pixel and depth samples for each super pixel
    vector<float> n_pixels_in_spixel = hist_sp.get_row_sum();
    vector<float> n_depth_in_spixel  = hist_d.get_row_sum();
    vector<bool>  valid_depths = getValidDepthSamples(hist_d);

	/// get mean/median of normals per spixel
	/// \todo TODO: SWITCH BETWEN MEAN/MEADIAN HERE
	if( normal.data ) 
	for (size_t i = 0; i < snormal.size(); ++i)
	{
		if (normals_sp[i].size())
		{
			vector<cv::Vec3f> normals = std::move(normals_sp[i]);
			/// \todo TODO: check how normals are sorted
			std::sort(normals.begin(), normals.end(), sortNormal);
			snormal[i] = normals[normals.size()/2];
		}
	}

    // color histograms have three histograms, so each pixel gets
    // counted thrice, divide by 3 to account for this
    for (int i=0; i<n_pixels_in_spixel.size(); i++) {
        n_pixels_in_spixel[i] /= 3;
    }

    // create the source and target superpixel lists
    createSourceTargetArrays(n_pixels_in_spixel, n_depth_in_spixel, valid_depths);

    // normalize histograms to convert them to PDF
    hist_sp.normalizeRows(n_pixels_in_spixel);
    hist_d .normalizeRows(n_depth_in_spixel);

    // create a copy for the original list of target superpixels
    original_target_sp.clear();
    original_target_sp.insert(original_target_sp.begin(), target_sp.begin(), target_sp.end());

    graph_sp.initialize(spixel, hist_sp);

    /// \todo TODO: add allowborderfill code

    for (int i=0; i<num_spixel; i++) {
        std::sort(depthvals[i].begin(), depthvals[i].end());
        if (!depthvals[i].empty()) {
            meddepth_sp.push_back(depthvals[i][ depthvals[i].size()/2 ]);
        } else {
            meddepth_sp.push_back(0);
        }
    }

    return true;
}


bool DepthSynthesizer::updateDepthPixels(int iteration) {
    if (source_sp.empty() || target_sp.empty()) {
        return false;
    }

    // source_sp -> vector of input sp's
    // target_sp -> vector of target sp's to synthesize
    // raw_source_p_hist / target are a linearized copy of the histograms for each source/target_sp
    vector<float> raw_source_sp_hist = hist_sp.extract_rows(source_sp).getRawData();
    vector<float> raw_target_sp_hist = hist_sp.extract_rows(target_sp).getRawData();

    // specify dimensions of the data which is linearized
    Mat clusterMembers((int)target_sp.size(),          // # rows in matrix
                       (int)hist_sp.getW(),            // # columns in matrix
                       CV_32F,                    // type
                       (void *)&raw_target_sp_hist[0]);   // raw data ptr

    Mat clusterCenters(
            (int)source_sp.size(),
            (int)hist_sp.getW(),
            CV_32F,
            (void *)&raw_source_sp_hist[0]);

	int knn = std::min(NN, (int)source_sp.size());

    // index and distance of each nearest neighbour
    Mat matches  ((int)target_sp.size(), knn, CV_32SC1);
    Mat distances((int)target_sp.size(), knn, CV_32FC1);

    // How many leaves to search in a tree = 32
    // Build 4 kd tree
    // Search KdTree for matches
    //flann::SearchParams params(32);
    flann::GenericIndex< cvflann::ChiSquareDistance<float> > kdtree(clusterCenters, cvflann::KDTreeIndexParams(1));
    kdtree.knnSearch(clusterMembers, matches, distances, knn, cvflann::SearchParams(-1));	

    // matches contains all the matching nearby spixels for each target
    vector<bool> isfilled(target_sp.size(), false);

    int filledcount = 0;

    for (int tss=0; tss<target_sp.size(); tss++) {
        vector<int> matchesarr;
        int target_spixel_id = target_sp[tss];
        for (int i=0; i<knn; i++) {
            int match_in_kdtree = matches.at<int>(tss, i);
            int match_source_spixel_id = source_sp[match_in_kdtree];
            matchesarr.push_back(match_source_spixel_id);
        }
        std::sort(matchesarr.begin(), matchesarr.end());

        // list of neighbor superpixel ids sorted on the distance
        // this performs the shortest walk using Dijkstra shortest path
        vector<spdist> sp_sorted_dist = graph_sp.dijkstra_shortest_walk(target_spixel_id, matchesarr);

        // if (sp_sorted_dist.size()>NN_final)
        {
            // list of superpixels that are best matches
            vector<int> neighbor_sp;

            // combined histogram of depth samples of all best match superpixels
            SpMat2<float> newd(1, hist_d.getW(), 0);

            // list of depth samples from all best match superpixels
            vector<float> depthvals;
			// list of normal samples from all best match superpixels
			vector<cv::Vec3f> normalvals;

            // touches only the first NN_final elements of sp_sorted_dist
            // other matches are ignored
            for (int i=0; i<std::min<int>((const int)sp_sorted_dist.size(),NN_final); i++) {
                neighbor_sp.push_back(sp_sorted_dist[i].index);
                for (std::tuple<int,int,float> ds: depths_in_sp[sp_sorted_dist[i].index]) {
                    float d = std::get<2>(ds);
                    int dbinpos = depth_histogram_bin(d);  // add depth sample to histogram
                    newd.incVal(0, dbinpos,1);
                    depthvals.push_back(d);                // add depth sample to list
                }
				// Concerning normals:
				// We already have calculated the median normal per superpixel.
				// That's why I collect it per superpixel and not per pixel (the
				// loop just above).
				// This is because the strategy when using median is to get real
				// value (so one real normal from one of neighboors).
				// But if we switch to 'mean'; it might make sense to do the
				// mean per pixels instead.
				if( normal.data ) {
				cv::Vec3f n = snormal[sp_sorted_dist[i].index];
				if (n[0] != 0 || n[1] != 0 || n[2] != 0)
					normalvals.push_back(n);
				}
            }
            assert(!depthvals.empty());

            std::sort(depthvals.begin(), depthvals.end());
			if( normal.data )
			std::sort(normalvals.begin(), normalvals.end(), sortNormal);

            // normalize the histogram
            newd.normalizeRows(newd.get_row_sum());

            // check that the histogram is valid - has only one peak
            if (getValidDepthSamples(newd)[0]) {
                // update the depth histogram of the target superpixel with the computed histogram
                hist_d.replaceRow(target_spixel_id, newd);

                // compute the median depth from the list of depth samples
                float mediandepth   = depthvals[depthvals.size()/2];
                float variancedepth = std::abs(depthvals[0]-depthvals[depthvals.size()-1]);
				if( normal.data ) {
				cv::Vec3f mediannormal(0.f, 0.f, 0.f);
				if (normalvals.size())
					mediannormal = normalvals[normalvals.size()/2];
				snormal[target_spixel_id] = mediannormal;
				}

                meddepth_sp[target_spixel_id] = mediandepth;

                int newsamples = std::max(MIN_MVS_POINTS_PER_SP,
                        int(MIN_MVS_DENSITY_PER_SP*pixels_in_sp[target_spixel_id].size()));

                // shuffle pixel indices so that we can select first newsamples number of pixels to add depth
                std::random_shuffle(pixels_in_sp[target_spixel_id].begin(), pixels_in_sp[target_spixel_id].end());

                // select the first few depth samples, interpolate and add them to the target superpixel
                for (int i=0; i<newsamples && i<pixels_in_sp[target_spixel_id].size(); i++) {
                    std::pair<int, int> pixelid = pixels_in_sp[target_spixel_id][i];
                    float d = interpolate_depth(pixelid, neighbor_sp, newd);
                    sdepth.setVal(pixelid.first, pixelid.second, d);
                    depths_in_sp[target_spixel_id].push_back(
                            std::tuple<int,int,float>(pixelid.first, pixelid.second, d));
                }

                // std::cerr << target_spixel_id << " ";
                // for (int i=0; i<neighbor_sp.size(); i++) {
                //     std::cerr << neighbor_sp[i] << " ";
                // }
                // std::cerr << variancedepth << endl;

                isfilled[tss] = true;
                filledcount++;
                iter_sp[target_spixel_id] = iteration;
            }
        }
    }

//    std::cout << "finished " << filledcount << "/" << target_sp.size() << std::endl;

    // recaluclate the list of target superpixels because some have been filled
    vector<int> new_targets;
    for (int tss=0; tss<target_sp.size(); tss++) {
        if (isfilled[tss]) {
            source_sp.push_back(target_sp[tss]);
        } else {
            new_targets.push_back(target_sp[tss]);
        }
    }
    target_sp.swap(new_targets);

    return (filledcount>0);
}

bool DepthSynthesizer::floodFillDepth(int lastiteration) {
    vector<bool> isfilled(target_sp.size(), false);

    int filledcount = 0;

    for (int tss=0; tss<target_sp.size(); tss++) {
        int target_spixel_id = target_sp[tss];

        // these neighbors are the ones that have a common border
        // with the target superpixel
        vector<int> spids = graph_sp.getNeighbors(target_spixel_id);

        vector<spdist> neighbordist;
        for (int nid: spids) {
            if (iter_sp[nid] <= lastiteration) {
                float dist = graph_sp.chisqdist(nid, target_spixel_id, hist_sp);
                neighbordist.push_back(spdist(nid, dist));
            }
        }

        {
            std::sort(neighbordist.begin(), neighbordist.end(), spdistcomp);

            vector<int> neighbor_sp;

            SpMat2<float> newd(1, hist_d.getW(), 0);
            vector<float> depthvals;
			// list of normal samples from all best match superpixels
			vector<cv::Vec3f> normalvals;

            for (int i=0; i<std::min<int>((const int)neighbordist.size(), NN_final); i++) {
                neighbor_sp.push_back(neighbordist[i].index);
                for (std::tuple<int,int,float> ds: depths_in_sp[neighbordist[i].index]) {
                    float d = std::get<2>(ds);
                    int dbinpos = depth_histogram_bin(d);
                    newd.incVal(0, dbinpos, 1);
                    depthvals.push_back(d);
                }
				// Concerning normals:
				// We already have calculated the median normal per superpixel.
				// That's why I collect it per superpixel and not per pixel (the
				// loop just above).
				// This is because the strategy when using median is to get real
				// value (so one real normal from one of neighboors).
				// But if we switch to 'mean'; it might make sense to do the
				// mean per pixels instead.
				cv::Vec3f n = snormal[neighbordist[i].index];
				if (n[0] != 0 || n[1] != 0 || n[2] != 0)
					normalvals.push_back(n);
            }
            if (depthvals.empty()) {
                continue;
            }

            std::sort(depthvals.begin(), depthvals.end());
			std::sort(normalvals.begin(), normalvals.end(), sortNormal);

            // normalize the depth histogram
            newd.normalizeRows(newd.get_row_sum());

            // check that the histogram is valid - has only one peak
            if (getValidDepthSamples(newd)[0]) {
                // update the depth histogram of the target superpixel with the computed histogram
                hist_d.replaceRow(target_spixel_id, newd);

                // compute the median depth from the list of depth samples
                float mediandepth   = depthvals[depthvals.size()/2];
                float variancedepth = std::abs(depthvals[0]-depthvals[depthvals.size()-1]);
				cv::Vec3f mediannormal(0.f, 0.f, 0.f);
				if (normalvals.size())
					mediannormal = normalvals[normalvals.size()/2];

                meddepth_sp[target_spixel_id] = mediandepth;
				snormal[target_spixel_id] = mediannormal;

                int newsamples = std::max(MIN_MVS_POINTS_PER_SP,
                        int(MIN_MVS_DENSITY_PER_SP*pixels_in_sp[target_spixel_id].size()));

                // shuffle pixel indices so that we can select first newsamples number of pixels to add depth
                std::random_shuffle(pixels_in_sp[target_spixel_id].begin(), pixels_in_sp[target_spixel_id].end());

                // select the first few depth samples, interpolate and add them to the target superpixel
                for (int i=0; i<newsamples && i<pixels_in_sp[target_spixel_id].size(); i++) {
                    std::pair<int, int> pixelid = pixels_in_sp[target_spixel_id][i];
                    float d = interpolate_depth(pixelid, neighbor_sp, newd);
                    sdepth.setVal(pixelid.first, pixelid.second, d);
                    depths_in_sp[target_spixel_id].push_back(
                            std::tuple<int,int,float>(pixelid.first, pixelid.second, d));
                }

                // std::cerr << target_spixel_id << " ";
                // for (int i=0; i<neighbor_sp.size(); i++) {
                //     std::cerr << neighbor_sp[i] << " ";
                // }
                // std::cerr << variancedepth << endl;

                isfilled[tss] = true;
                filledcount++;
                iter_sp[target_spixel_id] = lastiteration + 1;
            }
        }
    }

//    cout << "floodfill " << filledcount << "/" << target_sp.size() << endl;
 //   cout << "unfilled  " << (target_sp.size() - filledcount) << "/" << target_sp.size() << endl;

    return true;
}


float DepthSynthesizer::interpolate_depth(
        std::pair<int,int> newp,
        vector<int> sourcesps,
        SpMat2<float> pdf_d)
{
    double invdepth = 0.0;
    double w_total_total= 0.0;
    for (int ssp: sourcesps) {
        for (PointDepth depthsample: depths_in_sp[ssp]) {
            float d = std::get<2>(depthsample);
            int dpx = std::get<1>(depthsample);
            int dpy = std::get<0>(depthsample);
            float w_pixel = (float)(1.0 / ((dpx - newp.second ) * (dpx - newp.second )  + (dpy - newp.first ) * (dpy - newp.first )));
            float w_prob = pdf_d.getVal(0,depth_histogram_bin(d));
            float w_total = w_pixel * w_prob;
            invdepth +=  w_total  / d;
            w_total_total += w_total;
        }
    }

    return (float)(w_total_total / invdepth);
}

vector<bool> DepthSynthesizer::getValidDepthSamples(SpMat2<float> hist) {
    int h = hist.getH();
    int w = hist.getW();

    vector<bool> validDepths;

    // calculate the max value of each row i.e.
    // max value of each superpixel histogram
    vector<float> maxvals = hist.get_row_max();

    // loop over all rows, i.e. all histograms
    for (int y=0; y<h; y++) {
        vector<int> peakids;

        // calculate 20% of maximum value of histogram
        float filterline = 0.2f * maxvals[y];

        // loop over all columns, i.e. histogram bins
        // retains all historgam bins that are above this threshold
        for (int x=0; x<w; x++) {
            float currentVal = hist.getVal(y, x);
            if (currentVal >= filterline) {
                peakids.push_back(x);
                // if (peakids.size()>2) {
                //     break;
                // }
            }
        }

        // valid if there is a single peak or
        // if there are two peaks and they are both next to each other
        bool isvalidid = (peakids.size()==1 || (peakids.size()==2 && std::abs(peakids[1]-peakids[0])==1)) ;

        validDepths.push_back(isvalidid);
    }

    return validDepths;
}

void DepthSynthesizer::createSourceTargetArrays(
        vector<float> num_pixels_in_spixel,
        vector<float> num_depth_in_spixel,
        vector<bool> valid_depths)
{
    source_sp.clear();
    target_sp.clear();

    for (int y=0; y<num_pixels_in_spixel.size(); y++) {
        float sptot = num_pixels_in_spixel[y];
        float dtot  = num_depth_in_spixel[y];
        float mvs_density = dtot/sptot;
        bool isvaliddepth = valid_depths[y];

        // ignore the superpixel if it is too small
        if (sptot > MIN_PIXELS_PER_SP) {

            // propagate into this superpixel if it has fewer
            // than a given number of depth samples or it the
            // density of depth samples is too little
            if (dtot < MIN_MVS_POINTS_PER_SP || mvs_density < MIN_MVS_DENSITY_PER_SP) {
                target_sp.push_back(y);
            }

            // propagate from this superpixel if it has at least
            // a given number of depth samples and the density of
            // depth samples if reasonable, and it has only 1 peak
            // in depth histogram
            if (dtot >= MIN_MVS_POINTS_PER_SP && mvs_density >= MIN_MVS_DENSITY_PER_SP && isvaliddepth) {
                source_sp.push_back(y);
            }
        }
    }
std::cerr << "Source sp size " << source_sp.size() << std::endl;
std::cerr << "Target sp size " << target_sp.size() << std::endl;
}

SpMat2<float> DepthSynthesizer::getMedianDepthImage() {
    SpMat2<float> mp(sdepth.getH(), sdepth.getW(), 0.0f);
    for (int y=0; y<mp.getH(); y++) {
        for (int x=0; x<mp.getW(); x++) {
            mp.setVal(y,x,meddepth_sp[spixel.getVal(y,x)]);
        }
    }
    return mp;
}

SpMat2<float> DepthSynthesizer::getFinalDepthImage() {
    SpMat2<float> mp(sdepth.getH(), sdepth.getW(), 0.0f);
//std::cerr << "FINAL DEPTH " << sdepth.getH() << " x " << sdepth.getW() << std::endl;
    for (int i=0; i<depths_in_sp.size(); i++) {
        PointDepthList pointdepthlist = depths_in_sp[i];
        for (int j=0; j<pointdepthlist.size(); j++) {
            PointDepth depthsample = pointdepthlist[j];
            float d = std::get<2>(depthsample);
            int dpx = std::get<1>(depthsample);
            int dpy = std::get<0>(depthsample);
            mp.setVal(dpy, dpx, d);
        }
    }
    return mp;
}

PointDepthListPerSpix& DepthSynthesizer::getPointDepthListPerSpix() {
    return depths_in_sp;
}

std::vector<int> DepthSynthesizer::getOriginalTargetsSpix() {
    return original_target_sp;
}
