/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _DEPTH_SYNTH_H_
#define _DEPTH_SYNTH_H_
#include <core/graphics/Config.hpp>
#include <core/graphics/Image.hpp>

#include "spmat2.h"
#include "spgraph.h"

#include <iostream>
#include <vector>

#include <opencv2/opencv.hpp>
#if CV_VERSION_EPOCH == 2 && CV_VERSION_MAJOR == 4
#include <opencv2/ml/ml.hpp>
#include <opencv2/highgui/highgui.hpp>
#else
#include <opencv2/ml.hpp>
#include <opencv2/highgui.hpp>
#endif
#include <opencv2/flann/flann.hpp>


/// List of each depth of pixel in range [0-1] foreach superpixel of the current image being proceed
typedef std::tuple<int,int,float> PointDepth;     //pixel Y ; pixel X ; depth
typedef std::vector<PointDepth>     PointDepthList;
typedef std::vector<PointDepthList> PointDepthListPerSpix;

/**
 * Responsible for generating synthetic depth information for specified image.
 */
class DepthSynthesizer {
private:
	/** Median/Mean normal per spixel */
	std::vector<cv::Vec3f>	snormal;

    /** Contains spixel ids for each pixel */
    SpMat2<int> spixel;

    /** Contains depths for each pixel */
    SpMat2<float> sdepth;

    /** image */
    cv::Mat image;
    cv::Mat normal;

    /** Total number of super pixels for the image */
    int num_spixel;

    /** Histograms for colors. (i-th row contains histograms of L, a, b values for pixels in the spixel i) */
    SpMat2<float> hist_sp;

    /** Histograms for depths. (i-th row contains the histogram of depth for pixels in the spixel i) */
    SpMat2<float> hist_d;

    /** i-th row contains the iteration id where the values of i-th spixel is updated */
    std::vector<int> iter_sp;

    /** List of superpixels with have depth samples */
    std::vector<int> source_sp;

    /** List of superpixels with do not have depth samples for which depth must be synthesized */
    std::vector<int> target_sp;

    /** \todo TODO: check if this is needed */
    std::vector<int> original_target_sp;

    /** Median depth of superpixels, i-th row contains median depth of i-th spixel */
    std::vector<float> meddepth_sp;

    /** List of pixels in each superpixel, i-th member contains list of (y,x) pixel coords of i-th superpixel */
    std::vector<std::vector<std::pair<int,int> > > pixels_in_sp;

    /** List of pixels with depth values in each superpixel, i-th member contains list of (y,x,depth) of i-th superpixel */
    PointDepthListPerSpix depths_in_sp;

    /** Graph of superpixels that are needed for shortest walk calculation */
    SpGraph graph_sp;

private:

    /**
     * Computes list of superpixels that have valid depth,
     * if depth histogram has only 1 peak or 2 consecutive
     *
     * @param[in] hist depth histogram of i-th superpixel in i-th row
     *
     * @return list where i-th value is true if i-th superpixel contains valid depth info
     */
    std::vector<bool> getValidDepthSamples(SpMat2<float> hist);

    /**
     * Creates list of superpixels which contains depth samples as source superpixels
     * and list of superpixels which do not as target superpixels, depth synthesis has
     * to be done for target superpixels
     *
     * @param[in] n_pixels_in_spixel  vector where each i-th element is number depth samples in i-th superpixel
     * @param[in] n_depth_in_spixel   vector where each i-th element is number depth samples in i-th superpixel
     * @param[in] valid_depths flag for each superpixel if contains valid depth samples
     */
    void createSourceTargetArrays(
            std::vector<float> n_pixels_in_spixel,
            std::vector<float> n_depth_in_spixel,
            std::vector<bool> valid_depths);

    /**
     * Interpolates the depth values for pixel in newp position based on provided source superpixels list and pdf.
     *
     * @param[in] newp      (y,x) value of the pixel for depth is interpolated
     * @param[in] sourcesps vector of super pixel ids where depth values are used for interpolation
     * @param[in] pdf_d     probability density function for depth values in sourcesps
     *
     * @return  the interpolated depth value
     */
    float interpolate_depth(
            std::pair<int,int> newp,
            std::vector<int> sourcesps,
            SpMat2<float> pdf_d);

    /** Initializes necessory datastructures for processing */
    bool initialize();

    /**
     * Updates depth values for target superpixels. This will be iteratively for specified maximum number of iterations.
     * the depth values for filled target superpixels are also used in the next iteration.
     *
     * @param[in] iteration The current iteration id where this function is called.
     *
     * @return  true if the process is successfull. ( false if there is no more target superpixels)
     */
    bool updateDepthPixels(int iteration);

    /**
     * Flood-fills depth values for remaining target superpixels. This will be called only after updating deph values
     * using regular method for maximum specified number of iterations.
     * This will fill depth values from only immediate neighbour superpixels.
     *
     * @param[in] lastiteration The last iteration id the regular depth update process is run.
     *
     * @return  true if the process is successfull
     */
    bool floodFillDepth(int lastiteration);

public:

     /**
      * Constructor
      *
      * @param[in] spixel superpixel ids for each pixel in the image
      * @param[in] sdepth depth for each pixel in the image
      * @param[in] image  input image
      * @param[in] normal normal
      */
    DepthSynthesizer(SpMat2<int> spixel, SpMat2<float> sdepth, cv::Mat image, const cv::Mat& normal );

    /** Processes the datastructor for synthesiszing depth */
    bool process();

    /** Just init and pass on for refill stage (mvinpaint) */
    bool initNoProcess();

    /** Generates a depth image with median depth values  for each superpixel */
    SpMat2<float> getMedianDepthImage();

    /** Generates a depth image with median depth values  for each superpixel */
    SpMat2<float> getFinalDepthImage();

    /** Extract the list of depth samples in each superpixel */
    PointDepthListPerSpix& getPointDepthListPerSpix();

    /** Get the original list of superpixel which need to be filled by depthSynth */
    std::vector<int> getOriginalTargetsSpix();

	/** Get resulting normals */
	inline const std::vector<cv::Vec3f>& getNormalPerSpix( void ) const { return snormal; };
};

#endif // _DEPTH_SYNTH_H_
