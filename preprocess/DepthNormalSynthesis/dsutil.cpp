/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "dsutil.h"
#include <fstream>
#include <sstream>
#include <iomanip>
#include <string>
#include <exception>
#include <stdexcept>
#include <vector>
#include <algorithm>
#include <numeric>
#include <cstdlib>
#include <iostream>

#include "spmat2.h"
#include <core/graphics/Config.hpp>


// #define MATLAB_COMPARE
#ifdef  MATLAB_COMPARE
    #define OUT_EXT_PREFIX "_"
#else
    #define OUT_EXT_PREFIX ""
#endif

#define INPUT_IMAGE_FRAG    "/half_size/"
#define SUPER_PIXEL_FRAG    "/superpixels/"
#define SDEPTH_FRAG         "/depth/"
#define IMG_EXT             ".jpg"
#define SP_EXT              ".sp"
#define SDEPTH_EXT          ".sdepth"
#define ODEPTH_FRAG         "depth_"
#define MDEPTH_FRAG         "median_"
#define ODEPTH_EXT          ".jpg"
#define MVS_EXT             ".mvs"

using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;

DepthSynthesizer DepthSynthesisUtil::createDepthSynthesizer(
        string path,
        int imageid,
        int nbSamples, string ext, bool doProcess)
{
    std::stringstream ss;

	std::string swarpPath;

	if (sibr::directoryExists(path + "/half_size/")) {
		swarpPath = path;
	}
	else {
		swarpPath = path + "/spixelwarp/";
	}

    ss << std::setw(8) << std::setfill('0') << imageid;
    string image_str  = swarpPath + INPUT_IMAGE_FRAG + ss.str()+ ext;  //input image
    string spixel_str = swarpPath + SUPER_PIXEL_FRAG + ss.str() + SP_EXT;  // superpixel segmentation
    string sdepth_str = swarpPath + SDEPTH_FRAG +ss.str()+ SDEPTH_EXT ;    // sparse depth map

    int sw, sh, dw, dh;

    // create spixel and sdepth components and return their sizes
    SpMat2<int>   spixel = createSpixel(spixel_str, sw, sh);
    SpMat2<float> sdepth = createSdepth(sdepth_str, dw, dh);

//std::string moutdepth_str = path + SDEPTH_FRAG + "XX_" + ODEPTH_FRAG + ss.str() + OUT_EXT_PREFIX + ODEPTH_EXT;
//saveDepth(moutdepth_str, sdepth);

	// check if everything is coherent
	if( sw != dw || sh != dh ) {
		SIBR_ERR <<"sizes incorrect: depth " <<  dw << "x" << dh <<", spixel " << sw << "x" << sh << std::endl;
	}

//    assert(sw==dw && sh==dh);

    // create and resize the image
	std::cerr << "Reading " << image_str << std::endl;
    cv::Mat image = cv::imread(image_str);
    cv::Mat resized_image;

	cv::Mat normal;
	{ // Load normal image
		std::stringstream ssn;
		ssn << std::setw(8) << std::setfill('0') << imageid;
		string normal_str = path + "./normal/" + ssn.str() + "-normal.png";
		normal = cv::imread(normal_str);
		if( normal.data ) {
			cv::resize(normal, normal, cv::Size(sw, sh));
			// NOTE: it seems we lose precision with imread
			normal.convertTo(normal, CV_32FC3, 1.0f/255.0f);
		}
		else {
			std::cerr << "Normals not present" << std::endl;
		}
	}
	
    cv::resize(image, resized_image, cv::Size(sw, sh));
    resized_image.convertTo(resized_image, CV_32FC3, 1.0f/255.0f);
//std::cerr << "Resized image " << sw << " x " << sh << std::endl;
	
    //creates the ds structure
    DepthSynthesizer ds(spixel, sdepth, resized_image, normal);

    // processes the ds structure
	if( doProcess )
	    ds.process();
	else
		ds.initNoProcess();

    // file paths for saving depth images
    string outdepth_str = swarpPath + SDEPTH_FRAG + ODEPTH_FRAG + ss.str() + OUT_EXT_PREFIX + ODEPTH_EXT;
    string meddepth_str = swarpPath + SDEPTH_FRAG + MDEPTH_FRAG + ss.str() + OUT_EXT_PREFIX + ODEPTH_EXT;
    string mvs_str      = swarpPath + SDEPTH_FRAG + ss.str() + OUT_EXT_PREFIX + MVS_EXT;

    // saves the depth and median depth in png files
//std::cerr << "SDepth size " << sdepth.getH() << " x " << sdepth.getW() << std::endl;
    outdepth_str = swarpPath + SDEPTH_FRAG + ODEPTH_FRAG + ss.str() + OUT_EXT_PREFIX + ODEPTH_EXT;
    saveDepth(outdepth_str, ds.getFinalDepthImage());
    saveDepth(meddepth_str, ds.getMedianDepthImage());
    saveMVS  (mvs_str, sdepth.getH(), sdepth.getW(), ds, nbSamples, doProcess);

	if(normal.data)
	{ // save normal
		std::stringstream ssn;
		ssn << std::setw(8) << std::setfill('0') << imageid;
		string normal_str = swarpPath + "./normal/" + ssn.str() + "-normal-filled.png";
		
		std::vector<cv::Vec3f> normal_per_spix = ds.getNormalPerSpix();
		for (int y=0; y<sh; y++) {
			for (int x=0; x<sw; x++) {
				
				cv::Vec3f pixel_normal = normal.at<cv::Vec3f>(y,x);
				if (pixel_normal[0] == 0.f && pixel_normal[1] == 0.f && pixel_normal[2] == 0.f)
					/// \todo TODO: Instead do something like 'if (pixel is in the mask)'
				{
					int spid = spixel.getVal(y,x);
					// Apply correction
					pixel_normal = normal_per_spix[spid];
					normal.at<cv::Vec3f>(y,x) = pixel_normal;
				}
			}
		}
		normal.convertTo(normal, CV_8UC3, 255.0f/1.0f);
		cv::imwrite(normal_str, normal);
	}
    return ds;
}

bool DepthSynthesisUtil::saveMVS(
        string path,
        int height,
        int width,
        DepthSynthesizer ds,
        int nbSamples, bool doProcess)
{
//std::cerr<<"Writing MVS " << path << std::endl;
#ifdef  MATLAB_COMPARE
    string matlab_path = path;
    string prefix = OUT_EXT_PREFIX;
    string::size_type i = matlab_path.find_last_of(prefix);
    if (i!=string::npos) {
        matlab_path.erase(i, prefix.length());
    }

    vector<float> medDepth_perSpix;
    vector<float> medDepth_diff_perSpix;
    vector<float> matlab_medDepth_perSpix;
    int matlab_w = 0 , matlab_h = 0 , matlab_numSpixels = 0;
    std::ifstream matlab_mvs_file(matlab_path.c_str());
    std::ofstream diffOutFile;

    if(matlab_mvs_file.is_open()) {
        matlab_mvs_file >> matlab_w >> matlab_h >> matlab_numSpixels;
        for (int i=0; i<matlab_numSpixels; i++) {
            if (!matlab_mvs_file.eof()) {
                int   spixel    = 0 , numPoints = 0;
                float med_depth = 0;
                matlab_mvs_file >> spixel >> numPoints >> med_depth;
                matlab_medDepth_perSpix.push_back(med_depth);
                matlab_mvs_file.ignore (std::numeric_limits<std::streamsize>::max(), '\n' ); //skip the rest of the line
            }
            else {
                cerr<<"ERROR MATLAB_COMPARE: in "<< matlab_path.c_str() << "reach end of file whereas we parsed "<< i <<"/"<< matlab_numSpixels << "superpixels" <<endl;
                matlab_mvs_file.close();
                break;
            }
        }
        string diffOutFileStr = path;
        diffOutFile.open( diffOutFileStr.append("_diff.txt") );
    }
#endif

    std::ofstream mvsOut(path);
    if (!mvsOut.is_open()) {
        cerr << "Cannot open the output file." << endl;
        return false;
    }

    // write header
    mvsOut << width << " " << height << " " << ds.getPointDepthListPerSpix().size() << endl;

//std::cerr<<"Writing MVS " << ds.getPointDepthListPerSpix().size() << " DP " << doProcess << std::endl;
    // write all superpixels data
    for (int idSpix=0; idSpix<ds.getPointDepthListPerSpix().size(); idSpix++) {
        PointDepthList ptDepthList = ds.getPointDepthListPerSpix()[idSpix];

        // refine the list of depth samples to retain samples whose variance
        // is at most two depth histogram bins
        if (doProcess && !ptDepthList.empty()) {
            vector<float> depthvals;
            for (int i=0; i<ptDepthList.size(); i++) {
                float depth = std::get<2>(ptDepthList[i]);
                depthvals.push_back(depth);
            }
            std::sort(depthvals.begin(), depthvals.end());

            // variance of depth samples
            float original_variance = depthvals[depthvals.size()-1]-depthvals[0];

            // maximum allowed variance in depth samples
            // this is equal to with of depth histogram bins = 0.05
            float max_allowed_variance = 0.05f;

            if (original_variance > max_allowed_variance) {
                // get the median depth value and set max and min cutoffs
                // to be delta above and below the median
                float med_depth = depthvals[depthvals.size()/2];
                float min_depth = std::max(med_depth-max_allowed_variance/2.0f, 0.0f);
                float max_depth = std::min(med_depth+max_allowed_variance/2.0f, 1.0f);

                float new_min_depth = 1.0f;
                float new_max_depth = 0.0f;

                PointDepthList new_ptDepthList;
                for (int i=0; i<ptDepthList.size(); i++) {
                    float depth = std::get<2>(ptDepthList[i]);
                    if (depth>=min_depth && depth<=max_depth) {
                        new_ptDepthList.push_back(ptDepthList[i]);
                        new_min_depth = std::min(new_min_depth, depth);
                        new_max_depth = std::max(new_max_depth, depth);
                    }
                }

                // float new_variance = new_max_depth-new_min_depth;
                // cerr << idSpix << " " << original_variance << " " << new_variance << " (";
                // cerr << ptDepthList.size() << " -> " << new_ptDepthList.size() << ")";
                // cerr << endl;

                ptDepthList = new_ptDepthList;
            }
        }

        // take only nbSamples points available
        int ns = nbSamples;
        if (ptDepthList.empty()) {
            // cerr << endl << "WARNING: No depth samples in superpixel "<< idSpix << endl;
            ns = 0;
        }
        else if (ptDepthList.size() < nbSamples) { // force to get the whole list
            // cerr << endl << "WARNING: no. of samples " << nbSamples
            //     << " requested is greater than available samples "
            //     << ptDepthList.size() << " of superpixel " << idSpix << endl;
            ns = (int)ptDepthList.size();
        }
        else if (nbSamples < 0) { // get the whole list
            ns = (int)ptDepthList.size();
        }
        else {
	        random_unique(ptDepthList.begin(), ptDepthList.end(), ns);
        }

        // compute median depth based on recovered samples
        vector<float> depthvals;
        for (int i=0; i<ns; i++) {
            // 0=>y ; 1=> x ; 2=>depth
            depthvals.push_back(std::get<2>(ptDepthList[i]));
        }

        std::sort(depthvals.begin(), depthvals.end());

        float mediandepth = 0;
        float variancedepth = 0;

        if (!depthvals.empty()) {
            mediandepth   = depthvals[depthvals.size()/2];
            variancedepth = std::abs(depthvals[0]-depthvals[depthvals.size()-1]);
        }

#ifdef  MATLAB_COMPARE
        medDepth_perSpix.push_back(mediandepth);
        float matlab_diff = mediandepth - matlab_medDepth_perSpix[idSpix];
        medDepth_diff_perSpix.push_back( matlab_diff );
        if(diffOutFile.is_open())
            diffOutFile << "diff%: " << std::fixed << std::setprecision(5) << std::abs(matlab_diff/mediandepth)*100 << endl; // relative error in %
#endif

        // write superpixel line (start with superpixel global info)
        mvsOut << idSpix << " " << ns << " " << mediandepth; //is still in range [0;1] as the original code did

        // write superpixel line (continue with list of point/depth)
        for (int i=0; i<ns; i++) {
            float xResl = (float)std::get<1>(ptDepthList[i]);
            float yResl = (float)std::get<0>(ptDepthList[i]);
            float depth = std::get<2>(ptDepthList[i]);

            // convert x range scale from [1,maxResolX] to [-1,1]
            // convert y range scale from [1,maxResolY] to [-1,1]
            // convert depth range scale from [0,1] to [-1,1]
            float x = 2*(xResl-1)/(width-1)-1;
            float y = 2*(height-yResl)/(height-1)-1;
            float d = 2*depth-1;

            mvsOut << " " << x << " " << y << " " << d;
        }

        // write end of line
        mvsOut << endl;
    }

//std::cerr << "Finished MVS " << std::endl;
    mvsOut.close();

#ifdef  MATLAB_COMPARE
    // sum, average, square average, standard deviation : of all matlab depths
    float matlab_medDepth_sum   = std::accumulate(matlab_medDepth_perSpix.begin(), matlab_medDepth_perSpix.end(), 0.0);
    float matlab_medDepth_mean  = matlab_medDepth_sum / matlab_medDepth_perSpix.size();
    float matlab_medDepth_sqSum = std::inner_product(matlab_medDepth_perSpix.begin(), matlab_medDepth_perSpix.end(), matlab_medDepth_perSpix.begin(), 0.0);
    float matlab_medDepth_stdev = std::sqrt(matlab_medDepth_sqSum / matlab_medDepth_perSpix.size() - matlab_medDepth_mean * matlab_medDepth_mean);

    // sum, average, square average, standard deviation : of all current depths
    float medDepth_sum          = std::accumulate(medDepth_perSpix.begin(), medDepth_perSpix.end(), 0.0);
    float medDepth_mean         = medDepth_sum / medDepth_perSpix.size();
    float medDepth_sqSum        = std::inner_product(medDepth_perSpix.begin(), medDepth_perSpix.end(), medDepth_perSpix.begin(), 0.0);
    float medDepth_stdev        = std::sqrt(medDepth_sqSum / medDepth_perSpix.size() - medDepth_mean * medDepth_mean);

    // diff error % between the two
    if (diffOutFile.is_open()) {
        diffOutFile << "\t matlab \t|\t c++" << endl;
        diffOutFile << "sum:    " << std::fixed << std::setprecision(3) << matlab_medDepth_sum    << "\t|\t" << medDepth_sum     << endl;
        diffOutFile << "mean:   " << std::fixed << std::setprecision(3) << matlab_medDepth_mean   << "\t|\t" << medDepth_mean    << endl;
        diffOutFile << "sqSum:  " << std::fixed << std::setprecision(3) << matlab_medDepth_sqSum  << "\t|\t" << medDepth_sqSum   << endl;
        diffOutFile << "stdev:  " << std::fixed << std::setprecision(3) << matlab_medDepth_stdev  << "\t|\t" << medDepth_stdev   << endl;
    }

    // sum, average, square average, standard deviation : of the all diff
    float medDepth_diff_sum   = std::accumulate(medDepth_diff_perSpix.begin(), medDepth_diff_perSpix.end(), 0.0);
    float medDepth_diff_mean  = medDepth_diff_sum / medDepth_diff_perSpix.size();
    float medDepth_diff_sqSum = std::inner_product(medDepth_diff_perSpix.begin(), medDepth_diff_perSpix.end(), medDepth_diff_perSpix.begin(), 0.0);
    float medDepth_diff_stdev = std::sqrt(medDepth_diff_sqSum / medDepth_diff_perSpix.size() - medDepth_diff_mean * medDepth_diff_mean);

    // diff error % between the two
    if (diffOutFile.is_open()) {
        diffOutFile << "diff" << endl;
        diffOutFile << "sum:    " << std::fixed << std::setprecision(3) << medDepth_diff_sum    << endl;
        diffOutFile << "mean:   " << std::fixed << std::setprecision(3) << medDepth_diff_mean   << endl;
        diffOutFile << "sqSum:  " << std::fixed << std::setprecision(3) << medDepth_diff_sqSum  << endl;
        diffOutFile << "stdev:  " << std::fixed << std::setprecision(3) << medDepth_diff_stdev  << endl;
        diffOutFile << "err moy:" << medDepth_diff_mean/matlab_medDepth_mean << endl;
    }
#endif

    return true;
}

bool DepthSynthesisUtil::saveDepth(string path, SpMat2<float> depth_map) {
    vector<float> sdepthdata = depth_map.getRawData();
//std::cerr << "sdepth data size " << sdepthdata.size() << " == " << depth_map.getH() * depth_map.getW() << std::endl;
    vector<unsigned short> sdepth8t(sdepthdata.size(),0);
    for (int i=0; i<sdepthdata.size(); i++) {
        unsigned short val = (unsigned short)(255. * sdepthdata[i]);
        sdepth8t[i] = val;
    }
//std::cerr<< "Write " << path << " Size " <<  depth_map.getH() << " x " << depth_map.getW() << std::endl; 
    cv::Mat sdepthmat(depth_map.getH(), depth_map.getW(), CV_16S, &sdepth8t[0]);
    imwrite(path, sdepthmat);
    return true;
}


SpMat2<int> DepthSynthesisUtil::createSpixel(string path, int& w, int& h) {
    std::ifstream file (path, std::ios::binary);
    if (!file.is_open()) {
        string image_str = "file " +  path + " cannot be opened.";
        throw std::invalid_argument(image_str.c_str());
    }
    file.read(reinterpret_cast<char*>(&w),sizeof(w));
    file.read(reinterpret_cast<char*>(&h),sizeof(h));
    return SpMat2<int>(h,w,file);
}


SpMat2<float> DepthSynthesisUtil::createSdepth(string path, int& w, int& h) {
    std::ifstream file (path, std::ios::binary);
    if (!file.is_open()) {
        string image_str = "file " +  path + " cannot be opened.";
//        throw std::invalid_argument(image_str.c_str());
		std::cerr <<"Can't read " << image_str << std::endl;
		exit(-1);
    }
    file.read(reinterpret_cast<char*>(&w),sizeof(w));
    file.read(reinterpret_cast<char*>(&h),sizeof(h));
//std::cerr << "CREATING SDEPTH " << h << " " << w << std::endl;
    return SpMat2<float>(h,w,file);
}
