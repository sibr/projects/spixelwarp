/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _DS_UTILS_H_
#define _DS_UTILS_H_

#include <iostream>

#include "ds.h"
#include "spmat2.h"

/**
 * Handles file read/writes and for generating necessory datastructures
 * needed for depth synthesis.
 */
class DepthSynthesisUtil {
public:
    /**
     * Creates the depth synthesizer object and process it.
     * @param[in] path      The path to the dataset.
     * @param[in] imageid   The image id for the data synthesis.
     * @param[in] nbSamples The number of samples depth point to take into your final MVS output file
     * @param[in] ext       ext value
     * @param[in] doProcess do process
     *
     * @return  created DepthSynthesizer object.
     */
    static DepthSynthesizer createDepthSynthesizer(
            std::string path,
            int imageid,
            int nbSamples, std::string ext, bool doProcess = true);
private:
    /**
     * Creates spixel object from the spixel file in the dataset.This contains a h*w matrix  where
     * each cell contains superpixel id of the pixel.
     * @param[in]  path The path to the spixel file.
     * @param[out] w    The width of the image spixel file based on
     * @param[out] h    The height of the image spixel file based on
     *
     * @return  created Spixel datastructure.
     */
    static SpMat2<int> createSpixel(std::string path, int& w, int& h);

    /**
     * Creates sdepth object from the spixel file in the dataset. This contains a h*w matrix where
     * each cell contains the depth of the pixel
     * @param[in]  path The path to the sdepth file.
     * @param[out] w    The width of the image sdepth file based on
     * @param[out] h    The height of the image sdepth file based on
     *
     * @return  created Spixel datastructure. This contains a matrix of h*w where each cell contains superpixel id of the pixel
     */
    static SpMat2<float> createSdepth(std::string path, int& w, int& h);

    /**
     * Saves the depth datastructure in the path specified in a .png file
     * @param[in] path   The path to the depth file.
     * @param[in] sdepth The sdepth structure which contains depthinfo for each pixel
     *
     * @return  true if save is successful.
     */
    static bool saveDepth(std::string path, SpMat2<float> sdepth);

    /**
     * Save the *.mvs file =\> final result output data of depthSynth process which is structure like this for each image/file :
     *
     * \<width\> \<height\> \<numOfSuperpixels\>
     * \<superpixel1\> \n
     * \<superpixel2\> \n
     * ...
     * \<superpixelN\> \n
     *
     * Where \<superpixelN\> are : \<idSuperpixel\> \<numberOfPoints\> \<medianDepth\> \<point1\> \<point2\> ... \<pointN\>
     * Where each \<point\> of \<numberOfPoints\> are :  \<x\> \<y\> \<depth\> [in range -1;1]
     *
     *   @param[in] path                     The path to the .mvs file.
     *   @param[in] height                   The height pixels resolution of image
     *   @param[in] width                    The widht pixels resolution of image
     *   @param[in] ds                       see ds.h (list of depth per pixel foreach superpixel)
     *   @param[in] nbSamples                number of samples
     *   @param[in] doProcess                do process
     */
    static bool saveMVS(
            std::string path,
            int height,
            int width,
            DepthSynthesizer ds,
            int nbSamples = 10, bool doProcess = true);

    /**
     * Utility function to reorder a number num_random of items took randomly in the list,
     * so that the first num_random items of the list had taken randomly from the whole list.
     * see: Fisher-Yates shuffle algo
     */
    template<class bidiiter>
    static bidiiter random_unique(bidiiter begin, bidiiter end, size_t num_random) {
        size_t left = std::distance(begin, end);
        while (num_random--)
        {
            bidiiter r = begin;
            std::advance(r, rand()%left);
            std::swap(*begin, *r);
            ++begin;
            if(--left <= 0) break; else continue;
        }
        return begin;
    }
};

#endif // _DS_UTILS_H_
