/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


/**
 *  The main function. This will parse the arguments and call createDepthSynthesizer method of
 *  depth synthesis util for each of the images the depth synthesis is needed.
 */


#include <iostream>
#include <thread>
#include "dsutil.h"
#include <core/scene/BasicIBRScene.hpp>
#include <core/system/CommandLineArgs.hpp>

using std::endl;
using std::cout;
using std::vector;
using std::string;
using std::thread;
std::vector<bool> use_flag;

using namespace sibr;

void getActiveImages(std::string image_dir)
{
    int numCamera=0, numPoints=0;
    std::ifstream bundle_file;
    std::string bundle_file_str = image_dir + "/cameras/" + "bundle.out";
    std::string line;

	if (!sibr::fileExists(bundle_file_str))
		bundle_file_str = image_dir + std::string("spixelwarp/cameras/bundle.out");
	else if (!sibr::fileExists(bundle_file_str)) {                                        /* ignore if both not present */
		return;
	}

    bundle_file.open(bundle_file_str.c_str());
    std::getline(bundle_file, line);
    bundle_file >> numCamera >> numPoints;
    use_flag.resize(numCamera,true);
    std::ifstream mask_file((image_dir+ "/" +"active_images.txt").c_str());
    if (mask_file.is_open()) {
         std::fill(use_flag.begin(), use_flag.end(), false);
         while (!mask_file.eof()) {
            int t;
            mask_file >> t;
            if (t>=0 && t<numCamera) {
              use_flag[t] = true;
	        }
        }
	}
}

struct DepthNormalSynthesisArgs :
	virtual BasicIBRAppArgs {
	Arg<int> rangeStart = { "rangeStart", 0 };
	Arg<int> rangeEnd = { "rangeEnd", 0 };
	Arg<int> num = { "num", 5 };
	Arg<bool> doProcess = { "doProcess" };
	Arg<std::string> ext = { "ext", ".jpg" };
};



int main(int argc, char** argv) {
    if (argc<6) {
        cout << "Usage: depth_Synthesis [dataset path] [num of needed depth samples] [start image] [end image] [do full process 0|1] [ext]" << endl;
        return EXIT_FAILURE;
    }

	sibr::CommandLineArgs::parseMainArgs(argc, argv);
	DepthNormalSynthesisArgs myArgs;
	try
	{

    string path       = myArgs.dataset_path;         // the dataset path
    int    num        = myArgs.num;   // num of needed point/depth samples (if negative value => take the whole list)
    int    rangeStart = myArgs.rangeStart;   // from which image ID to begin the process
    int    rangeEnd   = myArgs.rangeEnd;   // the last image ID ending the process
//    bool   detectSky  = (bool)(atoi(argv[5]));   // TODO: has to be implemented
    bool   doProcess  = myArgs.doProcess;   /// \todo TODO: has to be implemented
	string ext = myArgs.ext;         // the dataset path

//std::cerr << "DO PROCESS " << doProcess << std::endl;
	//getActiveImages(path);

	BasicIBRScene::SceneOptions so;

	// no opengl context cant load texture
	so.texture = false;
	so.renderTargets = false;

	BasicIBRScene scene(myArgs, so);
	use_flag = scene.data()->activeImages();
	

//    vector<thread> tg;
	std::cerr<<"[DeptSynthesis] doing images from " << rangeStart << " to " << rangeEnd << std::endl;
#pragma omp parallel for
    for (int i=rangeStart; i<=rangeEnd; i++) {
		// only do active -- otherwise can crash since superpixels only does active
		if( use_flag[i] ) {
//       	tg.push_back(thread( bind( &DepthSynthesisUtil::createDepthSynthesizer,path,i,num) ) );
			std::cerr << "[DepthSynth] Processing Image " << i << std::endl;
   	    	DepthSynthesisUtil::createDepthSynthesizer(path,i,num,ext,doProcess);
		}
    }

/*    for (int i=0; i<tg.size(); i++) {
        tg[i].join();
    }
*/
	}
	catch ( std::exception& e )
	{
		std::cerr << e.what() << std::endl;
	}

	std::cout << " done " << std::endl;

    return EXIT_SUCCESS;
}
