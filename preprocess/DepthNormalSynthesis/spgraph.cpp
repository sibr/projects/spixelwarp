/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "spgraph.h"

void SpGraph::initialize(SpMat2<int> spmat, SpMat2<float> hist) {
    int num_nodes = hist.getH();

    int w = spmat.getW();
    int h = spmat.getH();

    graph = SPixelGraph(num_nodes);

    // adjacency matrix
    std::vector<int> edgepoints(num_nodes*num_nodes, 0);

    // loop over all pixels
    for (int y=0; y<h-1; y++) {
        for (int x=0; x<w-1; x++) {

            // find the superpixel id of current and right and down pixel
            int spcurr  = spmat.getVal(y,x);
            int spcolp1 = spmat.getVal(y,x+1);
            int sprowp1 = spmat.getVal(y+1,x);

            // add edges between these nodes
            if (spcurr != spcolp1) {
                edgepoints[spcurr *num_nodes + spcolp1] = 1;
                edgepoints[spcolp1*num_nodes + spcurr ] = 1;
            }

            if (spcurr != sprowp1) {
                edgepoints[spcurr *num_nodes + sprowp1] = 1;
                edgepoints[sprowp1*num_nodes + spcurr ] = 1;
            }

            // if (spcurr > spcolp1) {
            //     edgepoints[spcurr * num_nodes + spcolp1 ] = true;
            // } else if (spcurr < spcolp1){
            //     edgepoints[spcolp1 * num_nodes + spcurr ] = true;
            // }

            // if (spcurr > sprowp1) {
            //     edgepoints[spcurr * num_nodes + sprowp1 ] = true;
            // } else if (spcurr < sprowp1){
            //     edgepoints[sprowp1 * num_nodes + spcurr ] = true;
            // }
        }
    }

    // loop over superpixels which have edges
    // compute the edge weights for these pairs
    // and add them to the graph data structure
    for (int i=0; i<edgepoints.size(); i++) {
        if (edgepoints[i]) {
            int sp1 = i / num_nodes;
            int sp2 = i % num_nodes;
            float dist = chisqdist(sp1, sp2, hist);
            add_edge(sp1, sp2, weight(dist), graph);
        }
    }
}

std::vector<spdist> SpGraph::dijkstra_shortest_walk(int start_sp, std::vector<int> finish_sp)
{
    std::vector<spdist> spdistarr;
    std::vector<int>   p(num_vertices(graph));
    std::vector<float> d(num_vertices(graph));

    dijkstra_shortest_paths(graph, start_sp, predecessor_map(&p[0]).distance_map(&d[0]));

    for (int i=0; i<finish_sp.size(); i++) {
        int tid = finish_sp[i];
        if (p[tid] != tid) {
            spdistarr.push_back(spdist(tid, d[tid]));
        }
    }

    std::sort(spdistarr.begin(), spdistarr.end(), spdistcomp);

    return spdistarr;
}


float SpGraph::chisqdist(int sp1, int sp2, SpMat2<float> hist) {
    // d(x,y) = sum( (xi-yi)^2 / (xi+yi) ) / 2

    int n = hist.getW();

    float sum = 0.0f;

    // loop over all bins of histogram
    for (int i=0; i<n; i++) {
        float histdiff = hist.getVal(sp1,i) - hist.getVal(sp2,i);
        float histsum  = hist.getVal(sp1,i) + hist.getVal(sp2,i);

        // if (histsum>0 && histdiff!=0) {
        if (histsum > 0.0f) {
            sum += histdiff * histdiff / histsum;
        }
    }
    return sum / 2.0f;
}

std::vector<int> SpGraph::getNeighbors(int node) {
    std::vector<int> spids;
    std::pair<ai,ai> neighbors = adjacent_vertices(node, graph);

    IndexMap index = get(boost::vertex_index, graph);

    for(; neighbors.first != neighbors.second; neighbors.first++) {
        spids.push_back((int)index[*neighbors.first]);
    }

    return spids;
}
