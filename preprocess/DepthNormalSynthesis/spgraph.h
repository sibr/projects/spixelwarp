/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _SP_GRAPH_H_
#define _SP_GRAPH_H_

#include <iostream>
#include <boost/config.hpp>

#ifndef uint
#define uint unsigned int
#endif

#include <fstream>

# pragma warning(push, 0)
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>

#include <boost/graph/adjacency_iterator.hpp>
# pragma warning(pop)

#include "spmat2.h"

using namespace boost;

/** typedefs needed for boost graphs */
typedef property<edge_weight_t,double>                              weight;
typedef adjacency_list <vecS,vecS,undirectedS,no_property,weight>   SPixelGraph;
typedef graph_traits<SPixelGraph>::vertex_descriptor                vertex_descriptor;
typedef property_map<SPixelGraph,vertex_index_t>::type              IndexMap;
typedef graph_traits<SPixelGraph>::adjacency_iterator               ai;

/** Structure to hold superpixel distnance information */
class spdist {
public:
    spdist(uint i, float d) : index(i), chi_dist(d) {}
    unsigned int index;
    float chi_dist;
};

/**
 * Compares two spdist objects based on distance; used for sorting
 *
 * @param[in] i spdist object 1
 * @param[in] j spdist object 2
 *
 * @return true if distance for i is less than the distance for j
 */
static bool spdistcomp(spdist i, spdist j) {
    return (i.chi_dist<j.chi_dist);
}

/**
 * Superpixel graph for computing shortest Geodesic path between superpixels
 */
class SpGraph {
private:
    /** The graph object */
    SPixelGraph graph;

public:
    /**
     * Initializes the graph object
     *
     * @param[in] spmat superpixel id of each pixel of image (image width x image height)
     * @param[in] hist  LAB histogram of each spixel (num spixels x num histogram bins)
     */
    void initialize(SpMat2<int> spmat, SpMat2<float> hist);

     /**
     * Returns chi-sq distance for given two spixels
     *
     * @param[in] sp1   superpixel 1
     * @param[in] sp2   superpixel 2
     * @param[in] hist  histogram to calculate chi-sq distance
     *
     * @return chi-sq distance between superpixels
     */
    float chisqdist(int sp1, int sp2, SpMat2<float> hist);

     /**
     * Compute shortest walk from a superpixel to a list of superpixels
     *
     * @param[in] start_sp  spixel id as starting point of shortest walk
     * @param[in] finish_sp spixels id as finishing points of shortest walk
     *
     * @return shortest walk distances to all finishing points in ascending order
     */
    std::vector<spdist> dijkstra_shortest_walk(int start_sp, std::vector<int> finish_sp);

    /**
     * Returns a vector of immediate neighbors of a given superpixel
     *
     * @param[in] node source superpixel id
     *
     * @return list of neighboring superpixel ids
     */
    std::vector<int> getNeighbors(int node);
};

#endif // _SP_GRAPH_H_
