/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _SP_MAT_H_
#define _SP_MAT_H_

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>


/**
 * 2D matrix for datastrucures needed for depth synthesis
 */
template<class T>
class SpMat2 {
private:

    /** Width and height matrix */
    int w, h;

    /** Minimum and maximum values */
    T max_val, min_val;

    /** Is it needed to update the min and max values */
    bool maxMinUpdateNeeded;

    /** Raw data values */
    std::vector<T> data;

    /** Updates the min-max values */
    void updateMaxMin() {
        if (w*h == 0) {
            min_val = 0;
            max_val = 0;
        } else {
            min_val = data[0];
            max_val = data[0];
            for (int i=0; i<data.size(); i++) {
                max_val = std::max(max_val, data[i]);
                min_val = std::min(min_val, data[i]);
            }
        }
        maxMinUpdateNeeded = false;
    }

public:
    /**
     * Default empty constructor
     */
    SpMat2() : w(0), h(0) {}

    /**
     * Constructor
     *
     * @param[in] h height or number of rows
     * @param[in] w width or number of columns
     * @param[in] s datastream that contains data to fill
     */
    SpMat2(int h, int w, std::ifstream& s) : w(w), h(h) {
        data.resize(w*h);

        T val;
        char* valchar = reinterpret_cast<char*>(&val);
        for (int y=0; y<h; y++) {
            for (int x=0; x<w; x++) {
                s.read(valchar, sizeof(val));
                setVal(y,x,val);
            }
        }
        maxMinUpdateNeeded = true;
    }

    /**
     * Constructor.
     *
     * @param[in] h      height or number of rows
     * @param[in] w      width or number of columns
     * @param[in] defval default value
     */
    SpMat2(int h, int w, T defval) : w(w), h(h) {
        data.resize(w*h,defval);
        maxMinUpdateNeeded = true;
    }

    /** Matrix width */
    int getW() { return w; }

    /** Matrix height */
    int getH() { return h; }

    /** Matrix number of rows */
    int get_rows() { return w; }

    /** Matrix number of columns */
    int get_cols() { return h; }

    /** Matrix raw data */
    std::vector<T> getRawData() { return data; }

    /**
     * Returns maximum value
     *
     * @return maximum value
     */
    T getMax() {
        if (maxMinUpdateNeeded) {
            updateMaxMin();
        }
        return max_val;
    }

    /**
     * Returns maximum value of the row
     *
     * @param[in] rowid rowid of which max is needed
     *
     * @return maximum value of the row rowid
     */
    T getMax(int rowid) {
        if (w*h==0 || rowid>=w-1) {
            return 0;
        }

        T val = data[rowid*w];
        for (int x=0; x<w; x++) {
            val = std::max(val, data[rowid*w + x]);
        }
        return val;
    }

    /**
     * Returns minimum value
     *
     * @return minimum value
     */
    T getMin() {
        if (maxMinUpdateNeeded) {
            updateMaxMin();
        }
        return min_val;
    }

    /**
     * Returns value in given position
     *
     * @param[in] y y position or row index
     * @param[in] x x position or column index
     *
     * @return value of (y,x)
     */
    T getVal(int y, int x) {
        return data[y*w + x];
    }

    /**
     * Sets value in given position
     *
     * @param[in] y        y position
     * @param[in] x        x position
     * @param[in] val    value to be set
     */
    void setVal(int y, int x, T val) {
        maxMinUpdateNeeded = true;
        data[y*w + x] = val;
    }

    /**
     * Increments value at given position by inc
     *
     * @param[in] y      y position or row index
     * @param[in] x      x position or column index
     * @param[in] val    value to be incremented by
     */
    void incVal(int y, int x, T val) {
        maxMinUpdateNeeded = true;
        setVal(y, x, getVal(y,x)+val);
    }

    /**
     * Normalizes rows
     *
     * @param[in] factor normalization factor
     */
    void normalizeRows(std::vector<T> factor) {
        for (int y=0; y<h; y++) {
            T f = factor[y];
            for (int x=0; x<w; x++) {
                setVal(y, x, getVal(y,x)/f);
            }
        }
    }

    /**
     * Returns a row vector containing maximum of each row
     *
     * @returns row vector containing max of each row
     */
    std::vector<T> get_row_max() {
        std::vector<T> rowMaxs(h);
        for (int y=0; y<h; y++) {
            T maxv = getVal(y,0);
            for (int x=0; x<w; x++) {
                maxv = std::max(maxv, getVal(y,x));
            }
            rowMaxs[y] = maxv;
        }
        return rowMaxs;
    }

    /**
     * Returns a row vector containing total of each row
     *
     * @returns row vector containing total of each row
     */
    std::vector<T> get_row_sum() {
        std::vector<T> rowtot(h);
        for (int y=0; y<h; y++) {
            T total = 0;
            for (int x=0; x<w; x++) {
                total += getVal(y,x);
            }
            rowtot[y] = total;
        }
        return rowtot;
    }

    /**
     * Returns SpMat2 containing rows specified in input
     *
     * @param[in] rowids ids of rows to be extracted
     *
     * @return the filtered data
     */
    SpMat2<T> extract_rows(std::vector<int> rowids) {
        SpMat2<T> filtered((int)rowids.size(), w, 0);
        typename std::vector<T>::iterator iter = filtered.data.begin();

        for (int rid: rowids) {
            // rid: rowids
            // copy the row
            std::copy(data.begin()+rid*w, data.begin()+(rid+1)*w, iter);
            iter += w;
        }

        return filtered;
    }

    /**
     * Replaces a row with given data
     *
     * @param[in] rowid id of the row tobe replaced
     * @param[in] rowdata replacement data, should be single row
     */
    void replaceRow(int rowid, SpMat2<T> rowdata) {
        std::copy(rowdata.data.begin(), rowdata.data.begin()+w, data.begin()+rowid*w);
    }
};

#endif // _SP_MAT_H_
