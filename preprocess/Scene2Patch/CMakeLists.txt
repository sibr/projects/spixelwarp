# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


project(Swarp_scene2patch)

file(GLOB SOURCES "*.cpp" "*.h" "*.hpp" )

add_executable(${PROJECT_NAME} ${SOURCES})
source_group("Source Files" FILES ${SOURCES})

include_directories(${Boost_INCLUDE_DIRS} . )


target_link_libraries(${PROJECT_NAME}
	${Boost_LIBRARIES}
	${ASSIMP_LIBRARIES}
	${GLEW_LIBRARIES}
	${OPENGL_LIBRARIES}
    ${OpenCV_LIBRARIES}	
	OpenMP::OpenMP_CXX
	glfw3
	sibr_system
	sibr_view
	sibr_assets
	sibr_graphics
	sibr_raycaster
	sibr_swarp
)
set_target_properties(${PROJECT_NAME} PROPERTIES FOLDER "projects/spixelwarp/preprocess")

#define postfix for various debug/release
set_target_properties(${PROJECT_NAME} PROPERTIES
	DEBUG_POSTFIX "_d"
	RELWITHDEBINFO_POSTFIX "_rwdi"
	MINSIZEREL_POSTFIX "_msr"
)

## High level macro to install in an homogen way all our ibr targets
include(install_runtime)
ibr_install_target(${PROJECT_NAME}
    INSTALL_PDB                         ## mean install also MSVC IDE *.pdb file (DEST according to target type)
    STANDALONE  ${INSTALL_STANDALONE}   ## mean call install_runtime with bundle dependencies resolution
    COMPONENT   ${PROJECT_NAME}_install ## will create custom target to install only this project
)
