/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <core/assets/InputCamera.hpp>
#include <core/graphics/Image.hpp>
#include <core/system/CommandLineArgs.hpp>
#include <core/system/Utils.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <core/scene/ParseData.hpp>
#include <projects/spixelwarp/renderer/PatchFile.hpp>
#include <projects/spixelwarp/renderer/PatchPoint.hpp>

#include <fstream>
using namespace sibr;

// -----------------------------------------------------------------------

#define NUM_DESIRED_PATCHES		400000 /// \todo TODO should be a parameter
#define RECONS_COVERAGE			0.75 /// \todo TODO should be a parameter

// -----------------------------------------------------------------------

using namespace std;

// -----------------------------------------------------------------------

#define PROGRAM_NAME "sibr_scene2patch"

struct Scene2PatchArgs :
	virtual BasicIBRAppArgs {
};

static Scene2PatchArgs*	g_myArgsPtr = NULL;


const char* usage = ""
	"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
	;

// -----------------------------------------------------------------------

static void 	saveClippingPlanes(
		const std::string& dstFilename,
		const sibr::PatchFile& patchFile, 
		const std::vector<sibr::InputCamera::Ptr>& cams )
{
	float ZNEAR = 1e2f;
	float ZFAR  = 1e-2f;

	for (uint pointID = 0; pointID < patchFile.pointCount(); ++pointID)
	{
		const sibr::PatchPoint& p = patchFile.point(pointID);
		for(uint camIndex : p.visibleInImages())  
		{
			const sibr::InputCamera& cam = *cams[camIndex];
			sibr::Vector3f vec = p.position() - cam.position();                 /* select the z range */
			sibr::Vector3f cdirNorm = cam.dir();
			cdirNorm.normalize();
			float z = cdirNorm.dot(vec);
			if (z<0.01)                                       /* ignore points behind the camera */
				continue;

			ZNEAR = std::min(ZNEAR, z);
			ZFAR  = std::max(ZFAR, z);
		}
	}
/*
	for (size_t i = 0; i < cams.size(); i++) {
		ZNEAR = std::min(ZNEAR, cams[i].znear());
		ZFAR = std::max(ZFAR, cams[i].zfar());
	}*/

	SIBR_LOG << "Saving " << dstFilename << std::endl;

	std::ofstream file(dstFilename, std::ios::trunc | std::ios::out);
	if (file)
		file << ZNEAR << ' ' << ZFAR << std::endl;
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

void mesh_to_patch( const sibr::BasicIBRScene::Ptr );


/* Global definitions */
float		min_mvs_thld = 0.8f;



int main( int ac, char** av )
{
	SIBR_PROFILESCOPE;

	// No graphics
//	sibr::IBRScene		scene(ac, av, false);
//	std::cout << "\nRead scene ..." << std::endl;
	// Parse Commad-line Args
	CommandLineArgs::parseMainArgs(ac, av);
	Scene2PatchArgs	myArgs;

	g_myArgsPtr = &myArgs;

	BasicIBRScene::SceneOptions so;

	// no opengl context cant load texture
	so.texture = false;
	so.renderTargets = false;

	// Setup IBR
	BasicIBRScene::Ptr		scene(new BasicIBRScene(myArgs, so));
 

	/** Compute View-Perspective matrix with clipping planes [1,-1] */
/* VERIF
	for(int i=0; i<scene->cameras()->inputCameras().size(); i++) {
		if( scene->cameras()->inputCameras()[i]->isActive() ) {
			scene->cameras()->inputCamerasModif()[i].znear(1.f);
			scene->cameras()->inputCamerasModif()[i].zfar(-1.f);
			// camera update is done automatically after a change in near/far
		}
	}
*/
	
	/** Creates patches from sampling the mesh. */
	mesh_to_patch( scene );		// patches

	return EXIT_SUCCESS;
}

// -----------------------------------------------------------------------
/** Build a patch element and add it to the patches container. 
*/
static void populate_patches( const sibr::BasicIBRScene::Ptr scene, uint cam_curr, 
		sibr::Raycaster& rc, const sibr::Vector3f &pnt, vector<sibr::PatchPoint> &patches )
{
	sibr::PatchPoint patch_curr;
	patch_curr.position(pnt);
	patch_curr.visibleInImages().push_back(cam_curr);

	/** Compute visibility for pnt. */
	for(uint k=0; k<scene->cameras()->inputCameras().size(); k++) 
	{
		if(k==cam_curr) continue; //< Skip current camera.
		/** To be visible in a camera, the point must reprojects to the fov of the camera... AND MUST BE IN FRONT OF THE CAMERA */
		// SB EDIT: or we could re-launch another ray from the point to each cam
		// (used for embree)

		const sibr::InputCamera& cam = *scene->cameras()->inputCameras()[k];

		/// \todo Eigen needs explicit conversions .. TODO ?
		sibr::Vector3d pnt_vec(pnt.x(), pnt.y(), pnt.z());
		sibr::Vector3d posdiff = pnt_vec- cam.position().cast<double>();
		if( cam.dir().cast<double>().dot( posdiff) < FLT_EPSILON )
			continue;

		sibr::Vector3d pnt_vec_reprj = cam.project(pnt_vec.cast<float>()).cast<double>();
		if( (pnt_vec_reprj[0]<-1||pnt_vec_reprj[0]>1) || (pnt_vec_reprj[1]<-1||pnt_vec_reprj[1]>1) )
			continue;

		/** There must be only one single intersection (at pnt). */
		float alpha = 1e-9f;
		float dist2point = (cam.position()-pnt).norm();
		sibr::Vector3f dir = (pnt-cam.position()).normalized();
		sibr::Ray ray(cam.position(), dir);
		sibr::RayHit hit = rc.intersect(ray);

		if (hit.hitSomething() == true && hit.dist() < dist2point - alpha*2.f)
			continue;

		patch_curr.visibleInImages().push_back( k );
	}

	/** Keep patch only if it is visible in more than one view. */
	if( patch_curr.visibleInImages().size()>1 ) {
		patch_curr.confidence(1.0f);
		patches.push_back( patch_curr );
	}
}

static uint getActiveCamCount( const sibr::BasicIBRScene::Ptr scene )
{
	uint count = 0;
	for (const sibr::InputCamera::Ptr& cam : scene->cameras()->inputCameras())
		count += ((cam->isActive())? 1 : 0);
	return count;
}

static uint computePadding( const sibr::BasicIBRScene::Ptr scene )
{
	uint n_active_cams = getActiveCamCount(scene);
	uint w = 0;
	uint h = 0;

	for(int k=0; k<scene->cameras()->inputCameras().size(); k++) 
	{
		const sibr::InputCamera& cam = *scene->cameras()->inputCameras()[k];

		if (cam.isActive() == false) continue;

		/// \todo GD TODO: dont understand the following lines; take max ?
		/// SB: I guess so I corrected it
		//w = scene->cameras()->inputCameras()[k]->w();
		//h = scene->cameras()->inputCameras()[k]->h();
		w = std::max(w, cam.w());
		h = std::max(h, cam.h());
	}

	cout << "[SIBR::Scene2Patch] Approximate number of desired patches: "<<NUM_DESIRED_PATCHES<< endl;

	uint PXL_PADDING = uint(sqrt( w*h*RECONS_COVERAGE*n_active_cams/NUM_DESIRED_PATCHES ));	
	PXL_PADDING += (PXL_PADDING%2)*-1 + 1;		//< Make it odd.

	if(PXL_PADDING > 10)
		PXL_PADDING = 10;

	if( PXL_PADDING<=0 )
		SIBR_ERR << "PXL_PADDING  must be > 0 " ;
	return PXL_PADDING;
}

// -----------------------------------------------------------------------
/** Compute and writes the patch file from the intersections of viewing rays of all cameras 
	with triangles in the mesh.  
	*/
void mesh_to_patch( const sibr::BasicIBRScene::Ptr scene)
{
	/** Create debuging depth images. */
	bool saveAscii = true; // set to false when we have the qualityEstim in SIBR

	// init a raycaster instance
	sibr::Raycaster rc;
	if (rc.init() == false) {
		
		SIBR_ERR << "Failed to init raycaster " << std::endl;
		return;
	}

	rc.addMesh(scene->proxies()->proxy());
	
	uint n_active_cams = getActiveCamCount(scene);
	uint PXL_PADDING = computePadding(scene);
	std::cout << "PXL_PAD " << PXL_PADDING << std::endl;

	vector<vector<sibr::PatchPoint>> patchesPerCam(n_active_cams);
	for (auto& patches : patchesPerCam)
		patches.reserve( NUM_DESIRED_PATCHES/n_active_cams );

#pragma omp parallel for num_threads(8)
	for(int k=0; k<scene->cameras()->inputCameras().size(); k++) 
	{
		sibr::InputCamera cam = *scene->cameras()->inputCameras()[k];

		// camera update is done automatically after a change in near/far
		cam.znear(1.f);
		cam.zfar(-1.f);

		if( !cam.isActive() ) continue;

		std::cout << "[SIBR::Scene2Patch] Computing patches for camera: " << k << std::endl;

		//deb_depthpImgs[k].reset( new ImageRGB32F( scene->cameras()->inputImages()[k]->w(), scene->cameras()->inputImages()[k]->h() ) );

		for(uint j=2; j<cam.h(); j=j+PXL_PADDING )
		{
			for(uint i=(j%2)*2; i<cam.w(); i=i+PXL_PADDING )
			{
				float w = float(cam.w()), h = float(cam.h());
				const sibr::Vector3f v(float(i)*2.f/w - 1.f,1.f -float(j)*2.f/h, -1.0);
				sibr::Vector3f pnt_c_pos(cam.position());
				sibr::Vector3f pnt_ij(cam.unproject(v));
				sibr::Vector3f dir = (pnt_ij-pnt_c_pos).normalized();
				sibr::Ray	  ray_query( pnt_c_pos, dir);		

				sibr::RayHit hit = rc.intersect(ray_query);
				if ( hit.hitSomething() == false)
					continue;

				sibr::Vector3f p_intersect = pnt_c_pos + dir*hit.dist();

				// DEBUGGING 
				/*
				   float d = scene->cameras()->inputCameras()[k]->project(sibr::Vector3f(p_intersect.x(), p_intersect.y(), p_intersect.z()), false)[2];						
				   deb_depthpImgs[k](i,j)[0] = d;
				   deb_depthpImgs[k](i,j)[1] = d;
				   deb_depthpImgs[k](i,j)[2] = d;
				   */
				//< The ray intersects the mesh. Then build a patch.
				populate_patches(scene, k, rc, p_intersect, patchesPerCam[k]);
			}
		}			
	}
	// Gather patches
	vector<sibr::PatchPoint> patches;
	for (const auto& fromPatches : patchesPerCam)
		patches.insert(patches.end(), fromPatches.begin(), fromPatches.end());
	patchesPerCam.clear();

	/*
	for( Images_ptr::iterator it = deb_depthpImgs.begin(); it!= deb_depthpImgs.end(); ++it  ){		
		(*it)->remap(0,255);
		(*it)->save( sibr::sprint("%08d.png", 1+it-deb_depthpImgs.begin()) );
	}*/
	sibr::PatchFile patchFile;
	std::string fname;

	int i=0;
	std::cerr <<"[SIBR::Scene2Patch] Writing "<< patches.size() << " patches"<< std::endl;
	for (const sibr::PatchPoint& patch : patches) 
		patchFile.point(i++, patch);

	std::string swarpPath;
	
	if (sibr::directoryExists(std::string(g_myArgsPtr->dataset_path) + "/half_size/")) {
		swarpPath = std::string(g_myArgsPtr->dataset_path);
	}
	else {
		swarpPath = std::string(g_myArgsPtr->dataset_path) + "/spixelwarp/";
	}

	if (scene->data()->datasetType() == sibr::IParseData::Type::EMPTY) {
		SIBR_ERR << "Dataset type not recongnized" << std::endl;
	}

	if (!sibr::directoryExists(swarpPath)) {
		sibr::makeDirectory(swarpPath);
	}
	if (!sibr::directoryExists(swarpPath + "/meshes/")) {
		sibr::makeDirectory(swarpPath + "/meshes/");
	}

	if( saveAscii ) {
		fname = swarpPath + "/meshes/pmvs_recon.patch";
		std::cout << "[SIBR::Scene2Patch] Saving to: " << fname  << std::endl;
		patchFile.saveToASCIIFile(fname);
	}
	fname = swarpPath + "/meshes/pmvs_recon.patchbin";
	std::cout << "[SIBR::Scene2Patch] Saving to: " << fname  << std::endl;
	patchFile.saveToBinaryFile(fname); // must be saved AFTER the ascii version (update detection)

	// Save clipping_planes.txt
	saveClippingPlanes(swarpPath + "/clipping_planes.txt", patchFile, scene->cameras()->inputCameras());
}


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



