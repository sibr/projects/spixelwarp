/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <core/system/LoadingProgress.hpp>
#include <core/system/String.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <projects/spixelwarp/renderer/MVSFile.hpp>
#include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>
#include <projects/spixelwarp/renderer/Utils.hpp>
#include "PlanarEstimationUtils.hpp"
#include "Plane.h"

//----------------------------------------------------------------

#define NUM_PLANMODEL_BASE	3		//< Base of a plane: 3 points.
#define MIN_NUM_MVS			12		//< Minimun number of MVS points in a spixel to compute a plane.
#define MAX_RAT_FILTER		0.6		//< Maximun 55% of points can be filterd, otherwise gemetric median is NOT VALID!

#define RANSAC_THRESHOLD	1e-2	//< RANSAC threshold.
#define RANSAC_prob			0.99	//< RANSAC probability of choosing at least one sample free from outliers.
//#define MAX_ITER			1000	//< Maximum number of iterations.
//#define maxDataTrials		100		//< Maximun num of attemps to find a non degenerative model.

#define GEN_RAND_NUM(min,max) ((double)rand()/RAND_MAX)*(max-min) + min		//< Randon number generator (min,max)

//----------------------------------------------------------------

namespace sibr { 

//----------------------------------------------------------------

/** Returns a vector of uint of size s with indices values, sampled uniformly
	at random, without replacement, from the integers [0,npts-1].
*/
	std::vector<uint> rand_indices(uint npts, uint s)
	{
		SIBR_ASSERT(npts > s);

		//< Fill the indices vector.
		std::vector<uint> indices;
		for (uint i = 0; i < npts; i++)
			indices.push_back(i);
		/* init exclusion for inpainting (if requested) */

		uint max_ind = npts - 1;										//< Indice to the las position of indices vector.

		for (uint i = 0; i < s; i++) {
			uint r = (uint)(GEN_RAND_NUM(0, (double)max_ind) + 0.5); //< Generate a ramdon uint indice.		
			std::swap(indices[max_ind], indices[r]);				//< Swap values at positions.
			max_ind--;
		}

		return std::vector<uint>(indices.begin() + npts - s, indices.begin() + npts);
	}

	//----------------------------------------------------------------

	/** Test if 3 sibr::Vector3f points are collinear. */
	static bool isCollinear(sibr::Vector3f& a, sibr::Vector3f& b, sibr::Vector3f& c)
	{
		return ((b - a).cross(c - a)).norm() < DBL_EPSILON;
	}



	//----------------------------------------------------------------
	/** Recives the address of a superpixel and fit a plane to MVS points contained in the superpixel.
		Uses a modified version of RANSAC score.
		TODO: use plane_model as a Plane structure
	*/
	void ransac_plane_fitting(sibr::PlanarSPixelPreprocess::Ptr spixel,
		float THRESHOLD, sibr::Vector3f cam_dir) {

		std::vector<sibr::Vector3f> mvs;
		std::chrono::time_point<std::chrono::system_clock> all_start, all_end;
		std::chrono::time_point<std::chrono::system_clock> ind_start, ind_end;
		all_start = std::chrono::system_clock::now();

		std::vector<float> weights;
		// list3DPtLists is non zero if this is an SP which has depth created by inpainting
		// TODO -- wasteful since plane estimation does not change for the others
		if (list3DPtLists.size() == 0 ||
			// if we are here there are old MVS points
			list3DPtLists[spixel->id()].size() < MIN_NUM_MVS) {
			const std::vector<sibr::MVSPointPreprocess>& pmvs =
				spixel->getAllMVSPointsPreprocess();	//< Get list of mvs points.

			/** Compute weights dependieng of the number of visible views.
			*/
			float totalViews = 0;
			for (std::vector<sibr::MVSPointPreprocess>::const_iterator it =
				pmvs.begin(); it != pmvs.end(); ++it)
			{
				totalViews += it->patchPnt().visibleInImages().size();
			}
			weights.resize(pmvs.size());
			for (uint i = 0; i < weights.size(); i++)
			{
				weights[i] = ((float)(pmvs[i].patchPnt().visibleInImages().size())) / totalViews;
			}

			mvs.resize(pmvs.size());
			for (uint i = 0; i < pmvs.size(); i++)
				mvs[i] = pmvs[i].patchPnt().position();
		}
		else {
			// inpainted spixels which have 3D points
			uint numpts = (uint)list3DPtLists[spixel->id()].size();
			mvs.resize((size_t)numpts);
			for (uint i = 0; i < numpts; i++) {
				mvs[i] = list3DPtLists[spixel->id()][i];
			}
			weights.resize(numpts);
			for (uint i = 0; i < weights.size(); i++)
				weights[i] = 1.;
		}


		uint iteration = 0;
		float bestscore = 0;
		std::vector<uint> bestinliers;								//< List of indices of best inliers.

		/** Loop to find inliers.
			Select at random s datapoints to form a trial model, M.
			In selecting these points we have to check that they are not in a degenerate configuration. */
		uint N = 1;													//< Dummy initialisation for number of trials.

		std::chrono::duration<double> ind_elapsed_seconds = all_start - all_start; // 0
		uint stride = 1;
		if (isBig) {
			static int cnt = 0;
			// no more than 300 pts -- hack TODO -- should be a random sampling
			stride = (uint)mvs.size() > 300 ? (int)(mvs.size() / 300.f) : 1;
			if (cnt < 10 && stride != 1)
				std::cerr << "Stride is " << stride << " (MVS " << mvs.size() << ")" << std::endl, cnt++;
		}


		while (N > iteration) {

			bool degenerate = true;
			uint trialCount = 1;
			std::vector<sibr::Vector3f> plane_model;

			//** Loop to find a non degenerative model. */
			while (degenerate) {

				//* Generate s random indicies in the range 1..npts */
				std::vector<uint> inds =
					rand_indices((uint)mvs.size(), NUM_PLANMODEL_BASE);

				sibr::Vector3f p1 = mvs[inds[0]];
				sibr::Vector3f p2 = mvs[inds[1]];
				sibr::Vector3f p3 = mvs[inds[2]];

				//			sibr::Vector3f p1 = mvs[inds[0]].patchPnt().position();
				//			sibr::Vector3f p2 = mvs[inds[1]].patchPnt().position();
				//			sibr::Vector3f p3 = mvs[inds[2]].patchPnt().position();
				//
							//* Test that these points are not a degenerate configuration. */
				degenerate = isCollinear(p1, p2, p3);

				if (!degenerate) {
					//< Add random selected points to model. 
					plane_model.push_back(p1);
					plane_model.push_back(p2);
					plane_model.push_back(p3);
				}
				else {
					trialCount++;
				}

				//< Safeguard against being stuck in this loop forever            
				if (trialCount > maxDataTrials) {
					std::cout << "Unable to select a nondegenerate data set";
					break;
				}
			}

			/** Evaluate distances between points and model returning the indices
				of elements that are inliers. */
			std::vector<uint> inliers;
			inliers.clear();
			ind_start = std::chrono::system_clock::now();
			double score = plane_score(plane_model, mvs, weights, THRESHOLD, inliers, stride);
			ind_end = std::chrono::system_clock::now();
			ind_elapsed_seconds += (ind_end - ind_start);

			if (score > bestscore) {
				bestscore = (float)score;						//< Record data for this model.
				bestinliers = inliers;
				//best_plane_model	= plane_model;

				/** Update estimate of N, the number of trials to ensure we pick,
					with probability p, a data set with no outliers.*/
				float fracinliers = (float)inliers.size() / (float)mvs.size();
				double pNoOutliers = std::max(DBL_EPSILON, 1 - (double)pow(fracinliers, NUM_PLANMODEL_BASE));	//< Avoid division by -Inf
				pNoOutliers = std::min(1 - DBL_EPSILON, pNoOutliers);										//< Avoid division by 0.
				N = (uint)(log(1 - RANSAC_prob) / log(pNoOutliers));
			}

			iteration++;
			if (iteration > MAX_ITER) {
				/** Safeguard against being stuck in this loop forever. */
				static int cnt = 0;
				if (isBig)
					badCnt++;
				//			cout << "RANSAC reached the maximum number of iterations.";
				break;
			}
		}

		/** Use the set of found inliers to compute a plane equation in LS sense. */
		SIBR_ASSERT(!bestinliers.empty());

		//** Get the 3D inliers points. */
		std::vector<sibr::Vector3f> inliers_points;
		std::vector<float> inliers_weights;
		float totalWeightsInliers = 0;
		for (uint i = 0; i < bestinliers.size(); i++) {
			inliers_points.push_back(mvs[bestinliers[i]]);
			totalWeightsInliers += weights[bestinliers[i]];
		}

		for (uint i = 0; i < bestinliers.size(); i++) {
			inliers_weights.push_back(weights[bestinliers[i]] / totalWeightsInliers);
		}

		/** Perform least squares fit to the inlying points.  */
		Plane plane_obj;
		float fitting_err;
		plane_obj.FitToPoints(inliers_points, inliers_weights, fitting_err);

		/** Orient normals toward againts camera direction. */
		if (plane_obj.Normal().dot(-cam_dir) < 0)
			plane_obj.Flip();

		spixel->setNormal(sibr::Vector3f(plane_obj.a, plane_obj.b, plane_obj.c));
		spixel->setD(plane_obj.d);
		spixel->setPoint_p(sibr::Vector3f(plane_obj.centroid_x, plane_obj.centroid_y, plane_obj.centroid_z));
		spixel->setActive_PLAN(true);

		all_end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = all_end - all_start;

		//    std::cout <<  "Overall time: " << elapsed_seconds.count() << "s (" << ind_elapsed_seconds.count() << "s planescore) MVS size: " << mvs.size() << " iters " << iteration << " stride " << stride << " \n";
	}

	bool frustum_test(sibr::PlanarSPixelPreprocess::Ptr spixel, sibr::InputCameraPreprocess &cam)
	{
		bool pass_test = true;

		std::vector<sibr::Vector2d> t;
		//sebM spixel->planemesh(t);
		spixel->planemesh(t, cam.w(), cam.h());

		sibr::Vector3f plane_n = spixel->getNormal();
		float plane_d = spixel->getD();

		for (uint j = 0; j < 4; j++) {

			double xx = t[j][0], yy = t[j][1];

			sibr::Vector3f X = cam.unproject(sibr::Vector3f((float)xx, (float)yy, 0.f));
			sibr::Vector3f ray = X - cam.position();

			///< Intersect wiht \pi
			float denom = ray.dot(plane_n);
			if (abs(denom) < FLT_EPSILON) {
				pass_test = false;
				break;
			}

		///< Vertice inside the behind the camera 
		float alpha = -(cam.position().dot(plane_n) + plane_d) / denom;
		if (alpha < 1 - FLT_EPSILON) {
			pass_test = false;
			break;
		}

		sibr::Vector3f vec_i = alpha * ray;
		///< ZNEAR < depth < ZFAR
		float z_test = cam.dir().dot(vec_i);
		if (z_test > cam.zfar() || z_test < cam.znear()) {
			pass_test = false;
			break;
		}

		//sibr::Vector3f p3d = vec_i + cam.position();
	}

	return pass_test;
}



	
	//----------------------------------------------------------------

	/** Modified RANSAC score for plane and inliers generation. */
	//static float plane_score( vector<sibr::Vector3f> model, vector<PatchPoint> mvs, float THRESHOLD, vector<bool> &inliers ){
	float plane_score(std::vector<sibr::Vector3f>& model, 
		std::vector<sibr::Vector3f>& mvs, std::vector<float> w, float THRESHOLD, 
		std::vector<uint> &inliers, uint stride)
	{
		float score = 0;

		sibr::Vector3f plane_n = (model[1] - model[0]).cross(model[2] - model[1]);      //< Normal of plane model.
		plane_n = plane_n / (plane_n.norm());							//< Make it a unit vector.

		float dist_thres;


		for (uint i = 0; i < mvs.size(); i += stride) {
			//		dist_thres = (plane_n.dot( mvs[i].patchPnt().position() - model[0])/THRESHOLD);
			dist_thres = (plane_n.dot(mvs[i] - model[0]) / THRESHOLD);

			score = score + w[i] * exp(-(dist_thres*dist_thres) / 2);

			if (abs(dist_thres) < 1)
				inliers.push_back(i);
		}

		return score;
	}


	//----------------------------------------------------------------

	void planes_estimation(sibr::PlanarInputCameraPreprocess& cam, bool big ) {//, vector<sibr::Vector3f> points_ply = vector<sibr::Vector3f>() ){

		int approx_pxls_in_spixel = ((cam.w() / cam.getScale()) * (cam.h() / cam.getScale())) / cam.numSPixels();
		const int MIN_PXL_POINTS = (const int)(0.20 * approx_pxls_in_spixel);  //20% Of the expected size

		if (big) {
			isBig = true;
			MAX_ITER = 400;
			maxDataTrials = 80;
		}
		else {
			isBig = false;
			MAX_ITER = 1000;
			maxDataTrials = 100;
		}
		//points_ply.reserve( cam->numSPixels() );

		for (uint i = 0; i < cam.numSPixels(); i++) {

			sibr::PlanarSPixelPreprocess::Ptr spixel = cam.getPlanarSPixelPreprocess(i);
			
//			sibr::PlanarSPixelPreprocess::Ptr spixel = cam.getPlanarSPixel(i);
			/** SPHERE FILTER HERE!
				0. A sphere proportional to the 2D superpixel size will contains valid points.
				1. Find the center of the sphere.
				2. Find radio of the sphere.
				3. Separate MVS points outside the sphere.
			*/
			/*if( spixel->numPatchPoints() > MIN_NUM_MVS )
				sphere_filter( spixel, cam );*/

				/** Number of MVS points and number of pixels inside the superpixels must be greater
					than MIN_NUM_MVS and MIN_PXL_OINTS respectively.
				*/

			if ((int)spixel->numMVSPointsPreprocess() > MIN_NUM_MVS && 
				(int)spixel->getPixels().size() > MIN_PXL_POINTS) {
				ransac_plane_fitting(spixel, (float)RANSAC_THRESHOLD, cam.dir());	//< RANSAC

				if (spixel->getActive_PLAN()) {
					if (!frustum_test(spixel, cam))
						spixel->setActive_PLAN(false);
				}
			}
			cam.setPlanarSPixelPreprocess(spixel);
		}

		if (isBig && badCnt > 0)
			std::cerr << "Bad cnt " << badCnt << " out of " << cam.numSPixels() << std::endl;
		badCnt = 0;
		///< Save ply file
		//SIBR_ASSERT( !(points_ply.size()%4) );
	}




} /*namespace sibr*/ 
