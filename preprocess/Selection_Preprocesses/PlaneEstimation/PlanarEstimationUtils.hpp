/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_EXP_SPIXELWARP_PLANAR_ESTIMATION_UTILS_HPP__
# define __SIBR_EXP_SPIXELWARP_PLANAR_ESTIMATION_UTILS_HPP__

# include <core/scene/BasicIBRScene.hpp>
# include <projects/spixelwarp/renderer/Utils.hpp>
# include <projects/spixelwarp/renderer/SpixelImage.hpp>
# include "PlanarInputCameraPreprocess.hpp"
# include "PlanarSpixelPreprocess.hpp"

namespace sibr { 

		/*SIBR_EXP_SPIXELWARP_EXPORT void
			readNew3DPointsSpixels(const std::string& fname,
				std::vector<std::vector<sibr::Vector3f>>& list3DPtLists);

		SIBR_EXP_SPIXELWARP_EXPORT void
			init3DPtList(std::string& path, std::shared_ptr<sibr::SpixelImage> spim, int imgid);*/

		void
			ransac_plane_fitting(sibr::PlanarSPixelPreprocess::Ptr spixel, float THRESHOLD, sibr::Vector3f cam_dir);

		float
			plane_score(std::vector<sibr::Vector3f>& model, std::vector<sibr::Vector3f>& mvs, 
						std::vector<float> w, float THRESHOLD, std::vector<uint> &inliers,
						uint stride = 1);

		void
			planes_estimation(sibr::PlanarInputCameraPreprocess& cam, bool big = false);

		bool
			frustum_test(sibr::PlanarSPixelPreprocess::Ptr spixel, sibr::InputCameraPreprocess &cam);



} /*namespace sibr*/ 

#endif // __SIBR_EXP_SPIXELWARP_PLANAR_ESTIMATION_UTILS_HPP__
