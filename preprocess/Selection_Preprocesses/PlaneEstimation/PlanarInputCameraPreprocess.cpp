/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "PlanarInputCameraPreprocess.hpp"

namespace sibr
{
	PlanarInputCameraPreprocess::~PlanarInputCameraPreprocess() {};

	void PlanarInputCameraPreprocess::loadSuperpixelsAndReconstruction(std::string& path)
	{
		// Load Superpixels
		_planarSpixelImagePreprocess.reset(new sibr::PlanarSpixelImagePreprocess(_id));

		const unsigned int jpegLenghtExtention = 4;
		std::string nameImage(name().begin(), name().end() - jpegLenghtExtention);
		_planarSpixelImagePreprocess->load(path + "/spixelwarp/superpixels/" + nameImage + ".sp");

		setScale(this->w() / _planarSpixelImagePreprocess->w()); //< Scale of processed images

		// Populate with planar spixels
		for (uint i = 0; i < _planarSpixelImagePreprocess->numPlanarSpixelsPreprocess(); i++) {
			/*Spixel::Ptr pspixel_obj(new sibr::PlanarSPixelPreprocess( (_spixelImage->getSpixel(i)) ));
			std::cout << "B " << i << " " << pspixel_obj.get() << std::endl;
			_spixelImage->setSpixel( pspixel_obj ); // update the value of the SPixel
			std::cout << "A " << pspixel_obj->id() << " " << _spixelImage->getSpixelPtr(i).get() << std::endl;*/
			PlanarSPixelPreprocess::Ptr pspixel_obj(
				new PlanarSPixelPreprocess(_planarSpixelImagePreprocess->getPlanarSpixelPreprocess(i)));
			//std::cout << "B " << i << " " << pspixel_obj.get() << std::endl;
			_planarSpixelImagePreprocess->setPlanarSPixelPreprocess(pspixel_obj);
			//std::cout << "A " << pspixel_obj->id() << " " << _planarSpixelImagePreprocess->getSpixelPtr(i).get() << std::endl;

		}

		// Populate planar spixels with extended mvs
		for (uint i = 0; i < numAllPatchPoints(); i++) {

			// Get mvs point
			sibr::MVSPointPreprocess* mvs = mvsPntPreprocess(i);
			sibr::Vector2i pxl = mvs2DPos(i);

			pxl /= getScale();

			// Get superpixel associated to mvs point
			//int x, y;
			int x = pxl[0];
			int y = _planarSpixelImagePreprocess->h() - (pxl[1] + 1);

			if (x >= (int)_planarSpixelImagePreprocess->w() || x < 0 ||
				y >= (int)_planarSpixelImagePreprocess->h() || y < 0) {
				//std::cerr << "[SIBR plane estimation] ERROR pxl out of bounds (" << x << ", " << y << ")" << std::endl;
				continue;
			}

			uint spixelId = _planarSpixelImagePreprocess->getPlanarSpixelPreprocessID(x, y); //< Spixel are sored in img coordinates.

			/*std::shared_ptr<sibr::PlanarSPixelPreprocess> pspixel_obj =
				std::dynamic_pointer_cast<sibr::PlanarSPixelPreprocess>
				(_spixelImage->getSpixelPtr(spixelId));*/

				/*std::shared_ptr<sibr::PlanarSPixelPreprocess> pspixel_obj =
					std::make_shared<sibr::PlanarSPixelPreprocess>(_spixelImage->getSpixel(spixelId));*/

					//sibr::PlanarSPixelPreprocess* obj = _spixelImage->getSpixel(spixelId).get();

					/**sibr::PlanarSPixelPreprocess::Ptr pspixel_obj2 = std::static_pointer_cast<sibr::PlanarSPixelPreprocess>
						(_spixelImage->getSpixelPtr(spixelId));
					std::cout << "A " << spixelId << " " << _spixelImage->getSpixelPtr(spixelId).get() << std::endl;
					**/
			PlanarSPixelPreprocess::Ptr pspixel_obj2 = _planarSpixelImagePreprocess->
				getPlanarSPixelPreprocessPtr(spixelId);

			//sibr::Spixel* pSpixel = _spixelImage->getSpixelPtr(spixelId).get();
			//sibr::PlanarSPixelPreprocess* pspixel_obj = (sibr::PlanarSPixelPreprocess*)pSpixel;
			//sibr::PlanarSPixelPreprocess* pspixel_obj2 = static_cast<sibr::PlanarSPixelPreprocess*> 
				//											(pSpixel);
			/*sibr::PlanarSPixelPreprocess& pspixel_obj = static_cast<sibr::PlanarSPixelPreprocess&>
				(_spixelImage->getSpixel(spixelId));*/
				// Set mvs point to superpixel
			mvs->pos2D(pxl);
			pspixel_obj2->setMVSPointPreprocess(*mvs);
			//std::cout << "there" << std::endl;

			//sebM _spixelImage->setSpixel(pspixel_obj);

			//***************************************************************
			//file_ply << mvs->xyz()[0] <<" "<< mvs->xyz()[1] <<" "<< mvs->xyz()[2] << std::endl;
			//***************************************************************
		}


	}

	void PlanarInputCameraPreprocess::freeSuperpixelsAndReconstruction()
	{
		_planarSpixelImagePreprocess->freeStorage();
	}

	PlanarSPixelPreprocess::Ptr PlanarInputCameraPreprocess::getPlanarSPixelPreprocess(uint id) {
			return _planarSpixelImagePreprocess->getPlanarSPixelPreprocessPtr(id); 
	}

	void PlanarInputCameraPreprocess::setPlanarSPixelPreprocess(PlanarSPixelPreprocess::Ptr planarSpixelPrePtr) {
		_planarSpixelImagePreprocess->setSpixel(planarSpixelPrePtr);
	}

}

