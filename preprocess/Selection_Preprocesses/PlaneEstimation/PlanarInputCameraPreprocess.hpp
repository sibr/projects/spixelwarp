/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _PLANAR_INPUT_CAMERA_PREPROCESS_H_
# define _PLANAR_INPUT_CAMERA_PREPROCESS_H_

# include <core/graphics/Config.hpp>
# include <core/system/Vector.hpp>
# include <core/assets/InputCamera.hpp>
# include <core/scene/BasicIBRScene.hpp>

# include <projects/spixelwarp/renderer/PatchFile.hpp>
# include <projects/spixelwarp/renderer/MVSPoint.hpp>
# include <projects/spixelwarp/renderer/PatchPoint.hpp>
# include <projects/spixelwarp/renderer/MVSPointPreprocess.hpp>
# include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>
# include "PlanarSpixelImagePreprocess.hpp"

namespace sibr 
{


// add additional two fields: scale and spixelImage
// and a few additional functions to access PatchPoints

class PlanarInputCameraPreprocess : public InputCameraPreprocess {

private:
	/** Superpixel segemntation data structure for each input image */
	std::shared_ptr<PlanarSpixelImagePreprocess> _planarSpixelImagePreprocess;

public:
	~PlanarInputCameraPreprocess();
	/** \todo All the ugly stuff from old InputCamera; GD: TODO needs cleanup */

		/** Project into screen space
		* \param pt list of MVS points for this camera
		*/
		Vector3f projectScreen( const Vector3f& pt ) const;

	PlanarInputCameraPreprocess() { }
	PlanarInputCameraPreprocess(const InputCamera& c) {
		_focal = c.focal() ; _k1 = c.k1();  _k2 =  c.k2();
		_w = c.w(), _h = c.h(), _id = c.id() ; _active = c.isActive();
		_name = c.name();
		// Camera info
		_transform = c.transform(), _fov = c.fovy(), _aspect = c.aspect();
		_znear = c.znear(), _zfar = c.zfar(), _matViewProj = c.viewproj();

		forceUpdateViewProj();
	}


	uint	numSPixels(void) { return _planarSpixelImagePreprocess->numPlanarSpixelsPreprocess(); }


	
	/** Load superpixel color information and recostruction points associated. sets value of scale etc*/

	void loadSuperpixelsAndReconstruction( std::string& path )  ;

	void freeSuperpixelsAndReconstruction();

	PlanarSPixelPreprocess::Ptr getPlanarSPixelPreprocess(uint id);

	void setPlanarSPixelPreprocess(PlanarSPixelPreprocess::Ptr planarSpixelPrePtr);
	//PlanarSPixelPreprocess* getPlanarSPixel(uint id) { return (PlanarSPixelPreprocess*)_spixelImage->sPixelDIR(id); }
	/*PlanarSPixelPreprocess::Ptr getPlanarSPixel(uint id) { 
		//return (PlanarSPixelPreprocess*)_spixelImage->getSpixelPtr(id).get(); 
		return std::make_shared<PlanarSPixelPreprocess>(
			_planarSpixelImagePreprocess->getPlanarSpixelPreprocess (id));
	};

	uint getNumSpixelsInImage() {
		return _planarSpixelImagePreprocess->numPlanarSpixelsPreprocess();
	}

	void setPlanarSPixel(PlanarSPixelPreprocess::Ptr planarSpixelPrePtr) {
		_planarSpixelImagePreprocess->setSpixel(planarSpixelPrePtr);
	}*/

	/*PlanarSPixelPreprocess::Ptr getPlanarSPixel(uint id) { 
		return std::dynamic_pointer_cast<sibr::PlanarSPixelPreprocess::Ptr>_
			(spixelImage->getSpixelPtr(id)); }*/


	//MVSPointPreprocess* mvsPntPreprocess(uint i) { MVSPointPreprocess* pt = (MVSPointPreprocess*) _mvsPnts[i].get(); return pt; }


	/** Load Patch information */
	// add PMVS points to InputCameras
	static	void loadPatchMVSPoints(BasicIBRScene::Ptr scene , PatchFile& patchFile, std::vector<PlanarInputCameraPreprocess>& camlist) 
	{
		for(int pointID=0; pointID<patchFile.pointCount(); pointID++ ) {
			sibr::PatchPoint p = patchFile.point(pointID);
			for(const uint camIndex : p.visibleInImages() )  {
				std::shared_ptr<MVSPointPreprocess> ep(new MVSPointPreprocess(p.position()));
				ep->patchPnt(p);
				camlist[camIndex].addMVSPntPreprocess(ep);
			}
		}
	}

	/** Load Patch information per camera*/
	// add PMVS points to given input camera range to avoid memory saturation; free afterwards

	static	void loadPatchMVSPoints4CameraRange(BasicIBRScene::Ptr scene , PatchFile& patchFile, 
		std::vector<PlanarInputCameraPreprocess>& camlist, int starti, int endi) 
	{
		for(int pointID=0; pointID<patchFile.pointCount(); pointID++ ) {
			sibr::PatchPoint p = patchFile.point(pointID);
			for(const uint camIndex : p.visibleInImages() )  {
				if( camIndex >= (uint)starti && camIndex <= (uint)endi ) {
				std::shared_ptr<MVSPointPreprocess> ep(new MVSPointPreprocess(p.position()));
					ep->patchPnt(p);
					camlist[camIndex].addMVSPntPreprocess(ep);
				}
			}
		}
	}

};



} // namespace sibr

#endif
