/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


/**
 * \file PlanarSpixel.cpp
 *
 *  Planar Superpixel data structure.
 */

#include <cstdlib>

#include "PlanarSpixel.h"


namespace sibr
{

// -----------------------------------------------------------------------
/*
std::vector<uint> PlanarSPixel::getVisibility( std::vector<std::shared_ptr<InputCamera> > icams, Vector3f curr_pos, Vector3f curr_dir ){
	
	uint NUM_VIEWS = 2;
	
	std::vector<uint> vis_cam_id; 	
	bool filled;
	uint count_views = 0;

	std::vector<uint> sorted_cams = sort_closest_views( icams, curr_pos, curr_dir  ) ;
			
	for(uint k=1; k<sorted_cams.size() && count_views<NUM_VIEWS; k++){ // k starts in 1 -> dont considet its own camera
		
		uint curr_cam_id = sorted_cams[k];		// current camera id
		bool found_loop = false; 

		// double loop to find sorted_cams[k]
		
		for(uint i=0; i<_visib.size(); i++){			
			std::vector<uint> curr_points_views = _visib[i];
			for(uint j=0; j<_visib[i].size(); j++){
				if (curr_cam_id == curr_points_views[j]){
					found_loop = true;
					break;
				}
			}
			if( found_loop ) break;
		}

		// If it was found, add to visible cameras
		if( found_loop ){ 
			vis_cam_id.push_back( curr_cam_id );
			count_views++;
		}			
	}
	
	// if is not visible in other cameras.... give the closest.
	if ( vis_cam_id.size() == 0 ){
		for(uint k=1; k<sorted_cams.size() && count_views<NUM_VIEWS; k++){
			uint curr_cam_id = sorted_cams[k];		// current camera id
			vis_cam_id.push_back( curr_cam_id );
			count_views++;
		}
	}

	return vis_cam_id;
}
*/
// -----------------------------------------------------------------------


bool PlanarSpixel::is_patch_visible( uint i, uint cam_id )
{
	bool is_visible = false;
	std::vector<uint> current_visib = visib(i);

	for( std::vector<uint>::iterator it = current_visib.begin(); it!= current_visib.end(); ++it )
	{
		if (cam_id== *it)
		{
			is_visible = true;
			break;
		}
	}
	return is_visible;
}


#ifdef NOTDEF

#endif
// -----------------------------------------------------------------------
/*
std::vector<uint> PlanarSPixel::sort_closest_views( std::vector<std::shared_ptr<InputCamera> > icams, Vector3f pos, Vector3f dir  ) {
    std::vector<uint> warped_img_id;

    std::multimap<float,uint> dist;                 // distance wise closest input cameras
	
    for (uint i=0; i<icams.size(); i++) 
	{
        if (icams[i]->getActive()) 
		{
		//if ( _scene. // icams[i]->getActive()) {
            float d = distance(icams[i]->pos(), pos);
            float a = dot(icams[i]->dir(), dir );
            if (a > 0.707)                          // cameras with 45 degrees
                dist.insert(std::make_pair(d,i));        // sort distances in increasing order
        }
    }

    std::multimap<float,uint>::const_iterator d_it(dist.begin());
    for (uint i=0; d_it!=dist.end(); d_it++ ) {
        warped_img_id.push_back(d_it->second);
    }

    //SIBR_ASSERT(warped_img_id.size() <= NUM_WARPED);
    return warped_img_id;
}
}
*/
// -----------------------------------------------------------------------
	
// -----------------------------------------------------------------------
}
