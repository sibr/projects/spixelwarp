/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


/**
 * \file PlanarSpixel.h
 *
 *  Planar Superpixel data structure.
 */

#ifndef _PLANAR_SUPER_PIXEL_H_
#define _PLANAR_SUPER_PIXEL_H_

#include <projects/spixelwarp/renderer/PatchPoint.hpp>
#include <projects/spixelwarp/renderer/Spixel.hpp>

//#include <preprocess/utils/spixel.h>
//#include <sibr/assets/PatchPoint.hpp>

namespace sibr
{

	/**
	 * Superpixel data structure. Contains the list of pixels contained within
	 * the superpixel as well as superpixels of other images
	 * which are marked as "neighbors" i.e. have common MVS points
	 * and thus belong to the same 3D object.
	 */
	class PlanarSpixel : public Spixel {

	public:
		typedef std::shared_ptr<PlanarSpixel>		Ptr;
	private:

		/** normal of the plane  fitted to a spixel */
		Vector3f _normal;

		/** distance from origin of the plane fitted to a spixel */
		float _d;

		/** 3D Point ->depth for FPLAN */
		Vector3f _point_p;

		/** Additional labels of a superpixel. */
		bool _active_PLAN;
		bool _active_FPLAN;

		///TODO: this should be type enum
		uint _finalLabel; // 1=SELECTION / 2=PLAN / 3=FPLAN

		//< TODO: integrate inside Extended_MVS.h
		/** Structure to store pair of 3D points (Vector3f) and their projeciton (sibr::Vector2i). */
		std::vector< std::pair<Vector3f, sibr::Vector2i> > _mvsPixel;
		std::vector< float >			_conf;
		std::vector<std::vector<uint>>	_visib;
		//-----------------------------------------------


	public:
		~PlanarSpixel() {
			_conf.clear();
			_mvsPixel.clear();
			_visib.clear();
		}

		virtual void freeStorage()
		{
			_conf.clear();
			_mvsPixel.clear();
			_visib.clear();
			Spixel::freeStorage();
		}

		//PlanarSpixel( Spixel spobj ) : Spixel( spobj.id(), spobj.w(), spobj.h() ){ 
		PlanarSpixel(Spixel spobj) : Spixel(spobj),
			_normal{ 0.f,0.f,0.f },
			_d(0.f),
			_point_p{ 0.f,0.f,0.f },
			_active_PLAN(false),
			_active_FPLAN(false),
			_finalLabel(1.f)
		{
			/*_active		= spobj.getActive();
			_gid		= spobj.getGID();
			_medDepth	= spobj.getMedDepth();
			_pxl		= spobj.getPxls();
			_neighbors	= spobj.getNeigh();*/

			// Default values

			/*_normal		= Vector3f(0,0,0);
			_d			= 0;
			_point_p	= Vector3f(0,0,0);*/

			/*
			_active_PLAN	= true;
			_active_FPLAN	= true;
			*/
			/*	_active_PLAN	= false;
				_active_FPLAN	= false;
				_finalLabel = 1;*/
				//	_mvsPoints.clear();
		}
		// -----------------------------------------------------------------------
		bool  getActive_PLAN(void)   const { return _active_PLAN; }
		bool  getActive_FPLAN(void)   const { return _active_FPLAN; }
		uint  getFinalLabel(void)   const { return _finalLabel; }
		void setActive_PLAN(bool  a) { _active_PLAN = a; }
		void setActive_FPLAN(bool  a) { _active_FPLAN = a; }
		void setFinalLabel(uint label) { _finalLabel = label; }
		Vector3f	  getNormal(void)   const { return _normal; }
		float getD(void)	  const { return _d; }
		void setNormal(Vector3f normal) { _normal = normal; }
		void setD(float distance2origin) { _d = distance2origin; }

		//	PlanarSPixel( SPixel spixel );
		void planemesh(std::vector<sibr::Vector2d>& t, uint imgWidth, uint imgHeight) const {
			// actual range should be (-1,1), so it is safe
			// to use 2 -- actually we should DBL_MAX and DBL_MIN
			double xmin = 2.0, xmax = -2.0;
			double ymin = 2.0, ymax = -2.0;

			double halfPixelW01 = double(0.5 * 1.0 / imgWidth);
			double halfPixelH01 = double(0.5 * 1.0 / imgHeight);


			// calculate the x and y limits
			//for (uint s=0; s<_pxl.size(); s++) {
			for (uint s = 0; s < getPixels().size(); s++) {
				//double xx = double(2*_pxl[s][0])/_w - 1.0;
				//double yy = double(2*_pxl[s][1])/_h - 1.0;
				double xx = (double(getPixels()[s][0]) / double(imgWidth - 1))*2.0 - 1.0 + halfPixelW01;
				double yy = (double(getPixels()[s][1]) / double(imgHeight - 1))*2.0 - 1.0 + halfPixelH01;
				xmin = std::min(xmin, xx);
				xmax = std::max(xmax, xx);
				ymin = std::min(ymin, yy);
				ymax = std::max(ymax, yy);
			}
			//xmin -= .01; xmax += .01;      // displace the extremeties slightly outwards
			//ymax += .01; ymin -= .01;      // to avoid depth samples lying on boundary
			xmin += halfPixelW01; xmax -= halfPixelW01;      // displace the extremeties slightly outwards
			ymax -= halfPixelH01; ymin += halfPixelH01;      // to avoid depth samples lying on boundary

			sibr::Vector2d a = sibr::Vector2d(xmin, ymin);
			sibr::Vector2d b = sibr::Vector2d(xmin, ymax);
			sibr::Vector2d c = sibr::Vector2d(xmax, ymax);
			sibr::Vector2d d = sibr::Vector2d(xmax, ymin);
			t.push_back(a); t.push_back(b); t.push_back(c); t.push_back(d);

			//// some random warp mesh vertices in the interior of the superpixel
			//for (uint i=0; i<NUM_VERTICES_INSIDE; i++) {
			//    double u = double(rand()) / double(RAND_MAX);
			//    double v = double(rand()) / double(RAND_MAX);
			//    sibr::Vector2d p = u*v*a + u*(1.0-v)*b + (1.0-u)*v*c + (1.0-u)*(1.0-v)*c;
			//    t.push_back(p);
			//}
		}


		// removed all MVS STUFF

		const std::vector< std::pair<Vector3f, sibr::Vector2i> >& mvsPixel(void)  const { return _mvsPixel; }

		void addMVSPnt(sibr::Vector3f v, sibr::Vector2i p)
		{
			_mvsPixel.push_back(std::make_pair(v, p));
		}

		bool is_patch_visible(uint i, uint cam_id);

		std::vector<uint> visib(uint i)	const { return _visib[i]; }

		void visib(std::vector<uint> list_visib)
		{
			_visib.push_back(list_visib);
		}

#ifdef NOTDEF // removed all MVS STUFF
		sibr::Vector2i    mvs2DPos(uint i)							const { return _mvsPixel[i].second; }
		Vector3f    mvs3DPos(uint i)							const { return _mvsPixel[i].first; }
		std::vector< std::pair<Vector3f, sibr::Vector2i> > mvsPixel(void)  const { return _mvsPixel; }

		//-----------------------------------------------
		/**
			0 <= p.x < w_halfsize
			0 <= p.y < h_halfsize
		*/
		void   addMVSPnt(Vector3f v, sibr::Vector2i p, float conf, std::vector<uint> list_visib)
		{
			_mvsPixel.push_back(std::make_pair(v, p));
			_conf.push_back(conf);
			_visib.push_back(list_visib);
		}

		//-----------------------------------------------

		float conf(uint i)		const { return _conf[i]; }
		std::vector<uint> visib(uint i)	const { return _visib[i]; }

		std::vector<uint> getVisibility(std::vector<std::shared_ptr<InputCamera> > icams, Vector3f curr_pos, Vector3f curr_dir);

		/** Check if the mvs point i is visible in camera cam_id
		*/
		sibr::PlanarSPixelPreprocess* pspixel_obj = (sibr::PlanarSPixelPreprocess*)pSpixel;
		bool is_patch_visible(uint i, uint cam_id);
#endif

		//-----------------------------------------------

		/** Select a subset to images to warp according to novel view parameters */
		//std::vector<uint> sort_closest_views( std::vector<std::shared_ptr<InputCamera> > icams ,  Vector3f curr_pos, Vector3f curr_dir)  ; //< TODO: review this function

		//-----------------------------------------------
	/*
		void  planemesh(std::vector<sibr::Vector2d>& mesh) const; //< TODO: review this function

		//-----------------------------------------------

		Vector3f	  getNormal(void) const;
		void  setNormal(Vector3f normal);
		float getD(void) const;
		void  setD(float distance2origin);

		void  setActive_PLAN(bool a);
		bool  getActive_PLAN(void) const;

		void  setActive_FPLAN(bool a);
		bool  getActive_FPLAN(void) const;

		void setFinalLabel(uint label);
		uint getFinalLabel(void) const;

	*/
		Vector3f getPoint_p(void) { return _point_p; };
		void setPoint_p(Vector3f p) { _point_p = p; };

		// Functions to pass _pxl and _neighbors atributes from an PlanarSPixel to a PlanarSPixel object

		/*** WARNING ***
		Seb: I saw this in plane_estimation/fitting.cpp

		vector<sibr::Vector2i>			 pxls	= spixel->getPxls();
		// superpixel image was inverted in "y" axis. (for rendering)
		int h = spixel->h();
		for(uint i=0; i<pxls.size(); i++)
			pxls[i][1] = h - (1 + pxls[i][1]);

		When using this function, we expect pixels in the right order and
		not having to call this invert for-loop each time.

		TODO: Returned pixels should already be inverted
		**/

		//-----------------------------------------------

	};


}

#endif // _PLANAR_SUPER_PIXEL_H_

