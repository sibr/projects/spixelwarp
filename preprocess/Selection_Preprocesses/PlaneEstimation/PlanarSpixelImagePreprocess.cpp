/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <algorithm>

#include <core/graphics/Image.hpp>
#include "PlanarSpixelImagePreprocess.hpp"

namespace sibr {

	void PlanarSpixelImagePreprocess::setPlanarSPixelPreprocess(
			const PlanarSPixelPreprocess::Ptr& spobj) {
		SIBR_ASSERT(spobj && spobj->id() < getSpixels().size());
		_planarSPixelPreprocess[spobj->id()] = spobj;
	}


	const PlanarSPixelPreprocess::Ptr&
		PlanarSpixelImagePreprocess::getPlanarSPixelPreprocessPtr(uint id) {
		SIBR_ASSERT(id < getSpixels().size());
		return _planarSPixelPreprocess[id];
	}

	const std::vector<PlanarSPixelPreprocess::Ptr> &
		PlanarSpixelImagePreprocess::getPlanarSpixelsPreprocess() const {
		return _planarSPixelPreprocess;
	}

	uint	PlanarSpixelImagePreprocess::getPlanarSpixelPreprocessID( uint x, uint y ) const {
		return getSpixelID(x,y);
	}


	bool	PlanarSpixelImagePreprocess::load(const std::string& filename) {
		bool out = SpixelImage::load(filename);
		_planarSPixelPreprocess.resize(getSpixels().size());
		for (uint i = 0; i < _planarSPixelPreprocess.size(); i++)           // populate the superpixels
			_planarSPixelPreprocess[i].reset(new PlanarSPixelPreprocess(
				*(getSpixels().at(i))
			));

		return out;
	}


	PlanarSpixelImagePreprocess::PlanarSpixelImagePreprocess(uint imageID) :
		SpixelImage(imageID)
	{
	}

	PlanarSpixelImagePreprocess::~PlanarSpixelImagePreprocess() {};


	void PlanarSpixelImagePreprocess::freeStorage()
	{
		//	std::cerr <<"Deleting spixel " << std::endl;
		for (uint i = 0; i < _planarSPixelPreprocess.size(); i++)            // delete the superpixels
			_planarSPixelPreprocess[i].get()->freeStorage();

		_planarSPixelPreprocess.clear();
	}

	uint	PlanarSpixelImagePreprocess::numPlanarSpixelsPreprocess(void) const {
		return (uint)_planarSPixelPreprocess.size();
	}

	const PlanarSPixelPreprocess& PlanarSpixelImagePreprocess::getPlanarSpixelPreprocess(uint i) const
	{
		SIBR_ASSERT(i < getSpixels().size());
		return *(_planarSPixelPreprocess[i].get());
	}

	PlanarSPixelPreprocess&		PlanarSpixelImagePreprocess::getPlanarSpixelPreprocess(uint i) {
		SIBR_ASSERT(i < _planarSPixelPreprocess.size());
		return *(_planarSPixelPreprocess[i].get());
	}
} /*namespace sibr*/
