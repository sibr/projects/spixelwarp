/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_EXP_SPIXELWARP_PLANARSPIXELIMAGEPREPROCESS_HPP__
# define __SIBR_EXP_SPIXELWARP_PLANARSPIXELIMAGEPREPROCESS_HPP__

# include <vector>
# include <memory>
# include <string>

# include "core/system/Array2d.hpp"
# include <core/graphics/Texture.hpp>

# include <projects/spixelwarp/renderer/SpixelImage.hpp>
#include "PlanarSpixelPreprocess.hpp"

namespace sibr {
	/**
	* Superpixel segmentation of an input image.
	* Contains the superpixel id of each pixel of the input image.
	*/
	class PlanarSpixelImagePreprocess : public SpixelImage
	{
	public:
		typedef std::shared_ptr<PlanarSpixelImagePreprocess>	Ptr;

	public:

		/** Constructor
		* \param imageID ID of input image - same as ID of superpixel segmentation
		*/
		PlanarSpixelImagePreprocess(uint imageID);

		~PlanarSpixelImagePreprocess();

		/** Load spixel from a file and returns TRUE if successful. */
		bool		load( const std::string& filename ) ;

		/** Return the total number of superpixel within the superpixel segmentation
		* of the input image */
		uint		numPlanarSpixelsPreprocess(void) const;

		/** Overwrites the address of a Spixel object (using its Spixel::GID) */
		void				setPlanarSPixelPreprocess(const PlanarSPixelPreprocess::Ptr& spobj);

		/** Retrieve a superpixel of the segmentation by its ID  */
		const PlanarSPixelPreprocess&	getPlanarSpixelPreprocess(uint id) const;
		PlanarSPixelPreprocess&			getPlanarSpixelPreprocess(uint id);

		/** Get the superpixel id of a pixel (x,y) in the superpixel image assumging Y flipped image*/

		/** Retreives the address of a Spixel object */
		const PlanarSPixelPreprocess::Ptr&	getPlanarSPixelPreprocessPtr(uint id);

		uint	getPlanarSpixelPreprocessID( uint x, uint y ) const;

		/** get list of superpixels in the superpixel segmentation  */
		const std::vector<PlanarSPixelPreprocess::Ptr> & getPlanarSpixelsPreprocess() const;


		/** free all the allocated superpixels
		*/
		void freeStorage();

	private:


		/** List of superpixels in the superpixel segmentation */
		std::vector<PlanarSPixelPreprocess::Ptr> _planarSPixelPreprocess;
	};



} /*namespace sibr*/

#endif // __SIBR_EXP_SPIXELWARP_PLANARSPIXELIMAGEPREPROCESS_HPP__