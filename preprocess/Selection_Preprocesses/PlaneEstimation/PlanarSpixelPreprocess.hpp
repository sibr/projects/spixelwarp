/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _PLANAR_SUPER_PIXEL_PREPROCESS_H_
#define _PLANAR_SUPER_PIXEL_PREPROCESS_H_
/**
 * \file PlanarSpixelPreprocess.hpp
 *
 *  Planar Superpixel data structure extended for input preprocessing (ie with extended MVSpoints)
 */
//# include "Config.hpp"
# include <projects/spixelwarp/renderer/MVSPointPreprocess.hpp>
# include "PlanarSpixel.h"

namespace sibr {

class PlanarSPixelPreprocess : public PlanarSpixel {

public:
		typedef std::shared_ptr<PlanarSPixelPreprocess>		Ptr;

private:
		std::vector<MVSPointPreprocess> _mvsPoints;
	
public:
	PlanarSPixelPreprocess(Spixel spixel) : PlanarSpixel(spixel) {} ;

	~PlanarSPixelPreprocess() { _mvsPoints.clear(); }

	virtual void freeStorage() { _mvsPoints.clear(); PlanarSpixel::freeStorage(); }  

	/** NUmber of extended mvs points inside the superpixel. */
	uint numMVSPointsPreprocess(void){ return (uint)_mvsPoints.size(); }

	/** Return extended mvs point. */
	MVSPointPreprocess* getMVSPointPreprocess(uint i){ return &_mvsPoints[i]; }

	/** Return all mvs points. */
	const std::vector<MVSPointPreprocess>& getAllMVSPointsPreprocess(void){ return _mvsPoints; }

	/** Add extended mvs point. */
	void setMVSPointPreprocess(MVSPointPreprocess& mvs){ _mvsPoints.push_back( mvs ); }
	
	/** Return all mvs points. */
	void resetMVSPointsPreprocess(void){ _mvsPoints.clear(); }
};

}

#endif
