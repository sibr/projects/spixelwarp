/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __PLANE_H__
#define __PLANE_H__
#include <core/graphics/Config.hpp>
#include <core/system/Vector.hpp>

/*
Plane.h
Written by Matthew Fisher

A standard 3D plane (space plane;) i. e. the surface defined by a*x + b*y + c*z + d = 0
*/


struct Plane
{
    //
    // Initalization
    //
    Plane();
    Plane(const Plane &P);
    Plane(float _a, float _b, float _c, float _d);
    Plane(const sibr::Vector3f &NormalizedNormal, float _d);

    //
    // Static constructors
    //
    static Plane ConstructFromPointNormal(const sibr::Vector3f &Pt, const sibr::Vector3f &Normal);                //loads the plane from a point on the surface and a normal vector
    static Plane ConstructFromPointVectors(const sibr::Vector3f &Pt, const sibr::Vector3f &V1, const sibr::Vector3f &V2);    //loads the plane from a point on the surface and two vectors in the plane
    static Plane ConstructFromPoints(const sibr::Vector3f &V1, const sibr::Vector3f &V2, const sibr::Vector3f &V3);        //loads the plane from 3 points on the surface

    //
    // Math functions
    //
    float UnsignedDistance(const sibr::Vector3f &Pt) const;
    float SignedDistance(const sibr::Vector3f &Pt) const;
    bool FitToPoints(const std::vector<sibr::Vector3f> &Points, std::vector<float> &weights, float &ResidualError);
    bool FitToPoints(const std::vector<sibr::Vector4f> &Points, sibr::Vector3f &Basis1, sibr::Vector3f &Basis2, float &NormalEigenvalue, float &ResidualError);
    sibr::Vector3f ClosestPoint(const sibr::Vector3f &Point);

    //
    // Line intersection
    //
    sibr::Vector3f IntersectLine(const sibr::Vector3f &V1, const sibr::Vector3f &V2) const;        //determines the intersect of the line defined by the points V1 and V2 with the plane.
                                                            //Returns the point of intersection.  Origin is returned if no intersection exists.
    sibr::Vector3f IntersectLine(const sibr::Vector3f &V1, const sibr::Vector3f &V2, bool &Hit) const;    //determines the intersect of the line defined by the points V1 and V2 with the plane.
                                                                    //If there is no intersection, Hit will be false.
    float IntersectLineRatio(const sibr::Vector3f &V1, const sibr::Vector3f &V2);    //Paramaterize the line with the variable t such that t = 0 is V1 and t = 1 is V2.
                                                                //returns the t for this line that lies on this plane.
    //sibr::Vector3f IntersectLine(const Line3D &Line) const;

    static float Dot(const Plane &P, const sibr::Vector4f &V);            //dot product of a plane and a 4D vector
    static float DotCoord(const Plane &P, const sibr::Vector3f &V);        //dot product of a plane and a 3D coordinate
    static float DotNormal(const Plane &P, const sibr::Vector3f &V);    //dot product of a plane and a 3D normal

    //
    // Normalization
    //
    Plane Normalize();

    //
    // Accessors
    //
    __forceinline sibr::Vector3f Normal() const
    {
        return sibr::Vector3f(a, b, c);
    }

    __forceinline void Flip()//__forceinline Plane Flip()
    {
		this->a = -a;
        this->b = -b;
        this->c = -c;
        this->d = -d;
        //return Result;
    }

    //static bool PlanePlaneIntersection(const Plane &P1, const Plane &P2, Line3D &L);

#ifdef USE_D3D
    operator D3DXPLANE();
#endif

    float a, b, c, d;        //the (a, b, c, d) in a*x + b*y + c*z + d = 0.
	//sibr::Vector3f centroid;
	float centroid_x;
	float centroid_y;
	float centroid_z;
};

#endif //__PLANE_H__
