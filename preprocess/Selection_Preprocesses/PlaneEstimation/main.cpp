/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


/*#include <sibr/view/IBRView.hpp>
#include <sibr/view/IBRScene.hpp>
#include <sibr/view/IBRArgs.hpp>
#include <sibr/view/Resources.hpp>

#include <sibr/system/ProgramArg.hpp>
#include <sibr/assets/PatchFile.hpp>
#include <sibr/assets/MVSPoint.hpp>
#include <sibr/assets/PatchPoint.hpp>
#include <sibr/graphics/Config.hpp>
#include <sibr/graphics/Camera.hpp>
#include <sibr/graphics/Image.hpp>

#include <InputCameraPreprocess.hpp>
#include <PlanarSpixelPreprocess.hpp>


#include "fitting.cpp"*/

#pragma once
#include <fstream>
#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>
#include <core/scene/ParseData.hpp>
#include <projects/spixelwarp/renderer/SpixelWarpScene.hpp>
//#include <projects/spixelwarp/renderer/SelectionView.hpp>
#include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>
#include <projects/spixelwarp/renderer/Utils.hpp>
#include <projects/spixelwarp/renderer/PlanarRenderer.hpp>
#include "PlanarSpixelPreprocess.hpp"
#include "PlanarEstimationUtils.hpp"
//#include <projects/spixelwarp/apps/PlaneEstimation_app/PlanarSpixelImagePreprocess.hpp>

#define PROGRAM_NAME "sibr_plane_estimation_app"
using namespace sibr;

const char* usage = ""
"Usage: " PROGRAM_NAME " -path <dataset-path>"    	                                "\n"
;


/*
 
 // -----------------------------------------------------------------------
 <CLASS>
 
 Description:
 
...
 
 // -----------------------------------------------------------------------
 <MAIN FUNCTION>
 
 Create an IBRScene
 load Superpxiels save_sp_plane
 Load Patch file
 Load cameras
 Projects PMVS points to cameras
 
 For all cameras
 | Estimate planes for each superpixel (planes_estimation)
 | Save plane equations for each camera (save_sp_plane)
 End
 
 // -----------------------------------------------------------------------
 <HELPER FUNCTIONS>
 
 planes_estimation
 
 save_sp_plane
 
 
 // -----------------------------------------------------------------------
 
 */

// -----------------------------------------------------------------------

bool save_sp_plane( std::string image_dir, sibr::PlanarInputCameraPreprocess& cam ){
	
	constexpr uint numberOfCharForNumber = 8;
	const std::string numCam(cam.name().begin(), cam.name().begin() + numberOfCharForNumber);
	std::string spPlane_file_name = image_dir + "planes/" + numCam + ".sp_plane";
	std::ofstream spPlane_file(spPlane_file_name.c_str());
	std::cout << "Writing " + numCam + ".sp_plane" << std::endl;


	if (spPlane_file.is_open()) {
		for(uint i=0; i<cam.numSPixels(); i++){
			sibr::PlanarSPixelPreprocess::Ptr spixel = cam.getPlanarSPixelPreprocess(i);
			sibr::Vector3f plane_n = spixel->getNormal();			
			spPlane_file << i << " " << spixel->getActive_PLAN() << " " 
				"(" << plane_n.x() << ',' << plane_n.y() << ',' << plane_n.z() << ")"
			   	<< " " << spixel->getD() << std::endl;
		}
	}

	spPlane_file.close();

	return true;
}

// -----------------------------------------------------------------------

float												min_mvs_thld = 0.8f;
std::string											image_dir;
std::vector<std::shared_ptr<sibr::PlanarInputCameraPreprocess>>	cameras;
std::vector<std::shared_ptr<sibr::ImageRGB32F>>			input_imgs;

int main(int ac, char **av) 
{
//    parseArgs(argc, argv, min_mvs_thld, image_dir);
	// No graphics -- dont complain about args

	// Parse Commad-line Args
	CommandLineArgs::parseMainArgs(ac, av);
	SwarpAppArgs myArgs;

	// rendering size
	uint rendering_width = myArgs.rendering_size.get()[0];
	uint rendering_height = myArgs.rendering_size.get()[1];

	sibr::Window			window(100, 100, PROGRAM_NAME, myArgs);
	sibr::BasicIBRScene::Ptr scene ( new sibr::BasicIBRScene(myArgs));
	
	
	bool save_ply = false;

	std::cout << min_mvs_thld << std::endl;
	image_dir = myArgs.dataset_path.get();
	
	

    //loadBundleFile( image_dir, cameras );								//< Load cameras	
	//load_input_images( image_dir, cameras, input_imgs);					//< Load input images
	//input_imgs.resize( cameras.size() );
    //loadPMVSPoints( image_dir, min_mvs_thld, cameras);					//< Load reconstructions points

	// load PatchFile
	sibr::PatchFile patchFile;
	std::string patchFilePath = std::string(myArgs.dataset_path) + std::string("/spixelwarp") ;
	std::string fname = patchFile.search(patchFilePath);
	patchFile.load(fname);

	std::vector<sibr::PlanarInputCameraPreprocess> camlist;
	camlist.resize(scene->cameras()->inputCameras().size());

	int i = 0;
    for (int i=0;i<scene->cameras()->inputCameras().size(); i++) 
		camlist[i] = *scene->cameras()->inputCameras()[i];

	
	if( !sibr::fileExists( image_dir.c_str() + sibr::sprint("/spixelwarp/planes") ) )
		sibr::makeDirectory( image_dir.c_str() + sibr::sprint("/spixelwarp/planes") );

	/** For each camera, estimate planes their planes. */
	//if( !scene->args()._isBig ) {
	if( true ) {
		// add PMVS points to InputCameras
		sibr::PlanarInputCameraPreprocess::loadPatchMVSPoints(scene, patchFile, camlist);
			
		for (uint i = 0; i < scene->cameras()->inputCameras().size(); i++) {
			if (scene->cameras()->inputCameras()[i]->isActive()) {
				//std::string path = scene->args().image_path; 
				std::string path = myArgs.dataset_path;

				camlist[i].loadSuperpixelsAndReconstruction(image_dir); 	/** For each camera, load superpixels color info and reconstruction info. */
				planes_estimation(camlist[i]);
				save_sp_plane(image_dir + "/spixelwarp/", camlist[i]);
				camlist[i].freeSuperpixelsAndReconstruction();
			}
		}
	}
	else {
		

		int i = 0;
#define	STRIDE 	10 // variable with scene and memory availability should be tuned automatically
		do {
				int j = i+STRIDE; // batch 
				if( j >= camlist.size()) 
					j = (int)camlist.size()-1; 

				// check if at least one camera active for this batch
				bool camActive = false;
				for(int k=i;k<j;k++ ) 
					if( camlist[k].isActive() ) {
						camActive = true;
						break;
					}

				if( camActive ) {
					sibr::PlanarInputCameraPreprocess::loadPatchMVSPoints4CameraRange(scene, patchFile, camlist, i, j);
///#pragma omp parallel for
					for(int k=i;k<j;k++ ) {
						if( camlist[k].isActive() ) {
							std::cerr << "Process Camera " << k << std::endl;
							camlist[k].loadSuperpixelsAndReconstruction( image_dir ); 	/** For each camera, load superpixels color info and reconstruction info. */
							planes_estimation(camlist[k], true);
							save_sp_plane(image_dir + "/spixelwarp/", camlist[k]);
							camlist[k].freeSuperpixelsAndReconstruction( ); 	
						}
						else
							std::cerr << "Cam " << k << " inactive " << std::endl;
						camlist[k].deleteMVSPntPreprocess();
					}
				}
				i = i+STRIDE; // may go over to stop loop
		}
		while ( i < camlist.size() ) ;
	}
	    
    return EXIT_SUCCESS;
}
