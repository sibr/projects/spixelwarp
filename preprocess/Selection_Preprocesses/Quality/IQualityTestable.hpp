/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_IQUALITYTESTABLE_HPP__
# define __SIBR_IQUALITYTESTABLE_HPP__

#include <core/scene/BasicIBRScene.hpp>
#include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>

#define SIGMA_geom			100
#define SIGMA_phot			1

namespace sibr
{
	class IQualityTestable
	{
	public:
		typedef	std::shared_ptr<IQualityTestable>	Ptr;

	public:

		/**
		*	Virtual (unimplemented) method which should compute the superpixel geometric error.
		*
		*	@param truthImageID The ID of the image to use as reference.
		*	@param testedImageID The ID of the image whose superpixels are to be evaluated.
		*	@return A list of geometric errors per spixel.
		*/
		virtual std::vector<double>	spixelGeommetricErr( uint truthImageID, uint testedImageID ) = 0;

		/**
		*	Virtual (unimplemented) method which should compute the photometric error of the
		*	IBR superpixel algorithm.
		*	@param truthImageID The ID of the image to use as reference.
		*	@param testedImageID The ID of the image whose superpixels are to be evaluated.
		*	@return A list of geometric errors per spixel.
		*/
		virtual std::vector<double>	spixelPhotometricErr( uint truthImageID, uint testedImageID ) = 0;

		/**
		*	Should return a unique integer to use as Label.
		*	@return Class label number.
		*/
		virtual uint	getLabelID( void ) const = 0;

		/**
		*	Returns a multiplier that is factored into the variance for the gaussian
		*	while computing bayesian probabilities.
		*
		*	@return Variance multiplier.
		*/
		virtual double	getSigmaModifier(void) const = 0;

	protected: 

		std::vector<InputCameraPreprocess>		_mvsCams;//todo: verify compatibility


	};

} // namespace sibr

#endif // __SIBR_IQUALITYTESTABLE_HPP__
