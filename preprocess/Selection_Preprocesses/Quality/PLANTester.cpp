/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>
#include <thread>
#include <future>

#include <projects/spixelwarp/renderer/PatchFile.hpp>
#include <core/assets/Resources.hpp>
#include "Utils.hpp"
#include "PLANTester.hpp"

#include <projects/spixelwarp/renderer/Utils.hpp>
#include <projects/spixelwarp/renderer/ImagePlanes.hpp>
#include <projects/spixelwarp/renderer/SpixelImage.hpp>
#include <projects/spixelwarp/renderer/ImageWarper.hpp>

namespace sibr
{
	static double	usedComputeMSEFunc(const sibr::ImageFloat1& img, const std::array<float, 4>& bbox)
	{
		return computeMSE(img, &bbox);
	}
	PLANTester::PLANTester(	const BasicIBRScene::Ptr& scene, const PatchFile& patchfile,
							const std::string& dataSetPath)
	{
		_scene = scene;

		SIBR_ASSERT(_scene != nullptr);

		_spixelImage = loadSuperpixels(*_scene, dataSetPath + "/spixelwarp/");

		//todo : verify compatibility
		_mvsCams.resize(scene->cameras()->inputCameras().size());
		for (int i = 0; i < (int)scene->cameras()->inputCameras().size(); ++i) {
			_mvsCams[i] = *scene->cameras()->inputCameras()[i];
		}

		loadMVSPoints(*_scene, _spixelImage, dataSetPath + "/spixelwarp/", _mvsCams);

		loadAllSpixelMVSPoints(scene, patchfile, _spixelImage, _mvsPointsBySPixel);

		_spixelMap = loadSpixelMap(*_scene, _spixelImage);

		_planes = loadPlanes(*_scene, dataSetPath + "/spixelwarp/");

		// The planes returned by loadPlanes do not have a 3D point in them.
		// Further processing is done here to get the 3D point.
		for (uint i = 0; i < _planes.size(); i++) {
			for (uint j = 0; j < _spixelImage.at(i)->numSpixels(); j++) {
				auto spx = _spixelImage.at(i)->getSpixel(j);
				Rect<double>	bbox = spx.getBoundingBoxInWinCoord(_spixelImage.at(i)->w(), _spixelImage.at(i)->h());

				std::array<sibr::Vector2d, 4> quadVertices;

				quadVertices[0] = bbox.cornerLeftTop();
				quadVertices[1] = bbox.cornerLeftBottom();
				quadVertices[2] = bbox.cornerRightBottom();
				quadVertices[3] = bbox.cornerRightTop();

				sibr::Vector2d centroid = (quadVertices[0] + quadVertices[1] + 
					quadVertices[2] + quadVertices[3]) / 4.0f;

				//sibr::Vector3f P3D = _scene->inputCameras().at(i).unproject(Vector3f((float)centroid.x(), (float)centroid.y(), spx.getMedianDepth()));
				//_planes.at(i).plane(j).fplanP3D = P3D;
				_planes.at(i).plane(j).isFPLAN = !_planes.at(i).plane(j).isActive;
				_planes.at(i).plane(j).isActive = spx.isActive();
			}
		}

		// Build 2D vector with all TRUE (for FPLAN).
		std::vector<std::vector<bool> > fplanSwitches;
		// Build 2D vector with all FALSE (for PLAN).
		std::vector<std::vector<bool> > planSwitches;
		for (auto spimg : _spixelImage) {
			fplanSwitches.push_back(std::vector<bool>(spimg->numSpixels(), false));
			planSwitches.push_back(std::vector<bool>(spimg->numSpixels(), true));
		}
		
		_planarRenderer = new PlanarRenderer(scene);
		_planarRenderer->load(_spixelImage, _spixelMap, _planes, planSwitches, fplanSwitches);

	}


	uint				PLANTester::getLabelID(void) const
	{
		return 3;
	}
	double				PLANTester::getSigmaModifier(void) const 
	{
		return 1.0;
	};

	std::vector<double>	PLANTester::spixelGeommetricErr(uint truthImageID, uint testedImageID)
	{
		std::cout << "New : PlanTester" << std::endl;
		// TODO (Sai): Remove swap and change variable names.
		uint a = truthImageID;
		truthImageID = testedImageID;
		testedImageID = a;

		SIBR_DEBUG(testedImageID);
		SIBR_DEBUG(truthImageID);

		const InputCamera& cameraS = *_scene->cameras()->inputCameras()[truthImageID];
		const InputCamera& cameraD = *_scene->cameras()->inputCameras()[testedImageID];

		uint num_superpixels = _spixelImage[truthImageID]->numSpixels();

		std::vector<double>		errPerSpixel(num_superpixels);

#pragma omp parallel for schedule(dynamic)

		for (int spixelID = 0; spixelID < num_superpixels; ++spixelID)
		{
			uint width = _spixelImage[truthImageID]->w();
			uint height = _spixelImage[truthImageID]->h();

			Vector3f cam_pos = cameraS.position();
			Vector3f fplane_n = _planes[truthImageID].getPlane(spixelID).normal;		///< keep the current front facing plane
			float fplane_d = _planes[truthImageID].getPlane(spixelID).d;

			uint count_visib = 0;

			float error = 0.f;

			if (_planes[truthImageID].getPlane(spixelID).isFPLAN || !_planes[truthImageID].getPlane(spixelID).isActive) {
				errPerSpixel[spixelID] = SIGMA_geom;
				count_visib = 1;

				continue;
			}

			for (uint k = 0; k < _mvsPointsBySPixel[truthImageID]->at(spixelID)->size(); k++)
			{
				//if (!sp_obj.is_patch_visible(k, nCam.id()))
				//	continue;
				const PatchPoint* mvsPoint = _mvsPointsBySPixel[truthImageID]->at(spixelID)->at(k);

				// Check for visibility ///TODO (Sai): MOVE TO SEPARATE FUNCTION
				bool visib = false;
				for (auto img : mvsPoint->visibleInImages())
					if (img == testedImageID) {
						visib = true;
						break;
					}
				
				if (!visib)
					continue;
				// --------------------

				count_visib++;

				Vector3f X = mvsPoint->position();
				Vector3f ray = X - cam_pos;

				float alpha = -(dot(cam_pos, fplane_n) + fplane_d) / dot(ray, fplane_n);

				Vector3f X_prime = alpha*ray + (Vector3f)cameraS.position();
				Vector3f x_j2i = cameraD.project(X_prime);
				Vector3f x_i = cameraD.project(X);

				// Convert to screen coord.
				//	0 <= pxl_j2i.x < w_sp
				//	0 <= pxl_j2i.y < h_sp
				sibr::Vector2i pxl_j2i = sibr::Vector2i((x_j2i[0] + 1) * (width / 2), (1 - x_j2i[1]) * (height / 2));
				sibr::Vector2i pxl_i = sibr::Vector2i((x_i[0] + 1)	 * (width / 2), (1 - x_i[1])	* (height / 2));

				sibr::Vector2i temp2i = pxl_i - pxl_j2i;
				sibr::Vector2f temp2f(temp2i[0], temp2i[1]);

				error += sibr::length(temp2f);
			}

			errPerSpixel.at(spixelID) = (count_visib != 0) ? (error / count_visib) : 0.0f;
		}

		SIBR_DEBUG(errPerSpixel.size());
		SIBR_DEBUG(errPerSpixel[0]);

		return errPerSpixel;
	}

#define MAX_LAYERS 7
	std::vector<double>		PLANTester::spixelPhotometricErr(uint truthImageID, uint testedImageID)
	{
		uint a = truthImageID;
		truthImageID = testedImageID;
		testedImageID = a;

		//return std::vector<double>(_spixelImage.at(truthImageID)->numSpixels(), 0.0f);
		/// TODO: Remove this assertion. (May not always be true).
		SIBR_ASSERT(truthImageID != testedImageID);
		SIBR_ASSERT(truthImageID < _scene->cameras()->inputCameras().size());
		SIBR_ASSERT(testedImageID < _scene->cameras()->inputCameras().size());

		std::vector<RenderTargetRGBA32F::Ptr> layers;
		sibr::ImageFloat4 targetColor;
		_scene->renderTargets()->inputImagesRT().at(testedImageID)->readBack(targetColor);

		uint width = targetColor.w();
		uint height = targetColor.h();
		for (int i = 0; i < 1; i++) {
			RenderTargetRGBA32F::Ptr rtPtr;
			rtPtr.reset(new RenderTargetRGBA32F(width, height, SIBR_STENCIL_BUFFER, 2));
			layers.push_back(rtPtr);
		}

		std::vector<uint> imagelist;
		imagelist.push_back(truthImageID);

		std::vector<double> errors(_spixelImage.at(truthImageID)->numSpixels(), 0.0f);
		std::vector<uint> counts(_spixelImage.at(truthImageID)->numSpixels(), 0);

		layers[0]->clear();
		layers[0]->clearStencil();
		layers[0]->bind();

		//sibr::ImageFloat4 errorImage(layers[0]->w(), layers[0]->h());

		for (int i = 0; i < MAX_LAYERS; i++) {

			//_scene->userCamera(_scene->cameras()->inputCameras().at(testedImageID));
			glViewport(0, 0, layers[0]->w(), layers[0]->h());

			std::vector<RenderTargetRGBA32F::Ptr> temp_layer;
			temp_layer.push_back(layers[0]);

			glDisable(GL_DEPTH_TEST);
			glEnable(GL_STENCIL_TEST);

			CHECK_GL_ERROR;
			glStencilOp(GL_INCR, GL_INCR, GL_INCR);
			CHECK_GL_ERROR;
			glStencilFunc(GL_EQUAL, i, 0xFF);

			_planarRenderer->process(*_scene->cameras()->inputCameras().at(testedImageID),
				imagelist, temp_layer);

			glDisable(GL_STENCIL_TEST);
			glEnable(GL_DEPTH_TEST);

			sibr::ImageFloat4 color(layers[0]->w(), layers[0]->h());
			sibr::ImageFloat4 data(layers[0]->w(), layers[0]->h());
			sibr::ImageFloat4 diff(layers[0]->w(), layers[0]->h());

			layers[0]->readBack(color, 1);
			layers[0]->readBack(data, 0);
			
			for (uint x = 0; x < data.w(); x++) {
				for (uint y = 0; y < data.h(); y++) {
					Vector4f sCol = color(x, y);
					Vector4f tCol = targetColor(x, y);
					uint gid = static_cast<uint>(data(x, y).w());
					float depth = data(x, y).z();

					diff(x, y) = Vector4f(0.0f, 0.0f, 0.0f, 1.0f);

					if (depth == 0.0f) {
						// No depth;
						//diff.pixel(x, y) = Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
						continue;
					}

					double error = (1 * (sCol.xyz() - tCol.xyz())).squaredNorm() / 3 + 1e-12;

					//if ((data.pixel(x, y).w() - gid) != 0.0f) {
						// No GID in pixel.
					//	diff.pixel(x, y) = Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
					//	continue;
					//}

					//if (gid == 0)
					//	continue;

					uint local_id = gid & 0xFFFF;

					if (local_id >= errors.size()) {
						// Incorrect LocalID.
						diff(x, y) = Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
						continue;
					}

					errors.at(local_id) += error;
					counts.at(local_id) += 1;
					
					diff(x,y) = Vector4f(error * 5, error * 5, error * 5, 1.0f);
					
				}
			}
			//show(targetColor);
			//show(color);
			//show(diff);

			layers[0]->clear();
			layers[0]->clearStencil();

		}
		
		for (int k = 0; k < errors.size(); k++) {
			errors.at(k) = (counts.at(k) != 0) ? (errors.at(k) / counts.at(k)) : 0;
			
		}


		SIBR_LOG << "SHOWING PLAN" << std::endl;
		sibr::ImageFloat4 errorImage(_spixelImage[truthImageID]->w(), _spixelImage[truthImageID]->h());

		for(uint x = 0; x < errorImage.w(); x++)
			for (uint y = 0; y < errorImage.h(); y++) {
				uint spixelID = _spixelImage[truthImageID]->getSpixelID(x, y);
				float e = errors.at(spixelID);
				errorImage(errorImage.w() - x - 1, errorImage.h() - y - 1) = Vector4f(e * 5, e * 5, e * 5, 1.0f);
			}

		//show(errorImage);

		layers[0]->unbind();

		return errors;

	}
	
}
