/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_PLANTESTER_HPP__
# define __SIBR_PLANTESTER_HPP__

#include <core/graphics/Config.hpp>
#include <projects/spixelwarp/renderer/SelectionView.hpp>
#include <projects/spixelwarp/renderer/PlanarRenderer.hpp>
#include <projects/spixelwarp/renderer/SpixelImage.hpp>
#include <projects/spixelwarp/renderer/Spixel.hpp>
#include "IQualityTestable.hpp"

/*
* FPLANTester. Projected superpixels from one image onto another view (using FPLAN) and evaluates the
* Geometeric and Photometric error.
*/
namespace sibr
{
	class PLANTester : public IQualityTestable
	{
		//struct CacheBBox
		//{
		//	typedef std::array<float, 4> Box;

		//	uint					checkImageId;
		//	std::vector<Box>		boxPerSpixel;
		//};

	public:
		//typedef std::shared_ptr<SelectionRenderer>	SelectionPtr;
		typedef RenderTargetLum32F					RenderTarget;

	public:

		/**
		*	Main constructor.
		*	@param scene A pointer to an IBRScene object.
		*	@param patchfile A reference to a patchfile object.
		*	@param dataSetPath Path to dataset.
		*/
		PLANTester(const BasicIBRScene::Ptr& scene, const PatchFile& patchfile, 
					const std::string& dataSetPath);

		/**
		*	Computes the superpixel geometric error under the PLAN transform.
		*	Uses MVS points as reference to compute the error.
		*	See paper on 'Bayesian Superpixel Rendering', [Ortiz-Cayon et al. 2015]
		*
		*	@param truthImageID The ID of the image to use as reference.
		*	@param testedImageID The ID of the image whose superpixels are to be evaluated.
		*	@return A list of geometric errors per spixel.
		*/
		std::vector<double>	spixelGeommetricErr( uint truthImageID, uint testedImageID ) override;

		/**
		*	Computes the superpixel photometric error under the PLAN transform.
		*	Uses :PlanarRenderer to transform the superpixels in the source mesh
		*	to the destination.
		*	See paper on 'Bayesian Superpixel Rendering', [Ortiz-Cayon et al. 2015]
		*
		*	@param truthImageID The ID of the image to use as reference.
		*	@param testedImageID The ID of the image whose superpixels are to be evaluated.
		*	@return A list of photometric errors per spixel.
		*/
		std::vector<double>	spixelPhotometricErr( uint truthImageID, uint testedImageID ) override;

		/**
		*	Returns a unique label ID for this Tester class.
		*	@return Class label number.
		*/
		uint				getLabelID( void ) const override;

		/**
		*	Returns a multiplier that is factored into the variance for the gaussian
		*	in the next step.
		*	
		*	See paper on 'Bayesian Superpixel Rendering', [Ortiz-Cayon et al. 2015]
		*	
		*	@return Class label number.
		*/
		double				getSigmaModifier(void) const override;

	private:
		BasicIBRScene::Ptr					_scene;
//		std::shared_ptr<SelectionRenderer>	_selection;
		
		std::vector<SpixelImage::Ptr>		_spixelImage;
		std::vector<Texture2DRGBA32F::Ptr>	_spixelMap;
		std::vector<ImagePlanes>			_planes;
		GLShader							_qualityShader;
		GLParameter							_qualityShaderScreenSize;
		PlanarRenderer*						_planarRenderer;

		std::vector< std::vector<sibr::PatchPoint> > _mvsPointsByCamera;
		std::vector<std::vector<std::vector<sibr::PatchPoint* >* >* > _mvsPointsBySPixel;
	};

} // namespace sibr

#endif // __SIBR_OLDSELECTIONTESTER_HPP__
