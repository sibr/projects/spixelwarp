/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <map>
#include <core/system/LoadingProgress.hpp>
#include <core/assets/Resources.hpp>
#include "QualityEstimator.hpp"

// TODO: could be equal to '3'/N_VIEWS_QUALITY
#define NUM_WARPED		4

///< QUALITY ESTIMATION
#define N_VIEWS_QUALITY		3

namespace sibr
{
	QualityEstimator::QualityEstimator( const Window::Ptr& window, const BasicIBRScene::Ptr& scene ) :
		_window(window),
		_scene(scene)
	{
	}

	void	QualityEstimator::addRenderer( const IQualityTestable::Ptr&	renderer )
	{
		_algos.push_back(renderer);
	}

	void	QualityEstimator::clearRenderer( void )
	{
		_algos.clear();
	}

	std::vector<uint>	QualityEstimator::selectCameras( uint camCurrent, uint camCount ) const
	{
		std::vector<uint>				selectedCameras;	// Output
		std::multimap<float,uint>		dist;				// distance wise closest input cameras
		const std::vector<InputCamera::Ptr>&	cams = _scene->cameras()->inputCameras();
		const sibr::Camera& current = *cams.at(camCurrent);
		
		for ( uint i=0; i < cams.size(); ++i )
		{
			const sibr::InputCamera& cam = *cams.at(i);
			if (i != camCurrent && cam.isActive())
			{
				
				float d = sibr::distance(cam.position(), current.position());
				float a = sibr::dot(cam.dir(), current.dir());

				if (a > 0.707)	// cameras with 45 degrees
					dist.insert(std::make_pair(d,i));	// sort distances in increasing order
			}
		}

		std::multimap<float,uint>::const_iterator distIter(dist.begin());

		while (distIter != dist.end() && selectedCameras.size() < camCount)
		{
			//SIBR_DEBUG(distIter->second);
			selectedCameras.push_back(distIter->second);
			++distIter;
		} SIBR_ASSERT(selectedCameras.size() <= camCount);

		return selectedCameras;
	}

	bool	QualityEstimator::estimate( void )
	{
		_spixelResultsPerCam.resize(_scene->cameras()->inputCameras().size());

		sibr::LoadingProgress	progress(_scene->cameras()->inputCameras().size());
		//for (uint i = 0; i < 1; ++i)
		for (uint i = 0; i < _scene->cameras()->inputCameras().size(); ++i)
		{
			const sibr::InputCamera& cam = *_scene->cameras()->inputCameras().at(i);

			if (cam.isActive() == false)
			{
				progress.walk();
				continue;
			}
			
			std::vector<uint>	selectedCams = selectCameras(i, N_VIEWS_QUALITY);
			SIBR_DEBUG(selectedCams.size());

			_spixelResultsPerCam.at(i).resize(_algos.size());
			for (uint j = 0; j < _algos.size(); ++j)
			{
				//SIBR_LOG << "Running Algo [" << j << "]" << std::endl;
				std::vector<std::vector<double>>	spixelGeomErrPerCams(selectedCams.size());
				std::vector<std::vector<double>>	spixelPhotoErrPerCams(selectedCams.size());

				for (uint k = 0; k < selectedCams.size(); ++k)
				{
					uint cam = selectedCams.at(k);
					SIBR_ASSERT(cam < _scene->cameras()->inputCameras().size());

					spixelGeomErrPerCams.at(k) = _algos.at(j)->spixelGeommetricErr(cam, i);
					spixelPhotoErrPerCams.at(k) = _algos.at(j)->spixelPhotometricErr(cam, i);

					SIBR_LOG << j << ' ' << cam << ' ' << spixelGeomErrPerCams[k][0] << ' ' << spixelPhotoErrPerCams[k][0] << std::endl;
				}

				_spixelResultsPerCam.at(i).at(j).geomPerSpixel = averageErrPerCams(spixelGeomErrPerCams);
				_spixelResultsPerCam.at(i).at(j).photoPerSpixel = averageErrPerCams(spixelPhotoErrPerCams);
			}
			progress.walk();
		}

		return true;
	}

	std::vector<double>	QualityEstimator::averageErrPerCams( const std::vector<std::vector<double>>& errPerCams )
	{
		std::vector<double>		errPerSpixels(errPerCams[0].size());
		SIBR_DEBUG(errPerCams[0].size());
		SIBR_DEBUG(errPerCams.size());
		SIBR_DEBUG(errPerSpixels.size());
		for (uint spixelID = 0; spixelID < errPerSpixels.size(); ++spixelID)
		{
//			SIBR_DEBUG(spixelID);

			double sum = 0.0;
			uint count = 0;
			for (uint i = 0; i < errPerCams.size(); ++i)
			{
				try {
					double err = errPerCams.at(i).at(spixelID);
					//SIBR_DEBUG(err);
					sum += err;
					count += (err == 0.0) ? 0 : 1;
				}
				catch (const std::exception& e)
				{
					SIBR_DEBUG(spixelID);
					SIBR_DEBUG(i);
					SIBR_DEBUG(errPerCams.at(i).size());
				}
				
			}
			errPerSpixels.at(spixelID) = (count == 0) ? (-1) : (sum / (double)count);
		}
		return errPerSpixels;
	}

	void	QualityEstimator::save( const std::string& outputFolder )
	{
		sibr::makeDirectory( outputFolder );

		//for (uint i = 0; i < 1; ++i)
		for (uint i = 0; i < _scene->cameras()->inputCameras().size(); ++i)
		{
			if (_scene->cameras()->inputCameras().at(i)->isActive() == false)
				continue;

			const std::vector<TestResult>& tests = _spixelResultsPerCam[i];
			SIBR_ASSERT(tests.empty() == false);

			uint spixelCount = (uint)tests[0].geomPerSpixel.size();

			std::vector<std::vector<std::array<float, 3> > > debugResults(spixelCount);
			std::vector<uint>	selectedAlgoPerSpixels(spixelCount, 0);
			for (uint spixel = 0; spixel < spixelCount; ++spixel)
			{
				std::vector<float> resultsPerAlgo(tests.size(), 0.f);
				float fullGeomErr;
				float fullPhotoErr;
				debugResults[spixel].resize(tests.size());
				for (uint j = 0; j < tests.size(); ++j)
				{
					float geom_c = (float)tests[j].geomPerSpixel.at(spixel);
					float photo_c = (float)tests[j].photoPerSpixel.at(spixel);
					fullGeomErr = (float)(1 / sqrt(2 * M_PI*(SIGMA_geom*SIGMA_geom)))*exp(-(geom_c*geom_c) / (2 * SIGMA_geom*SIGMA_geom));
					fullPhotoErr = (float)(1/sqrt(2*M_PI*(SIGMA_phot*SIGMA_phot*_algos[j]->getSigmaModifier()*_algos[j]->getSigmaModifier())))*exp( -(photo_c*photo_c)/(2*SIGMA_phot*SIGMA_phot) );

					resultsPerAlgo[j] = fullGeomErr*fullPhotoErr;

					std::array<float, 3> debug;
					debug[0] = fullGeomErr*fullPhotoErr;
					debug[1] = geom_c;
					debug[2] = photo_c;
					debugResults[spixel][j] = debug;
				}
				auto maxIt = std::max_element(resultsPerAlgo.begin(), resultsPerAlgo.end());
				uint distIt = (uint)std::distance(resultsPerAlgo.begin(), maxIt);
				selectedAlgoPerSpixels[spixel] = distIt;
			}

			{ // final_labels_*.txt
				std::string filename = outputFolder + "/final_labels_" + sibr::imageIdToString((int)i) + ".txt";
				std::ofstream file(filename, std::ios::trunc | std::ios::out);

				if (file)
				{
					SIBR_LOG << "Writing to '" << filename << "'." << std::endl;
					file << spixelCount << '\n';
					for ( uint j = 0; j < selectedAlgoPerSpixels.size(); ++j)
					{
						uint label = selectedAlgoPerSpixels[j];

						SIBR_ASSERT(label < (uint)_algos.size());
						file << j << ' ' << _algos[label]->getLabelID() << '\n';
					}
					file << std::endl;
				}
				else
				{
					SIBR_ERR << "cannot write to " << filename << std::endl;
				}
			}

			{ // debug_labels_*.txt
				std::string filename = outputFolder + "/debug_labels_" + sibr::imageIdToString((int)i) + ".txt";
				std::ofstream file(filename, std::ios::trunc | std::ios::out);

				if (file)
				{
					SIBR_LOG << "Writing to '" << filename << "'." << std::endl;
					file << spixelCount << '\n';
					for ( uint j = 0; j < debugResults.size(); ++j)
					{
						file << j
							<< " [" << debugResults[j][0][0] << ' ' << debugResults[j][0][1] << ' ' << debugResults[j][0][2]
						<< "] [" << debugResults[j][1][0] << ' ' << debugResults[j][1][1] << ' ' << debugResults[j][1][2]
						<< "] [" << debugResults[j][2][0] << ' ' << debugResults[j][2][1] << ' ' << debugResults[j][2][2]
						<< '\n';
					}
					file << std::endl;
				}
				else
				{
					SIBR_ERR << "cannot write to " << filename << std::endl;
				}
			}

		}

	}

} // namespace sibr

	//std::vector<double>	QualityEstimator::testPhotometricQuality(
	//	IQualityTestable& renderer, uint currentCam, const std::vector<uint>& selectedCams )
	//{
	//	float* distMean;
	//	cudaMalloc((void**)&distMean, selectedCams.size() * num_spixles * sizeof(float));	// allocate device

	//	GLsizei idxSize = (GLsizei)selectionRenderer->_ImagePlanarWarper[j]->index_array().size();

	//	for (uint spixelID = 0; spixelID < spixelCount; ++spixelID )
	//	{
	//		for (uint i = 0; i < selectedCams.size(); ++i)
	//		{
	//			renderer.renderTransform(currentCam, spixel
	//		}
	//	}

	//			RenderUtility::useDefaultVAO();

	//			{ // Render transformation into '_transform_RT'
	//				_warpShader.begin(); //< _quality_warp.fp
	//				_warpRT[i]->clear();
	//				_warpRT[i]->bind();
	//				glViewport(0,0, _warpRT[i]->w(), _warpRT[i]->h());

	//				glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, _scene->inputImagesRT().at(j)->texture());
	//				glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, selectionRenderer->_spixelMap[j]->handle());
	//				glEnable(GL_DEPTH_TEST);
	//				glClear(GL_DEPTH_BUFFER_BIT);

	//				selectionRenderer->_ImagePlanarWarper[j]->prepareRender();

	//				glDrawElements(GL_TRIANGLES, idxSize, GL_UNSIGNED_INT, 0);
	//				glDisableVertexAttribArray(0);
	//				glDisableVertexAttribArray(1);
	//				glDisable(GL_DEPTH_TEST);

	//				_warpRT[i]->unbind();
	//				_warpShader.end();
	//			}

	//			// Read warped superpixel
	//			_qualityShader.begin();
	//			_qualityRT->clear();
	//			_qualityRT->bind();

	//			for(uint i=0; i<N_VIEWS_QUALITY; i++ )
	//			{
	//				_inputRGBShaderParam[i].set(2*i+0);
	//				_warpedUVShaderParam[i].set(2*i+1);

	//				glActiveTexture(GL_TEXTURE0+2*i+0);
	//				glBindTexture(GL_TEXTURE_2D, _scene->inputImagesRT()[ selectedCams[i] ]->texture());
	//				glActiveTexture(GL_TEXTURE0+2*i+1);
	//				glBindTexture(GL_TEXTURE_2D, _warpRT[i]->texture());
	//			}

	//			// render with quality_soft.fp which computes the difference
	//			sibr::RenderUtility::renderScreenQuad();
	//			_qualityRT->unbind();
	//			_qualityShader.end();

	//			texture_reduction(&d_mean[k * 3], graphic_resc, _qualityRT->w(), _qualityRT->h());

	//		}// Close loop superpixels

	//		std::vector<float> temp_phot_err(N_VIEWS_QUALITY * num_spixles);
	//		cudaMemcpy(&temp_phot_err[0], d_mean, N_VIEWS_QUALITY * num_spixles * sizeof(float), cudaMemcpyDeviceToHost);

	//	}

	//	std::vector<float> temp_phot_err(N_VIEWS_QUALITY * num_spixles);
	//	cudaMemcpy(&temp_phot_err[0], d_mean, N_VIEWS_QUALITY * num_spixles * sizeof(float), cudaMemcpyDeviceToHost);
	//}
