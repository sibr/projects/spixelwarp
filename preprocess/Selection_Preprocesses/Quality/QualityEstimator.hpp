/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_QUALITYESTIMATOR_HPP__
# define __SIBR_QUALITYESTIMATOR_HPP__

#include <core/graphics/Window.hpp>
#include <core/scene/BasicIBRScene.hpp>

#include "IQualityTestable.hpp"
//#include "cuda_func.cuh"

namespace sibr
{
	class QualityEstimator
	{
		SIBR_DISALLOW_COPY( QualityEstimator );

		struct TestResult
		{
			std::vector<double>		geomPerSpixel;
			std::vector<double>		photoPerSpixel;
		};

	public:

		QualityEstimator( const Window::Ptr& window, const BasicIBRScene::Ptr& scene );

		void	addRenderer( const IQualityTestable::Ptr&	renderer );
		void	clearRenderer( void );

		bool	estimate( void );

		void	save( const std::string& outputFolder );

	private:

		std::vector<uint>	selectCameras( uint camCurrent, uint camCount ) const;

		static std::vector<double>	averageErrPerCams( const std::vector<std::vector<double>>& errPerCams );

		sibr::Window::Ptr				_window;
		sibr::BasicIBRScene::Ptr				_scene;

		std::vector<std::vector<TestResult>>	_spixelResultsPerCam;
		std::vector<IQualityTestable::Ptr>		_algos;
	};

} // namespace sibr

#endif // __SIBR_QUALITYESTIMATOR_HPP__
