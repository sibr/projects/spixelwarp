/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>
#include <thread>
#include <future>

#include <projects/spixelwarp/renderer/PatchFile.hpp>
#include <core/assets/Resources.hpp>
#include "Utils.hpp"
#include "SWARPTester.hpp"

#include <projects/spixelwarp/renderer/Utils.hpp>

#include <projects/spixelwarp/renderer/ImagePlanes.hpp>
#include <projects/spixelwarp/renderer/SpixelImage.hpp>
//#include <sibr/assets/PatchFile.hpp>
#include <projects/spixelwarp/renderer/ImageWarper.hpp>

namespace sibr
{
	static double	usedComputeMSEFunc(const sibr::ImageFloat1& img, const std::array<float, 4>& bbox)
	{
		return computeMSE(img, &bbox);
	}

	SWARPTester::~SWARPTester() {
		std::cout << "DESTROYING SWARP TESTER" << std::endl << std::flush;
	}
	SWARPTester::SWARPTester(const BasicIBRScene::Ptr& scene, const PatchFile& patchfile, 
							const std::string& dataSetPath)
	{

		_scene = scene;

		SIBR_ASSERT(_scene != nullptr);
		
		_spixelImage = loadSuperpixels(*_scene, dataSetPath + "/spixelwarp/");

		//todo : verify compatibility
		_mvsCams.resize(scene->cameras()->inputCameras().size());
		for (int i = 0; i < (int)scene->cameras()->inputCameras().size(); ++i) {
			_mvsCams[i] = *scene->cameras()->inputCameras()[i];
		}

		loadMVSPoints(*_scene, _spixelImage, dataSetPath + "/spixelwarp/", _mvsCams);

		loadAllSpixelMVSPoints(scene, patchfile, _spixelImage, _mvsPointsBySPixel);

		_spixelMap = loadSpixelMap(*_scene, _spixelImage);

		//_planes = loadPlanes(*_scene);

		_swarpRenderer = new SWARPRenderer(_spixelImage, _spixelMap, _mvsCams);
		
	}

	uint				SWARPTester::getLabelID(void) const
	{
		return 1;
	}
	double				SWARPTester::getSigmaModifier(void) const
	{
		return 1.0;
	};

	sibr::Vector3d SWARPTester::barycentric_coeffs(Vector2f p, sibr::Vector2d a, sibr::Vector2d b, sibr::Vector2d c) const {
		double z = (b[0] - a[0])*(c[1] - a[1]) - (c[0] - a[0])*(b[1] - a[1]);
		double u = ((b[0] - p[0])*(c[1] - p[1]) - (c[0] - p[0])*(b[1] - p[1])) / z;
		double v = ((c[0] - p[0])*(a[1] - p[1]) - (a[0] - p[0])*(c[1] - p[1])) / z;
		return sibr::Vector3d(u, v, 1.0 - u - v);
	}

	std::vector<double>	SWARPTester::spixelGeommetricErr(uint truthImageID, uint testedImageID)
	{
		std::cout << "New : SwarpTester" << std::endl;
		uint a = truthImageID;
		truthImageID = testedImageID;
		testedImageID = a;

		/// TODO: Remove this assertion. (May not always be true).
		SIBR_ASSERT(truthImageID != testedImageID);
		SIBR_ASSERT(truthImageID < _scene->cameras()->inputCameras().size());
		SIBR_ASSERT(testedImageID < _scene->cameras()->inputCameras().size());

		//SIBR_DEBUG(truthImageID);
		//SIBR_DEBUG(testedImageID);
		//SIBR_DEBUG(_mvsPointsBySPixel[truthImageID]->size());
		//SIBR_DEBUG(_mvsPointsBySPixel[testedImageID]->size());
		//SIBR_DEBUG(_mvsPointsBySPixel[truthImageID]->at(0)->size());
		//SIBR_DEBUG(_mvsPointsBySPixel[testedImageID]->at(0)->size());
		//SIBR_DEBUG(_spixelImage.size());
		//SIBR_DEBUG(_spixelImage[truthImageID]->numSpixels());
		//SIBR_DEBUG(_spixelImage[testedImageID]->numSpixels());


		float scale = static_cast<float>(_scene->cameras()->inputCameras().at(truthImageID)->w()) / _spixelImage.at(truthImageID)->w();

		SIBR_ASSERT(scale > 0.0f);

		const InputCamera& cameraS = *_scene->cameras()->inputCameras().at(truthImageID);
		const InputCamera& cameraD = *_scene->cameras()->inputCameras().at(testedImageID);

		//uint num_superpixels = _spixelImage[truthImageID]->numSpixels();
		uint num_superpixels = _mvsPointsBySPixel.at(truthImageID)->size();

		std::vector<double>		errPerSpixel(num_superpixels, 0.0f);
		
		//const std::shared_ptr<sibr:::ImageWarper> & warper =  _swarpRenderer->getImgWarpers().at(truthImageID);
		std::vector<std::shared_ptr<ImageWarper> > warpers = _swarpRenderer->getImgWarpers();
		
#pragma omp parallel for schedule(dynamic)
		for (int spixelID = 0; spixelID < num_superpixels; ++spixelID)
		{
			uint width = _spixelImage.at(truthImageID)->w();
			uint height = _spixelImage.at(truthImageID)->h();

			//Vector3f cam_pos = (Vector3f)cameraD.position();
			//Vector3f fplane_n = -(Vector3f)cameraD.dir();		///< keep the current font facing plane
			//float fplane_d = -dot(fplane_n, _planes[truthImageID].getPlane(spixelID).fplanP3D);
			uint count_visib = 0;

			std::vector<double> solns = warpers.at(truthImageID)->warpSingle(cameraD, spixelID);
			//continue;

			float error = 0.f;
			for (uint k = 0; k < _mvsPointsBySPixel.at(truthImageID)->at(spixelID)->size(); k++)
			{
				
				const PatchPoint *mvsPoint = _mvsPointsBySPixel.at(truthImageID)->at(spixelID)->at(k);

				// Check for visibility
				try {
					bool visib = false;
					for (auto img : mvsPoint->visibleInImages())
						if (img == testedImageID) {
							visib = true;
							break;
						}
					if (!visib)
						continue;
				}
				catch (const std::exception& e) {
					std::cout << e.what() << std::endl << std::flush;

					exit(0);
				}

				Vector3f scrn_pt = _scene->cameras()->inputCameras().at(truthImageID)->projectScreen(mvsPoint->position());
				sibr::Vector2i p_i = Vector2i((int)scrn_pt[0], (int)scrn_pt[1]);

				// Convert to Spixel image space coordinates.
				p_i /= static_cast<int>(scale);
				///< Convert to range [-1,1]
				float p_x = ((float)p_i[0] * 2 / (float)width) - 1;
				float p_y = 1 - ((float)p_i[1] * 2 / (float)height);

				SIBR_ASSERT(p_x <= 1.0f && p_x >= -1.0f);
				SIBR_ASSERT(p_y <= 1.0f && p_y >= -1.0f);

				//SIBR_DEBUG(truthImageID);
				Vector3f p = Vector3f(p_x, p_y, 1);
				//SIBR_DEBUG(p);
				DelaunayMesh dtmesh = warpers.at(truthImageID)->getSpixelMesh().at(spixelID);
				//SIBR_DEBUG(spixelID);
				//SIBR_DEBUG(dtmesh.number_of_faces());
				//SIBR_DEBUG(dtmesh.number_of_vertices());
				DT_face_handle f = dtmesh.locate(Point(p.x(), p.y()));  // triangle containing the depth sample
				
				SIBR_ASSERT(f != NULL);


				if (warpers.at(truthImageID)->getSpixelMesh().at(spixelID).is_infinite(f)) { break; }

				sibr::Vector2d p1 = sibr::Vector2d(CGAL::to_double(f->vertex(0)->point().x()), CGAL::to_double(f->vertex(0)->point().y()));
				sibr::Vector2d p2 = sibr::Vector2d(CGAL::to_double(f->vertex(1)->point().x()), CGAL::to_double(f->vertex(1)->point().y()));
				sibr::Vector2d p3 = sibr::Vector2d(CGAL::to_double(f->vertex(2)->point().x()), CGAL::to_double(f->vertex(2)->point().y()));


				sibr::Vector3d r = barycentric_coeffs(Vector2f(p[0], p[1]), p1, p2, p3);	// compute barycentric coord of re-projected depth sample

				// Find the associated vertice in solns
				uint v0_pos = f->vertex(0)->info().spxl_id;
				uint v1_pos = f->vertex(1)->info().spxl_id;
				uint v2_pos = f->vertex(2)->info().spxl_id;

				//SIBR_DEBUG(v0_pos);
				//SIBR_DEBUG(v1_pos);
				//SIBR_DEBUG(v2_pos);

				sibr::Vector2d p1_prime = sibr::Vector2d(solns.at(2 * v0_pos + 0), solns.at(2 * v0_pos + 1));
				sibr::Vector2d p2_prime = sibr::Vector2d(solns.at(2 * v1_pos + 0), solns.at(2 * v1_pos + 1));
				sibr::Vector2d p3_prime = sibr::Vector2d(solns.at(2 * v2_pos + 0), solns.at(2 * v2_pos + 1));

				double x_j2i_x = r[0] * p1_prime[0] + r[1] * p2_prime[0] + r[2] * p3_prime[0];
				double x_j2i_y = r[0] * p1_prime[1] + r[1] * p2_prime[1] + r[2] * p3_prime[1];

				Vector3f x_i = cameraD.project(mvsPoint->position());

				// Convert to screen coord.
				//	0 <= pxl_j2i.x < w_sp
				//	0 <= pxl_j2i.y < h_sp
				sibr::Vector2i pxl_j2i = sibr::Vector2i((x_j2i_x + 1)*width / 2, (1 - x_j2i_y)*height / 2);
				sibr::Vector2i pxl_i = sibr::Vector2i((x_i[0] + 1)*width / 2, (1 - x_i[1]) *height / 2);

				sibr::Vector2i temp2i = pxl_i - pxl_j2i;
				sibr::Vector2f temp2f(temp2i[0], temp2i[1]);

				error += sibr::length(temp2f);
				count_visib++;
			}
			//SIBR_DEBUG(spixelID);
			//SIBR_DEBUG(count_visib);
			//SIBR_DEBUG(_mvsPointsBySPixel.at(truthImageID)->at(spixelID)->size());
			//SIBR_DEBUG(error);
			errPerSpixel.at(spixelID) = (count_visib != 0 ) ? (error / (double)count_visib) : 0.0f;
		}

		SIBR_DEBUG(errPerSpixel[0]);
		SIBR_DEBUG(errPerSpixel.size());
		return errPerSpixel;
	}

#define MAX_LAYERS 7
	std::vector<double>		SWARPTester::spixelPhotometricErr(uint truthImageID, uint testedImageID)
	{
		
		uint a = truthImageID;
		truthImageID = testedImageID;
		testedImageID = a;

		//return std::vector<double>(_spixelImage.at(truthImageID)->numSpixels(), 0.0f);
		/// TODO: Remove this assertion. (May not always be true).
		SIBR_ASSERT(truthImageID != testedImageID);
		SIBR_ASSERT(truthImageID < _scene->cameras()->inputCameras().size());
		SIBR_ASSERT(testedImageID < _scene->cameras()->inputCameras().size());

		std::vector<RenderTargetRGBA32F::Ptr> layers;
		sibr::ImageFloat4 targetColor;
		_scene->renderTargets()->inputImagesRT().at(testedImageID)->readBack(targetColor);

		uint width = targetColor.w();
		uint height = targetColor.h();
		for (int i = 0; i < 1; i++) {
			RenderTargetRGBA32F::Ptr rtPtr;
			rtPtr.reset(new RenderTargetRGBA32F(width, height, SIBR_STENCIL_BUFFER, 2));
			layers.push_back(rtPtr);
		}

		std::vector<uint> imagelist;
		imagelist.push_back(truthImageID);

		std::vector<double> errors(_spixelImage.at(truthImageID)->numSpixels(), 0.0f);
		std::vector<uint> counts(_spixelImage.at(truthImageID)->numSpixels(), 0);

		layers[0]->clear();
		layers[0]->clearStencil();
		layers[0]->bind();

		for (int i = 0; i < MAX_LAYERS; i++) {
			
			//_scene->userCamera(_scene->cameras()->inputCameras().at(testedImageID) );
			glViewport(0, 0, layers[0]->w(), layers[0]->h());
			
			std::vector<RenderTargetRGBA32F::Ptr> temp_layer;
			temp_layer.push_back(layers[0]);

			glDisable(GL_DEPTH_TEST);
			glEnable(GL_STENCIL_TEST);

			CHECK_GL_ERROR;
			glStencilOp(GL_INCR, GL_INCR, GL_INCR);
			CHECK_GL_ERROR;
			glStencilFunc(GL_EQUAL, i, 0xFF);

			_swarpRenderer->process(*_scene->cameras()->inputCameras().at(testedImageID), 
				imagelist, _scene->renderTargets()->inputImagesRT(), temp_layer);

			glDisable(GL_STENCIL_TEST);
			glEnable(GL_DEPTH_TEST);

			sibr::ImageFloat4 color(layers[0]->w(), layers[0]->h());
			sibr::ImageFloat4 data(layers[0]->w(), layers[0]->h());
			sibr::ImageFloat4 diff(layers[0]->w(), layers[0]->h());

			layers[0]->readBack(color, 1);
			layers[0]->readBack(data,  0);
			
			/*
			//UNCOMMENT FOR DEBUG STUFF.

			for (uint x = 0; x < data.w(); x++) {
				for (uint y = 0; y < data.h(); y++) {
					uint gid = static_cast<uint>(data.pixel(x, y).w());
					float depth = data.pixel(x, y).z();

					/// TODO: Is there a better way?
					if (depth == 0.0f) {
						data.pixel(x, y) = Vector4f(0.0f, 1.0f, 0.0f, 1.0f);
						continue;
					}

					if ((data.pixel(x, y).w() - gid) != 0.0f)
						data.pixel(x, y) = Vector4f(0.0f, 0.0f, 0.0f, 1.0f);
					else
						data.pixel(x, y) = Vector4f( ( (gid % 2 == 0) ? 1.0f : 0.0f ), (gid % 3 == 0) ? 1.0f : 0.0f, (gid % 5 == 0) ? 1.0f : 0.0f, 1.0f);
				}
			}*/


			uint empty_pixels = 0;
			uint empty_lid = 0;
			uint empty_depth = 0;
			for (uint x = 0; x < data.w(); x++) {
				for (uint y = 0; y < data.h(); y++) {
					Vector4f sCol = color(x, y);
					Vector4f tCol = targetColor(x, y);
					uint gid = static_cast<uint>(data(x, y).w());
					float depth = data(x, y).z();

					diff(x, y) = Vector4f(0.0f, 0.0f, 0.0f, 1.0f);

					/// TODO: Is there a better way?
					if (depth == 0.0f) {
						empty_depth++;
						continue;
					}

					//SIBR_DEBUG(depth);
					//SIBR_DEBUG(gid);
					double error = (1 * (sCol.xyz() - tCol.xyz()) ).squaredNorm() / 3 + 1e-12;

					/// TODO: If condition necessary?
					//if ( (data.pixel(x, y).w() - gid) != 0.0f ) {
						//std::cout << "GID INCORRECT" << std::endl;
					//	empty_pixels++;
					//	continue;
					//}
					//if (gid == 0)
					//	continue;

					uint local_id = gid & 0xFFFF;
					

					if (local_id >= errors.size()) {
						//std::cout << "GID INCORRECT" << std::endl;
						empty_lid++;
						continue;
					}

					uint image_id = gid >> 16;

					SIBR_ASSERT(image_id == truthImageID);

					errors.at(local_id) += error;
					counts.at(local_id) += 1;
					
					diff(x, y) = Vector4f(error * 10, error * 10, error * 10, 1.0f);

				}
			}
			//SIBR_LOG << "SWARP SHOWING" << std::endl;
			//show(targetColor);
			//show(color);
			//show(diff);

			layers[0]->clear();
			layers[0]->clearStencil();
			
		}

		for (int k = 0; k < errors.size(); k++) {
			errors.at(k) = ((counts.at(k) != 0) ? (errors.at(k) / counts.at(k)) : 0);
		}

		SIBR_LOG << "SHOWING SWARP" << std::endl;
		sibr::ImageFloat4 errorImage(_spixelImage[truthImageID]->w(), _spixelImage[truthImageID]->h());

		for (uint x = 0; x < errorImage.w(); x++)
			for (uint y = 0; y < errorImage.h(); y++) {
				
				uint spixelID = _spixelImage[truthImageID]->getSpixelID(x,y);
				float e = errors.at(spixelID);
				errorImage(errorImage.w()- x - 1, errorImage.h() - y - 1) = 
					Vector4f(e * 5, e * 5, e * 5, 1.0f);
			}

		//show(errorImage);

		layers[0]->unbind();
		
		SIBR_DEBUG(errors.at(5));
		return errors;

	}
	

}
