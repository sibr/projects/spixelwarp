/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_SWARPTESTER_HPP__
#define __SIBR_SWARPTESTER_HPP__

#include <core/graphics/Config.hpp>
#include <core/assets/InputCamera.hpp>
#include <projects/spixelwarp/renderer/PlanarRenderer.hpp>
#include <projects/spixelwarp/renderer/SWARPRenderer.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <projects/spixelwarp/renderer/PatchFile.hpp>


#include "IQualityTestable.hpp"

/*
* FPLANTester. Projected superpixels from one image onto another view (using FPLAN) and evaluates the
* Geometeric and Photometric error.
*/
namespace sibr
{
	class SWARPTester : public IQualityTestable
	{
		//struct CacheBBox
		//{
		//	typedef std::array<float, 4> Box;

		//	uint					checkImageId;
		//	std::vector<Box>		boxPerSpixel;
		//};

	public:

		typedef RenderTargetLum32F					RenderTarget;

		/**
		*	Main constructor.
		*	@param scene A pointer to an IBRScene object.
		*	@param patchfile A reference to a patchfile object.
		*	@param dataSetPath Path to dataset.
		*/
		SWARPTester(const BasicIBRScene::Ptr& scene, const PatchFile& patchfile,
					const std::string& dataSetPath);
		
		/**
		*	Computes the superpixel geometric error under the SpixelWarp transform.
		*	Uses :ImageWarper to build the mesh, and uses MVS points 
		*	as reference to compute the error.
		*	See paper on 'Bayesian Superpixel Rendering', [Ortiz-Cayon et al. 2015]
		*
		*	@param truthImageID The ID of the image to use as reference.
		*	@param testedImageID The ID of the image whose superpixels are to be evaluated.
		*	@return A list of geometric errors per spixel.
		*/
		std::vector<double>	spixelGeommetricErr(uint truthImageID, uint testedImageID) override;

		/**
		*	Computes the superpixel photometric error under the SpixelWarp transform.
		*	Uses :SWARPRenderer to transform the superpixels in the source mesh
		*	to the destination.
		*	See paper on 'Bayesian Superpixel Rendering', [Ortiz-Cayon et al. 2015]
		*
		*	@param truthImageID The ID of the image to use as reference.
		*	@param testedImageID The ID of the image whose superpixels are to be evaluated.
		*	@return A list of photometric errors per spixel.
		*/
		std::vector<double>	spixelPhotometricErr(uint truthImageID, uint testedImageID) override;


		/**
		*	Returns a unique label ID for this Tester class.
		*	@return Class label number.
		*/
		uint				getLabelID(void) const override;

		/**
		*	Returns a multiplier that is factored into the variance for the gaussian
		*	in the next step.
		*
		*	See paper on 'Bayesian Superpixel Rendering', [Ortiz-Cayon et al. 2015]
		*
		*	@return Class label number.
		*/
		double				getSigmaModifier(void) const override;


		sibr::Vector3d barycentric_coeffs(Vector2f p, sibr::Vector2d a, sibr::Vector2d b, sibr::Vector2d c) const;

		~SWARPTester();

	private:
		BasicIBRScene::Ptr					_scene;

		std::vector<SpixelImage::Ptr>		_spixelImage;
		std::vector<Texture2DRGBA32F::Ptr>	_spixelMap;
		std::vector<ImagePlanes>			_planes;
		GLShader							_qualityShader;
		GLParameter							_qualityShaderScreenSize;
		SWARPRenderer*	_swarpRenderer;

		std::vector<std::vector<sibr::PatchPoint> > _mvsPointsByCamera;
		std::vector<std::vector<std::vector<sibr::PatchPoint* >* >* > _mvsPointsBySPixel;
	};

} // namespace sibr

#endif // __SIBR_OLDSELECTIONTESTER_HPP__
