/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_ULRTESTER_HPP__
# define __SIBR_ULRTESTER_HPP__

#include <sibr/graphics/Config.hpp>

#include "IQualityTestable.hpp"

namespace sibr
{

	class ULRTester : public IQualityTestable
	{
	public:

		// TODO

	};

} // namespace sibr

#endif // __SIBR_ULRTESTER_HPP__
