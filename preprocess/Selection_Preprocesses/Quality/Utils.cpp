/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <fstream>
#include <thread>
#include <future>

#include <core/system/ThreadIdWorker.hpp>
#include "Utils.hpp"
#include <vector>
/*
	SEB Note - Speed Improvement -
	I am not sure CUDA will help a lot on this. I would rather
	implement a multithread CPU algo. The current one help a bit
	but I need to avoid the creation/destruction of all threads
	at each call.
	Or simply use a bbox opti.
*/

namespace sibr
{

	/*
		NEW CODE ----------------------
	*/

	void loadAllSpixelMVSPoints(const BasicIBRScene::Ptr& scene, const PatchFile& patchfile, const std::vector<SpixelImage::Ptr>& spixelImages, std::vector<std::vector<std::vector< PatchPoint* >* >* >& mvsPointsBySPixel) {
		// MVS Points by Camera.
		std::vector<std::vector<PatchPoint> >  _mvsPointsByCamera;
		for (uint i = 0; i < spixelImages.size(); i++) {
			_mvsPointsByCamera.push_back(std::vector<sibr::PatchPoint>());
			mvsPointsBySPixel.push_back( new std::vector<std::vector<sibr::PatchPoint*>* >());
			// MVS Points by SPixel.
			for (uint j = 0; j < spixelImages.at(i)->numSpixels(); j++)
				mvsPointsBySPixel.at(i)->push_back( new std::vector<sibr::PatchPoint*>() );
		}



		// Collect all the points.
		for (int pointID = 0; pointID < patchfile.pointCount(); pointID++) {
			const sibr::PatchPoint &p = patchfile.point(pointID);
			for (const uint camIndex : p.visibleInImages()) {
				//std::shared_ptr<MVSPointPreprocess> ep(new MVSPointPreprocess(p.position()));
				//ep->patchPnt(p);
				_mvsPointsByCamera.at(camIndex).emplace_back(p);
			}
		}
		int err_cnt = 0, oo_bndscnt=0, tot_cnt=0;

		// Project MVS point and assign it to the superpixel.
		for (uint cam = 0; cam < spixelImages.size(); cam++) {

			float scale = static_cast<float>(scene->cameras()->inputCameras()[cam]->w()) / spixelImages[cam]->w();
			const SpixelImage::Ptr& sPixelImage = spixelImages[cam];
			for (uint i = 0; i < _mvsPointsByCamera.at(cam).size(); i++) {

				// Get mvs point
				//sibr::MVSPointPreprocess* mvs = mvsPntPreprocess(i);

				sibr::PatchPoint pp = _mvsPointsByCamera.at(cam).at(i);
				//SIBR_DEBUG(pp.position());
				Vector3f scrn_pt = scene->cameras()->inputCameras().at(cam)->projectScreen(pp.position());
				Vector2i pxl = Vector2i((int)scrn_pt[0], (int)scrn_pt[1]);

				//SIBR_DEBUG(scale);
				//SIBR_DEBUG(pxl);
				pxl /= scale;

				// Get superpixel associated to mvs point
				int x = pxl[0];
				int y = sPixelImage->h() - (pxl[1] + 1);

				tot_cnt++;
				//SIBR_DEBUG(x); SIBR_DEBUG(y);
				if (x >= (int)sPixelImage->w() || x < 0 || y >= (int)sPixelImage->h() || y < 0) {
					//std::cerr << "[SIBR plane estimation] ERROR pxl out of bounds (" << x << ", " << y << ")" << std::endl;
					oo_bndscnt++;
					continue;
				}

				uint spixelId = sPixelImage->getSpixelID(x, y); //< Spixel are sored in img coordinates.
				//SIBR_DEBUG(spixelId);												//sibr::PlanarSPixelPreprocess* pspixel_obj = (sibr::PlanarSPixelPreprocess*)_spixelImage->sPixelDIR(spixelId);
				sibr::PatchPoint* pP = new sibr::PatchPoint(pp);
				
				/// TODO: TREMPORARY. ReMOVe ASAP_.
				if (pP->visibleInImages().size() > 30) {
					if( err_cnt ++ < 4)
						std::cout << "VisInImages > 30 ERROROROR **** ";
				}

				mvsPointsBySPixel.at(cam)->at(spixelId)->push_back(pP);

			}
		}
		std::cerr << "OO " << oo_bndscnt << " vs " << tot_cnt << std::endl;

	}

	/*
		-------------------------------
	*/

	struct	MSEThreadResult
	{
		uint count;
		float sum;
	};

	static MSEThreadResult	computeMSETask( const ImageFloat1& img, uint yStart, uint yEnd )
	{
		MSEThreadResult out;
		out.sum = 0.f;
		out.count = 0;
		for (uint y = yStart; y < yEnd; ++y)
		{
			for (int x = 0; x < img.w(); ++x)
			{
				float pixel = img(x, y)[0];
				out.count += pixel != 0.f ? 1 : 0;
				out.sum += pixel;
			}
		}
		return out;
	}
	
	static MSEThreadResult	computeMSETaskLimitX( const ImageFloat1& img, uint yStart, uint yEnd, uint xStart, uint xEnd )
	{
		MSEThreadResult out;
		out.sum = 0.f;
		out.count = 0;
		for (uint y = yStart; y < yEnd; ++y)
		{
			for (uint x = xStart; x < xEnd; ++x)
			{
				float pixel = img(x, y)[0];
				out.count += pixel != 0.f ? 1 : 0;
				out.sum += pixel;
			}
		}
		return out;
	}

	double	computeMSE( const sibr::ImageFloat1& img, const std::array<float, 4>* optionalBbox )
	{
		std::array<float, 4> bbox = {-1.f, -1.f, 1.f, 1.f};
		if (optionalBbox)
			bbox = *optionalBbox;

		int maxW = ((int)img.w()-1);
		int maxH = ((int)img.h()-1);
		int minX =  clamp((int) (bbox[0]* (float)maxW), 0, maxW);
		int minY =  clamp((int) (bbox[1]* (float)maxH), 0, maxH);
		int maxX =  clamp((int) (bbox[2]* (float)maxW), 0, maxW);
		int maxY =  clamp((int) (bbox[3]* (float)maxH), 0, maxH);

#if 1 == 1 /*Use MultiThread*/
		/////////////////////////////////////////////////////////////////////////
		int row = maxY-minY;
		//int numberOfThreads = (int)std::max(1U, std::min(std::thread::hardware_concurrency()-1U, 2U)); // do not use to much thread (for this small tasks) !!
		int numberOfThreads = 2; // do not use to much thread (for this small tasks) !!

		std::vector<uint>	threadsCount(numberOfThreads, 0);
		std::vector<float>	threadsSum(numberOfThreads, 0.f);

		std::vector<std::future<MSEThreadResult>>	threadResults(numberOfThreads - 1);
		uint iterPerThread = (uint)((float)row / (float)numberOfThreads);
		uint iterOffset = minY;

		for (uint i = 0; i < numberOfThreads - 1; ++i, iterOffset += iterPerThread)
			threadResults[i] = std::async(
			std::launch::async, computeMSETaskLimitX, std::cref(img), iterOffset, iterOffset+iterPerThread, minX, maxX+1);
		// We use also the main thread (it is include in numberOfThreads)
		MSEThreadResult mainThreadResults = computeMSETaskLimitX( img, iterOffset, (minY+row) - iterOffset, minX, maxX+1 );

		// Merge all results (I prefer this way than sharing the same buffer)
		float	sum = 0.f;
		uint	count = 0;
		for(uint j = 0; j < threadResults.size(); ++j)
		{
			auto results = std::move(threadResults[j].get());
			sum += results.sum;
			count += results.count;
		}
		sum += mainThreadResults.sum;
		count += mainThreadResults.count;
		return count == 0? 0.f : sum / (float)count;
#else
		///////////////////////////////////////////////////////////////////////

		float	sum = 0.f;
		uint	count = 0;

		for (int y = minY; y <= maxY; ++y)
		{
			for (int x = minX; x <= maxX; ++x)
			{
				float pixel = img.pixel(x, y)[0];
				count += pixel != 0.f ? 1 : 0;
				sum = sum + pixel;
			}
		}
		return count == 0? 0.f : sum / (float)count;
#endif
	}

} // namespace sibr
