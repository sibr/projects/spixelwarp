/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_UTILS_HPP__
# define __SIBR_UTILS_HPP__

#include <core/graphics/Config.hpp>

#include <core/graphics/RenderTarget.hpp>

#include <core/scene/BasicIBRScene.hpp>
#include <core/assets/InputCamera.hpp>
#include <projects/spixelwarp/renderer/PatchFile.hpp>
#include <projects/spixelwarp/renderer/SpixelImage.hpp>

namespace sibr
{
	// TODO: Optimization - Use a threadpool

	double		computeMSE( const sibr::ImageFloat1& img, 
		const std::array<float, 4>* optionalBbox = nullptr );
	double		computeMSE( const sibr::ImageFloat1& img );
	void loadAllSpixelMVSPoints(const BasicIBRScene::Ptr& scene, const PatchFile& patchfile, const std::vector<SpixelImage::Ptr>& spixelImages, std::vector<std::vector<std::vector< PatchPoint* >* >* >& mvsPointsBySPixel);

} // namespace sibr

#endif // __SIBR_UTILS_HPP__
