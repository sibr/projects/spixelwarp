/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "cuda_func.cuh"
#include "sum_reduction.cuh"

//-------------------------------------------------------------------------------------------

texture<float4, cudaTextureType2D, cudaReadModeElementType> tex_ref;

//-------------------------------------------------------------------------------------------

__global__ void reduce_textureBlock(
	float * per_block_results_x, 
	float * per_block_results_y, 
	float * per_block_results_z, 
	int * nnz_vals, 
	int w, int h, 
	const size_t shared_size)
{
	extern __shared__ float sdata[];

	int i = threadIdx.x + blockIdx.x * blockDim.x;
	int j = threadIdx.y + blockIdx.y * blockDim.y;

	int k = threadIdx.x + threadIdx.y * blockDim.x; // linear thread index
	int K = blockIdx.x + blockIdx.y * gridDim.x;	// linear block index 
	
	int n_threads = blockDim.x * blockDim.y;

	//load input into __shared__ memory
	float x = 0, y = 0, z = 0;
	if (i < w && j < h)
	{
		float4 val = tex2D(tex_ref, i, j);

		x = val.x;
		if (x > 0) atomicAdd(nnz_vals + 0, 1);
		y = val.y;
		if (y > 0) atomicAdd(nnz_vals + 1, 1);
		z = val.z;
		if (z > 0) atomicAdd(nnz_vals + 2, 1);
	}

	sdata[k + shared_size * 0] = x;
	sdata[k + shared_size * 1] = y;
	sdata[k + shared_size * 2] = z;
	__syncthreads();
	
	// contiguous range pattern
	for (int offset = ( n_threads )/ 2; offset > 0; offset >>= 1)
	{
		if (k < offset)
		{
			// add a partial sum upstream to our own
			sdata[k + shared_size * 0] += sdata[k + +shared_size * 0 + offset];
			sdata[k + shared_size * 1] += sdata[k + +shared_size * 1 + offset];
			sdata[k + shared_size * 2] += sdata[k + +shared_size * 2 + offset];
		}

		// wait until all threads in the block have
		// updated their partial sums
		__syncthreads();
	}
	
	// thread 0 writes the final result
	if (k == 0)
	{
		per_block_results_x[K] = sdata[shared_size * 0];
		per_block_results_y[K] = sdata[shared_size * 1];
		per_block_results_z[K] = sdata[shared_size * 2];
	}
}

//-------------------------------------------------------------------------------------------

cudaGraphicsResource* register_gl_cuda(GLuint text)
{
	cudaGraphicsResource* text_resc;
	//cudaError_t cuda_e1 = cudaGraphicsGLRegisterImage(&text_resc, text, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsReadOnly);
	cudaError_t cuda_e1 = cudaGraphicsGLRegisterImage(&text_resc, text, GL_TEXTURE_2D, cudaGraphicsRegisterFlagsNone);
	return text_resc;
}

//-------------------------------------------------------------------------------------------
//< Helper function
void mapping_gl_cuda(cudaGraphicsResource* graphicResc, unsigned int w, unsigned int h)
{
	// Create a CUDA array and allocate memory.
	cudaArray* d_array;
	size_t array_size = sizeof(float)* 4 * w * h;
	cudaMalloc((void **)&d_array, array_size);

	//Mapping the Resources: The kernet CAN NOT access to cudaArray!
	cudaError_t cuda_e2 = cudaGraphicsMapResources(1, &graphicResc, 0);
	cudaError_t cuda_e3 = cudaGraphicsSubResourceGetMappedArray(&d_array, graphicResc, 0, 0);

	// Bind texture to cudaArray
	cudaError_t cuda_e4 = cudaBindTextureToArray(tex_ref, d_array);

	//deallocate
	cudaFree(d_array);
}

//-------------------------------------------------------------------------------------------

// Generic funtion for block reduction
void block_reduction(
	float* d_partialSums_ptr, 
	float* d_texPartialSums_ptr, 
	const size_t n_blocks, 
	const size_t blocks_size, 
	const size_t threads_size, 
	float* d_result, 
	int* d_count)
{	
	// launch one kernel to compute, per-block, a partial sum
	block_sum <<<blocks_size, threads_size, threads_size * sizeof(float) >>>
		(d_texPartialSums_ptr, d_partialSums_ptr, n_blocks);

	// launch a single block to compute the sum of the partial sums
	block_sum <<<1, blocks_size, blocks_size * sizeof(float) >>>
		(d_partialSums_ptr, d_partialSums_ptr + blocks_size, blocks_size, d_result, d_count);
}

//-------------------------------------------------------------------------------------------

void texture_reduction(float* d_sum, cudaGraphicsResource* graphicResc, unsigned int w, unsigned int h)
{
	mapping_gl_cuda(graphicResc, w, h);

	//-------------------------------------------------------------
	// TEXTURE REDUCTION
	//-------------------------------------------------------------

	// Launch configuration
	const size_t thread_size_x	= 1 << 5;
	const size_t thread_size_y  = 1 << 5;
	const size_t block_size_x	= (w / thread_size_x) + ((w % thread_size_x) ? 1 : 0);
	const size_t block_size_y	= (h / thread_size_y) + ((h % thread_size_y) ? 1 : 0);
	const size_t n_threads		= thread_size_x * thread_size_y;
	const size_t n_blocks		= block_size_x * block_size_y;

	dim3 size_threads	(thread_size_x, thread_size_y, 1);
	dim3 size_blocks	(block_size_x , block_size_y , 1);
	
	// Allocate memory ------------------------------------------
	float*	d_texPartialSums_ptr_x = 0;
	cudaMalloc((void**)&d_texPartialSums_ptr_x, n_blocks*sizeof(float));
	float*	d_texPartialSums_ptr_y = 0;
	cudaMalloc((void**)&d_texPartialSums_ptr_y, n_blocks*sizeof(float));
	float*	d_texPartialSums_ptr_z = 0;
	cudaMalloc((void**)&d_texPartialSums_ptr_z, n_blocks*sizeof(float));
	int* d_count = 0;
	cudaMalloc((void**)&d_count, N_VIEWS_QUALITY*sizeof(int));
	cudaMemset((void*)d_count, 0, N_VIEWS_QUALITY*sizeof(int));
	//-------------------------------------------------------------

	reduce_textureBlock << <size_blocks, size_threads, n_threads*N_VIEWS_QUALITY*sizeof(float) >> >(
		d_texPartialSums_ptr_x, d_texPartialSums_ptr_y, d_texPartialSums_ptr_z, d_count, w, h, n_threads);

	//-------------------------------------------------------------
	// VECTOR REDUCTION
	//-------------------------------------------------------------

	// Launch configuration
	const size_t threads2_size = 1 << 9;
	size_t blocks_size = n_blocks / threads2_size + (n_blocks % threads2_size ? 1 : 0);
	blocks_size += blocks_size % 2 ? 1 : 0;		//< Ensure to be pair (how kernel is implemented)

	// Allocate memory ------------------------------------------
	float*	d_partialSums_ptr_x = 0;
	cudaMalloc((void**)&d_partialSums_ptr_x, (blocks_size + 1)*sizeof(float));
	float*	d_partialSums_ptr_y = 0;
	cudaMalloc((void**)&d_partialSums_ptr_y, (blocks_size + 1)*sizeof(float));
	float*	d_partialSums_ptr_z = 0;
	cudaMalloc((void**)&d_partialSums_ptr_z, (blocks_size + 1)*sizeof(float));
	//-------------------------------------------------------------

	block_reduction(d_partialSums_ptr_x, d_texPartialSums_ptr_x, n_blocks, blocks_size, threads2_size, d_sum + 0, d_count + 0);
	block_reduction(d_partialSums_ptr_y, d_texPartialSums_ptr_y, n_blocks, blocks_size, threads2_size, d_sum + 1, d_count + 1);
	block_reduction(d_partialSums_ptr_z, d_texPartialSums_ptr_z, n_blocks, blocks_size, threads2_size, d_sum + 2, d_count + 2);
		
	// deallocate device 	
	cudaFree(d_texPartialSums_ptr_x);
	cudaFree(d_texPartialSums_ptr_y);
	cudaFree(d_texPartialSums_ptr_z);
	cudaFree(d_partialSums_ptr_x);
	cudaFree(d_partialSums_ptr_y);
	cudaFree(d_partialSums_ptr_z);
	cudaFree(d_count);

	// unbid texture
	cudaUnbindTexture(tex_ref);
}


//-------------------------------------------------------------------------------------------
