
#ifndef __CUDA_FUNC_CUH__
#define __CUDA_FUNC_CUH__

// INCLUDE OPENGL
#include "GL/glew.h"

#include <iostream>

#include "cuda_runtime.h"				//cudaMemcpy	
#include "cuda_gl_interop.h"			//cuda-opengl
#include "cuda_texture_types.h"			//texture float4
#include "vector_types.h"				//float4
#include "device_launch_parameters.h"	// threadIdx
#include "texture_fetch_functions.h"	// tex2D

#define N_VIEWS_QUALITY		3

//-------------------------------------------------------------------------------------------

// Setup part 1 CUDA-GL: This can be done once! before rendering
cudaGraphicsResource* register_gl_cuda(GLuint text);

//-------------------------------------------------------------------------------------------
 
// Part 2 CUDA-GL: done after every render or every update
void texture_reduction(float* d_result, cudaGraphicsResource* graphicResc, unsigned int w, unsigned int h);

//-------------------------------------------------------------------------------------------

#endif //__CUDA_FUNC_CUH__
