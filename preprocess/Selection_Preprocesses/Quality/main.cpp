/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



/**
	TODO:
	 - Implement excluded-pixel (masks)

	Dev Note - Optimization:
	 - Do profiling
	 - Use Threadpool in computeMSE
	 - Or run computeMSE on multiple image at the same time
	 (- Not sure CUDA will help but test)

**/

#include <core/scene/ParseData.hpp>
#include <core/assets/Resources.hpp>

// Testers
//#include "OldSelectionTester.hpp"
//#include "ULRTester.hpp"
#include "Utils.hpp"

#include "QualityEstimator.hpp"
#include "SWARPTester.hpp"
#include "FPLANTester.hpp"
#include "PLANTester.hpp"

#include <projects/spixelwarp/renderer/Utils.hpp>
#include <projects/spixelwarp/renderer/PatchFile.hpp>
#include <core/graphics/Texture.hpp>
#include <projects/spixelwarp/renderer/SpixelImage.hpp>
#include <projects/spixelwarp/renderer/SpixelWarpScene.hpp>

#include <core/graphics/Window.hpp>
#include <core/view/MultiViewManager.hpp>
#include <core/raycaster/Raycaster.hpp>
#include <core/view/SceneDebugView.hpp>


#define PROGRAM_NAME "sibr_quality"
using namespace sibr;

static int	printUsage( void )
{
	std::cout << "Usage: " PROGRAM_NAME " --path <path/to/dataset> [--ulr]"		"\n"
		<< std::endl;
	return 1; // default error code for wrong usage
}

static std::vector<sibr::IQualityTestable::Ptr>	parseRequestedTests( const sibr::BasicIBRScene::Ptr& scene, 
							const std::string& dataSetPath)
{
	std::vector<sibr::IQualityTestable::Ptr>	renderers;

	//sibr::ProgramArg		argULR(ac, av, "--ulr", 0);
	//if (argULR)	renderers.emplace_back( new sibr::ULRTester() );

	//std::shared_ptr<sibr::SelectionRenderer>	selection(
	//	new sibr::SelectionRenderer(*scene, false, sibr::RENDERING_ALGOPRITHM::QUALITY_ESTIM) );

	// ... Future use ...

	//sibr::ProgramArg		argPLAN(ac, av, "--plan", 0);
	//if (argPLAN)	renderers.emplace_back( new sibr::OldSelectionTester(scene, selection, sibr::RENDERING_ALGOPRITHM::PLAN) );
	
	// Get PMVS points for use with the SWARP Tester.
	/// TODO: Put this inside the SWARPTester at some point.
	sibr::PatchFile pfile;
	std::string patchfile = pfile.search(dataSetPath+"/spixelwarp");
	pfile.load(patchfile);


	//std::vector<sibr:::SpixelImage::Ptr> _spixelImage = sibr:::loadSuperpixels(*scene);
	//std::vector<sibr::Texture2DRGBA32F::Ptr> _spixelMap = sibr:::loadSpixelMap(*scene, _spixelImage);

	/***** DEBUG / TMP *******/
	//renderers.emplace_back( new sibr::OldSelectionTester(scene, selection, sibr::RENDERING_ALGOPRITHM::SWARP) );
	//std::vector<std::vector<std::vector<sibr::PatchPoint> > >* vecpp = new std::vector<std::vector<std::vector<sibr::PatchPoint> > >();
	
	//sibr::loadAllSpixelMVSPoints(scene, pfile, _spixelImage, *vecpp);

	renderers.emplace_back(new sibr::SWARPTester(scene, pfile, dataSetPath));
	renderers.emplace_back(new sibr::FPLANTester(scene, pfile, dataSetPath));
	renderers.emplace_back(new sibr::PLANTester(scene, pfile, dataSetPath));
	//renderers.emplace_back( new sibr::OldSelectionTester(scene, selection, sibr::RENDERING_ALGOPRITHM::PLAN) );
	//renderers.emplace_back( new sibr::OldSelectionTester(scene, selection, sibr::RENDERING_ALGOPRITHM::FPLAN) );
	/*******************/

	return renderers;
}

struct QualityAppArgs : virtual SwarpAppArgs {
	Arg<std::string> debug = { "debug","" };
};

int main( int ac, char** av )
{
	//sibr::ProgramArg	argPath(ac, av, "--path", 1);

	// Parse Commad-line Args
	CommandLineArgs::parseMainArgs(ac, av);
	CommandLineArgs globalArgs = CommandLineArgs::getGlobal();
	QualityAppArgs myArgs;

	/*if (ac < 1+1 || !argPath)
		return printUsage();*/

 
	if (globalArgs.contains("debug")) {
		const bool doVSync = !myArgs.vsync;
		// rendering size
		uint rendering_width = myArgs.rendering_size.get()[0];
		uint rendering_height = myArgs.rendering_size.get()[1];
		// window size
		uint win_width = myArgs.win_width;
		uint win_height = myArgs.win_height;

		// Window setup
		sibr::Window        window(PROGRAM_NAME, sibr::Vector2i(50, 50), myArgs);

		// Setup Basic IBR Scene
		SpixelWarpScene::Ptr		scene(new SpixelWarpScene(myArgs));
		// check rendering size
		rendering_width = (rendering_width <= 0) ? scene->cameras()->inputCameras()[0]->w() : rendering_width;
		rendering_height = (rendering_height <= 0) ? scene->cameras()->inputCameras()[0]->h() : rendering_height;

		// check rendering size
		Vector2u usedResolution(rendering_width, rendering_height);
		std::cerr << "Used Resolution " << rendering_width << " x " << rendering_height << std::endl;

		const unsigned int sceneResWidth = usedResolution.x();
		const unsigned int sceneResHeight = usedResolution.y();


		// Raycaster.
		std::shared_ptr<sibr::Raycaster> raycaster = std::make_shared<sibr::Raycaster>();
		raycaster->init();
		raycaster->addMesh(scene->proxies()->proxy());

		// Camera handler for main view.
		sibr::InteractiveCameraHandler::Ptr generalCamera(new InteractiveCameraHandler());
		generalCamera->setup(scene->cameras()->inputCameras(), Viewport(0, 0, (float)usedResolution.x(), (float)usedResolution.y()), raycaster);


		// Create the Swarp view which passes data required to the renderer for each rendered image
		SelectionView::Ptr	selectionView = std::make_shared<SelectionView>
			(scene, 4, myArgs.dataset_path, sceneResWidth, sceneResHeight, true, generalCamera);

		// Add views to mvm.
		MultiViewManager        multiViewManager(window, false);
		multiViewManager.addIBRSubView("SpixelWarp view", selectionView, usedResolution, ImGuiWindowFlags_ResizeFromAnySide);
		multiViewManager.addCameraForView("SpixelWarp view", generalCamera);


		selectionView->onRender(window);
		while (window.isOpened())
		{

			sibr::Input::poll();
			window.makeContextCurrent();

			if (sibr::Input::global().key().isActivated(sibr::Key::Escape))
				window.close();

			multiViewManager.onUpdate(sibr::Input::global());
			multiViewManager.onRender(window);
			selectionView->onUpdate(sibr::Input::global());
			selectionView->onRender(window);

			//George : get the spixel here : 
			sibr::Spixel::Ptr sPixelDebug = selectionView->_selectedSpixelDebug;
			window.swapBuffer();

		}

	}
	else {
		sibr::Window::Ptr					window(new sibr::Window(100, 100, "QualityEstimator"));
		SpixelWarpScene::Ptr				scene(new SpixelWarpScene(myArgs));
		sibr::QualityEstimator				estimator(window, scene);
		//how to load proxy in the new sibr ?	scene->loadProxy(true);
		std::vector<sibr::IQualityTestable::Ptr>	renderers =
			parseRequestedTests(scene, myArgs.dataset_path.get());

		if (renderers.empty())
		{
			SIBR_WRG << "No test requested.";
			return printUsage();
		}

		for (sibr::IQualityTestable::Ptr r : renderers)
			estimator.addRenderer(r);

		if (estimator.estimate() == false)
			return -1;

		std::string outputFolder = myArgs.dataset_path.get() + "/spixelwarp/densities/";

		estimator.save(outputFolder);

		//delete renderers[0].get();

	}

	return 0;
}
