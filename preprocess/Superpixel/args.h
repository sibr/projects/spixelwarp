/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _ARGS_H_
#define _ARGS_H_

class Args {
public:
    Args(Args & ref)
    {
        _segment    = ref.segment();
        _graph      = ref.graph();
        _wc         = ref.w_comp();
        _n          = ref.n_spixel();
        _path       = ref.path();
    }

    Args(int argc, char** argv) :
      _segment(false),
      _graph(false),
      _wc(10),
      _n(1100),
      _path("")
    {
      std::string desc(
          "\nUsage\n  ./superpixel [--path path] [--help] [--segment] [--graph] "
          "[--n number] [--wc wc]\n\n"
          "\t path    \t directory containing images [compulsory] \n"
          "\t segment \t run over segmentation \n"
          "\t graph   \t write spixel graphs \n"
          "\t n       \t number of superpixels \n"
          "\t wc      \t compactness weight \n"
          "\t help    \t show help message \n"
          );

      try {
        for (int i=1; i<argc; i++) {
          std::string arg = argv[i];

          if (!arg.compare("-help") || !arg.compare("--help")) {
            throw std::runtime_error("Showing help message");
          }

          if (!arg.compare("-segment") || !arg.compare("--segment")) {
            _segment = true;
          }

          if (!arg.compare("-graph") || !arg.compare("--graph")) {
            _graph = true;
          }

          else if (!arg.compare("-n") || !arg.compare("--n")) {
            if ((i+1) < argc)
              _n = atoi(argv[++i]);
            else
              throw std::runtime_error("--n requires the number of superpixels");
          }

          else if (!arg.compare("-path") || !arg.compare("--path")) {
            if ((i+1) < argc)
              _path = argv[++i];
            else
              throw std::runtime_error("--path requires dataset path");
          }

          else if (!arg.compare("-n") || !arg.compare("--n")) {
            if ((i+1) < argc)
              _n = atoi(argv[++i]);
            else
              throw std::runtime_error("--n requires number of superpixels");
          }

          else if (!arg.compare("-wc") || !arg.compare("--wc")) {
            if ((i+1) < argc)
              _wc = atoi(argv[++i]);
            else
              throw std::runtime_error("--wc requires compactness weight");
          }


        }

        if (_path.empty())
          throw std::runtime_error("path to dataset directory not specified");

        if (_path.at(_path.length()-1) != '/') _path.append("/");

        if (!_segment && !_graph) {
          _segment = true;
          _graph   = true;
        }

      } catch (std::runtime_error& e) {
        std::cout << std::endl << e.what() << std::endl << desc << std::endl;
        exit(EXIT_FAILURE);
      }
    }

    int         n_spixel(void) const { return _n;        }
    int         w_comp  (void) const { return _wc;       }
    bool        segment (void) const { return _segment;  }
    bool        graph   (void) const { return _graph;    }
    std::string path    (void) const { return _path;     }

private:
    bool  _segment;
    bool  _graph;
    int   _wc;
    int   _n;
    std::string _path;
};

#endif  // _ARGS_H_
