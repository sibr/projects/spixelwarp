/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <core/system/Matrix.hpp>

class Camera {
private:
    uint  _id;
    uint  _w;
    uint  _h;

    sibr::Vector3f   _pos;
    sibr::Vector3f   _dir;
    sibr::Vector3f   _side;
    sibr::Vector3f   _up;
    float _f;

    std::vector< std::pair<uint,sibr::Vector2i> >  _mvsPnts;

public:
    Camera(uint id, float m[15], uint w, uint h) {
        float r[9], t[3];

        _id = id;
        _w  = w;
        _h  = h;

        _f = (float)(2.0f * atan( 0.5* _h / m[0]));

        for(int i=0; i<9; i++) r[i] = m[3+i];
        for(int i=0; i<3; i++) t[i] = m[12+i];

        _pos[0] = -(r[0]*t[0] + r[3]*t[1] + r[6]*t[2]);
        _pos[1] = -(r[1]*t[0] + r[4]*t[1] + r[7]*t[2]);
        _pos[2] = -(r[2]*t[0] + r[5]*t[1] + r[8]*t[2]);

		_dir  = (sibr::Vector3f(-r[6],-r[7],-r[8])).normalized();
        _side = (sibr::Vector3f( r[0], r[1], r[2])).normalized();
        _up   = (sibr::cross(_side, _dir)).normalized();

        _mvsPnts.clear();
    }

    uint   w          (void)        const { return _w;			                          }
    uint   h          (void)        const { return _h;			                          }
    float  aspect     (void)        const { return float(_w)/_h;                      }
    uint   numMVSPnts (void)        const { return (uint)_mvsPnts.size();                   }
    uint   mvsPntID   (uint i)      const { return _mvsPnts[i].first;                 }
    sibr::Vector2i    mvs2DPos   (uint i)      const { return _mvsPnts[i].second;                }
    void   addMVSPnt  (uint i,sibr::Vector2i p)      { _mvsPnts.push_back(std::make_pair(i,p));  }

    sibr::Vector2i project(sibr::Vector3f p3d) const {
		sibr::Matrix4f proj  = sibr::perspective(_f, aspect(), 0.01f, 100.0f);
		sibr::Matrix4f mview = sibr::lookAt(_pos, sibr::Vector3f(_pos+_dir), sibr::Vector3f((sibr::cross(_side,_dir)).normalized()));
        sibr::Vector4f p3d_t   = proj * mview * sibr::Vector4f(p3d[0],p3d[1],p3d[2],1.0f);
        p3d_t       = p3d_t / p3d_t[3];
        sibr::Vector2i p2d     = sibr::Vector2i((int)((p3d_t[0]+1.0f)*_w/2.), (int)((1.0f-p3d_t[1])*_h/2.));  /* (0,w);(0,h) starting from top */
        return p2d;
    }
};

#endif // _CAMERA_H_
