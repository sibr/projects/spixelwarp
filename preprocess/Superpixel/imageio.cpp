/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <core/graphics/Image.hpp>
#include <core/system/String.hpp>
#include <projects/spixelwarp/renderer/PatchFile.hpp>
#include <projects/spixelwarp/renderer/PatchPoint.hpp>

#include "precompiled.h"
#include "imageio.h"
#include "camera.h"
#include "spimg_int.h"
#include "mvs.h"

#define MIN_MVS_PHOTOCONSISTENCY 0.85

ImageIO::ImageIO(std::string path) : _path(path) {

	if (sibr::directoryExists(_path + "/half_size/")) {
		_swarpPath = _path + "/";
	}
	else {
		_swarpPath = _path + "/spixelwarp/";
	}
}

uint* ImageIO::getImageBuffer(std::string f, uint& w, uint& h) 
{
    sibr::ImageRGB img;
    if( !img.load(f, false) ) {
		std::cerr << "[SIBR superpixel] Cant open " << f << " exiting." << std::endl;
		exit(1);
	}
    w = img.w();
    h = img.h();

    uint* iBuff = new uint[w*h];
    for (uint j=0; j<h; j++) {
        for (uint i=0; i<w; i++) {
            uint color = 0x0;
            uint r = img(i,j)[0]; color = color | (r<<16);
            uint g = img(i,j)[1]; color = color | (g<<8);
            uint b = img(i,j)[2]; color = color | (b<<0);
            iBuff[ j*w + i] = color;
        }
    }
    return iBuff;
}

void ImageIO::saveImageBuffer(uint* iBuff, uint w, uint h, std::string s) 
{
    sibr::ImageRGB img(w,h);
    for (uint j=0; j<h; j++) {
        for (uint i=0; i<w; i++) {
            uint pixel = iBuff[ j*w + i];
            img(i,j)[0] = (pixel & 0x00ff0000)>> 16;
            img(i,j)[1] = (pixel & 0x0000ff00)>> 8;
            img(i,j)[2] = (pixel & 0x000000ff)>> 0;
        }
    }
    std::string outFileName = _swarpPath + "/superpixels/" + s + ".png";
    img.save(outFileName);
}

void ImageIO::saveSPixelLabels(int* labels, int w, int h, std::string s) {
    std::string finalpath = _swarpPath + "/superpixels/" + s + ".sp";
	
	std::ofstream outfile;
	outfile.exceptions( std::ifstream::failbit | std::ifstream::badbit );
	try
	{
		outfile.open(finalpath, std::ios::binary | std::ios::trunc | std::ios::out);
		std::cout << "Writing to " << finalpath << std::endl;
		outfile.write((const char*) &w, sizeof(int));
		outfile.write((const char*) &h, sizeof(int));
		outfile.write((const char*) labels, w*h*sizeof(int));
	}
	catch (std::ofstream::failure e)
	{
		std::cout << "Error while writing to " << finalpath <<  " [" << e.what() << "]" << std::endl;
	}

}

void ImageIO::loadBundleFile(void) {
    std::ifstream bundle_file;
    std::string bundle_file_str;
    std::string image_file_str;
    std::string line;

    int numCamera = 0;
    int numPoints = 0;

    bundle_file_str = _path + std::string("cameras/bundle.out");

	if (!sibr::fileExists(bundle_file_str))
		bundle_file_str = _path + std::string("spixelwarp/cameras/bundle.out");
	else if (!sibr::fileExists(bundle_file_str)) {                                        /* ignore if both not present */
		return;
	}

    bundle_file.open(bundle_file_str.c_str());
    SIBR_ASSERT(bundle_file.is_open());

    std::cout << "[BundleFile] loading from " << bundle_file_str.c_str() << std::endl;

    getline(bundle_file, line);
    bundle_file >> numCamera >> numPoints;

    for (int i=0; i<numCamera; i++) {
        float m[15];
        bundle_file >> m[0] >> m[1] >> m[2];
        bundle_file >> m[3] >> m[4] >> m[5];
        bundle_file >> m[6] >> m[7] >> m[8];
        bundle_file >> m[9] >> m[10]>> m[11];
        bundle_file >> m[12]>> m[13]>> m[14];

		std::string image_dir = _path + "/images/";
		if (!sibr::directoryExists(image_dir))
			image_dir = _path + "/spixelwarp/images/";

        image_file_str = image_dir + sibr::sprint("%08d.jpg",i);
		sibr::ImageRGB *img = new sibr::ImageRGB();
		if( !img->load(image_file_str) ) {
			std::cerr << "[SIBR superpixel] Cant open " << image_file_str << " exiting." << std::endl;
			exit(1);
		}
        std::shared_ptr<sibr::ImageRGB> image(img);
        std::shared_ptr<Camera> cam(new Camera(i,m,image->w(),image->h()));
        _camera.push_back(cam);
        std::cout << "[BundleFile] " << image_file_str.c_str() << " " << sibr::Vector2i(image->w(),image->h()) << std::endl;
    }

    bundle_file.close();
    std::cout << "[BundleFile] " << _camera.size() << " / "  << numCamera << " images" << std::endl;
}

void ImageIO::loadPMVSPoints(void) {
    std::string pmvs_str;
    std::vector<std::string> pmvs_file_names;
    pmvs_file_names.push_back("pmvs/models/option-highres.txt.patch");
    pmvs_file_names.push_back("pmvs/models/pmvs_options.patch");
    pmvs_file_names.push_back("pmvs/models/pmvs_recon.patch");
    pmvs_file_names.push_back("pmvs/models/proxy.patch");
    pmvs_file_names.push_back("pmvs/models/proxy.patch");
    pmvs_file_names.push_back("proxy/proxy.patch");
    pmvs_file_names.push_back("spixelwarp/meshes/pmvs_recon.patch");
	pmvs_file_names.push_back("meshes/pmvs_recon.patch");
    for (uint i=0; i<pmvs_file_names.size(); i++) {
        pmvs_str = _path + pmvs_file_names[i];
        if (sibr::fileExists(pmvs_str))
            break;
    }

    //std::ifstream pmvs_file(pmvs_str.c_str());
    //SIBR_ASSERT(pmvs_file.is_open());

    //std::string line;
    //int numPoints = 0;

    //std::cout << "\n[PMVSPoints] " << pmvs_str.c_str() << std::endl;
    //getline(pmvs_file, line);
    //pmvs_file >> numPoints;
	sibr::PatchFile patchFile;
	if (patchFile.load(pmvs_str) == false)
	{
		SIBR_ERR << "cannot load pmvs file: " << pmvs_str << std::endl;
		exit(EXIT_FAILURE);
	}


	for (int i=0; i<patchFile.pointCount(); i++) {
		const sibr::PatchPoint& point = patchFile.point(i);
        //int numCam=0, cam;
        //std::vector<uint> camList;
   //     sibr::Vector4f p, n, t;

   //     do {
   //       if(!pmvs_file.eof())
		 // {
   //         getline(pmvs_file, line);
		 // }
   //       else
   //       {
   //         pmvs_file.close();
			//std::cout << "[PMVSPoints] WARNING: We reached the end of the file whereas we didn't parse the total number of points:" << numPoints << std::endl;
   //         std::cout << "[PMVSPoints] total " << i << " points" << std::endl;
   //         return;
   //       }
   //     } while (line.compare("PATCHS"));
   //     pmvs_file >> p[0] >> p[1] >> p[2] >> p[3];
   //     pmvs_file >> n[0] >> n[1] >> n[2] >> n[3];
   //     pmvs_file >> p[3] >> t[1] >> t[2];
   //     pmvs_file >> numCam;
   //     for (int j=0; j<numCam; j++) {
   //         pmvs_file >> cam;
   //         camList.push_back(cam);
   //     }
   //     getline(pmvs_file, line);                              /* one integer */
   //     getline(pmvs_file, line);                              /* some images ID's */
   //     getline(pmvs_file, line);                              /* blank line */
   //     SIBR_ASSERT(camList.size() == uint(numCam));
   //     std::shared_ptr<MVSPoint> m(new MVSPoint(sibr::Vector3f(p[0],p[1],p[2]),p[3]));

		sibr::Vector3f p = sibr::Vector3f(point.position()[0], point.position()[1], point.position()[2]);
        std::shared_ptr<MVSPoint> m(new MVSPoint(p, 1.f));

		const sibr::PatchImageList& camList = point.visibleInImages();
		for (uint j=0; j<camList.size(); j++) {
            int k = camList[j];
            sibr::Vector2i pixel = _camera[k]->project(p);
            m->addCamera(k,pixel);
            _camera[k]->addMVSPnt(i,pixel);
        }
        _mvs.push_back(m);
    }
    //pmvs_file.close();
	std::cout << "[PMVSPoints] total " << patchFile.pointCount() << " points" << std::endl;
}

void ImageIO::mergeSuperPixels(void) {
    _spimg.resize(_camera.size());

    for (uint i=0; i<_spimg.size(); i++)
        _spimg[i].reset(new SPixelImageInternal(_swarpPath,i));             /* allocate superpixel image */

    /*-----------------------------------------------------------------------------
     *  Create graph; each MVS point gives edges between spixels from each image
     *  it was observed
     *-----------------------------------------------------------------------------*/
    std::cout << "[Superpixels] creating superpixel graph" << std::endl;
    std::vector< std::vector< std::pair<uint,uint> > > mergeGraph;
    for (uint i=0; i<_mvs.size(); i++) {                     /* for each MVS point */
        if (_mvs[i]->confidence() < MIN_MVS_PHOTOCONSISTENCY)
            continue;
        std::vector< std::pair<uint,uint> > mergeList;
        std::vector< std::pair<uint,sibr::Vector2i> > camList =_mvs[i]->cameraList();
        for (uint j=0; j<camList.size(); j++) {                /* go to its image list */
            uint c   = camList[j].first;                         /* find image id */
            sibr::Vector2i  v   = camList[j].second;                        /* find pixel coordinates in each image */
            if (_spimg[c].get() && _spimg[c]->ready()) {
                float s  = float(_spimg[c]->w())/float(_camera[c]->w());  /* input image & spixel image may not be same size */
                uint px  = uint(std::max(0,v[0])*s);
                uint py  = uint(std::max(0,v[1])*s);
if(py >= _spimg[c]->h() || px >= _spimg[c]->w())  { // Seb: replaced '>' by '>=' (I got pixels on the edge; because of scale changes?)
static int cnt =0;
if(cnt++ < 200 ) {
	std::cerr << "********** MVS Point " << i << " py " << py << " v " << v << " Camera " << c << std::endl;
	std::cerr << "********** MVS s value " << s << std::endl;
}
}
else {
                uint id  = _spimg[c]->spixel(px,py);               /* find superpixel id in each image */
                mergeList.push_back(std::make_pair(c,id));         /* add superpixel to be merged */
}
            }
        }
        if (mergeList.size() > 1) {
            mergeGraph.push_back(mergeList);
        }
    }

    /*-----------------------------------------------------------------------------
     *  Save graph structure to file
     *-----------------------------------------------------------------------------*/
    std::cout << "[Superpixels] saving graph to file" << std::endl;
    std::ofstream file((_swarpPath+"/spixel_graph.txt").c_str());
    SIBR_ASSERT(file.is_open());
    file << mergeGraph.size() << std::endl;
    for (uint i=0; i<mergeGraph.size(); i++) {
        file << mergeGraph[i].size();
        for (uint j=0; j<mergeGraph[i].size(); j++) {
            uint camera_id = mergeGraph[i][j].first;
            uint spixel_id = mergeGraph[i][j].second;
            _spimg[camera_id]->addNeighbors(spixel_id,mergeGraph[i]); /* add neighbors in superpixel image */
            file << " " << camera_id << " " << spixel_id;
        }
        file << std::endl;
    }
    file.close();
    mergeGraph.clear();

    return;

    /*-----------------------------------------------------------------------------
     *  Identify connected components
     *-----------------------------------------------------------------------------*/
    std::cout << "[Superpixels] identifying connected components" << std::endl;
    std::vector< std::vector< std::pair<uint,uint> > > segments;    /* segment = tuple list (image,spixel) */
    std::vector< std::set<uint> > all_spixels;                      /* set of all superpixels for each image */

    all_spixels.resize(_spimg.size());                              /* initialize all_spixels with all superpixels */
    for (uint i=0; i<_spimg.size(); i++) {                          /* remove each spixel touched during breadth-first traversal */
        if (_spimg[i].get() && _spimg[i]->ready()) {
            std::set<uint> image_spixels;
            for (uint j=0; j<_spimg[i]->spixels(); j++)
                image_spixels.insert(j);
            all_spixels[i] = image_spixels;
        } else {
            all_spixels[i].clear();
        }
    }

    /*-----------------------------------------------------------------------------
     *  Breadth-first traversal
     *-----------------------------------------------------------------------------*/
    std::cout << "[Superpixels] starting breadth-first traversal" << std::flush;
    uint segment_id = 0;
    for (uint i=0; i<_spimg.size(); i++) {                        /* remove each spixel traversed during BFS */
        while (!all_spixels[i].empty()) {                           /* until all superpixels from each image traversed */
            std::vector< std::pair<uint,uint> > s;                    /* connected component */
            std::queue < std::pair<uint,uint> > q;                    /* queue of 'to be processed' nodes for BFS */

            q.push(std::make_pair(i,*(all_spixels[i].begin())));      /* mark root as 'to be processed' and start */
            all_spixels[i].erase(all_spixels[i].begin());             /* mark root as traversed */

            do {
                uint curr_camera = q.front().first;                     /* current graph node */
                uint curr_spixel = q.front().second;
                q.pop();                                                /* remove from 'to be processed' list */
                s.push_back(std::make_pair(curr_camera, curr_spixel));  /* add as node of connected component */

                _spimg[curr_camera]->setSegmentID(curr_spixel, segment_id);

                std::vector< std::pair<uint,uint> > v = _spimg[curr_camera]->getNeighbors(curr_spixel);  /* all neighbors */
                for (uint k=0; k<v.size(); k++) {
                    std::set<uint>::iterator it = all_spixels[v[k].first].find(v[k].second);
                    if (it != all_spixels[v[k].first].end()) {                                        /* if not traversed */
                        q.push(v[k]);                                                                   /* add to 'to be processed' list */
                        all_spixels[v[k].first].erase(it);                                              /* mark as traversed */
                    }
                }
            } while (!q.empty());
            segments.push_back(s);        /* add connected component just extracted to overall list */
            segment_id++;
        }
    }

    std::map<uint,sibr::Vector3u> colorMap;
    for (uint i=0; i<segments.size(); i++)
        colorMap[i] = sibr::Vector3u(rand()%255,rand()%255,rand()%255);

#pragma omp parallel for
    for (int i=0; i<_spimg.size(); i++)
        if (_spimg[i].get() && _spimg[i]->ready())
            _spimg[i]->saveSegmentation(colorMap);
}
