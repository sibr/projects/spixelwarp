/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _IMAGE_IO_
#define _IMAGE_IO_

class Args;
class Camera;
class MVSPoint;
class SPixelImageInternal;

class ImageIO {
private:
    std::string  _path;
	std::string	 _swarpPath;

    std::vector<std::shared_ptr<Camera> >       _camera;
    std::vector<std::shared_ptr<MVSPoint> >     _mvs;
    std::vector<std::shared_ptr<SPixelImageInternal> >  _spimg;

public:
    ImageIO(std::string);

    uint* getImageBuffer  (std::string, uint&, uint&);
    void  saveImageBuffer (uint*, uint, uint, std::string);
    void  saveSPixelLabels(int*, int, int, std::string);
    void  loadBundleFile  (void);
    void  loadPMVSPoints  (void);
    void  mergeSuperPixels(void);
};

#endif // _IMAGE_IO_
