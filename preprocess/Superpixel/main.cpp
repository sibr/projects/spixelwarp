/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <core/system/Utils.hpp>
#include <core/assets/Config.hpp>
#include <core/system/CommandLineArgs.hpp>
#include <core/scene/BasicIBRScene.hpp>

#include "precompiled.h"
#include "slic.h"
#include "imageio.h"
//#include "args.h"
#include <omp.h>

#define MAX_IMAGES  500

/// \todo TODO make into a struct or something
using namespace sibr;

struct SuperpixelArgs :
	virtual BasicIBRAppArgs {
	Arg<bool> segment = { "segment" };
	Arg<bool> graph = { "graph" };
	Arg<int> n = { "n", 1100 };
	Arg<int> wc = { "wc", 10 };
	Arg<std::string> ext = { "ext", ".jpg" };
};

std::vector<bool> use_flag;
SuperpixelArgs *g_args;


void run(int i) {
	std::string image_name = sibr::imageIdToString(i);
	
	std::string swarpPath;

	if (sibr::directoryExists(g_args->dataset_path.get() + "/half_size/")) {
		swarpPath = g_args->dataset_path.get();
	}
	else {
		swarpPath = g_args->dataset_path.get() + "/spixelwarp/";
	}
	std::string image_file = swarpPath + "/half_size/" + image_name + g_args->ext.get();  /* try half size image */

	if (!sibr::fileExists(image_file))                                        /* try the original image */
		image_file = swarpPath + "/images/" + image_name + g_args->ext.get();
	else if (!sibr::fileExists(image_file)) {                                        /* ignore if both not present */
		return;
	}

	uint w=0, h=0;

	ImageIO* m_imageio = new ImageIO(swarpPath);
	uint* image = m_imageio->getImageBuffer(image_file, w, h);

	SLIC *slic = new SLIC(g_args->n, g_args->wc);

	std::cout << "[MainApp] over-segmenting " << image_file << std::endl;

	slic->overSegment(image, w, h);

	std::vector<int> labels = slic->labels();
	SIBR_ASSERT(!labels.empty());

	slic->drawContours(image, 0xffff0000);
	m_imageio->saveSPixelLabels(&(labels[0]),w, h, image_name);
	m_imageio->saveImageBuffer (image, w, h, image_name);
	std::cout << "[MainApp] " << image_file << " => " << slic->num_spixels() << " spixels" << std::endl;

	delete image;
	delete slic;
}

//};

/// \todo TODO -- needs to be converted to SIBR properly, change ImageIO

int main(int argc, char** argv) {
        //std::shared_ptr<Args>     args(g_args=new Args(argc, argv));

		CommandLineArgs::parseMainArgs(argc, argv);
		SuperpixelArgs myArgs;
		g_args = &myArgs;

		BasicIBRScene::SceneOptions so;

		// no opengl context cant load texture
		so.texture = false;
		so.renderTargets = false;

		BasicIBRScene scene(myArgs, so);

        std::shared_ptr<ImageIO>  imageio(new ImageIO(myArgs.dataset_path));
		int max_img=0;

        int numCamera=0, numPoints=0;
        std::ifstream bundle_file;
        std::string image_dir = myArgs.dataset_path.get() + "/images/";
        std::string bundle_file_str = myArgs.dataset_path.get() + "/cameras/" + "bundle.out";
        std::string line;
				
		if (!sibr::fileExists(bundle_file_str) || !sibr::directoryExists(image_dir)) {                                        /* ignore if both not present */
			return 0;
		}

        bundle_file.open(bundle_file_str.c_str());
        std::getline(bundle_file, line);
        bundle_file >> numCamera >> numPoints;
        use_flag.resize(numCamera,true);
		max_img=numCamera;

		use_flag = scene.data()->activeImages();

        /*
		std::ifstream mask_file((myArgs.dataset_path.get() +"/" + "active_images.txt").c_str());
        if (mask_file.is_open()) {
             std::fill(use_flag.begin(), use_flag.end(), false);
             while (!mask_file.eof()) {
                int t;
                mask_file >> t;
                if (t>=0 && t<numCamera) {
                  use_flag[t] = true;
				}
            }
        }*/

		if (sibr::directoryExists(myArgs.dataset_path.get() + "/spixelwarp/"))
			sibr::makeDirectory(myArgs.dataset_path.get() + "/spixelwarp/superpixels");
		else
			sibr::makeDirectory(myArgs.dataset_path.get() + "/superpixels");
  
        if (myArgs.segment) {
           std::cout << "[MainApp] running over-segmentation for " << myArgs.dataset_path << std::endl;

		   std::cerr << "MAX THREADS " << omp_get_max_threads() << std::endl;
// let OMP handle parallelism
			#pragma omp parallel for 
		   for (int i = 0; i < max_img; i++) {
			   if (use_flag[i]){
				   run(i);
			   }
		   }
        }

        if (myArgs.graph) {
            std::cout << std::endl << "[MainApp] superpixel graph for " << myArgs.dataset_path << std::endl;
            imageio->loadBundleFile();
            imageio->loadPMVSPoints();
            imageio->mergeSuperPixels();
        }

    	return EXIT_SUCCESS;
}
