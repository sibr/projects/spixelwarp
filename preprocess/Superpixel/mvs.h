/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _MVS_H_
#define _MVS_H_

class MVSPoint {
private:
    sibr::Vector3f   _p;
    float _conf;
    std::vector< std::pair<uint,sibr::Vector2i> >  _pos2D;

public:
    MVSPoint(sibr::Vector3f p,float c) : _p(p), _conf(c) {}

    sibr::Vector3f   pos3D     (void) const  { return _p;    }
    float confidence(void) const  { return _conf; }

    void addCamera(uint i,sibr::Vector2i p) {
        _pos2D.push_back(std::make_pair(i,p));
    }

    std::vector< std::pair<uint,sibr::Vector2i> > cameraList(void) const {
        return _pos2D;
    }
};

#endif // _MVS_H_
