/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _PRECOMPILED_H_
#define _PRECOMPILED_H_

#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cfloat>
#include <iostream>
#include <fstream>
#include <string>
#include <queue>
#include <vector>
#include <map>
#include <set>
#include <memory>

#include <core/system/Vector.hpp>
#include <core/system/Utils.hpp>
#include <core/system/Array2d.hpp>

#endif // _PRECOMPILED_H_
