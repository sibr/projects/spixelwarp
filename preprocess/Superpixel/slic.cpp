/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <core/system/LoadingProgress.hpp>
#include "precompiled.h"
#include "slic.h"
#include "spixel_int.h"

#define MAX_ITERATIONS 120

static const int dx4[4] = {-1,  0,  1,  0};
static const int dy4[4] = { 0, -1,  0,  1};
static const int dx8[8] = {-1, -1,  0,  1, 1, 1, 0, -1};
static const int dy8[8] = { 0, -1, -1, -1, 0, 1, 1,  1};

SLIC::SLIC(uint k, uint m) :
    _w(0),
    _h(0),
    _numspixels(0),
    _K(k),
    _M(m)
{}


sibr::Vector3d SLIC::rgb2lab(sibr::Vector3i rgb) {
    sibr::Vector3d XYZ = rgb2xyz(rgb);

    double epsilon = 0.008856;  //actual CIE standard
    double kappa   = 903.3;     //actual CIE standard

    double Xr = 0.950456;       //reference white
    double Yr = 1.0;            //reference white
    double Zr = 1.088754;       //reference white

    double xr = XYZ[0]/Xr;
    double yr = XYZ[1]/Yr;
    double zr = XYZ[2]/Zr;

    double fx, fy, fz;
    if(xr > epsilon)  fx = pow(xr, 1.0/3.0);
    else        fx = (kappa*xr + 16.0)/116.0;
    if(yr > epsilon)  fy = pow(yr, 1.0/3.0);
    else        fy = (kappa*yr + 16.0)/116.0;
    if(zr > epsilon)  fz = pow(zr, 1.0/3.0);
    else        fz = (kappa*zr + 16.0)/116.0;

    return sibr::Vector3d(116.0*fy-16.0, 500.0*(fx-fy), 200.0*(fy-fz));
}

sibr::Vector3d SLIC::rgb2xyz(sibr::Vector3i rgb) {
    double R = rgb[0]/255.0;
    double G = rgb[1]/255.0;
    double B = rgb[2]/255.0;
    double r, g, b;

    if(R <= 0.04045)  r = R/12.92;
    else        r = pow((R+0.055)/1.055,2.4);
    if(G <= 0.04045)  g = G/12.92;
    else        g = pow((G+0.055)/1.055,2.4);
    if(B <= 0.04045)  b = B/12.92;
    else        b = pow((B+0.055)/1.055,2.4);

    return sibr::Vector3d( r*0.4124564 + g*0.3575761 + b*0.1804375,
            r*0.2126729 + g*0.7151522 + b*0.0721750,
            r*0.0193339 + g*0.1191920 + b*0.9503041);
}

void SLIC::convertRGB2LAB(uint* ubuff) {
    uint sz = _w*_h;
    _lvec.resize(sz,0);
    _avec.resize(sz,0);
    _bvec.resize(sz,0);

    for (uint j=0; j<sz; j++) {
        int r = (ubuff[j] >> 16) & 0xFF;
        int g = (ubuff[j] >>  8) & 0xFF;
        int b = (ubuff[j]      ) & 0xFF;
        sibr::Vector3d lab = rgb2lab(sibr::Vector3i(r,g,b));
        _lvec[j] = lab[0];
        _avec[j] = lab[1];
        _bvec[j] = lab[2];
    }
}

void SLIC::drawContours(uint* ubuff, uint color) {
    uint mainindex = 0;

    std::vector<bool> istaken(_labels.size(), false);

    for (uint j=0; j<_h; j++) {
        for (uint k=0; k< _w; k++) {
            uint np = 0;
            for (uint i= 0; i<8; i++) {
                int x = k + dx8[i];
                int y = j + dy8[i];
                if ((x>=0 && x<(int)_w) && (y>=0 && y<(int)_h)) {
                    uint index = y*_w + x;
                    if (!istaken[index]) //comment this to obtain internal contours
                        if (_labels[mainindex] != _labels[index])
                            np++;
                }
            }
            if (np > 1) { //change to 2 or 3 for thinner lines
                ubuff[mainindex] = color;
                istaken[mainindex] = true;
            }
            mainindex++;
        }
    }
}

std::vector<double> SLIC::edgesColor(void) {
    uint sz = _w*_h;
    std::vector<double> edges(sz,0);

    for (uint j=1; j<_h-1; j++) {
        for (uint k=1; k<_w-1; k++) {
            uint i = j*_w+k;
            double dx = (_lvec[i-1]-_lvec[i+1])*(_lvec[i-1]-_lvec[i+1]) + // Horizontal color
                (_avec[i-1]-_avec[i+1])*(_avec[i-1]-_avec[i+1]) + // edge
                (_bvec[i-1]-_bvec[i+1])*(_bvec[i-1]-_bvec[i+1]);

            double dy = (_lvec[i-_w]-_lvec[i+_w])*(_lvec[i-_w]-_lvec[i+_w]) + // Vertical color
                (_avec[i-_w]-_avec[i+_w])*(_avec[i-_w]-_avec[i+_w]) + // edge
                (_bvec[i-_w]-_bvec[i+_w])*(_bvec[i-_w]-_bvec[i+_w]);

            edges[i] = (dx + dy);
        }
    }
    return edges;
}

void SLIC::perturbSeeds(std::vector<double> edges) {
    for (uint n = 0; n<_kseedsl.size(); n++) {
        uint ox = (uint)_kseedsx[n];     //original x
        uint oy = (uint)_kseedsy[n];     //original y
        uint oind = oy*_w + ox;

        uint storeind = oind;
        for (uint i = 0; i<8; i++) {
            int nx = ox + dx8[i]; //new x
            int ny = oy + dy8[i]; //new y
            if (nx>=0 && nx<(int)_w && ny>=0 && ny<(int)_h) {
                uint nind = ny*_w + nx;
                if (edges[nind] < edges[storeind])
                    storeind = nind;
            }
        }
        if (storeind != oind) {
            _kseedsx[n] = storeind%_w;
            _kseedsy[n] = storeind/_w;
            _kseedsl[n] = _lvec[storeind];
            _kseedsa[n] = _avec[storeind];
            _kseedsb[n] = _bvec[storeind];
        }
    }
}

void SLIC::getLABXYSeeds(std::vector<double> edgemag) {
    uint xoff = uint(_w / sqrt( (float)_K) );
    uint yoff = uint(_h / sqrt( (float)_K) );

    uint n = 0;
    for (uint y=yoff/2; y<_h; y+=yoff) {
        uint Y = y;
        if (Y > _h-1) break;

        for(uint x=xoff/2; x<_w; x+=xoff) {
            uint X = x;
            if (X>_w-1) break;

            uint i = Y*_w + X;

            _kseedsl.push_back(_lvec[i]);
            _kseedsa.push_back(_avec[i]);
            _kseedsb.push_back(_bvec[i]);
            _kseedsx.push_back(X);
            _kseedsy.push_back(Y);
            n++;
        }
    }

    perturbSeeds(edgemag);
}

void SLIC::overSegmentUsingSeeds(std::vector<double> edgemag) {
    uint sz     = _w*_h;
    uint numk   = (uint)_kseedsl.size();

    std::vector<double> clustersize(numk, 0);
    std::vector<double> inv(numk, 0);   //to store 1/clustersize[k] values

    std::vector<double> sigmal(numk, 0);
    std::vector<double> sigmaa(numk, 0);
    std::vector<double> sigmab(numk, 0);
    std::vector<double> sigmax(numk, 0);
    std::vector<double> sigmay(numk, 0);
    std::vector<double> distvec(sz, DBL_MAX);

    _klabels.resize(sz,-1);
    _labels .resize(sz,-1);

    uint offset = uint(sqrt(double(sz)/_K));
    double w_compact = double(_M)/offset;

	std::ostringstream ossProgress;
	ossProgress << "Segmentation Thread " << omp_get_thread_num();
	sibr::LoadingProgress progress(MAX_ITERATIONS, ossProgress.str());
    for (uint itr=0; itr<MAX_ITERATIONS; itr++) {
		progress.walk();

        distvec.assign(distvec.size(),DBL_MAX);
        for (uint n=0; n<numk; n++) {
            double y1 = std::max<double>(0,        _kseedsy[n]-2*offset);
            double y2 = std::min<double>(_h, _kseedsy[n]+2*offset);
            double x1 = std::max<double>(0,        _kseedsx[n]-2*offset);
            double x2 = std::min<double>(_w,  _kseedsx[n]+2*offset);

			sibr::Vector2d xy;
			sibr::Vector2d c_xy;
			sibr::Vector3d lab;
			sibr::Vector3d c_lab;

            for (uint y=(uint)y1; y<y2; y++) {
                for (uint x=(uint)x1; x<x2; x++) {
                    uint i    = y*_w + x;
                    xy    = sibr::Vector2d(x,y);
                    c_xy  = sibr::Vector2d(_kseedsx[n],_kseedsy[n]);
                    lab   = sibr::Vector3d(_lvec[i], _avec[i], _bvec[i]);
                    c_lab = sibr::Vector3d(_kseedsl[n],_kseedsa[n],_kseedsb[n]);

                    double dist = sibr::distance(lab,c_lab) + sibr::distance(xy,c_xy)*w_compact;
                    if (dist < distvec[i]) {
                        distvec[i] = dist;
                        _klabels[i] = n;
                    }
                }
            }
        }

        // Recalculate the centroid and store in the seed values

        sigmal.assign(numk, 0);
        sigmaa.assign(numk, 0);
        sigmab.assign(numk, 0);
        sigmax.assign(numk, 0);
        sigmay.assign(numk, 0);
        clustersize.assign(numk, 0);

        for (uint r=0, ind=0; r<_h; r++) {
            for (uint c=0; c<_w; c++) {
                sigmal[_klabels[ind]] += _lvec[ind];
                sigmaa[_klabels[ind]] += _avec[ind];
                sigmab[_klabels[ind]] += _bvec[ind];
                sigmax[_klabels[ind]] += c;
                sigmay[_klabels[ind]] += r;
                clustersize[_klabels[ind]] += 1.0;
                ind++;
            }
        }

        for (uint k=0; k<numk; k++) {
            if (clustersize[k] <= 0)
                clustersize[k] = 1;
            inv[k] = 1.0/clustersize[k]; //computing inverse now to multiply, than divide later
        }

        for (uint k=0; k<numk; k++) {
            _kseedsl[k] = sigmal[k]*inv[k];
            _kseedsa[k] = sigmaa[k]*inv[k];
            _kseedsb[k] = sigmab[k]*inv[k];
            _kseedsx[k] = sigmax[k]*inv[k];
            _kseedsy[k] = sigmay[k]*inv[k];
        }
    }
}

void SLIC::enforceLabelConnectivity(void) {
    std::fill(_labels.begin(), _labels.end(), -1);

    for (uint j=0,next_label=0; j<_h; j++) {
        for (uint i=0; i<_w; i++) {
            uint curr_ind = j*_w + i;
            if (_labels[curr_ind] >= 0)
                continue;

            _labels[curr_ind] = next_label++;
            std::shared_ptr<SPixelInternal> sp(new SPixelInternal(_labels[curr_ind]));
            sp->addPixel(sibr::Vector2i(i,j));

            // find all pixels of this superpixel and assign the same label
            std::queue< std::pair<uint,uint> > pixels;
            pixels.push(std::make_pair(i,j));
            while (!pixels.empty()) {
                uint x = pixels.front().first;
                uint y = pixels.front().second;
                pixels.pop();
                for (uint k=0; k<8; k++) {
                    uint ii = x+dx8[k];
                    uint jj = y+dy8[k];
                    if (ii>=0 && ii<_w && jj>=0 && jj<_h) {
                        uint sub_ind = jj*_w + ii;
                        if (_labels[sub_ind]<0 && _klabels[sub_ind]==_klabels[curr_ind]) {
                            _labels[sub_ind] = _labels[curr_ind];
                            pixels.push(std::make_pair(ii,jj));
                            sp->addPixel(sibr::Vector2i(ii,jj));
                        }
                    }
                }
            }
            _spixels.push_back(sp);
        }
    }

    // check that superpixel structure is sane
    for (uint i=0; i<_spixels.size(); i++)
        SIBR_ASSERT(_spixels[i]->id() == i);

    uint merged = 0;
    uint to_merge = 0;
    uint min_size = uint(float(_w*_h)/(_K*4.0f));

    do {
        // build spixel adjacency graph
        for (uint j=0; j<_h-1; j++) {
            for (uint i=0; i<_w-1; i++) {
                int sp0 = _labels[_w*j+i];
                int sp1 = _labels[_w*j+i+1];
                int sp2 = _labels[_w*(j+1)+i];
                int sp3 = _labels[_w*(j+1)+i+1];
                SIBR_ASSERT(!_spixels[sp0]->defunct());
                SIBR_ASSERT(!_spixels[sp1]->defunct());
                SIBR_ASSERT(!_spixels[sp2]->defunct());
                SIBR_ASSERT(!_spixels[sp3]->defunct());
                if (sp0 != sp1) { _spixels[sp0]->addNeighbor(sp1); _spixels[sp1]->addNeighbor(sp0); }
                if (sp0 != sp2) { _spixels[sp0]->addNeighbor(sp2); _spixels[sp2]->addNeighbor(sp0); }
                if (sp0 != sp3) { _spixels[sp0]->addNeighbor(sp3); _spixels[sp3]->addNeighbor(sp0); }
            }
        }

        // merge small superpixels with one of the neighbors
        to_merge = 0;
        merged   = 0;
        for (uint i=0; i<_spixels.size(); i++) {
            uint nu_pixels = _spixels[i]->numPixels();
            std::vector<int> neigh = _spixels[i]->neighbors();
            if (nu_pixels>0 && nu_pixels<min_size) {
                to_merge++;
                int selected_neigh = -1;
                double min_diff = DBL_MAX;
                sibr::Vector3d color = average_color(_spixels[i]->pixels());
                for (uint j=0; j<neigh.size(); j++) {
                    sibr::Vector3d color_neigh = average_color(_spixels[neigh[j]]->pixels());
					double diff     = sibr::length(sibr::Vector3d(color-color_neigh));
                    if (!_spixels[neigh[j]]->defunct() && diff<min_diff) {
                        min_diff        = diff;
                        selected_neigh  = neigh[j];
                    }
                }
                if (selected_neigh >= 0) {
                    _spixels[i]->merge(_spixels[selected_neigh]);
                    std::vector<sibr::Vector2i> p = _spixels[selected_neigh]->pixels();
                    for (uint j=0; j<p.size(); j++)
                        _labels[p[j][1]*_w+p[j][0]] = selected_neigh;
                    merged++;
                    to_merge--;
                }
            }
        }
    } while (to_merge>0 && merged>0);

    // update the spixel id's
    std::fill(_labels.begin(), _labels.end(), -1);
    for (uint i=0,next=0; i<_spixels.size(); i++) {
        if (!_spixels[i]->defunct() && _spixels[i]->numPixels()>0) {
            std::vector<sibr::Vector2i> p = _spixels[i]->pixels();
            for (uint j=0; j<p.size(); j++)
                _labels[p[j][1]*_w+p[j][0]] = next;
            next++;
            _numspixels = next;
        }
    }
}

void SLIC::overSegment(uint* ubuff, uint w, uint h) {
    _w = w;
    _h = h;

    convertRGB2LAB(ubuff);
    std::vector<double> edgemag = edgesColor();
    getLABXYSeeds(edgemag);
    overSegmentUsingSeeds(edgemag);
    enforceLabelConnectivity();
}

uint SLIC::num_spixels(void) const {
    return _numspixels;
}

std::vector<int> SLIC::labels(void) const {
    return _labels;
}

sibr::Vector3d SLIC::average_color(std::vector<sibr::Vector2i> pixels) const {
    sibr::Vector3d avg_color = sibr::Vector3d(0,0,0);
    for (uint i=0; i<pixels.size(); i++) {
        uint index = pixels[i][1]*_w + pixels[i][0];
        avg_color = avg_color+ sibr::Vector3d(_lvec[index],_avec[index],_bvec[index]);
    }
    return (avg_color/double(pixels.size()));
}
