/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _SLIC_H_
#define _SLIC_H_

class SPixelInternal;

class SLIC {
public:
    SLIC(uint, uint);

    void overSegment(uint*, uint, uint);
    void drawContours(uint*, uint);
    uint num_spixels(void) const;
    std::vector<int> labels(void) const;
    sibr::Vector3d  average_color(std::vector<sibr::Vector2i>) const;

private:

    void overSegmentUsingSeeds(std::vector<double>);
    void getLABXYSeeds(std::vector<double>);
    void perturbSeeds(std::vector<double>);
    void enforceLabelConnectivity(void);

    std::vector<double> edgesColor(void);

    sibr::Vector3d  rgb2lab(sibr::Vector3i);
    sibr::Vector3d  rgb2xyz(sibr::Vector3i);
    void convertRGB2LAB(uint*);

private:
    uint _w;
    uint _h;
    uint _numspixels;

    uint _K;
    uint _M;

    std::vector<double> _lvec;
    std::vector<double> _avec;
    std::vector<double> _bvec;

    std::vector<double> _kseedsl;
    std::vector<double> _kseedsa;
    std::vector<double> _kseedsb;
    std::vector<double> _kseedsx;
    std::vector<double> _kseedsy;

    std::vector<int> _labels;
    std::vector<int> _klabels;

    std::vector<std::shared_ptr<SPixelInternal> > _spixels;
};

#endif // _SLIC_H_
