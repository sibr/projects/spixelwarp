/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <core/graphics/Image.hpp>
#include <core/system/String.hpp>
#include "precompiled.h"
#include "spimg_int.h"

#define MIN_COMMON_MVS_POINTS 10


bool lessThan(std::pair<uint,uint> a, std::pair<uint,uint> b) {
    return (a.first<b.first || (a.first==b.first && a.second<b.second));
}

SPixelImageInternal::SPixelImageInternal(std::string path, uint id) :
    _id(id),
    _w(0),
    _h(0),
    _ready(false),
    _spixels(0)
{
    std::string fname = path + sibr::sprint("superpixels/%08d.sp",_id);

//    try {
        std::ifstream f(fname.c_str(), std::ios::binary);
        //SIBR_ASSERT(f.is_open());
		if( !f.is_open() ) {
			std::cerr << "[SIBR superpixel] Cannot open " << fname << " Exiting." << std::endl;
			exit(1);
		}

        f.read((char*) &_w, sizeof(int));                 /* read the size of superpixel image */
        f.read((char*) &_h, sizeof(int));

        _map = sibr::Array2d<int>(_w,_h);                             /* read the superpixel id's */
        f.read((char*)_map.data(), sizeof(int)*_w*_h);
        for (uint j=0; j<_h; j++)
            for (uint k=0; k<_w; k++)
                _spixels = std::max(_spixels, uint(_map(k,j)+1));

        _neighbors.resize(_spixels);

        f.close();
        _ready = true;
        std::cout << "[Superpixels] loaded " << fname.c_str() << " ";
        std::cout << _spixels << " superpixels" << std::endl;
//    } catch (std::runtime_error& e) {
 //       _ready = false;
  //      std::cout << "[Superpixels] could not load " << fname.c_str() << std::endl;
   // }
}

uint SPixelImageInternal::w       (void) const { return _w;       }
uint SPixelImageInternal::h       (void) const { return _h;       }
bool SPixelImageInternal::ready   (void) const { return _ready;   }
uint SPixelImageInternal::spixels (void) const { return _spixels; }
uint SPixelImageInternal::spixel(uint x, uint y) const { return _map(x,y); }

void SPixelImageInternal::addNeighbors(uint i, std::vector< std::pair<uint,uint> > v) {
    SIBR_ASSERT(i < _neighbors.size());
    for (uint k=0; k<v.size(); k++) {
        if (_neighbors[i].find(v[k]) == _neighbors[i].end())
            _neighbors[i][ v[k] ] = 1;
        else
            _neighbors[i][ v[k] ] += 1;
    }
}

std::vector< std::pair<uint,uint> > SPixelImageInternal::getNeighbors(uint i) const {
    SIBR_ASSERT(i < _neighbors.size());
    std::vector< std::pair<uint,uint> > v;
    std::map<std::pair<uint,uint>,uint>::const_iterator it(_neighbors[i].begin());
    for (; it!=_neighbors[i].end(); it++)
        if (it->second >= MIN_COMMON_MVS_POINTS) // min common MVS points
            v.push_back(it->first);
    return v;
}

void SPixelImageInternal::saveSegmentation(std::map<uint,sibr::Vector3u> colorMap) const {
    sibr::ImageRGB::Ptr img(new sibr::ImageRGB(_w,_h));
    for (uint i=0; i<img->w(); i++) {
        for (uint j=0; j<img->h(); j++) {
            uint spixel = _map(i,j);
            uint id     = _segment_mapping[spixel];
            img(i,j)[0] = colorMap[id][0];
            img(i,j)[1] = colorMap[id][1];
            img(i,j)[2] = colorMap[id][2];
        }
    }
    img->save(sibr::sprint("%04d.png",_id));
}

void SPixelImageInternal::setSegmentID(uint spixel, uint id) {
    if (_segment_mapping.empty())
        _segment_mapping.resize(_spixels);
    _segment_mapping[spixel] = id;
}
