/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _SUPER_PIXEL_IMAGE_INTERNAL_H_
#define _SUPER_PIXEL_IMAGE_INTERNAL_H_

class SPixelImageInternal {
public:
    SPixelImageInternal(std::string, uint);

    uint w      (void)  const;
    uint h      (void)  const;
    bool ready  (void)  const;
    uint spixels(void)  const;
    uint spixel(uint,uint) const;

    void addNeighbors(uint, std::vector< std::pair<uint,uint> >);
    std::vector< std::pair<uint,uint> > getNeighbors(uint) const;

    void saveSegmentation(std::map<uint,sibr::Vector3u>) const;
    void setSegmentID    (uint, uint);

private:
    uint _id;
    uint _w;
    uint _h;
    bool _ready;
    uint _spixels;

    sibr::Array2d<int> _map;

    std::vector<uint> _segment_mapping;

    std::vector< std::map<std::pair<uint,uint>,uint> > _neighbors;
};

#endif // _SUPER_PIXEL_IMAGE_H_
