/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _SUPER_PIXEL_INTERNAL_H_
#define _SUPER_PIXEL_INTERNAL_H_

class SPixelInternal {
public:
    SPixelInternal(int id) : m_id(id), m_defunct(false)  {
      m_avg_color = sibr::Vector3d(0,0,0);
      m_neighbors.clear();
      m_pixels.clear();
    }

    int  id     (void) const { return m_id; }
    bool defunct(void) const { return m_defunct; }

    uint numPixels   (void) const { return (uint)m_pixels.size();    }
    uint numNeighbors(void) const { return (uint)m_neighbors.size(); }

    void addPixel   (sibr::Vector2i p) { m_pixels.push_back(p); }
    void addNeighbor(int n) { m_neighbors.insert(n); }

    std::vector<sibr::Vector2i> pixels(void) const {
      return m_pixels;
    }

    std::vector<int> neighbors(void) const {
      std::vector<int> n;
      std::set<int>::const_iterator it(m_neighbors.begin());
      std::set<int>::const_iterator end(m_neighbors.end());
      for (; it!=end; it++)
        n.push_back(*it);
      return n;
    }

    void merge(std::shared_ptr<SPixelInternal> spixel) {
      SIBR_ASSERT(!spixel->m_defunct);

      std::vector<int> n = neighbors();
      for (uint i=0; i<n.size(); i++)
        spixel->m_neighbors.insert(n[i]);
      spixel->m_neighbors.erase(spixel->m_id);
      spixel->m_neighbors.erase(m_id);

      for (uint i=0; i<m_pixels.size(); i++)
        spixel->m_pixels.push_back(m_pixels[i]);

      m_pixels.clear();
      m_neighbors.clear();
      m_defunct = true;
    }

private:
    int  m_id;
    bool m_defunct;
    sibr::Vector3d  m_avg_color;

    std::vector<sibr::Vector2i> m_pixels;
    std::set<int>    m_neighbors;
};

#endif // _SUPER_PIXEL_H_
