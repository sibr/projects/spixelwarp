# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#--------------------------------------------

import subprocess
import shutil
import os
import re
from utils.commands import getProcess
from utils.paths import getBinariesPath

from os import walk
import struct
import imghdr
import sys, getopt
force_continue = False

def checkOutput( output, force_continue ):
    if( output != 0):
        if( not force_continue ):
            sys.exit()
        else:
            return False
    else:
        return True
    
#--------------------------------------------

#===============================================================================
#--------------------------------------------
# 0. Paths, commands and options

def main(argv, path_dest):
    opts, args  = getopt.getopt(argv, "hird:", ["idir=","bin="])
    executables_suffix = ""
    executables_folder = getBinariesPath()
    for opt, arg in opts:
        if opt == '-d':
            path_dest = arg
        elif opt in ( '-bin' , '--bin' ) :
            executables_folder = arg
            
    return (path_dest, executables_suffix, executables_folder)

path_dest = ""
path_dest, executables_suffix, executables_folder = main(sys.argv[1:], path_dest)
executables_suffix = "_rwdi"
print('PATH DEST ', path_dest);
print('EXEC FLDER DEST ', executables_folder)
path_install = executables_folder

print (['Setting path_dest to ', path_dest])
path_dest = os.path.abspath(path_dest + "/")  + "/"


plane_estimation = getProcess("SIBR_plane_estimation" + executables_suffix, executables_folder)
new_quality_estim    = getProcess("SIBR_quality" + executables_suffix, executables_folder)

#--------------------------------------------
# 1. Run Plane estimation

print('PLANE ESTIMATION ', plane_estimation)
program_exit =  subprocess.call([plane_estimation, "--path", path_dest, "--offscreen" ])
sys.stdout.flush()
if program_exit != 0:
    print("SIBR_ERROR: cant do plane estimation for selective rendering, exiting")
    sys.exit(-1)

    
#--------------------------------------------
# 2. Quality estimation

print('PLANE ESTIMATION ', new_quality_estim)
print( [new_quality_estim, path_dest ])
sys.stdout.flush()
program_exit =  subprocess.call([new_quality_estim, "--path", path_dest, "--width", "600" , "--offscreen" ], cwd=path_install)
sys.stdout.flush()
if program_exit != 0:
    print("SIBR_ERROR: cant do plane estimation for selective rendering, exiting")
#--------------------------------------------
