# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


#!/usr/bin/env python
#! -*- encoding: utf-8 -*-
# --------------------------------------------
""" @package swarp_preprocess
This script creates SpixelWarp related data from a SIBR template dataset which can be fed to a SPixelWarp application

Parameters: -h help,
            -i <path to input directory which is the output from RC> <default: ${CMAKE_INSTALL_DIR}/bin/datasets/rc_out/>,
            -o <path to output directory which can be fed into SIBR apps> <default: input directory> [optional],
            -r use release w/ debug symbols executables

Usage: python ibr_preprocess_rc_to_sibr.py -r
                                           -i <path_to_sibr>\sibr\install\bin\datasets\museum_sibr_new_preproc_template_RCOut
                                           -o <path_to_sibr>\sibr\install\bin\datasets\museum_sibr_new_preproc2

"""

import subprocess
import shutil
import os, sys, getopt
import re
from utils.commands import getProcess
from utils.paths import getBinariesPath

from os import walk

#--------------------------------------------

#===============================================================================

import struct
import imghdr

def get_image_size(fname):
    '''Determine the image type of fhandle and return its size.
    from draco'''
    with open(fname, 'rb') as fhandle:
        head = fhandle.read(24)
        if len(head) != 24:
            return
        if imghdr.what(fname) == 'png':
            check = struct.unpack('>i', head[4:8])[0]
            if check != 0x0d0a1a0a:
                return
            width, height = struct.unpack('>ii', head[16:24])
        elif imghdr.what(fname) == 'gif':
            width, height = struct.unpack('<HH', head[6:10])
        elif imghdr.what(fname) == 'jpeg':
            try:
                fhandle.seek(0) # Read 0xff next
                size = 2
                ftype = 0
                while not 0xc0 <= ftype <= 0xcf:
                    fhandle.seek(size, 1)
                    byte = fhandle.read(1)
                    while ord(byte) == 0xff:
                        byte = fhandle.read(1)
                    ftype = ord(byte)
                    size = struct.unpack('>H', fhandle.read(2))[0] - 2
                # We are at a SOFn block
                fhandle.seek(1, 1)  # Skip `precision' byte.
                height, width = struct.unpack('>HH', fhandle.read(4))
            except Exception: #IGNORE:W0703
                return
        else:
            return
        return width, height
 
def checkOutput( output, force_continue ):
    if( output != 0):
        if( not force_continue ):
            sys.exit()
        else:
            return False
    else:
        return True
    

#===============================================================================

#--------------------------------------------
# 0. Paths, commands and options

def main(argv, path_dest):
    opts, args = getopt.getopt(argv, "hi:ro:", ["idir=", "bin="])
    executables_suffix = ""
    executables_folder = getBinariesPath()
    path_data = ""
    for opt, arg in opts:
        if opt == '-h':
            print("-i path_to_rc_data_dir -o path_to_destination_dir [-r (use release w/ debug symbols executables)]")
            sys.exit()
        elif opt == '-i':
            path_data = arg
            print(['Setting path_data to ', path_data])
        elif opt == '-r':
            executables_suffix = "_rwdi"
            print("Using rwdi executables.")
        elif opt == '-o':
            path_dest = arg
            print(['Setting path_dest to ', path_dest])
        elif opt in ('-bin', '--bin'):
            executables_folder = os.path.abspath(arg)

    return (path_data, path_dest, executables_suffix, executables_folder)

path_dest = ""
path_data, path_dest, executables_suffix, executables_folder = main(sys.argv[1:], path_dest)

if(path_data == ""):
    path_data = os.path.abspath(os.path.join(os.path.dirname(__file__), "../datasets"))

if(path_dest == ""):
    path_dest = path_data

path_data = os.path.abspath(path_data + "/") + "/"
path_dest = os.path.abspath(path_dest + "/") + "/"
executables_folder = os.path.abspath(executables_folder + "/") + "/"

path_in_imgs = path_data + "images/"


print(['Raw_data folder: ', path_data])
print(['Path_dest: ', path_dest])
print(['Executables folder: ', executables_folder])


#path_dest_pmvs    = path_dest + "pmvs/models/";
bundler_file = path_data + "cameras/bundle.out"

file_nameList   = path_data + "images/list_images.txt";
path_half_size  = path_dest + "spixelwarp/half_size/";
path_superpixels= path_dest + "spixelwarp/superpixels/";
path_scene_metadata = path_data + "scene_metadata.txt"
# 0.d Path of the excecutable of scene2patch_INRIA and depth synthesis

scene2patch = getProcess("Swarp_scene2patch" + executables_suffix, executables_folder)
depth_Synth = getProcess("Swarp_depthNormalSynthesis" + executables_suffix, executables_folder)
img_normal = getProcess("img_normal" + executables_suffix, executables_folder)

# 0.e Path of executables of superpixels and sparse depth generator

depth_gener  = getProcess("Swarp_depth" + executables_suffix, executables_folder)
superpixels  = getProcess("Swarp_superpixel" + executables_suffix, executables_folder)

# copy the contents of the input directory to output directory
if path_dest != path_data:
    for filename in os.listdir(path_data):
        if(os.path.isdir(os.path.join(path_data, filename))):
            shutil.copytree(os.path.join(path_data, filename), os.path.join(path_dest, filename))
        else:
            shutil.copy(os.path.join(path_data, filename), path_dest)

if not os.path.exists(path_half_size):
    os.makedirs( path_half_size )


if os.path.exists(bundler_file):
    if not os.path.exists(os.path.join(path_dest, "spixelwarp/cameras/")):
        os.makedirs(os.path.join(path_dest, "spixelwarp/cameras/"))
    shutil.copy(bundler_file, os.path.join(path_dest, "spixelwarp/cameras/"))

# 
# reconstruction and copy done !
# only mogrify if needed
#
file_names = os.listdir( path_in_imgs)
m = re.search("(.+?).jpg", file_names[1])
ext = '.jpg'
if m is None:
    m = re.search("(.+?).png", file_names[1])
    ext = '.png'
if m is None:
    m = re.search("(.+?).JPG", file_names[1])
    ext = '.JPG'


num_str = m.group(1)


img_id = int(num_str)-1
file_jpg = ("%08d" + ext) % img_id
fname = os.path.abspath(os.path.join(path_in_imgs, file_jpg))
fname_half = os.path.abspath(os.path.join(path_half_size, file_jpg))  
fsize = get_image_size(fname)

if os.path.exists(fname_half) :
    fhsize = get_image_size(fname_half)
else: # force half size since not already done
    fhsize  = fsize
    # copy files
    for file_name in os.listdir( path_in_imgs ):
            original_img_path = os.path.abspath(os.path.join(path_in_imgs, file_name))
            half_size_img_path = os.path.abspath(os.path.join(path_half_size, file_name))
            m = re.search("(.+?)" + ext, file_name)
            if m:
                print('Copy ', original_img_path, ' to ', half_size_img_path)
                shutil.copyfile(original_img_path, half_size_img_path)

print(["fsize = ", fsize[1], " fh ", fhsize[1]])


#--------------------------------------------
# 0. Half size -- do only once

half_size_img_wildcard = os.path.abspath(os.path.join(path_half_size, "*" + ext))

if( fsize[1]/2 != fhsize[1] ) :
    print ("Mogrifying")
    fsize = get_image_size(fname)
    subprocess.call(["mogrify.exe", "-resize", "%dx%d" % (fsize[0]/2, fsize[1]/2), half_size_img_wildcard])
    print(["mogrify.exe", "-resize", "%dx%d" % (fsize[0]/2, fsize[1]/2), half_size_img_wildcard])
    fhsize = get_image_size(fname_half)
    if( fsize[1]/2 != fhsize[1] ) :
        print ("error: half size mismatch between %s (%f, %f) and %s (%f, %f)" % (fname, fsize[0]/2, fsize[1]/2, fname_half, fhsize[0], fhsize[1]))
        sys.exit(-1)
        
else:
    print ("Half size already computed")

#--------------------------------------------

file_listImgs = open(file_nameList, 'r')
img_id = 0

for line in file_listImgs:
    img_id += 1

file_listImgs.close()

force_continue = False

#--------------------------------------------
# 1. Run scene2path_INRIA exe

print(scene2patch)
print([scene2patch, "--path", path_dest, executables_folder])
program_exit = subprocess.call([scene2patch, "--path", path_dest], cwd=executables_folder)
print("scene 2 patch exited with ", program_exit)
sys.stdout.flush()
if( not checkOutput(program_exit, force_continue) ):
    sys.exit(-1)


#--------------------------------------------
# 1.5 Create swarp scene metadata file from list image file
scene_metadata = "Scene Metadata File\n\n"

# read list image file
path_list_images = os.path.join(path_in_imgs, "list_images.txt")
list_images = []

if os.path.exists(path_list_images):
    list_image_file = open(path_list_images, "r")

    for line in list_image_file:
        list_images.append(line)

    list_image_file.close()

# read clipping planes file
path_clipping_planes = os.path.join(path_dest, "spixelwarp/clipping_planes.txt")
clipping_planes = []

if os.path.exists(path_clipping_planes):
    clipping_planes_file = open(path_clipping_planes, "r")

    for line in clipping_planes_file:
        clipping_planes.append(line)

    clipping_planes_file.close()


scene_metadata = scene_metadata + "[list_images]\n<filename> <image_width> <image_height> <near_clipping_plane> <far_clipping_plane>\n"
# new_list = [a[:-1] + " " + b for a, b in zip(list_images, clipping_planes)]
for line in list_images:
    line = line[:-1] + " " + clipping_planes[0]
    scene_metadata = scene_metadata + line

scene_metadata = scene_metadata + "\n\n// Always specify active/exclude images after list images\n\n[exclude_images]\n<image1_idx> <image2_idx> ... <image3_idx>\n"

# if len(exclude) > 0:
#     for line in exclude:
#         scene_metadata = scene_metadata + str(line) + " "

scene_metadata = scene_metadata + "\n\n\n[other parameters]"

path_scene_metadata = os.path.join(path_dest, "spixelwarp/scene_metadata.txt")

scene_metadata_file = open(path_scene_metadata, "w")
scene_metadata_file.write(scene_metadata)
scene_metadata_file.close()

#--------------------------------------------
# 2. Run depth.exe

if img_id >= 50:
    program_exit = subprocess.call([depth_gener, "--path", path_dest, "--ext", ext, "--big_dataset"], cwd=executables_folder)
else:
    program_exit = subprocess.call([depth_gener, "--path", path_dest, "--ext", ext], cwd=executables_folder)
print("depth exited with ", program_exit)
sys.stdout.flush()
if( not checkOutput(program_exit, force_continue) ):
    sys.exit(-1)
     
#--------------------------------------------
# 3. Run superpixels.exe

program_exit =  subprocess.call([superpixels, "--path", path_dest, "--segment",  "--n", "1500"], cwd=executables_folder)
# program_exit =  subprocess.call([superpixels, "--path", path_dest, "--segment",  "--graph" , "--n", "1500"], cwd=executables_folder) DONT DO GRAPH !
print("spixels exited with ", program_exit)
sys.stdout.flush()
if( not checkOutput(program_exit, force_continue) ):
    sys.exit(-1)

#--------------------------------------------
# 4. Run depth_Synthesis.exe

# last arg == do depth synthesis (0 for post inpaint)

print(" ".join([depth_Synth, "--path", path_dest, "--num", "5", "--rangeStart", "0", "--rangeEnd", str(img_id-1), "--doProcess", "--ext", ext]))
program_exit =  subprocess.call([depth_Synth, "--path", path_dest, "--num", "5", "--rangeStart", "0", "--rangeEnd", str(img_id-1), "--doProcess", "--ext", ext])
sys.stdout.flush()
print("depth synth exited with ", program_exit)
if( not checkOutput(program_exit, force_continue) ):
    sys.exit(-1)

#--------------------------------------------
