/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <iostream>
#include <core/system/String.hpp>
#include <projects/spixelwarp/renderer/BlendRenderer.hpp>

namespace sibr 
{
	
	BlendRenderer::~BlendRenderer( ) {} ;

	BlendRenderer::BlendRenderer( const BasicIBRScene::Ptr& scene, uint numWarp ) :
		_scene( scene )
	{
		GLShader::Define::List defines;
		defines.emplace_back("NUM_WARPED", numWarp);

		_numWarp = numWarp;
		_blendShader.init("BlendShader",
			loadFile(sibr::getShadersDirectory("core") + "/noproj.vert"),
			loadFile(sibr::getShadersDirectory("spixelwarp") + "/spx_blend.frag", defines )
			);
		initShaderParameter();
	}

	void	BlendRenderer::initShaderParameter( void )
	{
		in_inv_proj	.resize(_numWarp);
		icam_pos	.resize(_numWarp);
		warped_uv	.resize(_numWarp);
		input_rgb	.resize(_numWarp);
		mask_rgb	.resize(_numWarp);

		_blendShader.begin();

		ncam_proj	.init(_blendShader,"ncam_proj");
		ncam_pos	.init(_blendShader,"ncam_pos");
		blend_f		.init(_blendShader,"blend_f");
	    doMasking   .init(_blendShader,"doMasking");

		int size = useMasks() ? 3 : 2;
		for (uint i=0; i<_spixelGraph.size(); i++)
		{
			sp_graph[i]	.init(_blendShader, sibr::sprint("sp_graph[%d]",i));
			sp_graph[i]	.set(size*_numWarp+i);
		}

		for (uint i=0; i<(uint)_numWarp; i++)
		{
			in_inv_proj[i]	.init(_blendShader, sibr::sprint("in_inv_proj[%d]",i));
			icam_pos[i]		.init(_blendShader, sibr::sprint("icam_pos[%d]",i));

			warped_uv[i]	.init(_blendShader, sibr::sprint("warped_uv[%d]",i));
			warped_uv[i]	.set(size*i+0);

			input_rgb[i]	.init(_blendShader, sibr::sprint("input_rgb[%d]",i));
			input_rgb[i]	.set(size*i+1);
			if( useMasks() ) {
				mask_rgb[i]	.init(_blendShader, sibr::sprint("mask_rgb[%d]",i));
				mask_rgb[i]	.set(size*i+2);
			}
		}
		_blendShader.end();
	}

	void BlendRenderer::process(
			/*input*/ const sibr::Camera& eye,
			/*input*/ const std::vector<uint>& imageIDs,
			/*input*/ const std::vector<RenderTargetRGBA32F::Ptr>& warpUVRTs,
			/*input*/ const std::vector<RenderTargetRGBA32F::Ptr>& imageRTs,
			/*output*/ IRenderTarget& dst )
	{
		_blendShader.begin();
		blend_f.set(2.0f);
		ncam_proj.set(eye.viewproj());
		ncam_pos .set(eye.position());
		doMasking .set( useMasks());

		int size = useMasks() ? 3 : 2;

		for ( uint i = 0; i < (uint)imageIDs.size() && i < (uint)_numWarp; ++i )
		{
			uint k = imageIDs[i];
			const InputCamera& cam = *_scene->cameras()->inputCameras().at(k);

			in_inv_proj[i].set(sibr::Matrix4f(cam.viewproj().inverse()));
			icam_pos   [i].set(cam.position());

			glActiveTexture(GL_TEXTURE0+size*i+0);
			glBindTexture(GL_TEXTURE_2D, warpUVRTs[i]->texture());
			glActiveTexture(GL_TEXTURE0+size*i+1);
			glBindTexture(GL_TEXTURE_2D, imageRTs.at(k)->texture());
			if( useMasks() )
			{
				glActiveTexture(GL_TEXTURE0+size*i+2);
				glBindTexture(GL_TEXTURE_2D,  getMasks()[k]->texture() );
			}
		}

		for ( uint i = 0; i < _spixelGraph.size(); ++i )
		{
			glActiveTexture(GL_TEXTURE0+size*_numWarp+i);
			glBindTexture(GL_TEXTURE_2D, _spixelGraph[i]->handle());
		}

		dst.clear();
		dst.bind();

		glViewport( 0, 0, dst.w(), dst.h() );
		RenderUtility::renderScreenQuad();

		dst.unbind();
		_blendShader.end();
	}

} /*namespace sibr*/ 
