/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SPIXELWARP_BLENDRENDERER_HPP__
# define __SIBR_EXP_SPIXELWARP_BLENDRENDERER_HPP__

# include <core/graphics/Texture.hpp>
# include <core/graphics/Shader.hpp>
# include <core/scene/BasicIBRScene.hpp>

# include <core/renderer/RenderMaskHolder.hpp>
# include <projects/spixelwarp/renderer/Config.hpp>

namespace sibr 
{
	class SIBR_EXP_SPIXELWARP_EXPORT BlendRenderer : public RenderMaskHolder
	{
	public:
		typedef std::shared_ptr<BlendRenderer>	Ptr;

	public:
		~BlendRenderer();
		BlendRenderer( const BasicIBRScene::Ptr& scene, uint numWarp );

		void process(
			/*input*/ const sibr::Camera& eye,
			/*input*/ const std::vector<uint>& imageIDs,
			/*input*/ const std::vector<RenderTargetRGBA32F::Ptr>& warpUVRTs,
			/*input*/ const std::vector<RenderTargetRGBA32F::Ptr>& imageRTs,
			/*output*/ IRenderTarget& dst );

	private:

		void initShaderParameter( void );

		BasicIBRScene::Ptr	_scene;

		sibr::GLShader		_blendShader;
		bool				_needInitShaderParameters;
		int					_numWarp;

		sibr::GLParameter	blend_f;
		sibr::GLParameter	ncam_proj;
		sibr::GLParameter	ncam_pos;
		sibr::GLParameter	doMasking;
		std::vector<sibr::GLParameter>	in_inv_proj;
		std::vector<sibr::GLParameter>	icam_pos;
		std::vector<sibr::GLParameter>	warped_uv;
		std::vector<sibr::GLParameter>	input_rgb;
		std::vector<sibr::GLParameter>	mask_rgb;
		std::vector<sibr::GLParameter>	sp_graph;

		/** Textures containing superpixel correspondance across images
		* \todo More documentation needed
		*/
		std::vector<std::shared_ptr<sibr::Texture2DRGBA32F> > _spixelGraph;
	};

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SPIXELWARP_BLENDRENDERER_HPP__
