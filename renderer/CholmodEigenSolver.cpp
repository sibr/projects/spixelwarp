/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <projects/spixelwarp/renderer/CholmodEigenSolver.hpp>

namespace sibr  {
	CholmodEigenSolver::CholmodEigenSolver( void ) :
		_L (NULL),
		_At(NULL),
		_numOfUnknows(0),
		_ready(false)
	{
		_choleskySolver = new Eigen::SimplicialLLT<Eigen::SparseMatrix<double>>();
	}

	CholmodEigenSolver::~CholmodEigenSolver( void )
	{
		free(_choleskySolver);
		_choleskySolver = NULL;
	}

	bool CholmodEigenSolver::precompute( const Eigen::SparseMatrix<double>& A )
	{
		_choleskySolver->compute(A);
		Eigen::ComputationInfo info = _choleskySolver->info();
		if (info != Eigen::Success)
			_ready = false;
		else
			_ready = true;

		return _ready;
	}

	bool CholmodEigenSolver::solve(
		const Eigen::SparseMatrix<double>& Aj,
		const std::vector<double>& B,
		std::vector<double>& X )
	{
		if (_choleskySolver->info() != Eigen::Success)
			return false;

		Eigen::VectorXd b = Eigen::VectorXd::Map(B.data(), B.size());
		Eigen::VectorXd x = _choleskySolver->solve(Aj * b);

		if (_choleskySolver->info() != Eigen::Success)
			throw std::runtime_error("Eigen solver error (x)");

		X.resize(x.size());
		Eigen::VectorXd::Map(&X[0], x.size()) = x;

		return true;
	}

	bool CholmodEigenSolver::ready( void ) const
	{
		return _ready;
	}

} /*namespace sibr*/ 
