/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_CHOLMOD_EIGEN_SOLVER_HPP__
# define __SIBR_CHOLMOD_EIGEN_SOLVER_HPP__

# include <algorithm>
# include <stdexcept>

# include <Eigen/SparseCholesky>

# include <core/system/Config.hpp>
# include <projects/spixelwarp/renderer/Config.hpp>

namespace sibr 
{
	///
	/// C++ wrapper for Cholmod solver using Eigen
	///
	class SIBR_EXP_SPIXELWARP_EXPORT CholmodEigenSolver
	{
		SIBR_DISALLOW_COPY( CholmodEigenSolver );

	public:
		CholmodEigenSolver( void );
		~CholmodEigenSolver( void );

		bool precompute( const Eigen::SparseMatrix<double>& A );

		bool solve(
			const Eigen::SparseMatrix<double>& Aj,
			const std::vector<double>& B,
			std::vector<double>& X );

		/// Determine if solver is factorized
		bool ready( void ) const;

	private:

		Eigen::MatrixXd*				_L;
		Eigen::SparseMatrix<double>*	_At;
		Eigen::SimplicialLLT<Eigen::SparseMatrix<double>>*	_choleskySolver;

		unsigned int	_numOfUnknows;
		bool			_ready;
	};

} /*namespace sibr*/ 

#endif // __SIBR_CHOLMOD_EIGEN_SOLVER_HPP__
