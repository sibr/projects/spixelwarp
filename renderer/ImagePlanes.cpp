/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <core/system/LoadingProgress.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <projects/spixelwarp/renderer/ImagePlanes.hpp>

namespace sibr { 
	void				ImagePlanes::setPlanes( const PlaneList& planes )
	{
		_planes = planes;
	}

	const ImagePlanes::PlaneList&	ImagePlanes::getPlanes( void ) const
	{
		return _planes;
	}

	const ImagePlanes::Plane&		ImagePlanes::getPlane( uint index ) const
	{
		SIBR_ASSERT(index < _planes.size());
		return _planes.at(index);
	}

	ImagePlanes::Plane&				ImagePlanes::plane(uint index) {
		return _planes.at(index);
	}

	bool		ImagePlanes::load( const std::string& filename )
	{
		std::ifstream	file(filename.c_str());

		_planes.clear();
		if( file )
		{
			int				spixelID = 0;
			std::string		line;
			while (std::getline(file, line, '\n'))
			{
				std::istringstream iss(line);

				int			spixelIDFromFile;
				Plane		plane;
				char		discardOneChar;

				iss >> spixelIDFromFile >> plane.isActive
					>> discardOneChar >> plane.normal.x()
					>> discardOneChar >> plane.normal.y()
					>> discardOneChar >> plane.normal.z()
					>> discardOneChar >> plane.d;

				if (spixelIDFromFile != spixelID)
					SIBR_ERR << "file '" << filename << "' corrupted." << std::endl;

				plane.isFPLAN = false;
				_planes.push_back(plane);
				++spixelID;
			}
			return true;
		}
		else
			SIBR_ERR << "cannot open file: " << filename << std::endl;
		return false;
	}

	std::vector<ImagePlanes>	loadPlanes( const BasicIBRScene& scene, 
											const std::string& path )
	{
		//const std::vector<InputCamera::Ptr>&	cams = scene.inputCameras();
		const std::vector<InputCamera::Ptr>&	cams = scene.cameras()->inputCameras();
		std::string						pathPlanes = path + "/planes/";
		sibr::LoadingProgress			progress(cams.size(), "loading sp_planes from path '" 
												+ pathPlanes + "'");

		std::vector<ImagePlanes>				planes(cams.size());

#pragma omp parallel for
		for ( int i = 0; i < (int)cams.size(); ++i )
		{
			if ( cams[i]->isActive() )
			{
				std::string filename = pathPlanes + sibr::sprint("%08d.sp_plane", i);
				planes[i].load(filename);
			}
			progress.walk();
		}
		return planes;
	}

} /*namespace sibr*/ 
