/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SELECTION_IMAGEPLANES_HPP__
# define __SIBR_EXP_SELECTION_IMAGEPLANES_HPP__

# include <core/scene/BasicIBRScene.hpp>
# include <projects/spixelwarp/renderer/Config.hpp>

namespace sibr
{
	class IBRScene;

} // namespace sibr

namespace sibr { 
	class SIBR_EXP_SPIXELWARP_EXPORT ImagePlanes
	{
	public:
		struct Plane
		{
			//int			spixelID; (use list index)
			bool		isActive;
			bool		isFPLAN; // tmp: need clarifications
			Vector3f	normal;
			float		d;

			Vector3f	fplanP3D; // tmp: need clarifications
		};

		typedef std::vector<Plane>	PlaneList;

	public:
		bool		load( const std::string& filename );

		void				setPlanes( const PlaneList& planes );
		const PlaneList&	getPlanes( void ) const;

		const Plane&		getPlane( uint index ) const;
		Plane&				plane(uint index);

	private:
		PlaneList		_planes;
	};

	SIBR_EXP_SPIXELWARP_EXPORT
		std::vector<ImagePlanes>	loadPlanes(const BasicIBRScene& scene,
												const std::string& path );

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SELECTION_IMAGEPLANES_HPP__
