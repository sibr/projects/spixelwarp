/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <core/assets/InputCamera.hpp>
#include <core/graphics/RenderUtility.hpp>

#include <projects/spixelwarp/renderer/MVSPoint.hpp>

#include <projects/spixelwarp/renderer/SpixelImage.hpp>
#include <projects/spixelwarp/renderer/Spixel.hpp>
#include <projects/spixelwarp/renderer/ImageWarper.hpp>
#include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>

namespace sibr { 
	ImageWarper::ImageWarper( const InputCameraPreprocess* c, const SpixelImage::Ptr& s ) :
		_mesh(nullptr)
	{
		_camera = c;           // associated input camera
		_spimg  = s;           // associated superpixel segmentation
	}

	void	ImageWarper::init( const std::vector<bool>& activatedSpixels )
	{
		createWarpMesh();
		createLinearSystem(activatedSpixels);
		createVertexBuffer();
	}

	std::vector<double>	ImageWarper::warpSingle( const Camera& nCam, uint spixel) 
	{
		SIBR_ASSERT(_mesh != nullptr);

		// Correction to avoid aniostropic scaling due to aspect ratio difference
		float in_aspect = _camera->aspect();
		float out_aspect = nCam.aspect();

		float aspect_correction = out_aspect / in_aspect;

		uint i = spixel;
		if (_lhs.at(i)->ready()) {
			bool is_behind = false;
			std::fill(_rhs[i].begin(), _rhs[i].end(), 0.0);           // initialize solution to 0
			std::fill(_x[i].begin(), _x[i].end(), 0.0);           // initialize rhs to 0
			for (uint j = 0, eqn = 0; j < _mvs[i].size(); j++) {
				Vector3f p3d = _camera->mvsPnt(_mvs[i][j]).xyz();        // 3D object space coords of depth sample
				Vector3f p2d = nCam.project(p3d);                          // project 3D coords in novel view

																		   // Note (seb): I want to discard points that are outside the "2d frustum"
																		   // of the user camera. But if I don't use a 'margin', small holes appear
																		   // on borders due to discarded spixels that had a (small) part visible in
																		   // the current view but without any point inside.
																		   // I think this margin should be equal to one max spixel width (in [0,1])
																		   // because we cannot cut spixels.
																		   // Or another way would be to make this margin equal to the maximum
																		   // distance of two points in a spixel (but I guess results would be worst
																		   // because only based on few points for transforming one spixel)
				const float margin = 0.2f; /*should be equal to the standard width of a spixel in [0,1]*/
				const float border = 1.f + margin;// / nCam.aspect() + margin;
				if (p2d[0] < -border || p2d[0] > border)// || p2d[1] < -border || p2d[1] > border)
					is_behind |= true;

				_rhs[i][eqn++] = w_depth * p2d[0] * aspect_correction;// update RHS of linear system
				_rhs[i][eqn++] = w_depth * p2d[1];

				is_behind |= (nCam.dir().dot((p3d - nCam.position()).normalized()) <= 0.35); // 70 degrees
			}
			if (!is_behind)
				_lhs[i]->solve(_A_adjoint[i], _rhs[i], _x[i]);

			return _x[i];
		}
		//else return std::vector<double>();
		
		return _x[i];

	}
	void	ImageWarper::warp( const Camera& nCam )
	{
		SIBR_ASSERT(_mesh != nullptr);

		// Correction to avoid aniostropic scaling due to aspect ratio difference
		float in_aspect  = _camera->aspect();
		float out_aspect = nCam.aspect();

		float aspect_correction = out_aspect / in_aspect;

		uint num_superpixels = (uint)_lhs.size();
		for (uint i=0; i<num_superpixels; i++) {
			//SIBR_ASSERT(_lhs.at(i)->ready());

			if (_lhs[i]->ready()) {
				
				bool is_behind = false;
				std::fill(_rhs[i].begin(), _rhs[i].end(), 0.0);           // initialize solution to 0
				std::fill(_x[i].begin(),   _x[i].end(),   0.0);           // initialize rhs to 0
				for (uint j=0,eqn=0; j<_mvs[i].size(); j++) {
					Vector3f p3d = _camera->mvsPnt( _mvs[i][j] ).xyz();        // 3D object space coords of depth sample
					Vector3f p2d = nCam.project(p3d);                          // project 3D coords in novel view

					// Note (seb): I want to discard points that are outside the "2d frustum"
					// of the user camera. But if I don't use a 'margin', small holes appear
					// on borders due to discarded spixels that had a (small) part visible in
					// the current view but without any point inside.
					// I think this margin should be equal to one max spixel width (in [0,1])
					// because we cannot cut spixels.
					// Or another way would be to make this margin equal to the maximum
					// distance of two points in a spixel (but I guess results would be worst
					// because only based on few points for transforming one spixel)
					const float margin = 0.2f; /*should be equal to the standard width of a spixel in [0,1]*/
					const float border = 1.f + margin;// / nCam.aspect() + margin;
					if (p2d[0] < -border || p2d[0] > border)// || p2d[1] < -border || p2d[1] > border)
						is_behind |= true;

					_rhs[i][eqn++] = w_depth * p2d[0] * aspect_correction;// update RHS of linear system
					_rhs[i][eqn++] = w_depth * p2d[1];

					is_behind |= (nCam.dir().dot((p3d-nCam.position()).normalized()) <= 0.35); // 70 degrees
				}
				if (!is_behind)
					_lhs[i]->solve(_A_adjoint[i], _rhs[i], _x[i]);
			}
		}

		for (uint i=0,k=0; i<num_superpixels; i++) {
			for (uint j=0; j<_x[i].size()/2; j++) {
				_vertices[k++] = (float)_x[i][2*j+0];
				_vertices[k++] = (float)_x[i][2*j+1];
			}
		}
	}

	const std::vector<DelaunayMesh>& ImageWarper::getSpixelMesh()
	{
		return _spmesh;
	}

	void	ImageWarper::createWarpMesh( void )
	{
		_spmesh.resize(_spimg->numSpixels());

		// Create a separate warp mesh for each superpixel
		// Warp mesh vertex coordinates lie in x = [-w/2,w/2], y = [-h/2,h/2] space
		for (uint i=0; i<_spimg->numSpixels(); i++)
		{
			std::vector<sibr::Vector2d> t;
			_spimg->getSpixel(i).warpMesh(t, _spimg->w(), _spimg->h());       // select some pixels from superpixel as warp mesh vertices

			DelaunayMesh tr;
			for (uint j=0,id=0; j<t.size(); j++)
			{
				double xx = t[j][0], yy = t[j][1];
				Point pnt(xx, yy);
				DT_vertex_handle vh;
				try {
					vh = tr.insert(pnt);
				}
				catch (std::exception& e) {
					std::cerr << "problem in insert " << e.what() <<"  continue " << std::endl;
					continue;
				}
				if (vh->info().spxl_id < 0)           // newly added vertex to warp mesh
					vh->info().spxl_id = id++;            // vertex id within the spixel warp mesh
				
				if(vh->info().spxl_id > 10)
					SIBR_DEBUG(vh->info().spxl_id);
			}
			_spmesh[i] = tr;
		}

		// Assign global numbers to warp mesh vertices, numbering across all warp meshes
		// useful for setting up single OpenGL index buffer for all warp meshes
		for (uint i=0, id=0; i<_spmesh.size(); i++)
		{
			DelaunayMesh* t = &_spmesh[i];
			DT_vertex_iterator v = t->finite_vertices_begin();
			for (; v!=t->finite_vertices_end(); v++)
				v->info().id = id++;
		}
		//SIBR_DEBUG(_spmesh.size());
	}

	void	ImageWarper::createLinearSystem( const std::vector<bool>& activatedSpixels )
	{
		/// \todo TODO: This code doesn't seem to reduce the number of vertices when some
		/// spixels are disable.

		uint num_superpixels = (uint)_spmesh.size();
		uint num_factorized  = 0;

		// one linear system for each superpixel
		_lhs.resize(num_superpixels);
		for ( std::unique_ptr<CholmodEigenSolver>& solverPtr : _lhs )
			solverPtr.reset(new CholmodEigenSolver());

		_rhs.resize(num_superpixels);
		_x  .resize(num_superpixels);
		_mvs.resize(num_superpixels);
		_A_adjoint.resize(num_superpixels);

		// build a list of depth samples in each superpixel
		for (uint i=0; i<_camera->numMVSPnts(); i++) {
			uint k = _camera->mvsPnt(i).spixelID();
			//	SIBR_ASSERT(k>=0 && k<num_superpixels);
			if( k< 0 || k > num_superpixels ) {
				SIBR_DEBUG(k);
				SIBR_DEBUG(num_superpixels);
				std::cerr <<"[SIBR swarp] ERROR IN SUPERPIXELS " << k << " For MVS ID " << i << std::endl;
				continue;
			}
			_mvs[k].push_back(i);
		}

		for (uint i=0; i<num_superpixels; i++) {
			int eqn     = 0;
			int numEqn  = int(2*_spmesh[i].number_of_faces() + 2*_mvs[i].size());
			int numVars = int(2*_spmesh[i].number_of_vertices());

			_rhs[i].resize(numEqn,  0.0);
			_x  [i].resize(numVars, 0.0);

			if (_spimg->getSpixel(i).isActive() == false || activatedSpixels[i] == false) {
				//SIBR_DEBUG(_spimg->getSpixel(i).isActive() == false);
				//SIBR_DEBUG(activatedSpixels[i] == false);
				continue;	// skip if superpixel is inactive or warp mesh is empty
			}

			Eigen::SparseMatrix<double> A(numEqn, numVars);
			A.resize(numEqn, numVars);
			A.setZero();
			std::vector<Eigen::Triplet<double>> tripletList;
			tripletList.reserve(numEqn * numVars);

			// Re-projection constraint for all depth samples of the superpixels
			for (uint j=0; j<_mvs[i].size(); j++) {
				Vector3f p = _camera->project(_camera->mvsPnt( _mvs[i][j] ).xyz());
				p[0] = std::min(std::max(p[0],-1.0f),1.0f);
				p[1] = std::min(std::max(p[1],-1.0f),1.0f);

				DT_face_handle f = _spmesh[i].locate(Point(p[0], p[1]));  // triangle containing the depth sample
				if (f == NULL || _spmesh[i].is_infinite(f))
					continue;

				sibr::Vector2d p1 = sibr::Vector2d(CGAL::to_double(f->vertex(0)->point().x()), CGAL::to_double(f->vertex(0)->point().y()));
				sibr::Vector2d p2 = sibr::Vector2d(CGAL::to_double(f->vertex(1)->point().x()), CGAL::to_double(f->vertex(1)->point().y()));
				sibr::Vector2d p3 = sibr::Vector2d(CGAL::to_double(f->vertex(2)->point().x()), CGAL::to_double(f->vertex(2)->point().y()));

				sibr::Vector3d r = barycentric_coeffs(Vector2f(p[0],p[1]), p1, p2, p3);                  // compute barycentric coord of re-projected depth sample
				tripletList.push_back(Eigen::Triplet<double>(eqn,   2*f->vertex(0)->info().spxl_id + 0, w_depth * r[0]));
				tripletList.push_back(Eigen::Triplet<double>(eqn,   2*f->vertex(1)->info().spxl_id + 0, w_depth * r[1]));
				tripletList.push_back(Eigen::Triplet<double>(eqn,   2*f->vertex(2)->info().spxl_id + 0, w_depth * r[2]));
				tripletList.push_back(Eigen::Triplet<double>(eqn+1, 2*f->vertex(0)->info().spxl_id + 1, w_depth * r[0]));
				tripletList.push_back(Eigen::Triplet<double>(eqn+1, 2*f->vertex(1)->info().spxl_id + 1, w_depth * r[1]));
				tripletList.push_back(Eigen::Triplet<double>(eqn+1, 2*f->vertex(2)->info().spxl_id + 1, w_depth * r[2]));

				eqn += 2;
			}

			// Similarity (shape-preserving) constraint for all triangles of each warp mesh
			DelaunayMesh* t = &_spmesh[i];
			for (DT_face_iterator f=t->finite_faces_begin(); f!=t->finite_faces_end(); f++) {
				DT_vertex_handle vh0 = f->vertex(0);
				DT_vertex_handle vh1 = f->vertex(1);
				DT_vertex_handle vh2 = f->vertex(2);

				sibr::Vector2d v0 = sibr::Vector2d(CGAL::to_double(vh0->point().x()), CGAL::to_double(vh0->point().y()));
				sibr::Vector2d v1 = sibr::Vector2d(CGAL::to_double(vh1->point().x()), CGAL::to_double(vh1->point().y()));
				sibr::Vector2d v2 = sibr::Vector2d(CGAL::to_double(vh2->point().x()), CGAL::to_double(vh2->point().y()));
				sibr::Vector2d uv = similar_constraint_coeffs(v0, v1, v2);

				tripletList.push_back(Eigen::Triplet<double>(eqn, 2*vh0->info().spxl_id + 0, w_sim * 1.0));
				tripletList.push_back(Eigen::Triplet<double>(eqn, 2*vh1->info().spxl_id + 0, w_sim * -(1.0 - uv[0])));
				tripletList.push_back(Eigen::Triplet<double>(eqn, 2*vh2->info().spxl_id + 0, w_sim * -uv[0]));
				tripletList.push_back(Eigen::Triplet<double>(eqn, 2*vh1->info().spxl_id + 1, w_sim * uv[1]));
				tripletList.push_back(Eigen::Triplet<double>(eqn, 2*vh2->info().spxl_id + 1, w_sim * -uv[1]));

				tripletList.push_back(Eigen::Triplet<double>(eqn+1, 2*vh0->info().spxl_id + 1, w_sim * 1.0));
				tripletList.push_back(Eigen::Triplet<double>(eqn+1, 2*vh1->info().spxl_id + 1, w_sim * -(1.0-uv[0])));
				tripletList.push_back(Eigen::Triplet<double>(eqn+1, 2*vh2->info().spxl_id + 1, w_sim * -uv[0]));
				tripletList.push_back(Eigen::Triplet<double>(eqn+1, 2*vh1->info().spxl_id + 0, w_sim * -uv[1]));
				tripletList.push_back(Eigen::Triplet<double>(eqn+1, 2*vh2->info().spxl_id + 0, w_sim * uv[1]));

				eqn += 2;
			}

			//SIBR_ASSERT(eqn == numEqn);
			if (eqn != numEqn)
				continue;

			A.setFromTriplets(tripletList.begin(), tripletList.end());
			_A_adjoint[i] = A.adjoint();
			if (_lhs[i]->precompute(_A_adjoint[i] * A))
				num_factorized += 1;

		}

		std::cout << "[" << num_factorized << "/" << num_superpixels << "] " << std::flush;
	}

	void	ImageWarper::createVertexBuffer( void )
	{
		size_t faceCount = 0;
		size_t vertexCount = 0;

		// vertices/faces over all warp meshes
		for (uint i=0; i<_spmesh.size(); i++)
		{
			faceCount += _spmesh[i].number_of_faces();
			vertexCount += _spmesh[i].number_of_vertices();
		}

		std::vector<float> vertices(2*vertexCount);
		std::vector<float> texCoord(4*vertexCount);
		std::vector<uint> indices(3*faceCount);

		for (uint i=0, idx=0; i<_spmesh.size(); i++)
		{
			DelaunayMesh* t = &_spmesh[i];
			DT_vertex_iterator v = t->finite_vertices_begin();
			for (; v!=t->finite_vertices_end(); v++)
			{
				vertices[2*v->info().id+0] = (float)CGAL::to_double(v->point().x());	// x = [-1,1] for GL
				vertices[2*v->info().id+1] = (float)CGAL::to_double(v->point().y());	// y = [-1,1] for GL

				texCoord[4*v->info().id+0] = 0.5f+0.5f*(float)CGAL::to_double(v->point().x());	// tex coord x = [0,1]
				texCoord[4*v->info().id+1] = 0.5f+0.5f*(float)CGAL::to_double(v->point().y());	// tex coord y = [0,1]
				//_texCoord[4*v->info().id+2] = _spimg->sPixel(i).getMedianDepth();		// median superpixel depth
				texCoord[4*v->info().id+2] = (_spimg->getSpixel(i).getMedianDepth()/2.0f)+0.5f;		// median superpixel depth

				texCoord[4*v->info().id+3] = (float)_spimg->getSpixel(i).getGID().toUint();		// superpixel gid
			}

			DT_face_iterator f(t->finite_faces_begin());     // index buffer
			for (; f!=t->finite_faces_end(); f++)
			{
				indices[idx++] = f->vertex(0)->info().id;     // indices for each
				indices[idx++] = f->vertex(1)->info().id;     // vertex of each triangle
				indices[idx++] = f->vertex(2)->info().id;     // of the warp mesh
			}
		}

		//_spmesh.clear();    // deallocate warp meshes - OpenGL vertex buffers already setup
		_vertices = std::move(vertices);
		_texCoord = std::move(texCoord);
		_indices = std::move(indices);
		_mesh.reset(new WarpMesh());
		_mesh->create(_indices, _vertices, _texCoord);
	}

	Vector3d  ImageWarper::barycentric_coeffs(const Vector2f& p,
		const Vector2d& a, const Vector2d& b, const Vector2d& c)
	{
		double z = (b[0]-a[0])*(c[1]-a[1]) - (c[0]-a[0])*(b[1]-a[1]);
		double u = ((b[0]-p[0])*(c[1]-p[1]) - (c[0]-p[0])*(b[1]-p[1])) / z;
		double v = ((c[0]-p[0])*(a[1]-p[1]) - (a[0]-p[0])*(c[1]-p[1])) / z;
		return sibr::Vector3d(u, v, 1.0-u-v);
	}

	sibr::Vector2d  ImageWarper::similar_constraint_coeffs(
		const Vector2d& p1, const Vector2d& p2, const Vector2d& p3)
	{
		sibr::Vector2d base = p3-p2;
		sibr::Vector2d alt  = sibr::Vector2d(base[1],-base[0]);
		double u = dot(sibr::Vector2d(p1-p2), base) / sqLength(base);
		double v = dot(sibr::Vector2d(p1 - (p2 + base*u)), alt) / sqLength(alt);
		return sibr::Vector2d(u,v);
	}

	void			ImageWarper::draw( void )
	{
		_mesh->draw(_vertices, _texCoord);
	}

	SpixelImage::Ptr ImageWarper::getSPImg( void ) const
	{
		return _spimg;
	}

	ImageWarper::~ImageWarper() {}

} /*namespace sibr*/ 
