/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


/**
 * \todo TODO SEB:
 * - We should be able to remove "_spmesh" out of
 *   member data. It seems only used at the
 *   construction.
 * - We only have to keep in this class what is
 *   continuously used at runtime (createWarpMesh
 *   should be removed from here)
 **/

#ifndef __SIBR_EXP_SPIXELWARP_IMAGEWARPER_HPP__
# define __SIBR_EXP_SPIXELWARP_IMAGEWARPER_HPP__

# include <vector>

# include <core/graphics/Config.hpp>

# include <projects/spixelwarp/renderer/Config.hpp>
# include <projects/spixelwarp/renderer/WarpMesh.hpp>
# include <projects/spixelwarp/renderer/CholmodEigenSolver.hpp>
# include <projects/spixelwarp/renderer/cgaldefs.h>

# include <projects/spixelwarp/renderer/MVSPoint.hpp>

namespace sibr
{
	class InputCamera;
	class InputCameraPreprocess;
	class Camera;
} // namespace sibr

namespace sibr 
{
	class SpixelImage;

	/**
	* Computes variational warp for an input image
	* using a linear optimization on the CPU.
	*/
	class SIBR_EXP_SPIXELWARP_EXPORT ImageWarper
	{
		SIBR_DISALLOW_COPY( ImageWarper );

	public:
		~ImageWarper();

		/** Constructor, creates the image warper.
		* \param c associated input camera
		* \param s associated superpixel segmentation
		*/
		ImageWarper( const InputCameraPreprocess* c, const SpixelImage::Ptr& s );

		/** Initialized the image warper - computes the warp meshes, sets up the
		* linear system matrix, factorizes it and creates OpenGL vertex buffers
		*/
		void			init( const std::vector<bool>& activatedSpixels );

		/** Projects all depth samples into novel view and solves the linear system.
		* \param ncam novel camera object to render current view
		*/
		void			warp( const Camera& ncam );

		/** Projects a single super_pixel into the novel view.
		* \param ncam novel camera object to render current view
		* \param spixel The ID of the spixel to warp.
		* Returns a solution to the linear system.
		*/
		std::vector<double>		warpSingle(const Camera& ncam, uint spixel);

		void			draw( void );

		///** Mesh containg geometry for each (warped) superpixels.*/
		//inline const Mesh&		getMesh( void ) const			{ return *_mesh; }

		SpixelImage::Ptr getSPImg( void ) const;

		/** 
		* Returns the triangulated DelaunayMesh. 
		* (Initially added for estimating the quality of the mesh).
		*/
		const std::vector<DelaunayMesh>&	getSpixelMesh();

	private:
		/** Create a warp meshes for each superpixel in the image */
		void createWarpMesh(void);

		/** Populates the linear system matrices and computes
		* Cholesky factorization */
		void createLinearSystem( const std::vector<bool>& activatedSpixels );

		/** Set up OpenGL vertex/index buffers and deallocate warp mesh
		keepSpixelMesh if you need to estimate quality for ibr_selection.
		*/
		void createVertexBuffer( void );

		/**Computes barycentric coordinates of point \a p in a triangle with
		* vertices \a a, \a b and \a c. */
		Vector3d  barycentric_coeffs(const Vector2f& p,
			const Vector2d& a, const Vector2d& b, const Vector2d& c);

		/**Similarity constraint coefficients -  Point \a p1 can be expressed as
		* linear sum of points \a p2 and \a p3 using these coefficients. */
		Vector2d  similar_constraint_coeffs(
			const Vector2d& p1, const Vector2d& p2, const Vector2d& p3);

	protected:
		/** Weight of reprojection constraint, see Chaurasia et al 2001, 2013
		* for details */
		static const int w_depth = 7;

		/** Weight of shape preserving constraint, see Chaurasia 2011, 2013
		* for details */
		static const int w_sim = 1;

		/** Input camera matrices for the associated image. */
		const InputCameraPreprocess*  _camera;

		/** Superpixel segmentation for the associated image. */
		SpixelImage::Ptr	_spimg;

		std::shared_ptr<WarpMesh>	_mesh;
		std::vector<float>			_vertices;
		std::vector<float>			_texCoord;
		std::vector<uint>			_indices;

		/** Warp meshes for each superpixel of input image, each warp
		* mesh is a Delaunay triangulation. */
		std::vector<DelaunayMesh>  _spmesh;

		/** EIGEN solver for each warp mesh. (unique_ptr because CholdModEigenSolver cannot be copied)*/
		std::vector<std::unique_ptr<CholmodEigenSolver>> _lhs;

		/** List of RHS of each linear system. */
		std::vector< std::vector<double> >  _rhs;

		/** Solution vector for each linear system. */
		std::vector< std::vector<double> >  _x;

		/** List of MVS points contained within each warp mesh */
		std::vector< std::vector<uint> >  _mvs;

		std::vector<Eigen::SparseMatrix<double>> _A_adjoint;
	};

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SPIXELWARP_IMAGEWARPER_HPP__
