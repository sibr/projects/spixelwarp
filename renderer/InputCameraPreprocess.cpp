/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "InputCameraPreprocess.hpp"

namespace sibr
{
	InputCameraPreprocess::~InputCameraPreprocess() {};
	uint InputCameraPreprocess::numMVSPnts(void)  const { return (uint)_mvsPnts.size(); }

	/* compatibility for preprocess (depth) */

	Vector2i  InputCameraPreprocess::mvs2DPos(uint i) const {
		Vector3f scrn_pt = projectScreen(_mvsPnts[i]->xyz());
		return Vector2i((int)scrn_pt[0], (int)scrn_pt[1]);
	}

	double InputCameraPreprocess::mvsPntDepth(uint i) const {
		double d = (double)project(_mvsPnts[i]->xyz())[2];
		return d * 0.5 + 0.5;
	}

	void		InputCameraPreprocess::mvsPoints(const MVSPoint::List& pnts) { _mvsPnts = pnts; }

	void InputCameraPreprocess::addMVSPnt(const Vector3f& pos) {
		_mvsPnts.emplace_back(new MVSPoint(pos));
	}

	Vector3f InputCameraPreprocess::projectScreen(const Vector3f& pt) const {
		Vector3f proj_pt = project(pt);
		Vector3f screen_pt((proj_pt[0] + 1.f)*_w / 2.0f, (1.f - proj_pt[1])*_h / 2.0f, proj_pt[2] * 0.5f + 0.5f);

		return screen_pt;
	}

	void InputCameraPreprocess::loadSuperpixelsAndReconstruction(const std::string& path)
	{
		// Load Superpixels
		_spixelImage.reset(new sibr::SpixelImage(_id));

		const unsigned int jpegLenghtExtention = 4;
		std::string nameImage(name().begin(), name().end() - jpegLenghtExtention);
		_spixelImage->load(path + "/spixelwarp/superpixels/" + nameImage + ".sp");

		setScale(this->w() / _spixelImage->w()); //< Scale of processed images


	}
}
