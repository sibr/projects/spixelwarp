/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef _INPUT_CAMERA_PREPROCESS_H_
# define _INPUT_CAMERA_PREPROCESS_H_

# include <core/graphics/Config.hpp>
# include <core/system/Vector.hpp>
# include <core/assets/InputCamera.hpp>
# include <core/scene/BasicIBRScene.hpp>

# include <projects/spixelwarp/renderer/PatchFile.hpp>
# include <projects/spixelwarp/renderer/MVSPoint.hpp>
# include <projects/spixelwarp/renderer/PatchPoint.hpp>
# include <projects/spixelwarp/renderer/MVSPointPreprocess.hpp>
# include <projects/spixelwarp/renderer/SpixelImage.hpp>
# include "Config.hpp"

namespace sibr
{


	// add additional two fields: scale and spixelImage
	// and a few additional functions to access PatchPoints

	class SIBR_EXP_SPIXELWARP_EXPORT InputCameraPreprocess : public InputCamera {

	private:
		/**	Scale of input images w.r.t. superpixel images */
		int _scale;

		/** Superpixel segemntation data structure for each input image */
		std::shared_ptr<SpixelImage> _spixelImage;

		/** */
		PatchPoint* getLastPatchPnt(void);

		/** List of 3D multi-view stereo points */
		MVSPoint::List	_mvsPnts;


	public:
		~InputCameraPreprocess();
		/** \todo All the ugly stuff from old InputCamera; GD: TODO needs cleanup */
			/** Return the i'th MVS point
			* \param i id of MVS point to return
			* \returns MVS points
			* \sa MVS
			*/
		const MVSPoint& mvsPnt(uint i) const { SIBR_ASSERT(i < _mvsPnts.size()); return *_mvsPnts[i]; }

		/** Number of MVS points
		* \returns total MVS points loaded for all superpixels for this input image
		*/
		uint numMVSPnts(void) const;

		/** Update image dimensions. Calls \a update() after changing image width and height
		* \param w image width
		* \param h image height
		*/
		void size(uint w, uint h);

		/** Load MVS points for the camera
		* \param pnts list of MVS points for this camera
		*/
		//void loadMVSPnts( const MVSPoint::List& pnts );
		void					mvsPoints(const MVSPoint::List& pnts);
		const MVSPoint::List&	mvsPoints(void) const;
		/** following there are compatibility functions for depth preprocessing */

		/** unproject the MVS point and return screen coords (uint in [0,w] [0,h]) */
		Vector2i	mvs2DPos(uint i) const;

		/** return 3D pt of mvsPoint */
		Vector3f    mvs3DPos(uint i) { return mvsPnt(i).xyz(); }

		/** return double valued depth */
		double mvsPntDepth(uint i) const;

		/** add an MVS point (typically when loading PatchFile) */
		void	addMVSPnt(const Vector3f& pos);

		/** project into screen space */
		Vector3f projectScreen(const Vector3f& pt) const;

		InputCameraPreprocess() { _scale = 1; }
		InputCameraPreprocess(const InputCamera& c) {
			_focal = c.focal(); _k1 = c.k1();  _k2 = c.k2();
			_w = c.w(), _h = c.h(), _id = c.id(); _active = c.isActive();
			_name = c.name();
			// Camera info
			_transform = c.transform(), _fov = c.fovy(), _aspect = c.aspect();
			_znear = c.znear(), _zfar = c.zfar(), _matViewProj = c.viewproj();

			forceUpdateViewProj();
		}

		/** ratio of size of spixel image to input image */
		int		getScale(void) const { return _scale; }

		uint	numAllPatchPoints(void) { return (uint)_mvsPnts.size(); }	//< Number of visible Patch points for this camera.

		uint	numSPixels(void) { return _spixelImage->numSpixels(); }




		/** Load superpixel color information and recostruction points associated. sets value of scale etc*/

		void loadSuperpixelsAndReconstruction(const std::string& path);

		void freeSuperpixelsAndReconstruction()
		{
			_spixelImage->freeStorage();
		}

		//PlanarSPixelPreprocess* getPlanarSPixel(uint id) { return (PlanarSPixelPreprocess*)_spixelImage->sPixelDIR(id); }
		/*PlanarSPixelPreprocess::Ptr getPlanarSPixel(uint id) {
			//return (PlanarSPixelPreprocess*)_spixelImage->getSpixelPtr(id).get();
			return std::make_shared<PlanarSPixelPreprocess>(
				_planarSpixelImagePreprocess->getPlanarSpixelPreprocess (id));
		};

		uint getNumSpixelsInImage() {
			return _planarSpixelImagePreprocess->numPlanarSpixelsPreprocess();
		}

		void setPlanarSPixel(PlanarSPixelPreprocess::Ptr planarSpixelPrePtr) {
			_planarSpixelImagePreprocess->setSpixel(planarSpixelPrePtr);
		}*/

		/*PlanarSPixelPreprocess::Ptr getPlanarSPixel(uint id) {
			return std::dynamic_pointer_cast<sibr::PlanarSPixelPreprocess::Ptr>_
				(spixelImage->getSpixelPtr(id)); }*/

		void addMVSPntPreprocess(const std::shared_ptr<MVSPointPreprocess>& ep) { _mvsPnts.push_back(ep); }

		void deleteMVSPntPreprocess() {
			_mvsPnts.clear();
		}

		//MVSPointPreprocess* mvsPntPreprocess(uint i) { MVSPointPreprocess* pt = (MVSPointPreprocess*) _mvsPnts[i].get(); return pt; }


		std::shared_ptr<SpixelImage>& getSpixelImage() { return _spixelImage; };


		MVSPointPreprocess* mvsPntPreprocess(uint i) {
			MVSPointPreprocess*  pt = (MVSPointPreprocess*)_mvsPnts[i].get();
			/*MVSPointPreprocess::Ptr pt = std::make_shared<MVSPointPreprocess>(
				_mvsPnts[i]->xyz(),
				_mvsPnts[i]->spixelID());*/
			return pt;
		}

		/** Load Patch information */
		// add PMVS points to InputCameras
		static	void loadPatchMVSPoints(BasicIBRScene::Ptr scene, PatchFile& patchFile, std::vector<InputCameraPreprocess>& camlist)
		{
			for (int pointID = 0; pointID < patchFile.pointCount(); pointID++) {
				sibr::PatchPoint p = patchFile.point(pointID);
				for (const uint camIndex : p.visibleInImages()) {
					std::shared_ptr<MVSPointPreprocess> ep(new MVSPointPreprocess(p.position()));
					ep->patchPnt(p);
					camlist[camIndex].addMVSPntPreprocess(ep);
				}
			}
		}

		/** Load Patch information per camera*/
		// add PMVS points to given input camera range to avoid memory saturation; free afterwards

		static	void loadPatchMVSPoints4CameraRange(BasicIBRScene::Ptr scene, PatchFile& patchFile, std::vector<InputCameraPreprocess>& camlist, int starti, int endi)
		{
			for (int pointID = 0; pointID < patchFile.pointCount(); pointID++) {
				sibr::PatchPoint p = patchFile.point(pointID);
				for (const uint camIndex : p.visibleInImages()) {
					if (camIndex >= (uint)starti && camIndex <= (uint)endi) {
						std::shared_ptr<MVSPointPreprocess> ep(new MVSPointPreprocess(p.position()));
						ep->patchPnt(p);
						camlist[camIndex].addMVSPntPreprocess(ep);
					}
				}
			}
		}

		protected : 
			void setScale(int scale) { _scale = scale; }

	};



} // namespace sibr

#endif
