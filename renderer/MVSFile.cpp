/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <fstream>
#include "MVSFile.hpp"
#include "Config.hpp"

namespace sibr
{
	MVSFile::MVSFile() {};

	MVSFile::~MVSFile() {};

	bool MVSFile::load( const std::string& filename, bool verbose )
	{
		std::fstream	file(filename, std::ios::in);

		_width = _height = 0;
		_spixels.clear();
		if (file)
		{
			file.exceptions(std::ios::failbit | std::ios::badbit);
			try
			{
				uint trashSpixelId;
				uint nbSpixels = 0;
				file >> _width >> _height >> nbSpixels;

				_spixels.resize(nbSpixels);
				for (uint i = 0; i < nbSpixels; ++i)
				{
					uint nbPoints = 0;
					Vector3f v;

					file >> trashSpixelId >> nbPoints >> _spixels[i].medianDepth;
					_spixels[i].points.reserve(nbPoints);
					for (uint j = 0; j < nbPoints; ++j)
					{
						file >> v[0] >> v[1] >> v[2];
						_spixels[i].points.emplace_back(v, i);
					}
				}

			}
			catch (std::exception& e)
			{
				SIBR_ERR << "error detected while reading '"<<filename<<"': "<<e.what();
			}

			if (!file.fail())
			{
				if( verbose )
					SIBR_FLOG << "'"<< filename <<"' successfully loaded." << std::endl;
				else
					std::cerr << ".";

				return true;
			}
		}
		SIBR_WRG << "file not found: '"<<filename<<"'"<<std::endl;
		return false;
	}



} // namespace sibr
