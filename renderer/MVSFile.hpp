/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_ASSETS_MVSFILE_HPP__
# define __SIBR_ASSETS_MVSFILE_HPP__

# include <core/graphics/Config.hpp>
# include <core/system/Vector.hpp>
# include <core/assets/IFileLoader.hpp>
# include <projects/spixelwarp/renderer/MVSPoint.hpp>
# include "Config.hpp"

namespace sibr
{

	/**
	Handle MVS file data loading.
	\ingroup sibr_assets
	*/
	class SIBR_EXP_SPIXELWARP_EXPORT MVSFile : public IFileLoader
	{
	public:
		struct Spixel
		{
			float					medianDepth;
			std::vector<MVSPoint>	points;
		};

	public:
		MVSFile();
		virtual ~MVSFile();
		virtual bool load( const std::string& filename, bool verbose=true );

		uint						height( void ) const;
		uint						width( void ) const;
		const std::vector<Spixel>&	spixels( void ) const;


		//* \<width\> \<height\> \<numOfSuperpixels\>
		//\<idSuperpixel\> \<numberOfPoints\> \<medianDepth\> \<point1\> \<point2\> ... \<pointN\>

	private:
		uint				_width;
		uint				_height;
		std::vector<Spixel>	_spixels;
	};

	///// DEFINITIONS /////

	inline uint						MVSFile::height( void ) const {
		return _height;
	}

	inline uint						MVSFile::width( void ) const {
		return _width;
	}

	inline const std::vector<MVSFile::Spixel>&	MVSFile::spixels( void ) const {
		return _spixels;
	}


} // namespace sibr

#endif // __SIBR_ASSETS_MVSFILE_HPP__
