/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "MVSPoint.hpp"

namespace sibr
{

	MVSPoint::MVSPoint( Vector3f xyz, uint spixelID ) :
		_xyz(xyz), _spixelID(spixelID)
	{
	}

} // namespace sibr
