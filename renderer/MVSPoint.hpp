/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_ASSETS_MVSPOINT_HPP__
# define __SIBR_ASSETS_MVSPOINT_HPP__

# include <projects/spixelwarp/renderer/Config.hpp>
# include "core/system/Vector.hpp"

namespace sibr
{

	/** 3D point computed by multi-view stereo. Each input camera has a list of MVS points
	* each with its 3D coordinates and ID of superpixel that containts the 2D projection
	* of the MVS point.
	*
	* \sa InputCamera
	* \ingroup sibr_assets
	*/
	class SIBR_EXP_SPIXELWARP_EXPORT MVSPoint
	{
	public:
		typedef std::shared_ptr<MVSPoint>	Ptr;
		typedef std::vector<MVSPoint::Ptr>		List;
	public:
		MVSPoint( Vector3f xyz=Vector3f(0.f, 0.f, 0.f), uint spixelID=0 );

		/** Returns the 3D coordinates of MVS point. */
		const Vector3f&		xyz( void ) const;
		/** Returns the ID of superpixel that contains the MVS point. */
		uint				spixelID( void ) const;

	private:
		/** 3D coordinates of MVS point. */
		Vector3f	_xyz;
		/** ID of superpixel that contains the MVS point. */
		uint		_spixelID;
	};

	///// DEFINITIONS /////

	inline const Vector3f&		MVSPoint::xyz( void ) const {
		return _xyz;
	}

	inline uint				MVSPoint::spixelID( void ) const {
		return _spixelID;
	}


} // namespace sibr

#endif // __SIBR_ASSETS_MVSPOINT_HPP__
