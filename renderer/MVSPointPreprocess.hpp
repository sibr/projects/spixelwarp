/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_ASSETS_MVSPOINTPREPROCESS_HPP__
# define __SIBR_ASSETS_MVSPOINTPREPROCESS_HPP__

# include "core/system/Vector.hpp"

# include "MVSPoint.hpp"
# include <projects/spixelwarp/renderer/PatchPoint.hpp>

namespace sibr
{
	/**
	MVS preprocess for points.
	\ingroup sibr_assets
	*/
	class SIBR_EXP_SPIXELWARP_EXPORT MVSPointPreprocess  : public MVSPoint {
	public :
		typedef std::shared_ptr<MVSPointPreprocess>			Ptr;
		typedef std::vector<MVSPointPreprocess::Ptr>		List;
	private:
		// gives access to the visibility list
		PatchPoint _patchPnt;
		Vector2i	_pos2D;
	public:
		MVSPointPreprocess(Vector3f p) : MVSPoint(p) {} ;
		MVSPointPreprocess(Vector3f p, uint spixelID ) : MVSPoint(p, spixelID) {};
		
		PatchPoint&			patchPnt() { return _patchPnt; } 
		const PatchPoint&	patchPnt() const { return _patchPnt; } 
		void				patchPnt(const PatchPoint& p) { _patchPnt = p; }
		
		Vector2i&			pos2D() { return _pos2D; }
		const Vector2i&		pos2D() const { return _pos2D; }
		void				pos2D( const Vector2i& pxl ) { _pos2D = pxl; }
		
	
	};
}

#endif
