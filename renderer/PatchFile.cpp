/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <iostream>
#include <fstream>
#include <exception>
#include <bitset>
#include <algorithm>
#include <boost/filesystem/operations.hpp>
#include <ctime>
#include <iostream>

#include <core/system/ByteStream.hpp>
#include <core/system/LoadingProgress.hpp>
#include <core/assets/InputCamera.hpp>

#include <projects/spixelwarp/renderer/PatchFile.hpp>
#include <projects/spixelwarp/renderer/PatchPoint.hpp>

namespace sibr
{
	std::string		PatchFile::search( const std::string& datasetPath )
	{
		std::string out = "";

		// Searching paths for the Patch file.
		std::vector<std::string> patchFilenames;
		//patchFilenames.push_back("pmvs/models/option-highres.txt.patch");
		//patchFilenames.push_back("pmvs/models/pmvs_options.patch");
		//patchFilenames.push_back("pmvs/models/pmvs_recon.patch");
		//patchFilenames.push_back("pmvs/models/pmvs_recon.patchbin");
		//patchFilenames.push_back("pmvs/models/proxy.patch");
		//patchFilenames.push_back("proxy/proxy.patch");
		/// \todo TODO
		patchFilenames.push_back("pmvs/models/pmvs_recon.patchbin");
		patchFilenames.push_back("meshes/pmvs_recon.patchbin");
		patchFilenames.push_back("meshes/pmvs_recon.patch");

		bool loaded = false;
		SIBR_LOG << "Searching for the patch file..." << std::endl;
		for (unsigned i = 0; i < patchFilenames.size() && loaded == false; ++i)
		{
			out = datasetPath + "/" + patchFilenames[i];
			if (fileExists(out))
			{
				SIBR_LOG << "Searching in: " << out << "...found!" << std::endl;
				return out;
			}
			else
				SIBR_LOG << "Searching in: " << out << "..." << std::endl;
		}
		return out;
	}

	const PatchPoint&	PatchFile::point( uint id ) const {
		return _points.at(id);
	}
	void				PatchFile::point( uint n, const PatchPoint& p )
	{
		if (_points.size() <= n)
			_points.resize(n+1);
		_points[n] = p;
	}

	void				PatchFile::resize( uint count )
	{
		_points.resize(count);
	}


	bool	PatchFile::load( const std::string& filename )
	{
		std::string extension = filename.substr(filename.find_last_of('.'));
		if (extension == ".patch")
		{
			std::string binaryName = filename.substr(0, filename.size() - extension.size()) + ".patchbin";
			
			boost::filesystem::path asciiPath( filename );
			boost::filesystem::path binPath( binaryName );

			if (boost::filesystem::exists(asciiPath))
			{
				if (boost::filesystem::exists(binPath))
				{
					// Comparing both modification time
					auto asciiTime = boost::filesystem::last_write_time(asciiPath);
					auto binTime = boost::filesystem::last_write_time(binPath);
					if (asciiTime <= binTime)
						return loadFromBinaryFile(binaryName);
				}
				SIBR_LOG << "The binary version of '" << filename << "' does not exist or is not up to date." << std::endl;
				bool success = loadFromASCIIFile(filename);

				if (!success)
					return false;

				SIBR_LOG << "Building binary version in '" << binaryName << "'." << std::endl;
				saveToBinaryFile(binaryName);

				return success;
			}
			else if (boost::filesystem::exists(binPath))
			{
				SIBR_LOG << "file not found '" << filename << 
					"', searching loading binary version." << std::endl;
				return loadFromBinaryFile(binaryName);
			}
			else
				SIBR_ERR	<< "error: file not found '" << filename
							<< "' (ascii or binary version)" << std::endl;
		}
		else if (extension == ".patchbin")
			return loadFromBinaryFile(filename);
		else
			SIBR_LOG << "ERROR: file extension '" << extension << "'not recognized when loading "
			"patchfile (should be either .patch or .patchbin)." << std::endl;
		return false;
	}

	static std::string	formatError( const char* msg, uint line )
	{
		std::stringstream oss;
		oss << msg << " (line: " << line << ")" << std::endl;
		return oss.str();
	}

	bool	PatchFile::loadFromASCIIFile( const std::string& filename )
	{
		SIBR_PROFILESCOPE;
		SIBR_LOG << "Reading Patch file '" << filename << "'..." << std::endl; 
		std::ifstream	file(filename.c_str(), std::ios::in);
		
		try
		{

			if (!file)
				throw std::runtime_error("cannot open file");

			file.exceptions( std::ifstream::failbit | std::ifstream::badbit );

			std::string line;
			int linecount = -1;

			std::getline(file, line), ++linecount;
			if (line != "PATCHES")
				throw std::runtime_error(formatError("incorrect header: wrong magic string", linecount));

			uint totalPoints = 0;
			file >> totalPoints;
			if (totalPoints == 0)
				throw std::runtime_error(formatError("incorrect header: invalid number of points", linecount));
			std::getline(file, line), ++linecount;

			resize(totalPoints);
			PatchPoint	point;
			float				position[3];
			float				normal[3];
			float				confidence = 0.f;
			float				f = 0.f;
			uint				imageCount = 0;

			// Display purpose only
			const uint showProgressEveryNPatches = 50000;
			uint patchCount = 0;

			for (uint i = 0; i < totalPoints; ++i)
			{
				// Sub-header
				std::getline(file, line), ++linecount;

				if (line != "PATCHS")
					throw std::runtime_error(formatError("missing string 'PATCHS' detected", linecount));

				// Position
				file >> position[0] >> position[1] >> position[2] >> f;
				if (f != 0.f && f != 1.f)
				{
					position[0] = position[0] / f;
					position[1] = position[1] / f;
					position[2] = position[2] / f;
				}
				point.position(position[0], position[1], position[2]);
				std::getline(file, line), ++linecount;

				// Normal
				file >> normal[0] >> normal[1] >> normal[2] >> f;
				if (f != 0.f && f != 1.f)
				{
					normal[0] = normal[0] / f;
					normal[1] = normal[1] / f;
					normal[2] = normal[2] / f;
				}
				point.normal(normal[0], normal[1], normal[2]);
				std::getline(file, line), ++linecount;

				// confidence
				file >> confidence;
				point.confidence(confidence);
				// Discard debug values
				file >> f >> f;
				std::getline(file, line), ++linecount;

				// Visible in Images
				{
					file >> imageCount;
					std::getline(file, line), ++linecount;
					uint id = 0;
					PatchImageList visibleInImages;
					if (imageCount)
					{
						for (uint j = 0; j < imageCount; ++j)
						{
							file >> id;
							visibleInImages.emplace_back(id);
						}
						std::getline(file, line), ++linecount;
					}
					point.visibleInImages() = visibleInImages;
				}

				uint uncertainImageCount = 0;
				// Uncertain Images
				{
					file >> imageCount;
					uncertainImageCount = imageCount;
					std::getline(file, line), ++linecount;
					uint id = 0;
					PatchImageList uncertainImages;
					if (imageCount)
					{
						for (uint j = 0; j < imageCount; ++j)
						{
							file >> id;
							uncertainImages.emplace_back(id);
						}
						std::getline(file, line), ++linecount;
					}
					point.uncertainImages() = uncertainImages;
				}
				// Two empty lines if uncertain = 0, else just one
				std::getline(file, line), ++linecount;
				if( uncertainImageCount == 0 )
					std::getline(file, line), ++linecount;

				// Display progression
				if (++patchCount > showProgressEveryNPatches)
				{
					patchCount = 0;
					SIBR_LOG << "Progress " << (100.f * (float)i) / (float)totalPoints << "%" << std::endl;
				}
				PatchFile::point(i, point);
			}

			file.close();
			SIBR_LOG << "Reading Patch file '" << filename << "'.[SUCCESS]" << std::endl; 
			return true;
		}
		catch (const std::exception& e)
		{
			SIBR_LOG << "error: cannot load file '" << filename
				<< "' ("<< e.what() << ")" << std::endl;
			file.close();
		}
		return false;
	}

	void	PatchFile::saveToASCIIFile( const std::string& filename ) const
	{
		SIBR_PROFILESCOPE;
		std::ofstream file(filename.c_str(), std::ios::out | std::ios::trunc);

		if (!file)
		{
			SIBR_LOG << "error: cannot save to file '" << filename << "'"
				" (no permission access)" << std::endl;
			return;
		}

		// Note: I don't use std::endl but '\n' to avoid flushing/be faster
		file << "PATCHES"<< '\n';
		file << pointCount() << '\n';
		LoadingProgress	progress(pointCount(), "saving .patch");
		for (uint i = 0; i < pointCount(); ++i, progress.walk())
		{
			const PatchPoint& p = point(i);
			const sibr::Vector3f position	= p.position();
			const sibr::Vector3f normal		= p.normal();

			file << "PATCHS" << '\n';
			file
				<< position[0] << ' '
				<< position[1] << ' '
				<< position[2] << ' '
				<< 1 << '\n';
			file
				<< normal[0] << ' '
				<< normal[1] << ' '
				<< normal[2] << ' '
				<< ((!normal[0]&&!normal[1]&&!normal[2])? 0 : 1) << '\n';
			file << p.confidence() << ' '
				<< "0 0" << '\n'; // used for debugging purposes
			
			// write number of elements in a list and then write each element
			auto saveImageIdListFct = [&file] ( const PatchImageList& list ) {
				file << list.size() << '\n';
				if (list.size())
				{
					file << (uint)list[0];
					for (uint i = 1; i < list.size(); ++i)
						file << ' ' << (uint)list[i];
					// Note: here there is a special case for the first one so
					// that we can avoid to print an additional ' ' at the end.
					// (but in the original files they got this useless space)
					file << ' ';
					file << '\n';
				}
			};

			saveImageIdListFct(p.visibleInImages());
			saveImageIdListFct(p.uncertainImages());

			file << '\n' << '\n';
		}
		SIBR_LOG << "saved " << filename << " (patch file)" << std::endl;
		file.close();
	}


	/// Find in the 8RankHeader the index of the byte ( \param bytePos ),
	/// and the index of the bit in this byte ( \param bitPos )
	/// corresponding to the rank containg the given \param imageId . (You absolutly need to
	/// read specs of .patchbin to understand this sentence...)
	///
	/// used by: imageIdsTo8Ranks()
	static void find8RankPosition( uint32 imageId, uint32& bytePos, uint8& bitPos )
	{
		const uint idPerBit = 8; // read .patchbin's spec for more details on this
		const uint idPerByte = 64 /*(idPerBit * 8)*/; // read .patchbin's spec for more details on this
		bytePos = (imageId / idPerByte);
		bitPos = static_cast<uint8>(((imageId % idPerByte) / idPerBit));
	}

	/// Convert a list of image index into an 8RankHader and its ranks. (8Rank system is
	/// explained in specs if .patchbin)
	///
	/// used by: imageIdsToByteStream()
	static void imageIdsTo8Ranks( const std::vector<uint>& ids,
		std::vector<uint8>& id8RankHeader, std::vector<uint8>& ranks )
	{
		std::vector<uint> sortedIds = ids;
		// this is really important (we assume after that number can inly be greater)
		std::sort(sortedIds.begin(), sortedIds.end());

		for (uint j = 0; j < sortedIds.size(); ++j)
		{
			uint imageId = sortedIds[j];
			uint32 byte8RankPos;
			uint8 bit8RankPos;
			find8RankPosition(imageId, byte8RankPos, bit8RankPos);

			assert(id8RankHeader.size() > byte8RankPos);
			if ( (id8RankHeader[byte8RankPos] & (0x1 << bit8RankPos)) == false)
			{ // if the 8rank flag is not enabled yet
				ranks.push_back(0); // this mean its rank does not exist, so we create it
				id8RankHeader[byte8RankPos] = id8RankHeader[byte8RankPos] | (0x1 << bit8RankPos); // enable this flag
			}
			uint8 bitRankPos = imageId % 8 /* 8 = number of id per rank*/;
			// because we work using an ordered list, we know that the last rank is always the current one.
			ranks.back() = ranks.back() | (0x1 << bitRankPos);
		}
	}

	/// Encode a list of image index into a ByteStream using 8Rank system
	/// (8Rank system is explained in specs if .patchbin).
	///
	/// used by: saveToBinaryFile()
	static void imageIdsToByteStream( uint32 numberOf8RankBytes, const std::vector<uint>& ids, ByteStream& bytes )
	{
		std::vector<uint8>			id8RankHeader( numberOf8RankBytes, 0);
		std::vector<uint8>			ranks;
		imageIdsTo8Ranks(ids, id8RankHeader, ranks);
		// Write the 8Rank 'header'
		for (uint j = 0; j < id8RankHeader.size(); ++j)
			bytes << id8RankHeader[j];
		// ... and all its ranks
		for (uint j = 0; j < ranks.size(); ++j)
			bytes << ranks[j];
	}

	void	PatchFile::saveToBinaryFile( const std::string& filename )
	{
		SIBR_PROFILESCOPE;
		SIBR_LOG << "Saving PatchFile to binary file '" << filename << "'" << std::endl;
		ByteStream	bytes;

		//// Write the Header ////
		int8	magicString[] = "PATCHBIN";
		uint32	versionNumber = SIBR_PATCHFILE_VERSION;
		uint32	numberOfPoints = static_cast<uint32>(pointCount());
		uint32	numberOf8RankBytes;
		{ // Calculate the numberOfFlagBits
			// We could calculate this simply using the number of images minus one.
			// However, in theory, this is not always true (if we remove some images)
			// ... and I want to decouple this from images.

			uint maxImageId = 0;
			for (uint i = 0; i < pointCount(); ++i)
			{ //Find the image id that is the biggest through each point
				// Search max id through visibleInImages
				auto imageIds = point(i).visibleInImages();
				for( uint j = 0; j < imageIds.size(); ++j)
					maxImageId = std::max(imageIds.at(j), maxImageId);
				// Search max id through uncertainImages
				imageIds = point(i).uncertainImages();
				for( uint j = 0; j < imageIds.size(); ++j)
					maxImageId = std::max(imageIds.at(j), maxImageId);
			}

			// Determine the number of bytes required for storing a such images
			const uint idPerByte = 64; // read .patchbin's spec for more details on this
			numberOf8RankBytes = maxImageId / idPerByte + int((maxImageId % idPerByte)? 1 : 0);
		}

		// Write the magic number
		for (uint8 i = 0; i < 8; ++i)
			bytes << magicString[i];
		bytes << int8('\0');

		// and other settings
		bytes << versionNumber << numberOfPoints << numberOf8RankBytes;

		// Tools for showing progress
		const uint showProgressEveryPoint = 500000;
		uint	showProgressPointCount = 0;

		// Write each point			
		for (uint32 i = 0; i < pointCount(); ++i)
		{
			const PatchPoint& p = point(i);
			const sibr::Vector3f normal = p.normal();
			const sibr::Vector3f position = p.position();
			float confidence = p.confidence();
			bool hasNormal = (normal[0] != 0.f || normal[1] != 0.f || normal[2] != 0.f);
			bool hasUncertainImages = (p.uncertainImages().size() > 0);

			// Write flags for optional point params
			{
				std::bitset<8> flags(0);
				flags.set(0, hasNormal);
				flags.set(1, hasUncertainImages);
				// had future flags here
				bytes << static_cast<uint8>(flags.to_ulong());
			}
			bytes << position[0] << position[1] << position[2]; // 3d position
			if (hasNormal)
				bytes << normal[0] << normal[1] << normal[2];	// normal
			bytes << confidence;								// confidence score

			// Write the list of image ids where this point is visible
			imageIdsToByteStream( numberOf8RankBytes, p.visibleInImages(), bytes);

			// Optionally, write the list of image ids where this point may be visible
			if (hasUncertainImages)
				imageIdsToByteStream( numberOf8RankBytes, p.visibleInImages(), bytes);
			if (++showProgressPointCount > showProgressEveryPoint)
			{
				showProgressPointCount = 0;
				SIBR_LOG << "writing .patchbin " << ((100.f*i) / pointCount()) << "%..." << std::endl;
			}
		}
		SIBR_LOG << "writing .patchbin 100%..." << std::endl;

		bytes.saveToFile(filename);

	} 

	/// Decore a list of image index from a ByteStream using 8Rank system
	/// (8Rank system is explained in specs if .patchbin).
	///
	/// used by: loadToBinaryFile()
	static std::vector<uint>	byteStreamToImageIds(uint numberOf8RankBytes, ByteStream& bytes)
	{
		std::vector<uint>	ids;

		std::vector<uint8> id8RankHeader(numberOf8RankBytes);
		// load the 8rank 'header'
		for (uint i = 0; i < id8RankHeader.size(); ++i)
			bytes >> id8RankHeader[i];	

		struct PairRank
		{
			uint32	nbr8Rank;	// positiion defined by the 8RankHeader
			uint8	rank;		// flags of the rank
		};

		std::vector<PairRank> ranks;
		// load each rank (if any)
		for (uint i = 0; i < id8RankHeader.size(); ++i)
		{
			// for each byte in the 8rank header
			for (uint j = 0; j < 8; ++j)
			{
				if (id8RankHeader[i] & (0x1 << j))
				{
					// if a bit is enable the next byte in the byte stream
					// contains a rank corresponding to the current pos
					// (its a bit complex here, you should read specs)
					PairRank newPair = {i * 8 + j, 0};
					bytes >> newPair.rank;
					ranks.push_back(std::move(newPair));
				}
			}
		}

		// Now we have load each rank (and their position in the 8RankHeader),
		// we can read all of them and retrieve image ids from bits
		for (uint i = 0; i < ranks.size(); ++i)
		{
			// Each rank contains 8 bits used as flags.
			// The position of a bit indicates the image id.
			// If the bit is enabled, this means the corresponding
			// image id is in the list.
			// Easy, isn't ?
			// "But we will retrieve same bit positions ranging from 0 to 8 for
			// each rank, no ?"
			// No because we use information from the 8RankHeader as an offset
			// for the whole rank.
			uint32 offset = ranks[i].nbr8Rank * 8;
			for (uint j = 0; j < 8; ++j)
			{ // Check each bit of the rank
				if (ranks[i].rank & (0x1 << j)) // if bit enabled
				{
					uint32 imageId = j + offset;
					ids.push_back(imageId); // add a new imageId
				}
			}
		}
		return ids;
	}

	bool	PatchFile::loadFromBinaryFile( const std::string& filename )
	{
		SIBR_PROFILESCOPE;
		SIBR_LOG << "Loading PatchFile from binary file '" << filename << "'" << std::endl;
		try
		{
			ByteStream	bytes;

			if (bytes.load(filename) == false)
				throw std::runtime_error("cannot open the file");

			//// Read the Header ////
			const std::string magicStringExpected = "PATCHBIN";
			const uint32	versionNumberExpected = SIBR_PATCHFILE_VERSION;
			std::string magicString = "";
			uint32	versionNumber = 0;
			uint32	numberOfPoints = 0;
			uint32	numberOf8RankBytes = 0;

			// Read the magic number
			uint8 c = 0;
			for (uint8 i = 0; i < magicStringExpected.size(); ++i)
			{
				bytes >> c;
				magicString.push_back(c);
			}
			bytes >> c; // retrieve the final '\0' according to the specs

			if ((magicString != magicStringExpected) || (c != '\0'))
				throw std::runtime_error("Wrong magic string (expecting 'PATCHBIN\\0')");

			bytes >> versionNumber;
			if (versionNumber != versionNumberExpected) {
				SIBR_ERR << "wrong version number when loading file '"
				<< filename << "' (expecting: " << versionNumberExpected << ", has: "
				<< versionNumber << "). Please delete this file before trying again." << std::endl;
				return false;
			}

			bytes >> numberOfPoints >> numberOf8RankBytes;		

			resize(numberOfPoints);

			// Tools for showing progress
			const uint showProgressEveryPoint = 500000;
			uint	showProgressPointCount = 0;

			// Read each point			
			for (uint i = 0; i < pointCount(); ++i)
			{
				PatchPoint p;
				float position[3] = {0.f, 0.f, 0.f};
				float normal[3] = {0.f, 0.f, 0.f};
				float confidence = 0.f;
				bool hasNormal = false;
				bool hasUncertainImages = false;

				uint8 flagbyte;
				bytes >> flagbyte;
				std::bitset<8> flags(flagbyte);
				hasNormal = flags.test(0);
				hasUncertainImages = flags.test(0);

				bytes >> position[0] >> position[1] >> position[2]; // 3d position
				p.position(position[0], position[1], position[2]);
				if (hasNormal)
				{
					bytes >> normal[0] >> normal[1] >> normal[2];	// normal
					p.normal(normal[0], normal[1], normal[2]);
				}
				bytes >> confidence;								// confidence score
				p.confidence(confidence);

				// Read the list of image ids where this point is visible
				auto visibleInImages = byteStreamToImageIds( numberOf8RankBytes, bytes );
				for (uint j = 0; j < visibleInImages.size(); ++j)
					p.visibleInImages().emplace_back(visibleInImages[j]);

				// Optionally, read the list of image ids where this point may be visible
				if (hasUncertainImages)
				{
					auto uncertainImages = byteStreamToImageIds( numberOf8RankBytes, bytes );
					for (uint j = 0; j < uncertainImages.size(); ++j)
						p.uncertainImages().emplace_back(uncertainImages[j]);
				}
				point(i, p);

				if (++showProgressPointCount > showProgressEveryPoint)
				{
					showProgressPointCount = 0;
					SIBR_LOG << "loading .patchbin " << ((100.f*i) / pointCount()) << "%..." << std::endl;
				}
			}
			SIBR_LOG << "loading .patchbin 100%..." << std::endl;
			return true;
		}
		catch (const std::exception& e)
		{
			SIBR_LOG << "ERROR: cannot load file '" << filename
				<< "' ("<< e.what() << ")" << std::endl;
		}
		return false;
	}

	size_t			PatchFile::pointCount( void ) const {
		return _points.size();
	}

} // namespace sibr
