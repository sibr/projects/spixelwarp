/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_ASSETS_PATCHFILE_HPP__
# define __SIBR_ASSETS_PATCHFILE_HPP__

# include <projects/spixelwarp/renderer/Config.hpp>
# include <vector>
# include <string>
# include "core/system/Vector.hpp"
# include <projects/spixelwarp/renderer/PatchPoint.hpp>

# define SIBR_PATCHFILE_VERSION 102

namespace sibr
{

	///
	/// Stores data for patch point. Typically, data are loaded from a
	/// .patch or .patchbin (binary version).
	/// \ingroup sibr_assets
	///
	class SIBR_EXP_SPIXELWARP_EXPORT PatchFile
	{
		SIBR_CLASS_PTR( PatchFile );
	public:
		/// 'Deprecated' (prefer 'path' values from a config file)
		/// Utility function for searching a .patch file. Return empty string
		/// if nothing found.
		static std::string	search( const std::string& datasetPath );

	public:
		typedef std::vector<PatchPoint> PointList;

		/// Load from a .patch but automatically load its binary version
		/// if it exists (and is still up to date). 
		/// In the other case, generate it.
		bool	load( const std::string& filename );
		/// Load from a .patchbin (the binary version).
		bool	loadFromBinaryFile( const std::string& filename );
		/// Load from a .patch (strictly its ASCII version).
		bool	loadFromASCIIFile( const std::string& filename );

		/// Save to a .patchbin. Note that the order of image indexes
		/// in visibility lists will be sorted for encoding purposes.
		void	saveToBinaryFile( const std::string& filename );
		/// Save to a .patch
		void	saveToASCIIFile( const std::string& filename ) const;


		// Add PatchPoint p as patch number n
		void						point( uint n, const PatchPoint& p );
		const PatchPoint&	point( uint id ) const;

		/// Resize the current point list (so pre-allocate memory)
		void						resize( uint count );

		/// Get total point count
		size_t				pointCount( void ) const;


	private:
		PointList		_points;		///< List of points
	};



	//// INLINE FUNTCIONS ////

} // namespace sibr

#endif // __SIBR_ASSETS_PATCHFILE_HPP__
