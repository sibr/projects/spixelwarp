/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "PatchPoint.hpp"

namespace sibr
{

	/*PatchPoint PatchPoint::operator=(const PatchPoint& m)
	{
		_position=m._position;
		_normal=m._normal;
		return *this;
	}

	PatchPoint::PatchPoint( const PatchPoint& m ) {
		_position=m._position;
		_normal=m._normal;
	}*/

	PatchPoint::PatchPoint( void ) :
		_confidence(0.f)
	{
		_position[0] = 0.f;
		_position[1] = 0.f;
		_position[2] = 0.f;

		_normal[0] = 0.f;
		_normal[1] = 0.f;
		_normal[2] = 0.f;
	}

	static void dumpIdList( const std::vector<uint>& list )
	{
		std::cout << "PatchImageList{ ";
		for ( uint i = 0; i < list.size(); ++i )
			std::cout << list[i] << ((i+1 == list.size())? "" : ", ");
		std::cout << " }";
	}

	void	PatchPoint::dump( void ) const
	{
		std::cout << "PatchPoint{ "
			<< "position{ " << _position[0] << ", " << _position[1] << ", " << _position[2]
		<< " }, normal{ " << _normal[0] << ", " << _normal[1] << ", " << _normal[2]
		<< " }, confidence = " << _confidence << ", ";


		std::cout << "visibleInImages = ";
		dumpIdList(_visibleInImages);
		std::cout << ", uncertainImages = ";
		dumpIdList(_uncertainImages);
		std::cout << " }" << std::endl;
	}
	
	PatchPoint::~PatchPoint() {
		//std::cout << "Deleting: " << _position << std::endl << std::flush;
		//_position[0] = _position[0] + 1;
	}

	const sibr::Vector3f&	PatchPoint::position( void ) const {
		return _position;
	}
	const sibr::Vector3f&	PatchPoint::normal( void ) const {
		return _normal;
	}
	void PatchPoint::position( float x, float y, float z ) {
		_position[0] = x; _position[1] = y; _position[2] = z;
	}
	void PatchPoint::position( const sibr::Vector3f& p ) {
		_position = p;
	}
	void PatchPoint::normal( float x, float y, float z ) {
		_normal[0] = x; _normal[1] = y; _normal[2] = z;
	}
	void PatchPoint::normal( const sibr::Vector3f& n ) {
		_normal = n;
	}

} // namespace sibr
