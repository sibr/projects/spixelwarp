/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_ASSETS_PATCHPOINT_HPP__
# define __SIBR_ASSETS_PATCHPOINT_HPP__

# include <projects/spixelwarp/renderer/Config.hpp>
# include <vector>
# include <string>
# include "core/system/Vector.hpp"

# define SIBR_PATCHFILE_VERSION 102

namespace sibr
{
	/// index of an image
//	typedef uint				uint;
	typedef std::vector<uint>	PatchImageList;

	///
	/// Information about one patch point.
	/// \ingroup sibr_assets
	class SIBR_EXP_SPIXELWARP_EXPORT PatchPoint
	{
		SIBR_CLASS_PTR( PatchPoint );
	public:
		PatchPoint( void );
		/*PatchPoint( const PatchPoint& );
		PatchPoint operator=(const PatchPoint& );*/

		void						position( float x, float y, float z );
		void						position( const sibr::Vector3f& pos );
		const sibr::Vector3f&	position( void ) const;

		void						normal( float x, float y, float z );
		void						normal( const sibr::Vector3f& n );
		const sibr::Vector3f&	normal( void ) const;

		/// photometric consistency score
		inline void						confidence( float value );
		inline float					confidence( void ) const;

		/// List of images where this point is visible (and textures agree).
		inline PatchImageList&			visibleInImages( void );
		inline const PatchImageList&	visibleInImages( void ) const;

		/// List of images where this point is visible (but textures may not agree).
		inline PatchImageList&			uncertainImages( void );
		inline const PatchImageList&	uncertainImages( void ) const;

		/// Print content on the standard output (Debug).
		void	dump( void ) const;

		~PatchPoint();

	private:
		sibr::Vector3f		_position;			///< 3d pos
		sibr::Vector3f		_normal;
		float				_confidence;		///< photometric consistency score
		PatchImageList		_visibleInImages;	///< list of images where this point is visible
		PatchImageList		_uncertainImages;	///< list of images where this point is visible but texture may not agree.
	};



	//// INLINE FUNTCIONS ////

	void PatchPoint::confidence( float value ) {
		_confidence = value;

		// The docs (http://www.di.ens.fr/pmvs/documentation.html) says:
		// "[...] score ranging from -1.0 (worse) to 1.0 (good)."
		// However, this is not the case in file I tested...
		//assert(_confidence >= -1.f && _confidence <= 1.f);
	}


	float		PatchPoint::confidence( void ) const {
		return _confidence;
	}

	PatchImageList&			PatchPoint::visibleInImages( void ) {
		return _visibleInImages;
	}
	const PatchImageList&	PatchPoint::visibleInImages( void ) const {
		return _visibleInImages;
	}

	PatchImageList&			PatchPoint::uncertainImages( void ) {
		return _uncertainImages;
	}
	const PatchImageList&	PatchPoint::uncertainImages( void ) const {
		return _uncertainImages;
	}
} // namespace sibr

#endif // __SIBR_ASSETS_PATCHPOINT_HPP__
