/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */




#include <core/system/LoadingProgress.hpp>
#include "PlanarRenderer.hpp"

namespace sibr { 
	PlanarRenderer::PlanarRenderer( const BasicIBRScene::Ptr& scene ) :
	_scene(scene)
	{
		_shader.init("PlanarShader",
			loadFile(sibr::getShadersDirectory("spixelwarp") + "/sel_planar.vert"),
			loadFile(sibr::getShadersDirectory("spixelwarp") + "/sel_planar.frag"));

		_paramProj.init(_shader,"proj");
		_paramICamProj.init(_shader,"iCamProj");
	    _paramDoMasking.init(_shader, "doMasking");

	}

	void	PlanarRenderer::load(
		const std::vector<SpixelImage::Ptr>& spxImages,
		std::vector<Texture2DRGBA32F::Ptr>& spxMap,
		const std::vector<ImagePlanes>& planes,
		const std::vector<std::vector<bool>>& activePLAN,
		const std::vector<std::vector<bool>>& activeFPLAN )
	{
		_spixelMap = spxMap;
		createMeshes(spxImages, planes, activePLAN, activeFPLAN);
	}

	/* In Short:
	- Create rectangle from 2d position (based on spixel)
	- Project these 2d position in 3d
	- and put these 3d pos in a mesh ready to be rendered
	**/
	void	PlanarRenderer::createMeshes(
			const std::vector<SpixelImage::Ptr>& spxImages,
			const std::vector<ImagePlanes>& planes,
			const std::vector<std::vector<bool>>& activePLAN,
			const std::vector<std::vector<bool>>& activeFPLAN )
	{
		const std::vector<InputCamera::Ptr>&	cams = _scene->cameras()->inputCameras();
		SIBR_ASSERT(cams.size() == spxImages.size());
		SIBR_ASSERT(planes.size() == spxImages.size());
		SIBR_ASSERT(activePLAN.size() == spxImages.size());
		SIBR_ASSERT(activeFPLAN.size() == spxImages.size());

		_meshes.clear();
		_meshes.resize(cams.size());

		std::array<sibr::Vector2d, 4>	quadVertices;
		for ( int i = 0; i < spxImages.size(); ++i )
		{
			if (cams[i]->isActive() == false)
				continue;

			SIBR_ASSERT(spxImages[i] != nullptr);
			SIBR_ASSERT(planes[i].getPlanes().size() == spxImages[i]->numSpixels());
			SIBR_ASSERT(activePLAN[i].size() == spxImages[i]->numSpixels());
			SIBR_ASSERT(activeFPLAN[i].size() == spxImages[i]->numSpixels());

			Mesh::Vertices		vertices;
			Mesh::UVs			UVs;
			Mesh::Colors		colors;
			std::vector<uint>	indices;

			for ( int j = 0; j < (int)spxImages[i]->numSpixels(); ++j )
			{
				const Spixel&	spx = spxImages[i]->getSpixel(j);

				//TODO : verify comptatibility :
				const Camera&	cam = *_scene->cameras()->inputCameras().at(i);
				//const Camera&	cam = *_scene->cameras(i);

				//if (planes[i][j]->isActive == false) // TODO: check (by default, do we want to render this plan?)
				//	continue;

				Rect<double>	bbox = spx.getBoundingBoxInWinCoord(spxImages[i]->w(), spxImages[i]->h());
				std::vector<float>	depthPerP3D(quadVertices.size());

				uint indexOffset = (uint)vertices.size();
				quadVertices[0] = bbox.cornerLeftTop();
				quadVertices[1] = bbox.cornerLeftBottom();
				quadVertices[2] = bbox.cornerRightBottom();
				quadVertices[3] = bbox.cornerRightTop();



				for ( int k = 0; k < quadVertices.size(); ++k )
				{
					const Vector2d& p = quadVertices[k];


					Vector3f P3D;
					if (activePLAN[i][j])
					{
						Vector3f farP3D = cam.unproject( Vector3f((float)p.x(), (float)p.y(), 1.f) );
						Vector3f ray = farP3D - cam.position();
						Vector3f n	= planes[i].getPlanes()[j].normal;
						float d	= planes[i].getPlanes()[j].d;
						float alpha = -(dot(cam.position(), n) + d) / dot(ray , n);

						P3D = alpha*ray + cam.position();
					}
					else if (activeFPLAN[i][j])
					{
						P3D = cam.unproject( Vector3f((float)p.x(), (float)p.y(), spx.getMedianDepth()) );
					}

					vertices.push_back( P3D );

					UVs.emplace_back(
						0.5f + 0.5f*quadVertices[k][0],
						0.5f + 0.5f*quadVertices[k][1] );
					float depth = (cam.project(P3D)[2] + 1.f) / 2.f;
					colors.emplace_back(
						depth,							// R
						(float)spx.getGID().toUint(),	// G
						0.f);							// B

				}


				indices.push_back( indexOffset + 0 ); // indices for each
				indices.push_back( indexOffset + 3 ); // vertex of each triangle
				indices.push_back( indexOffset + 2 ); // of the warp mesh

				indices.push_back( indexOffset + 2 ); // indices for each
				indices.push_back( indexOffset + 1 ); // vertex of each triangle
				indices.push_back( indexOffset + 0 ); // of the warp mesh
			}

			_meshes[i].vertices(vertices);
			_meshes[i].triangles(indices);
			_meshes[i].texCoords(UVs);
			_meshes[i].colors(colors);
		}
	}

	void	PlanarRenderer::process(
					const sibr::Camera& eye,
		/*input*/	const std::vector<uint>& imageIDs,
		/*in/output*/	std::vector<RenderTargetRGBA32F::Ptr>& warpRT )
	{
		/** Hack for render beyond clipping planes */
		Camera cam = eye;
		cam.znear(0.01f);


		for (uint i=0; i<imageIDs.size(); i++)
		{
			uint k = imageIDs[i];

			warpRT[i]->bind();
			glViewport(0,0, warpRT[i]->w(), warpRT[i]->h());

			_shader.begin();
			{
				_paramProj.set(cam.viewproj());
				_paramICamProj.set(_scene->cameras()->inputCameras().at(k)->viewproj());
				_paramDoMasking.set(useMasks());

				glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, 
					_scene->renderTargets()->inputImagesRT().at(k)->texture());
				glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, 
					_spixelMap[k]->handle());
				if (useMasks())
				{
					glActiveTexture(GL_TEXTURE2);
					glBindTexture(GL_TEXTURE_2D, getMasks()[k]->texture());
				}
				_meshes[k].render();
			}
			_shader.end();
			warpRT[i]->unbind();
		}
	}

} /*namespace sibr*/ 
