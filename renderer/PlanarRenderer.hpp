/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SELECTION_PLANARRENDERER_HPP__
# define __SIBR_EXP_SELECTION_PLANARRENDERER_HPP__

//# include "Config.hpp"
# include <core/graphics/Config.hpp>
# include <core/graphics/Shader.hpp>
# include <core/graphics/Mesh.hpp>
# include <core/scene/BasicIBRScene.hpp>
# include <core/renderer/RenderMaskHolder.hpp>
# include <projects/spixelwarp/renderer/SpixelImage.hpp>
# include <projects/spixelwarp/renderer/ImagePlanes.hpp>

namespace sibr { 
	class SIBR_EXP_SPIXELWARP_EXPORT  PlanarRenderer : public RenderMaskHolder
	{
		SIBR_CLASS_PTR(PlanarRenderer);

	public:
		PlanarRenderer( const BasicIBRScene::Ptr& scene );

		void	load(
			const std::vector<SpixelImage::Ptr>& spxImages,
			std::vector<Texture2DRGBA32F::Ptr>& spxMap,
			const std::vector<ImagePlanes>& planes,
			const std::vector<std::vector<bool>>& activePLAN,
			const std::vector<std::vector<bool>>& activeFPLAN );

		void	process(
						const sibr::Camera& eye,
			/*input*/	const std::vector<uint>& imageIDs,
			/*in/output*/	std::vector<RenderTargetRGBA32F::Ptr>& warpRT );

	private:
		void	createMeshes(
			const std::vector<SpixelImage::Ptr>& spxImages,
			const std::vector<ImagePlanes>& planes,
			const std::vector<std::vector<bool>>& activePLAN,
			const std::vector<std::vector<bool>>& activeFPLAN );

		BasicIBRScene::Ptr	_scene;

		GLShader			_shader;
		GLParameter			_paramProj;
		GLParameter			_paramICamProj;
		GLParameter			_paramDoMasking;

		std::vector<Texture2DRGBA32F::Ptr>		_spixelMap;
		std::vector<Mesh>						_meshes;
	};

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SELECTION_PLANARRENDERER_HPP__
