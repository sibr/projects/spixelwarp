/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <iostream>
#include <fstream>
#include <thread>
#include <algorithm>

#include <core/system/LoadingProgress.hpp>
#include <core/graphics/Input.hpp> // for debugging
#include <core/graphics/RenderUtility.hpp>

#include <projects/spixelwarp/renderer/MVSFile.hpp>
#include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>

#include <projects/spixelwarp/renderer/SpixelImage.hpp>
#include <projects/spixelwarp/renderer/ImageWarper.hpp>
#include <projects/spixelwarp/renderer/SWARPRenderer.hpp>

namespace sibr { 
	
	SWARPRenderer::~SWARPRenderer( ) {} ;
	SWARPRenderer::SWARPRenderer(
		const std::vector<SpixelImage::Ptr>& spxImages,
		std::vector<Texture2DRGBA32F::Ptr>& spxMap,
		const std::vector<InputCameraPreprocess>& cams, 
		const std::vector<std::vector<bool>>& activatedSpixels,
		bool useCustomDepth) :
		_spixelMap(spxMap)
	{
		_imageFitParams = Vector4f(1.f, 1.f, 0.f, 0.f);
		_useCustomDepth = useCustomDepth;

		GLShader::Define::List defines;
		defines.emplace_back("USE_CUSTOM_DEPTH", (useCustomDepth == false? 0 : 1));

		_warpShader.init("Warp",
			sibr::loadFile(sibr::getShadersDirectory("spixelwarp") + "/spx_ibr_ratio.vert"),
			sibr::loadFile(sibr::getShadersDirectory("spixelwarp") + "/spx_warp.frag", defines));
		_warpShaderScreenSizeParam.init(_warpShader, "imagefit");
	    _warpShaderDoMasking.init(_warpShader, "doMasking");

		std::vector<std::vector<bool>> activeSpx = activatedSpixels;
		if (activeSpx.empty()) // if not prodived, enable every spixels by default
		{
			for ( const SpixelImage::Ptr& spxImg : spxImages )
			{
				uint numSpixels = (spxImg != nullptr) ? spxImg->numSpixels() : 0;
				activeSpx.push_back(std::vector<bool>(numSpixels, true));
			}
		}
		
		uploadImageWarpers(spxImages, activeSpx, cams);
		
	}

	void SWARPRenderer::process(
			/*input*/   const sibr::Camera& eye,
			/*input*/	const std::vector<uint>& imageIDs,
			/*input*/	const std::vector<RenderTargetRGBA32F::Ptr>& imageRTs,
			/*output*/	std::vector<RenderTargetRGBA32F::Ptr>& warpRT )
	{
#pragma omp parallel for
		for (int i=0; i<imageIDs.size(); i++)
			_imageWarper[imageIDs[i]]->warp( eye ) ;

		// render warped images into render targets
		for (uint i=0; i<imageIDs.size(); i++)
		{
			uint k = imageIDs[i];

			_warpShader.begin();
			_warpShaderScreenSizeParam.set(_imageFitParams);
			_warpShaderDoMasking.set(useMasks());

			warpRT[i]->bind();
			glViewport( 0, 0, warpRT[i]->w(), warpRT[i]->h());

			glActiveTexture(GL_TEXTURE0); glBindTexture(GL_TEXTURE_2D, imageRTs.at(k)->texture());
			glActiveTexture(GL_TEXTURE1); glBindTexture(GL_TEXTURE_2D, _spixelMap[k]->handle());

			if (_useCustomDepth)
			{
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, _customDepthRTs[k]->handle());
			}

			if (useMasks())
			{
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, getMasks()[k]->texture());

				// Should be something like this but not seen in the shader
				//glActiveTexture(GL_TEXTURE?);
				//glBindTexture(GL_TEXTURE_2D, getMasks()[k]->texture());
			}

			_imageWarper[k]->draw();

			warpRT[i]->unbind();
			_warpShader.end();
		}
		
	}

	void SWARPRenderer::uploadImageWarpers(  const std::vector<SpixelImage::Ptr>& spixelImage,
			const std::vector<std::vector<bool>>& activatedSpixels,
			const std::vector<InputCameraPreprocess>& cams )
	{
		SIBR_LOG << "[SWARPRenderer] Upload image warpers " << std::endl;

//		const std::vector<InputCamera::Ptr>&		cams = _scene->cameras()->inputCameras();
//		const std::vector<InputCameraPreprocess>		cams; // 
//std::cerr << "FIX MVS POINTS " << std::endl;
		_imageWarper.resize(cams.size());

		SIBR_ASSERT(activatedSpixels.size() == cams.size());
		for ( int i = 0; i < (int)cams.size(); ++i )
		{
			if (cams.at(i).isActive())
			{
				SIBR_ASSERT(spixelImage[i] != nullptr);
				SIBR_ASSERT(activatedSpixels[i].size() == spixelImage[i]->numSpixels());

				_imageWarper[i].reset(new ImageWarper(&(cams.at(i)), spixelImage[i]));
				_imageWarper[i]->init(activatedSpixels[i]);
			}
		}

		std::cout << "IN UploadImageWarpers";
		//SIBR_DEBUG(_imageWarper[0]->getSpixelMesh()[0].number_of_faces());
	}

	void	SWARPRenderer::setCustomDepth(
		const std::vector<Texture2DLum32F::Ptr>& customDepthRTs )
	{
		_customDepthRTs = customDepthRTs;
	}

	const std::vector<std::shared_ptr<ImageWarper> > & SWARPRenderer::getImgWarpers( void ) const
	{
		return _imageWarper;
	}

} /*namespace sibr*/ 
