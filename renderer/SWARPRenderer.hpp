/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SPIXELWARP_SWARPRENDERER_HPP__
# define __SIBR_EXP_SPIXELWARP_SWARPRENDERER_HPP__

# include <memory>

# include "Config.hpp"
# include <core/graphics/Texture.hpp>
# include <core/graphics/Shader.hpp>
# include <core/scene/BasicIBRScene.hpp>

# include <core/renderer/RenderMaskHolder.hpp>
# include <projects/spixelwarp/renderer/Config.hpp>
# include <projects/spixelwarp/renderer/SpixelImage.hpp>
# include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>

namespace sibr
{
	class InputCamera;
	class BasicIBRScene;
} // namespace sibr


namespace sibr { 
	class SpixelImage;
	class ImageWarper;
	class Spixel;



	class SIBR_EXP_SPIXELWARP_EXPORT SWARPRenderer : public RenderMaskHolder
	{
		SIBR_CLASS_PTR( SWARPRenderer );

	public:
		~SWARPRenderer();

		SWARPRenderer( 
			const std::vector<SpixelImage::Ptr>& spxImages,
			std::vector<Texture2DRGBA32F::Ptr>& spxMap,
			const std::vector<InputCameraPreprocess>& cams, 
			const std::vector<std::vector<bool>>& activatedSpixels = 
				std::vector<std::vector<bool>>(), bool useCustomDepth=false );

		/** Note that RenderTargets are not cleared */
		void	process(
			/*input*/   const sibr::Camera& eye,
			/*input*/	const std::vector<uint>& imageIDs,
			/*input*/	const std::vector<RenderTargetRGBA32F::Ptr>& imageRTs,
			/*output*/	std::vector<RenderTargetRGBA32F::Ptr>& warpRT );

		void	setCustomDepth(
			const std::vector<Texture2DLum32F::Ptr>& customDepthRTs );

		const std::vector<std::shared_ptr<ImageWarper> > & getImgWarpers( void ) const;

	private:
		/** Load systems for warping images */
		void	uploadImageWarpers( const std::vector<SpixelImage::Ptr>& spixelImage,
			const std::vector<std::vector<bool> >& activatedSpixels, 
			const std::vector<InputCameraPreprocess>& cams
			);

		/** Warp input images and render them in \p _warp_RT using superpixel warp */
		void	spixel_warp(std::vector<uint> warped_img_id, bool use_mask=false);

	private:

		Vector4f							_imageFitParams;
		BasicIBRScene::Ptr	_scene;

		bool								_useCustomDepth;
		std::vector<Texture2DLum32F::Ptr>	_customDepthRTs;

		/** Shader for rendering shape preserving warp results for input images */
		GLShader		_warpShader;
		GLParameter		_warpShaderScreenSizeParam;
		GLParameter		_warpShaderDoMasking;

		/** Image warper to render left eye view each input image */
		std::vector<std::shared_ptr<ImageWarper> > _imageWarper;

		std::vector<Texture2DRGBA32F::Ptr>		_spixelMap;

	};

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SPIXELWARP_SWARPRENDERER_HPP__
