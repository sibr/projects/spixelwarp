/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <core/system/LoadingProgress.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <projects/spixelwarp/renderer/SelectionLabel.hpp>
#include <fstream>

namespace sibr { 
	void				SelectionLabel::setImage( ImageID imageID , const SpixelData& labels )
	{
		_images[imageID] = labels;
	}

	void				SelectionLabel::removeImage( ImageID imageID )
	{
		auto it = _images.find(imageID);
		if (it != _images.end())
			_images.erase(it);
	}

	const SelectionLabel::SpixelData&	SelectionLabel::getLabels( ImageID imageID ) const
	{
		static SpixelData nullSpixelData;

		auto it = _images.find(imageID);
		return it != _images.end()? it->second : nullSpixelData;
	}

	bool	SelectionLabel::loadAll(	const BasicIBRScene& scene, 
										const std::string& datasetPath )
	{
		const std::vector<InputCamera::Ptr>&		cams = scene.cameras()->inputCameras();

		SIBR_LOG << "[SelectionLabels] Loading labels " << std::endl;
		for ( uint i = 0; i < cams.size(); ++i )
		{
			if (cams[i]->isActive() == false)
				continue;

			std::string		filename = datasetPath + sibr::sprint("/densities/final_labels_%08d.txt",i);
			if (load(i, filename) == false)
				return false;
		}
		return true;
	}

	bool	SelectionLabel::load( ImageID imageID, const std::string& filename )
	{
		std::ifstream	file( filename );

		std::map<uint, Name>	labelConnection;
		labelConnection[1] = SWARP;
		labelConnection[2] = PLAN;
		labelConnection[3] = FPLAN;

		Labels	initLabelValues;
		std::fill(initLabelValues.begin(), initLabelValues.end(), false);

		if (file)
		{
			int numSpixels = 0;
			int spid_label, final_label;

			file >> numSpixels;
			SpixelData	spxData(numSpixels, initLabelValues);

			for (int j = 0; j < spxData.size(); ++j)
			{
				file >> spid_label >> final_label;

				if (!spid_label == j)
					SIBR_ERR << "[Selection] Label problem (corrupted label file)" << std::endl;

				/** Activate only the algorithm given by the label and
				*	deactivate all algorithms for the current spixel.
				*/
				if (labelConnection.find(final_label) != labelConnection.end())
					spxData[j][labelConnection[final_label]] = true;
				else
					SIBR_WRG << "[Selection] Unknown label ID in file " << filename << ", spix ID and label ID are " << spid_label << " " << final_label << std::endl;
			}
			static bool once = false;
			if (!once) {
				once = true;
				std::ofstream outf("spxdatalabels.txt");
				for (int j = 0; j < spxData.size(); ++j)
					outf << "SP " << j << " val= (" << 
						spxData[j][PLAN] << "/" <<
						spxData[j][FPLAN] << "/" <<
						spxData[j][SWARP] << ")" <<
					std::endl;
				outf.close();
			}


			setImage(imageID, spxData);
			return true;
		}
		else
			SIBR_ERR << "[Selection] Can't open file: " << filename << std::endl;

		return false;
	}

	std::vector<bool>				SelectionLabel::extractLabel( Name label, ImageID imageID )
	{
		const SpixelData& labels = getLabels(imageID);

		std::vector<bool>	enabledLabel(labels.size());
		for (uint j = 0; j < enabledLabel.size(); ++j)
			enabledLabel[j] = labels[j][label];
		return enabledLabel;
	}

	std::vector<std::vector<bool>>	SelectionLabel::extractLabelForImages( Name label, const std::vector<ImageID>& ofImages )
	{
		std::vector<std::vector<bool>>	images(ofImages.size());
		for (uint i = 0; i < images.size(); ++i)
			images[i] = extractLabel(label, ofImages[i]);
		return images;
	}

	std::vector<std::vector<bool>>	SelectionLabel::extractLabelForImages( Name label, uint imageCount )
	{
		std::vector<ImageID>	indices(imageCount);
		for (uint i = 0; i < indices.size(); ++i)
			indices[i] = i;
		return extractLabelForImages(label, indices);
	}

	SelectionLabel::Name SelectionLabel::getLabel( int spixelId, const SpixelData& data)
	{
		auto & possibleLabel = data.at(spixelId);
		int label = 0;
		for( bool l : possibleLabel){
			if( l ) {
				return (SelectionLabel::Name)label;
			} 
			++label;
		}
		return (SelectionLabel::Name)label;
	}

} /*namespace sibr*/ 
