/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SELECTION_LABEL_HPP__
# define __SIBR_EXP_SELECTION_LABEL_HPP__

# include <array>
# include <map>
# include <vector>
# include <string>
# include <projects/spixelwarp/renderer/Config.hpp>
#include <core/scene/BasicIBRScene.hpp>

namespace sibr
{
	class IBRScene;

} // namespace sibr

namespace sibr { 
	class SIBR_EXP_SPIXELWARP_EXPORT SelectionLabel
	{
	public:
		enum	Name
		{
			NONE,

			PLAN,
			FPLAN,
			SWARP,

			Count
		};

		/// Used to allow multiple labels enable.
		typedef std::array<bool, Count>			Labels;

		typedef std::vector<Labels>				SpixelData;
		typedef	uint							ImageID;
		typedef std::map<ImageID, SpixelData>	Images;

	public:
		void				setImage( ImageID imageID , const SpixelData& labels );
		void				removeImage( ImageID imageID );
		const SpixelData&	getLabels( ImageID imageID ) const;

		bool	loadAll( const BasicIBRScene& scene, const std::string& datasetPath );
		/// Load only one image label file (and add it)
		bool	load( ImageID imageID, const std::string& filename );

		std::vector<bool>				extractLabel( Name label, ImageID imageID );
		/// Extract currently stored \param label . The returned indices matches
		/// the indices of the given \param ofImages (so you can retrieve
		/// ImageIDs).
		std::vector<std::vector<bool>>	extractLabelForImages( Name label, const std::vector<ImageID>& ofImages );
		/// Variant used for small datasets where you can load every images.
		std::vector<std::vector<bool>>	extractLabelForImages( Name label, uint imageCount );

		static Name getLabel( int spixelId, const SpixelData& data);

	private:
		Images			_images;

	};

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SELECTION_LABEL_HPP__
