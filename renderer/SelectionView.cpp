/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <projects/spixelwarp/renderer/SelectionLabel.hpp>
#include <projects/spixelwarp/renderer/ImagePlanes.hpp>
#include <projects/spixelwarp/renderer/SelectionView.hpp>
#include <projects/spixelwarp/renderer/Utils.hpp>
#include <core/view/IBRBasicUtils.hpp>

#include <imgui/imgui.h>

namespace sibr {
        SelectionView::SelectionView(	const BasicIBRScene::Ptr& scene, uint numWarp,
										const std::string& datasetPath, 
										uint render_w, uint render_h, bool debugMode,
                                        sibr::InteractiveCameraHandler::Ptr cameraDebug):
        _scene(scene),sibr::ViewBase(render_w, render_h),
        _selectionLabel(std::make_shared<SelectionLabel>()),
        _debugMode(debugMode),
        _mousePositionDebug({0,0}),
        _currentCamDebug(0),
        _cameraDebug(cameraDebug)
        {
                //uint imageCount = (uint)scene->inputImages().size();
				//todo : verify comptatibility
				uint imageCount = (uint)scene->images()->inputImages().size();
				uint w = render_w; /*getResolution().x();*/
				uint h = render_h; /*getResolution().y();*/
			
				//todo : verify compatibility
				_mvsCams.resize(scene->cameras()->inputCameras().size());
				for (int i = 0; i < (int)scene->cameras()->inputCameras().size(); ++i) {
					_mvsCams[i] = *scene->cameras()->inputCameras()[i];
			        _mvsCams[i].loadSuperpixelsAndReconstruction(datasetPath); 	/** For each camera, load superpixels color info and reconstruction info. */
				}

				std::string swarpDatasetPath;
				if (directoryExists(datasetPath + "/spixelwarp/")) {
					swarpDatasetPath = datasetPath + "/spixelwarp/";
				}
				else {
					swarpDatasetPath = datasetPath + "/";
				}

                std::vector<SpixelImage::Ptr> spixelImage = loadSuperpixels(*scene,
															swarpDatasetPath);
                loadMVSPoints(*scene, spixelImage, swarpDatasetPath, _mvsCams);
                std::vector<Texture2DRGBA32F::Ptr> spixelMap = loadSpixelMap(*scene, spixelImage);
				//------------
                //SelectionLabel	labels;
                _selectionLabel->loadAll(*scene, swarpDatasetPath); //Todo : verify compatibility
                auto labelsSWARP = _selectionLabel->extractLabelForImages(SelectionLabel::SWARP, imageCount);
                auto labelsPLAN = _selectionLabel->extractLabelForImages(SelectionLabel::PLAN, imageCount);
                auto labelsFPLAN = _selectionLabel->extractLabelForImages(SelectionLabel::FPLAN, imageCount);

                _blendRT.reset( new RenderTargetRGBA(w, h) );
                _poissonRT.reset( new RenderTargetRGBA(w, h) );

                setNumWarp(numWarp,scene);
				


                _swarp.reset(new SWARPRenderer(/*scene, */spixelImage, spixelMap,
												_mvsCams, labelsSWARP)); //Todo: verify compatibility
                _planar.reset(new PlanarRenderer(scene));
                _poisson.reset(new PoissonRenderer(w, h));
                _copy.reset(new CopyRenderer());

                _planar->load(spixelImage, spixelMap, loadPlanes(*scene,swarpDatasetPath), labelsPLAN, labelsFPLAN);

                if (debugMode && !cameraDebug) {
                    SIBR_ERR << "You turned on the Debug mode but your Interactive Camera does not exist..." 
                        << std::endl;
                }
        }

        void				SelectionView::setNumWarp(  uint numWarp, 
														BasicIBRScene::Ptr scene )
        {
                _numWarp = numWarp;

                Vector2i size = getResolution();
                _warpRT.resize(_numWarp);
                for (auto& rt : _warpRT)
                        rt.reset(new RenderTargetRGBA32F(size.x(), size.y(), 0, 2));
                // NOTE: the second layer in theses RTs exists only for debugging purpose

                _blend.reset(new BlendRenderer(scene, numWarp));
        }

        void SelectionView::onUpdate(Input& input) {
            if (_debugMode) {
                onDebugGUI();
                if (input.mouseButton().isActivated(Mouse::Button1)) {
                    _mousePositionDebug = input.mousePosition();

                    auto spixelImage = _mvsCams.at(_currentCamDebug).getSpixelImage();
                    uint spixelID = spixelImage->getSpixelIDinversedY
                        (_mousePositionDebug.x()/2, _mousePositionDebug.y()/2);
                    _selectedSpixelDebug = spixelImage->getSpixelPtr(spixelID);
			        auto spixelsData = getSelectionLabel()->getLabels(_currentCamDebug);

                }
            }
        }

        void SelectionView::onRenderIBR( sibr::IRenderTarget& dst, const sibr::Camera& eye )
        {
				IBRBasicUtils bu;
				std::vector<uint> selectedImages = bu.selectCameras(
					_scene->cameras()->inputCameras(), eye, _numWarp);
                _scene->cameras()->debugFlagCameraAsUsed(selectedImages);

                //doFirstTwoPass( selectedImages );
                doFirstTwoPass( selectedImages, eye); //todo : verify compatibility

				_blend->process(
						eye, //todo : verify compatibility
                        /*input*/	selectedImages, _warpRT,
                        /*input*/	_scene->renderTargets()->inputImagesRT(),
                        /*output*/	*_blendRT);
                _poisson->process(
                        /*input*/	_blendRT,
                        /*output*/	_poissonRT);

                uint displayedRT = 0;
                switch( _whichRT )
                {
                case 1: displayedRT = _warpRT[0]->texture(1); break;
                case 2: displayedRT = _warpRT[1]->texture(1); break;
                case 3: displayedRT = _warpRT[2]->texture(1); break;
                case 4: displayedRT = _warpRT[3]->texture(1); break;
                case 5: displayedRT = _blendRT->texture(); break;
                default:displayedRT = _poissonRT->texture();
                }

                _copy->process(
                        /*input*/	displayedRT,
                        /*output*/	dst);
        }

        void SelectionView::onDebugGUI()
        {
			if (ImGui::Begin("Debug Quality")) {

				ImGui::SliderInt("Image number", &_currentCamDebug, 0,_mvsCams.size()-1 );
                _cameraDebug->fromCamera(_mvsCams.at(_currentCamDebug));
			}
        }

        void SelectionView::doFirstTwoPass( const std::vector<uint> & selectedImages, 
											const sibr::Camera& eye)
        {
                for (auto& rt : _warpRT)
                        rt->clear();

                _swarp->process(
									eye, //todo :: verify compatibility
                        /*input*/	selectedImages,
                        /*input*/	_scene->renderTargets()->inputImagesRT(),
                        /*in/output*/	_warpRT);

                _planar->process(
									eye,
                        /*input*/	selectedImages,
                        /*in/output*/ _warpRT);
        }

        const std::vector<RenderTargetRGBA32F::Ptr> & SelectionView::getWarpRTs( void ) const
        {
                return _warpRT;
        }

        const SWARPRenderer::Ptr & SelectionView::getWarpRenderer(void) const
        {
                return _swarp;
        }

        const std::vector<InputCameraPreprocess>& SelectionView::getMvsCams()
        {
            return _mvsCams;
        }

        const std::shared_ptr<SelectionLabel>& SelectionView::getSelectionLabel()
        {
            return _selectionLabel;
        }

} /*namespace sibr*/
