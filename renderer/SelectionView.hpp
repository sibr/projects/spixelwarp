/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_EXP_SELECTION_SELECTIONVIEW_HPP__
# define __SIBR_EXP_SELECTION_SELECTIONVIEW_HPP__

# include "Config.hpp"

# include <core/graphics/Config.hpp>
# include <core/view/ViewBase.hpp>
# include <core/view/InteractiveCameraHandler.hpp>
# include <core/renderer/CopyRenderer.hpp>
# include <core/renderer/PoissonRenderer.hpp>
# include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>
# include <projects/spixelwarp/renderer/SWARPRenderer.hpp>
# include <projects/spixelwarp/renderer/BlendRenderer.hpp>
# include <projects/spixelwarp/renderer/PlanarRenderer.hpp>
#include <projects/spixelwarp/renderer/SelectionLabel.hpp>

namespace sibr {
        class SIBR_EXP_SPIXELWARP_EXPORT SelectionView : public sibr::ViewBase
        {
			SIBR_CLASS_PTR( SelectionView );
			typedef std::shared_ptr<SelectionView>		Ptr;

        public:
                SelectionView(	const BasicIBRScene::Ptr& scene, uint numWarp,
								const std::string& datasetPath, 
								uint render_w, uint render_h, bool debugMode = false,
                                sibr::InteractiveCameraHandler::Ptr cameraDebug = nullptr);

                virtual void		onUpdate(Input& input);

                virtual void		onRenderIBR( sibr::IRenderTarget& dst, const sibr::Camera& eye );

                void                onDebugGUI();

				void				setNumWarp(uint numWarp, BasicIBRScene::Ptr scene);

                void				doFirstTwoPass( const std::vector<uint> & selectedImages,
													const sibr::Camera& eye);

                const std::vector<RenderTargetRGBA32F::Ptr> & 
									getWarpRTs( void ) const;
                const SWARPRenderer::Ptr & 
									getWarpRenderer(void) const;

                const std::vector<InputCameraPreprocess>& getMvsCams();
                const std::shared_ptr<SelectionLabel>& getSelectionLabel();


                //Debug mode attributes
                bool                                    _debugMode;
                sibr::Vector2i                          _mousePositionDebug;
                int                                     _currentCamDebug;
                sibr::InteractiveCameraHandler::Ptr     _cameraDebug;
                sibr::Spixel::Ptr                       _selectedSpixelDebug;

        protected:

				std::shared_ptr<sibr::BasicIBRScene> 	_scene;
                std::vector<RenderTargetRGBA32F::Ptr>	_warpRT;
                RenderTargetRGBA::Ptr					_blendRT;
                RenderTargetRGBA::Ptr					_poissonRT;

                SWARPRenderer::Ptr		_swarp;
                PlanarRenderer::Ptr		_planar;
                BlendRenderer::Ptr		_blend;
                PoissonRenderer::Ptr	_poisson;
                CopyRenderer::Ptr		_copy;

                uint					                _numWarp;
				std::vector<InputCameraPreprocess>		_mvsCams;//todo: verify compatibility
                std::shared_ptr<SelectionLabel>         _selectionLabel;
                


        };

} /*namespace sibr*/

#endif // __SIBR_EXP_SELECTION_SELECTIONVIEW_HPP__
