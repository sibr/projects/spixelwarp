/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <cstdlib>
#include <memory>
#include <algorithm>

#include <projects/spixelwarp/renderer/Spixel.hpp>

# define SPIXEL_NEIGHBORS_MINLINKS 10

namespace sibr { 
	Spixel::~Spixel( ) {} ;

	Spixel::Spixel( void ) :
		_medDepth(0.f),
		_active(true)
	{
	}

	void Spixel::addNeighbors(const std::vector<uint>& gids)
	{
		for (uint i=0; i<gids.size(); i++) {
			if (_neighbors.find(gids[i]) == _neighbors.end())
				_neighbors[gids[i]] = 1;
			else
				_neighbors[gids[i]]+= 1;
		}
	}

	std::vector<uint> Spixel::getNeighbors(void) const
	{
		uint uintGID = _gid.toUint();

		std::vector<uint> n;
		std::map<uint,uint>::const_iterator i(_neighbors.begin());
		for (; i != _neighbors.end(); i++ )
			if (i->first != uintGID && i->second>=SPIXEL_NEIGHBORS_MINLINKS)
				n.push_back(i->first);
		return n;
	}

	void Spixel::warpMesh( std::vector<sibr::Vector2d>& t, uint imgWidth, uint imgHeight ) const
	{
		Rect<double> bbox = getBoundingBoxInWinCoord(imgWidth, imgHeight);
		bbox = Rect<double>(
			bbox.left() -.01, bbox.top() -.01,			// displace the extremeties slightly outwards
			bbox.right() +.01, bbox.bottom() +.01 );	// to avoid depth samples lying on boundary

		sibr::Vector2d a = bbox.cornerLeftTop();
		sibr::Vector2d b = bbox.cornerLeftBottom();
		sibr::Vector2d c = bbox.cornerRightBottom();
		sibr::Vector2d d = bbox.cornerRightTop();

		t.push_back(a);
		t.push_back(b);
		t.push_back(c);
		t.push_back(d);

		// some random warp mesh vertices in the interior of the superpixel
		for (uint i=0; i<5; i++)
			//for (uint i=0; i<10; i++)
		{
			double u = double(rand()) / double(RAND_MAX);
			double v = double(rand()) / double(RAND_MAX);
			sibr::Vector2d p = u*v*a + u*(1.0-v)*b + (1.0-u)*v*c + (1.0-u)*(1.0-v)*c;
			t.push_back(p);
		}
	}

	Rect<int>		Spixel::getBoundingBoxInPixel( void ) const
	{
		if (_pixels.empty())
			return Rect<int>(0,0,0,0);

		Vector2i	min = _pixels[0];
		Vector2i	max = _pixels[0];
		for ( const Vector2i& p : _pixels )
		{
			min.x() = std::min(min.x(), p.x());
			min.y() = std::min(min.y(), p.y());
			max.x() = std::max(max.x(), p.x());
			max.y() = std::max(max.y(), p.y());
		}

		return Rect<int>(min.x(), min.y(), max.x(), max.y());
	}

	Rect<double>		Spixel::getBoundingBoxInWinCoord( int imgWidth, int imgHeight ) const
	{
		Rect<int>	bboxInPixel = getBoundingBoxInPixel();
		return Rect<double>(
			2.0*double(bboxInPixel.left())/double(imgWidth) - 1.0,
			2.0*double(bboxInPixel.top())/double(imgHeight) - 1.0,
			2.0*double(bboxInPixel.right())/double(imgWidth) - 1.0,
			2.0*double(bboxInPixel.bottom())/double(imgHeight) - 1.0);
	}

	void Spixel::freeStorage() {
			_pixels.clear();
			_neighbors.clear();
	}

	void Spixel::addPixel( const sibr::Vector2i& v ) {
		_pixels.push_back(v);
	}

} /*namespace sibr*/ 
