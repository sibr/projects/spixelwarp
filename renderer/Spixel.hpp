/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SPIXELWARP_SPIXEL_HPP__
# define __SIBR_EXP_SPIXELWARP_SPIXEL_HPP__

# include <memory>
# include <vector>
# include <map>

# include <core/graphics/Config.hpp>
# include <core/assets/InputCamera.hpp>
# include <projects/spixelwarp/renderer/Config.hpp>
# include <projects/spixelwarp/renderer/Rect.hpp>
# include <projects/spixelwarp/renderer/MVSPoint.hpp>

namespace sibr { 
	/**
	* Superpixel data structure. Contains the list of pixels contained within
	* the superpixel as well as superpixels of other images
	* which are marked as "neighbors" i.e. have common MVS points
	* and thus belong to the same 3D object.
	*/
	class SIBR_EXP_SPIXELWARP_EXPORT Spixel
	{
	public:
		typedef std::shared_ptr<Spixel>		Ptr;

     	/** free all the allocated superpixels
         */
		virtual void freeStorage();

		/**
		* Superpixels in each input image have ID's from 0 to (say) 1400;
		* GID is a global id that uniquely identifies each superpixel from the
		* set of all superpixels over all images.
		*
		* This GID is now directly made up from imageID and local
		* superpixel ID.
		*/
		class GID
		{
		public:
			GID( uint imageID=0, uint localID=0 ) :
				_imageID((ushort)imageID),
				_localID((ushort)localID)
			{ }

			inline uint imageID( void ) const		{ return _imageID; }
			inline uint localID( void ) const		{ return _localID; }
			inline uint	toUint( void ) const		{ return (_imageID << 16) | (_localID & 0xffff); }

		private:
			ushort _imageID;	//< unique id corresponding to the image
			ushort _localID;	//< local id of this spixel in its image
		};

	public:

		virtual ~Spixel();
		Spixel( void );


		/**
		* Create a set of vertices which can be triangulated to
		* give a 2D mesh that envelopes the superpixel. Selects the
		* x and y extermities and some random points from the interior
		* of the superpixels.
		*/
		void				warpMesh( std::vector<sibr::Vector2d>& mesh, uint imgWidth, uint imgHeight ) const;

		/** Add a superpixel from another image as a neighbor; neighbor superpixels in
		* other input images are those which share MVS points with this superpixel.
		* \param gids GID of neighbor superpixels
		*/
		void				addNeighbors( const std::vector<uint>& gids );
		/** Get the list of neighboring superpixels - superpixels in
		* other input images which share MVS points with this superpixel.
		*/
		std::vector<uint>	getNeighbors( void ) const;
		/** Return a list of neighbors (map.first) and the number of link,
		* i.e, the number (map.second) of 3d points shared with this other
		* spixel.
		*/
		inline const std::map<uint,uint>&	getNeighborsLink( void ) const;

		/** Get the local ID of superpixel  */
		inline uint			id( void ) const;
		inline void			setGID( const GID& gid );
		inline const GID&	getGID( void ) const;

		/** Add a pixel to the superpixel */
		void	addPixel( const sibr::Vector2i& p );
		/** Returns the list of pixels contained in this spixel */
		inline const std::vector<sibr::Vector2i>&	getPixels( void ) const;

		/** Set median depth of the superpixel */
		inline void		setMedianDepth( float d );
		/** Get the median depth of the superpixel, median depth
		* is calculated at median of depth of all MVS points in the superpixel */
		inline float	getMedianDepth( void ) const;

		/** Set if the superpixel is useful for IBR; by default all superpixels are set
		* active, superpixels containing fewer than 2 MVS points are deactivated */
		inline void		setActive( bool a );
		/** Get if the superpixel is useful for IBR */
		inline bool		isActive( void ) const;

		/** Return 2D bouding box based on contained pixels (pixel range)*/
		Rect<int>		getBoundingBoxInPixel( void ) const;
		/** Return 2D bouding box based on contained pixels ([-1.0, 1.0] range)*/
		Rect<double>	getBoundingBoxInWinCoord( int imgWidth, int imgHeight ) const;

	private:

		/** Global id of the superpixel in the whole dataset */
		GID		_gid;

		/** List of all pixels contained in the superpixel */
		std::vector<sibr::Vector2i>  _pixels;

		/** List of neighboring superpixels (image id, number of
		* shared MVS points); neighbor superpixels in other
		* input images are those which share MVS points with
		* this superpixel */
		std::map<uint,uint> _neighbors;

		/** Median depth of the superpixel; computed as median
		* of depth all MVS points conatined within the superpixel. */
		float	_medDepth;
		/// \todo TODO EXP: Should we put this into SpixelWarpView only ?

		/** Superpixel useful for image-based rendering or not; set to
		* false if superpixel contains no MVS points. */
		bool	_active;
	};

	///// INLINES /////

	const std::map<uint,uint>&		Spixel::getNeighborsLink( void ) const	{
		return _neighbors;
	}

	uint		Spixel::id( void ) const {
		return _gid.localID();
	}

	void		Spixel::setGID( const GID& gid ) {
		_gid = gid;
	}

	const Spixel::GID&	Spixel::getGID( void ) const {
		return _gid;
	}


	const std::vector<sibr::Vector2i>&	Spixel::getPixels( void ) const	{
		return _pixels;
	}

	void	Spixel::setMedianDepth( float d ) {
		_medDepth = d;
	}
	float	Spixel::getMedianDepth( void ) const {
		return _medDepth;
	}

	void	Spixel::setActive( bool a ) {
		_active = a;
	}
	bool	Spixel::isActive( void ) const {
		return _active;
	}

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SPIXELWARP_SPIXEL_HPP__
