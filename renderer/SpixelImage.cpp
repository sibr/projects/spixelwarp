/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <algorithm>

#include <core/graphics/Image.hpp>
#include <projects/spixelwarp/renderer/SpixelImage.hpp>

namespace sibr { 
	SpixelImage::SpixelImage( uint imageID ) :
		_id(imageID)
	{
	}

	SpixelImage::~SpixelImage( ) {} ;

	bool	SpixelImage::load( const std::string& fname )
	{
		uint	maxSpixelID = 0;

		std::ifstream f(fname.c_str(), std::ios::binary);

		if( !f.is_open() )
		{
			SIBR_ERR << "Can't open " << fname << std::endl;
			return false;
		}

		// read the size of superpixel image
		f.read((char*) &_w, sizeof(int));
		f.read((char*) &_h, sizeof(int));

		// read the superpixel id's
		_map = sibr::Array2d<int>(_w,_h);
		f.read((char*)_map.data(), sizeof(int)*_w*_h);
		f.close();

		for (uint j=0; j<=_h/2; j++)
			for (uint k=0; k<_w; k++)
				std::swap(_map(k,_h-1-j),_map(k,j));  // flip along y so that origin is lower left corner
		for (uint j=0; j<_h; j++)
			for (uint k=0; k<_w; k++)
				maxSpixelID = std::max(maxSpixelID, uint(_map(k,j)));

		_spixel.resize(maxSpixelID+1);                // spixel id's are 0 to n for n+1 spixels
		for (uint i=0; i<_spixel.size(); i++)           // populate the superpixels
			_spixel[i].reset(new Spixel());

		for (uint j=0; j<_h; j++)                       // assign pixels to respective superpixels
			for (uint i=0; i<_w; i++)
				_spixel[_map(i,j)]->addPixel(sibr::Vector2i(i,j));

		return true;
	}



	sibr::ImageRGBA32F::Ptr		SpixelImage::map(void) const
	{
		std::map<uint, std::set<uint> > neigh;                // superpixel spatial neighbors
		for (uint i=0; i<_map.width()-1; i++) {
			for (uint j=0; j<_map.height()-1; j++) {
				int id =  _map(i,j);
				int id1 = _map(i+1,j);
				int id2 = _map(i,j+1);
				int id3 = _map(i+1,j+1);
				if (id != id1) { neigh[id].insert(id1); }
				if (id != id2) { neigh[id].insert(id2); }
				if (id != id3) { neigh[id].insert(id3); }
			}
		}

		sibr::ImageRGBA32F::Ptr cmap(new sibr::ImageRGBA32F(_w,_h));
		for (uint i=0; i<_spixel.size(); i++) {
			float gid   = float(_spixel[i]->getGID().toUint());
			float depth = _spixel[i]->getMedianDepth();

			std::multimap<float,float> same_depth_neigh;
			std::set<uint> adjacent = neigh[i];
			for (std::set<uint>::iterator n(adjacent.begin()); n!=adjacent.end(); n++) {
				float n_gid = float(_spixel[ *n ]->getGID().toUint());                     // GID of spatially adjacent superpixel
				float n_dep = _spixel[ *n ]->getMedianDepth();                    // median depth of adjacent superpixel
				float depth_diff = fabs(depth-n_dep);                          // depth diff with current superpixel
				if (depth_diff < 0.08f)                                        // discard neighbor if depth diff>0.08
					same_depth_neigh.insert(std::make_pair(depth_diff,n_gid));   // multimap stores smaller depth diff first
			}

			std::multimap<float,float>::iterator n(same_depth_neigh.begin());
			std::multimap<float,float>::iterator e(same_depth_neigh.end());
			float gid1 = (n!=e ? (n++)->second : gid);           // store 3 spatial adjacent GID's with min
			float gid2 = (n!=e ? (n++)->second : gid);           // depth diff (if present), else repeat
			float gid3 = (n!=e ? (n++)->second : gid);           // GID of current spixel.
			for (uint j=0; j<_spixel[i]->getPixels().size(); j++) {
				uint x = _spixel[i]->getPixels().at(j)[0];
				uint y = _spixel[i]->getPixels().at(j)[1];
				cmap(x,y)[0] = gid;
				cmap(x,y)[1] = gid1;
				cmap(x,y)[2] = gid2;
				cmap(x,y)[3] = gid3;
			}
		}

		return cmap;
	}

void
SpixelImage::freeStorage()
{
//	std::cerr <<"Deleting spixel " << std::endl;
    for (uint i=0; i<_spixel.size(); i++)            // delete the superpixels
		_spixel[i].get()->freeStorage();

	_spixel.clear();
}

uint	SpixelImage::numSpixels( void ) const {
		return (uint)_spixel.size();
}

const Spixel&	SpixelImage::getSpixel( uint i ) const {
		SIBR_ASSERT(i < _spixel.size());
		return *(_spixel[i].get());
	}

Spixel&		SpixelImage::getSpixel( uint i ) {
		SIBR_ASSERT(i < _spixel.size());
		return *(_spixel[i].get());
	}

} /*namespace sibr*/ 
