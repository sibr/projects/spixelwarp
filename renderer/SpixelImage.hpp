/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SPIXELWARP_SPIXELIMAGE_HPP__
# define __SIBR_EXP_SPIXELWARP_SPIXELIMAGE_HPP__

# include <vector>
# include <memory>
# include <string>

# include "core/system/Array2d.hpp"
//# include <core/graphics/Config.hpp>
# include <core/graphics/Texture.hpp>

# include <projects/spixelwarp/renderer/Config.hpp>
# include <projects/spixelwarp/renderer/Spixel.hpp>

namespace sibr { 
	/**
	* Superpixel segmentation of an input image.
	* Contains the superpixel id of each pixel of the input image.
	*/
	class SIBR_EXP_SPIXELWARP_EXPORT SpixelImage
	{
	public:
		typedef std::shared_ptr<SpixelImage>	Ptr;

	public:

		/** Constructor
		* @param imageID ID of input image - same as ID of superpixel segmentation
		*/
		SpixelImage( uint imageID );

		~SpixelImage( )  ;

		/** Load spixel from a file and returns TRUE if successful. */
		bool			load( const std::string& filename );

		inline uint		w( void ) const;
		inline uint		h( void ) const;

		/** Return the total number of superpixel within the superpixel segmentation
		* of the input image */
		uint		numSpixels(void) const;

		/** Overwrites the address of a Spixel object (using its Spixel::GID) */
		inline void				setSpixel( const Spixel::Ptr& spobj );

		/** Retrieve a superpixel of the segmentation by its ID  */
		const Spixel&	getSpixel( uint id ) const;
		Spixel&			getSpixel( uint id );
		/** Get the superpixel id of a pixel (x,y) in the superpixel image */
		inline uint				getSpixelID( uint x, uint y ) const;

		/** Get the superpixel id of a pixel (x,y) in the superpixel image assumging Y flipped image*/
		inline uint					getSpixelIDinversedY(uint x, uint y) const;

		/** Retreives the address of a Spixel object */
		inline const Spixel::Ptr&	getSpixelPtr( uint id );

		/** Get the mapping of each pixel to its superpixel ID as RGBA float image.
		* Each pixel of the returned image contains GID of the superpixel
		* and GID's of three of the spatially adjacent superpixels which are
		* at the same depth as that pixel. See inline documentation of this function also.
		*/
		sibr::ImageRGBA32F::Ptr		map(void) const;

		/** get list of superpixels in the superpixel segmentation  */
		inline const std::vector<Spixel::Ptr> & getSpixels() const;

     	/** free all the allocated superpixels
      	*/
	void freeStorage();

	private:

		/** ID of input image */
		uint _id;

		/** Width of superpixel segmentation */
		uint _w;
		/** Height of superpixel segmentation */
		uint _h;

		/** Mapping of each pixel to its superpxiel ID */
		sibr::Array2d<int> _map;

		/** List of superpixels in the superpixel segmentation */
		std::vector<Spixel::Ptr> _spixel;
	};

	///// INLINES /////

	uint	SpixelImage::w( void ) const {
		return _w;
	}
	uint	SpixelImage::h( void ) const {
		return _h;
	}

	uint	SpixelImage::getSpixelID( uint x, uint y ) const {
		return _map(x,y);
	}

	uint	SpixelImage::getSpixelIDinversedY(uint x, uint y) const {
		return _map(x, _h - 1 - y);
	}


	void					SpixelImage::setSpixel( const Spixel::Ptr& spobj ) {
		SIBR_ASSERT(spobj && spobj->id() < _spixel.size());
		_spixel[spobj->id()] = spobj;
	}


	const Spixel::Ptr&		SpixelImage::getSpixelPtr( uint id ) {
		SIBR_ASSERT(id < _spixel.size());
		return _spixel[id];
	}

	const std::vector<Spixel::Ptr> & SpixelImage::getSpixels() const {
		return _spixel;
	}

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SPIXELWARP_SPIXELIMAGE_HPP__
