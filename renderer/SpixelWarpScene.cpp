/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "SpixelWarpScene.hpp"

#include <core/scene/CalibratedCameras.hpp>

namespace sibr {

	SpixelWarpScene::SpixelWarpScene(const SwarpAppArgs & myArgs): BasicIBRScene(myArgs)
	{
		_myArgs = myArgs;
		initFromCustomData();
	}

	SpixelWarpScene::SpixelWarpScene(const BasicIBRScene::Ptr & scene, const SwarpAppArgs & myArgs): BasicIBRScene(*scene)
	{
		_myArgs = myArgs;
		initFromCustomData();
	}

	void SpixelWarpScene::initFromCustomData()
	{
		// Create an instance of custom data class called SwarpParseData which inherits basic IBR ParseData class 
		// and initializes the instance with default behavior
		SwarpParseData::Ptr customData(new SwarpParseData(_myArgs));

		// [Option 2] To create the cameras with swarp clipping planes
		// Update the custom data with the desired data specific to the current application
		if (!customData->parseSwarpClippingPlane(_myArgs))
			SIBR_ERR << "Swarp clipping planes file not present!" << std::endl;


		// populate the IBR scene with custom data and create the scene
		if (!this->cameras()) {
			this->createFromCustomData(customData);
		}
		else {
			_cams = std::shared_ptr<CalibratedCameras>(new CalibratedCameras());
			this->cameras()->setupFromData(customData);
		}
	}

}
