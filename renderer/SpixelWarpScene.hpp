/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#ifndef __SIBR_EXP_SPIXELWARPSCENE_HPP__
# define __SIBR_EXP_SPIXELWARPSCENE_HPP__

#pragma once

#include "core/scene/BasicIBRScene.hpp"
#include "Config.hpp"

#include "projects/spixelwarp/renderer/SwarpParseData.hpp"
#include "projects/spixelwarp/renderer/SpixelWarpView.hpp"

namespace sibr {


	struct SwarpAppArgs :
		virtual BasicIBRAppArgs {
	};

	class SIBR_EXP_SPIXELWARP_EXPORT SpixelWarpScene : public sibr::BasicIBRScene
	{
		SIBR_DISALLOW_COPY(SpixelWarpScene);

	public:
		typedef std::shared_ptr<SpixelWarpScene>		Ptr;

		SpixelWarpScene::SpixelWarpScene(const SwarpAppArgs & myArgs);

		SpixelWarpScene::SpixelWarpScene(const BasicIBRScene::Ptr & scene, const SwarpAppArgs & myArgs);

	protected:

		SwarpAppArgs _myArgs;

		void SpixelWarpScene::initFromCustomData();
	};
}

#endif