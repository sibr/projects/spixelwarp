/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include "Config.hpp"
#include <projects/spixelwarp/renderer/SpixelWarpView.hpp>
#include <projects/spixelwarp/renderer/Utils.hpp>
#include <projects/spixelwarp/renderer/Utils.hpp>
#include <core/view/IBRBasicUtils.hpp>

namespace sibr { 

	SpixelWarpView::SpixelWarpView( const BasicIBRScene::Ptr& scene, uint numWarp, const std::string& datasetPath, uint render_w, uint render_h, bool useMergedDepth )
: _scene(scene), sibr::ViewBase(render_w, render_h)
	{
		uint w = render_w; /*getResolution().x();*/
		uint h = render_h; /*getResolution().y();*/
		_whichRT = 6; // Poisson filling

		// to maintain rendering aspect ratio same as camera
		//h = (((float)w) / scene->data()->imgInfos().at(0).width * scene->data()->imgInfos().at(0).height);
		
		// create list of input cameras used for all MVS stuff
		_mvsCams.resize(scene->cameras()->inputCameras().size());
		for ( int i = 0; i < (int)scene->cameras()->inputCameras().size(); ++i ) {
			_mvsCams[i] = *scene->cameras()->inputCameras()[i];
		}

		std::string swarpDatasetPath;
		if (directoryExists(datasetPath + "/spixelwarp/")) {
			swarpDatasetPath = datasetPath + "/spixelwarp/";
		}
		else {
			swarpDatasetPath = datasetPath + "/";
		}

		std::vector<SpixelImage::Ptr> spixelImage = loadSuperpixels(*scene, swarpDatasetPath);
		loadMVSPoints(*scene, spixelImage, swarpDatasetPath, _mvsCams);
		std::vector<Texture2DRGBA32F::Ptr> spixelMap = loadSpixelMap(*scene, spixelImage);
		std::vector<std::vector<bool>>	activatedSpixels; // will be ignored
		
		_blendRT.reset( new RenderTargetRGBA(w, h) );
		_poissonRT.reset( new RenderTargetRGBA(w, h) );

		setNumWarp(numWarp, scene);
		_swarp.reset(new SWARPRenderer(spixelImage, spixelMap, _mvsCams, activatedSpixels, useMergedDepth ));
		_poisson.reset(new PoissonRenderer(w, h));
		_copy.reset(new CopyRenderer());

		if (useMergedDepth)
			_swarp->setCustomDepth( loadMergedDepth(*scene, swarpDatasetPath) );

	}

	void				SpixelWarpView::setNumWarp( uint numWarp, BasicIBRScene::Ptr scene )
	{
		_numWarp = numWarp;

		Vector2i size = getResolution();
		_warpRT.resize(_numWarp);
		for (auto& rt : _warpRT)
			rt.reset(new RenderTargetRGBA32F(size.x(), size.y(), 0, 2));
		// NOTE: the second layer in theses RTs exists only for debugging purpose

		_blend.reset(new BlendRenderer(scene, numWarp));
	}

	void SpixelWarpView::setMasks( const std::vector<RenderTargetLum::Ptr>& masks )
	{
		//ViewBase::setMasks(masks);
		_swarp->setMasks(masks);
		_blend->setMasks(masks);
	}

	void SpixelWarpView::onRenderIBR( sibr::IRenderTarget& dst, const sibr::Camera& eye )
	{
		IBRBasicUtils bu;
		std::vector<uint> selectedImages = bu.selectCameras(_scene->cameras()->inputCameras(),
															eye, _numWarp);
		_scene->cameras()->debugFlagCameraAsUsed(selectedImages);

		/// \todo TODO: include swarp->process() and blend->process()
		/// in the following loop
		for (auto& rt : _warpRT)
			rt->clear();

		_swarp->process(
			/*input*/   eye,
			/*input*/	selectedImages,
			/*input*/	_scene->renderTargets()->inputImagesRT(),
			/*in/output*/	_warpRT);
/*
		show(*_warpRT[0], 0, "Warp 0");
		show(*_warpRT[1], 0, "Warp 1");
		show(*_warpRT[2], 0, "Warp 2");
*/

		_blend->process(
			/*input*/   eye,
			/*input*/	selectedImages, _warpRT,
			/*input*/	_scene->renderTargets()->inputImagesRT(),
			/*output*/	*_blendRT);

//		show(*_blendRT, 0, "Blend");
		_poisson->process(
			/*input*/	_blendRT,
			/*output*/	_poissonRT);

//		show(*_poissonRT, 0, "Poisson");
		uint displayedRT = 0;
		switch( _whichRT )
		{
		case 1: displayedRT = _warpRT[0]->texture(1); break;
		case 2: displayedRT = _warpRT[1]->texture(1); break;
		case 3: displayedRT = _warpRT[2]->texture(1); break;
		case 4: displayedRT = _warpRT[3]->texture(1); break;
		case 5: displayedRT = _blendRT->texture(); break;
		default:displayedRT = _poissonRT->texture();
		}

		_copy->process(
			/*input*/	displayedRT,
			/*output*/	dst);
	}

} /*namespace sibr*/ 
