/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SPIXELWARP_SPIXELWARP_HPP__
# define __SIBR_EXP_SPIXELWARP_SPIXELWARP_HPP__


# include "Config.hpp"
# include <core/graphics/Config.hpp>
# include <core/view/ViewBase.hpp>
# include <core/renderer/PoissonRenderer.hpp>
# include <core/renderer/CopyRenderer.hpp>

# include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>
# include <projects/spixelwarp/renderer/SWARPRenderer.hpp>
# include <projects/spixelwarp/renderer/BlendRenderer.hpp>


namespace sibr { 
	/**
	*** Dev Note: - '\<any\>View' classes should not contain shaders. If you need
	***             new shaders/new components in your pipepline, create a
	***             small Renderer class.
	**/

	class SIBR_EXP_SPIXELWARP_EXPORT SpixelWarpView : public sibr::ViewBase
	{
		SIBR_CLASS_PTR( SpixelWarpView );
		typedef std::shared_ptr<SpixelWarpView>		Ptr;

	public:
		SpixelWarpView( const BasicIBRScene::Ptr& scene, uint numWarp, const std::string& datasetPath, uint render_w, uint render_h, bool useMergedDepth=false);

		virtual void		onUpdate( Input& /*input*/ )	{ }
		virtual void		onRenderIBR( sibr::IRenderTarget& dst, const sibr::Camera& eye );

		void				setNumWarp( uint numWarp, BasicIBRScene::Ptr scene );

		void				setMasks( const std::vector<RenderTargetLum::Ptr>& masks ) ;

	private:
		std::shared_ptr<sibr::BasicIBRScene> 	_scene;
		std::vector<InputCameraPreprocess>		_mvsCams;
		std::vector<RenderTargetRGBA32F::Ptr>	_warpRT;
		RenderTargetRGBA::Ptr					_blendRT;
		RenderTargetRGBA::Ptr					_poissonRT;

		SWARPRenderer::Ptr		_swarp;
		BlendRenderer::Ptr		_blend;
		PoissonRenderer::Ptr	_poisson;
		CopyRenderer::Ptr		_copy;

		uint				_numWarp;
	};

	///// INLINES /////

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SPIXELWARP_SPIXELWARP_HPP__
