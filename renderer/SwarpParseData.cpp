/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#include "SwarpParseData.hpp"

namespace sibr {
	
	SwarpParseData::SwarpParseData(const sibr::BasicIBRAppArgs & myArgs)
	{
		if (directoryExists(myArgs.dataset_path.get() + "/spixelwarp/")) {
			ParseData::getParsedData(myArgs, "/spixelwarp/");
		}
		else {
			ParseData::getParsedData(myArgs, "");
		}
		//SIBR_LOG << "Derived called!" << std::endl;
	}

	bool SwarpParseData::parseSwarpClippingPlane(const sibr::BasicIBRAppArgs & myArgs)
	{

		std::string swarpPath;
		if (directoryExists(myArgs.dataset_path.get() + "/spixelwarp/")) {
			swarpPath = myArgs.dataset_path.get() + "/spixelwarp/";
		}
		else {
			swarpPath = myArgs.dataset_path.get();
		}
		std::ifstream clipping_planes(swarpPath + "/clipping_planes.txt");
		if (!clipping_planes.is_open()) {
			return false;
		}

		float nearZ, farZ;

		clipping_planes >> nearZ >> farZ;

		for (int i = 0; i < _camInfos.size(); ++i) {
			_camInfos[i]->znear(nearZ);
			_camInfos[i]->zfar(farZ);
		}
		return true;
	}

}