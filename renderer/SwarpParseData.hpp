/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#pragma once
#include <core/scene/ParseData.hpp>
# include "Config.hpp"


namespace sibr 
{

	class SIBR_EXP_SPIXELWARP_EXPORT SwarpParseData : public sibr::ParseData
	{
		SIBR_CLASS_PTR(SwarpParseData);
	public:
		
		SwarpParseData(const sibr::BasicIBRAppArgs & myArgs);
		bool SwarpParseData::parseSwarpClippingPlane(const sibr::BasicIBRAppArgs & myArgs);
	};

}