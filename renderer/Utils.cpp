/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <core/system/LoadingProgress.hpp>
#include <core/system/String.hpp>
#include <core/scene/BasicIBRScene.hpp>
#include <projects/spixelwarp/renderer/MVSFile.hpp>
#include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>
#include <projects/spixelwarp/renderer/Utils.hpp>

//----------------------------------------------------------------

#define NUM_PLANMODEL_BASE	3		//< Base of a plane: 3 points.
#define MIN_NUM_MVS			12		//< Minimun number of MVS points in a spixel to compute a plane.
#define MAX_RAT_FILTER		0.6		//< Maximun 55% of points can be filterd, otherwise gemetric median is NOT VALID!

#define RANSAC_THRESHOLD	1e-2	//< RANSAC threshold.
#define RANSAC_prob			0.99	//< RANSAC probability of choosing at least one sample free from outliers.
//#define MAX_ITER			1000	//< Maximum number of iterations.
//#define maxDataTrials		100		//< Maximun num of attemps to find a non degenerative model.

#define GEN_RAND_NUM(min,max) ((double)rand()/RAND_MAX)*(max-min) + min		//< Randon number generator (min,max)

//----------------------------------------------------------------

namespace sibr { 



	std::vector<SpixelImage::Ptr>	loadSuperpixels( const BasicIBRScene& scene, const std::string& dataset_path)
	{
		const std::vector<InputCamera::Ptr>&		cams = scene.cameras()->inputCameras();
		std::vector<SpixelImage::Ptr> spixelImage(cams.size(), nullptr);

		uint max_spixel_gid = 0;

		//  Load all superpixel segmentations in parallel using threads
		SIBR_LOG << "[SWARPRenderer] parallel superpixels load " << std::endl;

#pragma omp parallel for
		for ( int i = 0; i < cams.size(); ++i )
		{
			if (cams[i]->isActive())
			{
				spixelImage[i] = SpixelImage::Ptr(new SpixelImage(i));
				std::string filename = dataset_path + sibr::sprint("superpixels/%08d.sp", i);
				spixelImage[i]->load(filename);

				for ( uint j = 0; j < spixelImage[i]->numSpixels(); ++j)
					spixelImage[i]->getSpixel(j).setGID(Spixel::GID(i, j));
			}
		}
		return spixelImage;
	}

	void	loadMVSPoints( BasicIBRScene& scene, std::vector<SpixelImage::Ptr>& spixelImage, const std::string& dataset_path, std::vector<InputCameraPreprocess>& cams)
	{
		LoadingProgress		progress(cams.size(), "[SWARPRenderer] Loading MVS Points...");
#pragma omp parallel for
		for ( int i = 0; i < cams.size(); ++i )
		{
			if (cams.at(i).isActive())
			{
				MVSFile		file;
				file.load(dataset_path + sibr::sprint("depth/%08d.mvs",i), false);

				MVSPoint::List mPnts;
				for ( int j = 0; j < file.spixels().size(); ++j )
				{
					const MVSFile::Spixel& spixels = file.spixels().at(j);
					int   numPoints = 0;
					float med_depth = 0;
					std::vector<float> depths;
					std::vector<Vector3f>   xyd;

					for (const MVSPoint& point: spixels.points)
					{
						/// Convert depth from [-1, 1] to [0, 1]
						/// \todo SEB TODO: this comment above say 'convert' it's not done,
						/// and it should (see shaders)
						/// SEB: after investigations, all the code seems to use it as [-1,1]
						/// except shaders that all expect [0,1]
						Vector3f v(point.xyz().x(), point.xyz().y(), point.xyz().z());
						xyd.push_back(v);
						depths.push_back(v.z());
					}

					// 5 MVS points are sufficient and at least 2 MVS points are necessary
					xyd.resize(std::min(int(xyd.size()),5));
					if ((int)spixelImage[i]->numSpixels() <= j)
					{
						SIBR_ERR << "invalid spixel id '" << j << " (img " << i << " contains only "
							<< spixelImage[i]->numSpixels() << " spixels)" << std::endl;
					}

					/// \todo TODO: Not Generic ? (confirm)
					/// Check if there is at least 4 MVS points per spixels
					spixelImage[i]->getSpixel(j).setActive(xyd.size()>3);

					std::sort(depths.begin(), depths.end());
					med_depth = (depths.empty() ? 0 : depths[depths.size()/2]); /// \todo SEB TODO: Why recomputing med_depth if already in .mvs ?
					spixelImage[i]->getSpixel(j).setMedianDepth(med_depth);

					for (uint k=0; k<xyd.size(); k++)
					{
						Vector3f p3 = cams.at(i).unproject(xyd[k]);
						mPnts.emplace_back(new MVSPoint(p3, j));
					}
				}

				cams.at(i).mvsPoints(mPnts);
			}
			progress.walk();
		}
	}

	std::vector<Texture2DRGBA32F::Ptr>	loadSpixelMap( const BasicIBRScene& scene, const std::vector<SpixelImage::Ptr>& spixelImage )
	{
		// Load superpixel image - each pixel has superpixel id in R channel
		// and id's of 3 neighboring superpixels at same depth in GBA channels
		SIBR_LOG << "[SWARPRenderer] uploading superpixel segmentations to GPU " << std::endl;

		const std::vector<InputCamera::Ptr>&		cams = scene.cameras()->inputCameras();
		std::vector<Texture2DRGBA32F::Ptr>	spixelMap(cams.size());
		for (uint i=0; i<cams.size(); i++)
		{
			if (cams.at(i)->isActive()) 
				spixelMap[i].reset(new sibr::Texture2DRGBA32F(*spixelImage[i]->map()));
			else
				spixelMap[i].reset();
		}

		return spixelMap;
	}

	std::vector<Texture2DLum32F::Ptr> loadMergedDepth( BasicIBRScene& scene, const std::string& datasetPath )
	{
		int								camCount = (int)scene.cameras()->inputCameras().size();
		std::vector<sibr::ImageL32F>	images(camCount);

		SIBR_ASSERT(camCount == scene.renderTargets()->inputImagesRT().size());

#pragma omp parallel for
		for (int i = 0; i < camCount; ++i)
		{
			if (scene.cameras()->inputCameras().at(i)->isActive() == false)
				continue;

			std::ostringstream filename;
			filename << datasetPath << "/depth/merged_" << sibr::imageIdToString(i) << ".tiff";
			sibr::Image<uint16, 3>	loader;
			if (loader.load(filename.str()) == false)
				SIBR_ERR << "can't open merged depth '" << filename.str()
				<< "' (Did you run merge_depth.exe before?)." << std::endl;

			float downscale = 2.f;
			loader = loader.resized(
				int(scene.renderTargets()->inputImagesRT().at(i)->w()/downscale),
				int(scene.renderTargets()->inputImagesRT().at(i)->h()/downscale));

			sibr::ImageL32F img(loader.w(), loader.h());
			for (uint y = 0; y < loader.h(); ++y)
				for (uint x = 0; x < loader.w(); ++x)
					img.color(x, y, loader.color(x, y));

			img.flipH();
			images[i] = std::move(img);
		}

		std::vector<Texture2DLum32F::Ptr>	mergedDepthTex(camCount);
		for (int i = 0; i < camCount; ++i)
			mergedDepthTex[i].reset(new Texture2DLum32F(images[i]));

		return mergedDepthTex;
	}


} /*namespace sibr*/ 
