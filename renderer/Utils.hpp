/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SPIXELWARP_UTILS_HPP__
# define __SIBR_EXP_SPIXELWARP_UTILS_HPP__

# include <core/scene/BasicIBRScene.hpp>
# include <projects/spixelwarp/renderer/SpixelImage.hpp>
# include <projects/spixelwarp/renderer/InputCameraPreprocess.hpp>
//# include "PlanarSpixelPreprocess.hpp"

namespace sibr { 
		/** Load superpixel segmentations from \a $DATASET/superpixel/.sp */
		SIBR_EXP_SPIXELWARP_EXPORT std::vector<SpixelImage::Ptr>
			loadSuperpixels( const BasicIBRScene& scene, const std::string&  );

		/** Load MVS points from \a $DATASET_DIR/depth/.mvs */
		SIBR_EXP_SPIXELWARP_EXPORT void
			loadMVSPoints( BasicIBRScene& scene, std::vector<SpixelImage::Ptr>& spixelImage, const std::string&, std::vector<InputCameraPreprocess>& cams);

		SIBR_EXP_SPIXELWARP_EXPORT std::vector<Texture2DRGBA32F::Ptr>
			loadSpixelMap( const BasicIBRScene& scene, const std::vector<SpixelImage::Ptr>& spixelImage );

		SIBR_EXP_SPIXELWARP_EXPORT std::vector<Texture2DLum32F::Ptr>
			loadMergedDepth( BasicIBRScene& scene, const std::string&  );

		SIBR_EXP_SPIXELWARP_EXPORT void
			readNew3DPointsSpixels(const std::string& fname,
				std::vector<std::vector<sibr::Vector3f>>& list3DPtLists);

		SIBR_EXP_SPIXELWARP_EXPORT void
			init3DPtList(std::string& path, std::shared_ptr<sibr::SpixelImage> spim, int imgid);


		// list of 3D points in spixels which are excluded from pmvs point treatment (after inpainting for example)
		static std::vector<std::vector<sibr::Vector3f>> list3DPtLists;
		static uint MAX_ITER, maxDataTrials;
		static bool isBig = false;
		static int badCnt = 0;


} /*namespace sibr*/ 

#endif // __SIBR_EXP_SPIXELWARP_UTILS_HPP__
