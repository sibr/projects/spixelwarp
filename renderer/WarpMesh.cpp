/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#include <projects/spixelwarp/renderer/WarpMesh.hpp>

namespace sibr { 
	WarpMesh::WarpMesh( void ) :
		_vao(0),
		_dataVBO(0),
		_idxVBO(0),
		_indexCount(0),
		_vertCount(0),
		_texcoordCount(0)
	{
	}

	WarpMesh::~WarpMesh( void )
	{
		destroy();
	}

	void	WarpMesh::destroy( void )
	{
		if (!isCreated())
			return;

		GLuint* bufferIDs[] = {&_dataVBO, &_idxVBO};

		glDeleteBuffers(2, bufferIDs[0]);
		glDeleteVertexArrays(1, &_vao);
	}

	void	WarpMesh::create( const IndexData& indices, const VertexData& vert, const VertexData& texcoord )
	{
		if (isCreated())
			destroy();

		// note that vertex is not used, only its size. But I prefer do this
		// size calculation here.
		_indexCount = (uint)indices.size();
		_vertCount = (uint)vert.size();
		_texcoordCount = (uint)texcoord.size();

		GLuint* bufferIDs[] = {&_dataVBO, &_idxVBO};

		glGenVertexArrays(1, &_vao);
		glGenBuffers(2, bufferIDs[0]);

		glBindVertexArray(_vao);
		{
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _idxVBO);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, _indexCount*sizeof(GLuint), &(indices[0]), GL_STATIC_DRAW);

			glBindBuffer(GL_ARRAY_BUFFER, _dataVBO);
			glBufferData(GL_ARRAY_BUFFER, (_vertCount+_texcoordCount)*sizeof(GLfloat), NULL, GL_DYNAMIC_DRAW);

			glEnableVertexAttribArray(0);
			glEnableVertexAttribArray(1);
			glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (GLfloat*)(0));
			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 0, (GLfloat*)(0)+_vertCount);
		}
		glBindVertexArray(0);

	}

	void	WarpMesh::draw( const VertexData& vert, const VertexData& texcoord )
	{
		SIBR_ASSERT( isCreated() );
		SIBR_ASSERT( _vertCount == vert.size() );
		SIBR_ASSERT( _texcoordCount == texcoord.size() );

		glEnable(GL_DEPTH_TEST);
		glClear(GL_DEPTH_BUFFER_BIT);

		glBindVertexArray(_vao);
		{
			uint vertSize = (uint)(sizeof(GLfloat)*vert.size());
			uint texcoordSize = (uint)(sizeof(GLfloat)*texcoord.size());

			glBindBuffer(GL_ARRAY_BUFFER, _dataVBO);
			glBufferSubData(GL_ARRAY_BUFFER, 0, vertSize, &(vert[0]));
			glBufferSubData(GL_ARRAY_BUFFER, vertSize, texcoordSize, &(texcoord[0]));
			glDrawElements(GL_TRIANGLES, _indexCount, GL_UNSIGNED_INT, (void*)0);
		}
		glBindVertexArray(0);

		CHECK_GL_ERROR;
	}

} /*namespace sibr*/ 
