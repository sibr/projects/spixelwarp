/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */



#ifndef __SIBR_EXP_SPIXELWARP_WARPMESH_HPP__
# define __SIBR_EXP_SPIXELWARP_WARPMESH_HPP__

# include <vector>

# include <core/graphics/Config.hpp>
# include <projects/spixelwarp/renderer/Config.hpp>

namespace sibr { 
	/**
	* Act as sibr::Mesh but specialized for SpixelWarp:
	* - has only required paramaters (and texcoord as vec4)
	* - (has GL_DYNAMIC_DRAW)
	*/
	class SIBR_EXP_SPIXELWARP_EXPORT WarpMesh
	{
		SIBR_DISALLOW_COPY( WarpMesh );
		SIBR_CLASS_PTR( WarpMesh );

	public:
		typedef	std::vector<float>	VertexData;
		typedef std::vector<uint>	IndexData;

	public:
		WarpMesh( void );
		~WarpMesh( void );

		void	create( const IndexData& indices, const VertexData& vert, const VertexData& texcoord );
		void	draw( const VertexData& vert, const VertexData& texcoord );

		inline bool		isCreated( void ) const;

	private:
		void			destroy( void );

		GLuint		_vao;
		GLuint		_dataVBO;
		GLuint		_idxVBO;
		uint		_indexCount;
		uint		_vertCount;
		uint		_texcoordCount;

	};

	///// INLINES /////

	bool		WarpMesh::isCreated( void ) const {
		return _vao != 0;
	}

} /*namespace sibr*/ 

#endif // __SIBR_EXP_SPIXELWARP_WARPMESH_HPP__
