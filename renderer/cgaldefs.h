/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


/**
 * \file cgaldefs.h
 *
 * CGAL data structures
 *
 */

#ifndef _CGALDEFS_H_
#define _CGALDEFS_H_

#include <CGAL/Cartesian.h>
#include <CGAL/Delaunay_triangulation_2.h>
#include <CGAL/Triangulation_vertex_base_with_info_2.h>
#include <CGAL/Triangulation_face_base_with_info_2.h>

/** Warp mesh vertex.
 * Extends the CGAL vertex class to add metadata to each vertex.
 * This is used as the vertex class for creating warp meshes
 * in \a ImageWarper class.
 * \sa ImageWarper
 */
class point_info {
  public:
    /** Serial number of the vertex in the warp mesh. */
    int id;

    /** ID of superpixel to which the warp mesh containing
     * current vertex belongs.
     * \sa SpixelImage Spixel
     */
    int spxl_id;

    /** Approximate depth of the superpixel to which the warp
     * mesh containing this vertex belongs. */
    float depth;

    /** Constructor to intialize metadata elements. */
    point_info(void) { id=-1; spxl_id=-1; depth = 0; }
};

/** Warp mesh face or triangle.
 * Extends the CGAL vertex class to add metadata to each warp mesh triangle.
 * This is used as the face or triangle class for creating warp meshes
 * in \p ImageWarper class.
 * \note Currently, this is redundant as no metadata is needed for warp
 * mesh traingles.
 * \sa ImageWarper
 */
class face_info {
  public:
    /** Empty constructor */
    face_info(void) {}
};

/** All CGAL operations using Cartesian kernel, fast and sufficient precision */
typedef CGAL::Cartesian<double>  Kernel;

/** Abbreviation for CGAL point */
typedef Kernel::Point_2 Point;

/** CGAL vertex class that contains extra metadata using \p point_info  */
typedef CGAL::Triangulation_vertex_base_with_info_2<point_info,Kernel> Vb;

/** CGAL traingle class that contains extra metadata using \p face_info  */
typedef CGAL::Triangulation_face_base_with_info_2<face_info,Kernel>    Fb;

/** CGAL traingulation data structure using \p point_info and \p face_info as vertex and triangle classes */
typedef CGAL::Triangulation_data_structure_2<Vb,Fb> Tds;

/** CGAL triangulation used for as warp mesh in \p ImageWarper  */
typedef CGAL::Delaunay_triangulation_2<Kernel,Tds> DelaunayMesh;

/** Vertex handles for traversing the warp mesh  */
typedef DelaunayMesh::Vertex_handle DT_vertex_handle;

/** Face handles for traversing the warp mesh  */
typedef DelaunayMesh::Face_handle DT_face_handle;

/** Face iterators for traversing the warp mesh  */
typedef DelaunayMesh::Finite_faces_iterator DT_face_iterator;

/** Face iterators for traversing the warp mesh  */
typedef DelaunayMesh::Finite_vertices_iterator DT_vertex_iterator;

/** Edge iterators for traversing the warp mesh  */
typedef DelaunayMesh::Finite_edges_iterator DT_edge_iterator;

/** Vertex circulators for traversing the warp mesh  */
typedef DelaunayMesh::Vertex_circulator DT_vertex_circulator;

/** Edge circulators for traversing the warp mesh  */
typedef DelaunayMesh::Edge_circulator DT_edge_circulator;

/** Face circulators for traversing the warp mesh  */
typedef DelaunayMesh::Face_circulator DT_face_circulator;

#endif // _CGALDEFS_H_
