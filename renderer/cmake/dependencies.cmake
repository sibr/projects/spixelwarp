# Copyright (C) 2020, Inria
# GRAPHDECO research group, https://team.inria.fr/graphdeco
# All rights reserved.
# 
# This software is free for non-commercial, research and evaluation use 
# under the terms of the LICENSE.md file.
# 
# For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr


##############
## Find CGAL
##############
set(Boost_REQUIRED_COMPONENTS "${Boost_REQUIRED_COMPONENTS};thread" CACHE INTERNAL "Boost Required Components")
find_package(Boost 1.71.0 REQUIRED COMPONENTS ${Boost_REQUIRED_COMPONENTS})

include_directories(${BOOST_INCLUDEDIR} ${Boost_INCLUDE_DIRS})
link_directories(${BOOST_LIBRARYDIR} ${Boost_LIBRARY_DIRS})

if(Boost_FOUND)
    list(APPEND CGAL_LIBRARIES ${Boost_LIBRARIES})
else()
    message(SEND_ERROR "Boost not found. Set BOOST_ROOT or BOOST_DIR and Boost_LIBRARYDIR.")
    message("CGAL use boost and if it is not build with embedded static boost lib, you will need it at runtime.")
endif()

if (MSVC11 OR MSVC12)
    set(cgal_multiset_arguments 
        CHECK_CACHED_VAR CGAL_INCLUDE_DIR   PATH "CGAL-4.3/include"
        #CHECK_CACHED_VAR CGAL_LIBRARIES	    STRING LIST "debug;CGAL-4.3/${LIB_BUILT_DIR}/CGAL-${CGAL_WIN3RDPARTY_VCID}-mt-gd-4.3.lib;optimized;CGAL-4.3/${LIB_BUILT_DIR}/CGAL-${CGAL_WIN3RDPARTY_VCID}-mt-4.3.lib"
        CHECK_CACHED_VAR CGAL_LIBRARIES	    STRING LIST "debug;CGAL-4.3/${LIB_BUILT_DIR}/CGAL-vc110-mt-gd-4.3.lib;optimized;CGAL-4.3/${LIB_BUILT_DIR}/CGAL-vc110-mt-4.3.lib"
    )
elseif (MSVC14)
    set(cgal_multiset_arguments 
        CHECK_CACHED_VAR CGAL_INCLUDE_DIR   PATH "CGAL-4.10/include"
		CHECK_CACHED_VAR CGAL_LIBRARIES	    STRING LIST "debug;CGAL-4.10/${LIB_BUILT_DIR}/CGAL-vc140-mt-gd-4.10.lib;optimized;CGAL-4.10/${LIB_BUILT_DIR}/CGAL-vc140-mt-4.10.lib"
        )
else ()
    message("There is no provided CGAL library for your version of MSVC")
endif()

sibr_addlibrary(NAME CGAL VCID #VERBOSE ON
    MSVC11 "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC11-splitted%20version/CGAL-4.3.7z"
    MSVC12 "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC11-splitted%20version/CGAL-4.3.7z"
    MSVC14 "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC15-splitted%20version/CGAL-4.10.7z"    # cgal compatible with msvc14
    MULTI_SET ${cgal_multiset_arguments}
)
find_package(CGAL QUIET NO_MODULE NO_CMAKE_BUILDS_PATH)
if(NOT CGAL_FOUND)
    find_package(CGAL REQUIRED)
    include_directories(${CGAL_INCLUDE_DIR})
else()
    if(CGAL_USE_FILE) ## FOUND from the standard official install dir
        include(${CGAL_USE_FILE})
    else()
        message(SEND_ERROR "CGAL not found.")
        set(BUILD_IBR_HYBRID 		OFF)
        set(BUILD_IBR_SPIXELWARP 	OFF)
        message("BUILD_IBR_HYBRID		= ${BUILD_IBR_HYBRID}")
        message("BUILD_IBR_SPIXELWARP	= ${BUILD_IBR_SPIXELWARP}")
    endif()
endif()

sibr_addlibrary(NAME SuiteSparse #VERBOSE ON
MSVC11 "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC11-splitted%20version/SuiteSparse-4.2.1.7z"
MSVC12 "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC11-splitted%20version/SuiteSparse-4.2.1.7z"
MSVC14 "https://repo-sam.inria.fr/fungraph/dependencies/ibr-common/win3rdParty-MSVC11-splitted%20version/SuiteSparse-4.2.1.7z"    # TODO SV: provide a valid version if required
MULTI_SET
    CHECK_CACHED_VAR SuiteSparse_DIR             PATH "SuiteSparse-4.2.1"
    CHECK_CACHED_VAR SuiteSparse_USE_LAPACK_BLAS BOOL ON
)
find_package(SuiteSparse QUIET NO_MODULE)
if(NOT SuiteSparse_FOUND)
SET(SuiteSparse_VERBOSE ON)
find_package(SuiteSparse REQUIRED COMPONENTS amd camd colamd ccolamd cholmod)
if(SuiteSparse_FOUND)
    include_directories(${SuiteSparse_INCLUDE_DIRS})
else()
   message(SEND_ERROR "SuiteSparse not found.")
   set(BUILD_IBR_HYBRID 		OFF)
   set(BUILD_IBR_SPIXELWARP 	OFF)
   set(BUILD_IBR_ULRSKY        OFF)
   message("BUILD_IBR_HYBRID		= ${BUILD_IBR_HYBRID}")
   message("BUILD_IBR_SPIXELWARP	= ${BUILD_IBR_SPIXELWARP}")
   message("BUILD_IBR_ULRSKY	    = ${BUILD_IBR_ULRSKY}")
endif()
else()
message(STATUS "Find SuiteSparse: INCLUDE(${USE_SuiteSparse})")
include(${USE_SuiteSparse})
endif()