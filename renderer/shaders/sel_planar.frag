/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

layout(binding  = 0) uniform sampler2D image;
layout(binding 	= 1) uniform sampler2D spimg;
layout(binding 	= 2) uniform sampler2D mask;

layout(location = 0) out vec4 out_data;
layout(location = 1) out vec4 out_color;

uniform mat4 iCamProj;     // input camera projection

in vec4 texture_coord;  // TODO: only .w is used (but fow now it's useful
// to be able to switch back to the old system, so I keep it)
in vec3 pos3D;
//in vec3 texture_coord;
//in vec2 texture_coord;
uniform bool doMasking;	// do masking

vec3 project(vec3 point, mat4 projmat)
{
  vec4 p1 = projmat * vec4(point, 1.0);
  return (p1.xyz/p1.w);
}

void main(void)
{

  vec3 uvd = project(pos3D, iCamProj);
  uvd = uvd*vec3(0.5) + vec3(0.5);
  //uvd = texture_coord.xyz;        // NOTE: uncomment this to use the old way
  // if (gl_FragCoord.x < 512.0) uvd = texture_coord.xyz; // NOTE: even better way to compare (and set your half width)

  vec2 uv = uvd.xy;

  vec4 rgb 	= texture(image, uv);
  vec4 sp_id 	= texture(spimg, uv);

  float depth = uvd.z;
  float sp_id_img = texture_coord.w;

  if (doMasking && texture(mask, uv).r < 0.5) 
	out_color = vec4(0);
  else if (abs(sp_id_img-sp_id.x) < 0.5      // same superpixel
  || abs(sp_id_img-sp_id.y) < 0.5     // HACK #1: same depth neighbor
  || abs(sp_id_img-sp_id.z) < 0.5     // HACK #1: same depth neighbor
  || abs(sp_id_img-sp_id.w) < 0.5)   // HACK #1: same depth neighbor
  {
    out_data = vec4(uv, depth, sp_id_img);
    out_color = rgb;
    gl_FragDepth =  depth;
  }
  else
  {
    discard;
  }

}
