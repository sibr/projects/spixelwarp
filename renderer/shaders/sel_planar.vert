/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

uniform mat4 proj;


layout(location = 0) in vec3 in_vertex;
layout(location = 1) in vec3 in_color;
layout(location = 2) in vec2 in_texcoord;
layout(location = 3) in vec3 in_normal;

out vec4 texture_coord;
out vec3 pos3D;
//out vec3 texture_coord;
//out vec2 texture_coord;
out vec3 normal_coord;

void main(void) {
	gl_Position = proj * vec4(in_vertex,1.0);
	pos3D = in_vertex;

	// For the moment I want to modify shader few as possible.
	// At the origin, there was only one vec4 texcoord. So I
	// convert back my data to this.
	// TODO: Modify fragment shader (clean) once this is validated
	// //texture_coord = in_texcoord;
	texture_coord.xy = in_texcoord;
	texture_coord.z = in_color.r;
	texture_coord.w = in_color.g;
    normal_coord  = in_normal;
}
