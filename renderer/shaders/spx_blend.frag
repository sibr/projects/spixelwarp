/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

#define NUM_WARPED (4) //value can be modified at runtime by updateNumWarp, this comment is needed to update the value.
#define SPGRAPH_LAYERS (3)
#define SPGRAPH_NEIGHBORS   (SPGRAPH_LAYERS*4)

int roundInt(float n);

vec3 project(vec3 v, mat4 p);

vec3 unproject(vec3 v, mat4 p);

int can_blend(int gida, int gidb, int na[SPGRAPH_NEIGHBORS], int nb[SPGRAPH_NEIGHBORS]);

vec4 blend(vec4 ca, vec4 cb, int gida, int gidb, float da, float db);

layout(location = 0) out vec4 out_color;
in vec4 texcoord;

uniform sampler2D sp_graph [SPGRAPH_LAYERS];
uniform sampler2D warped_uv[NUM_WARPED];
uniform sampler2D input_rgb[NUM_WARPED];
uniform sampler2D mask_rgb[NUM_WARPED];
uniform vec3 icam_pos      [NUM_WARPED];
uniform mat4 in_inv_proj   [NUM_WARPED];
uniform mat4 ncam_proj;
uniform vec3 ncam_pos;
uniform float blend_f;
uniform bool doMasking;	// do masking


void main(void) {
    vec4  samples[NUM_WARPED];
    vec4  masks[NUM_WARPED];
    int   gid    [NUM_WARPED];
    float depth  [NUM_WARPED];

    // The 2 following variables are used to manage Alpha (rendering several pass)
    // They have been introduced with moveobj (but can be used elsewhere)
    bool  alpha  [NUM_WARPED];
    int nbvalidalpha = 0;

    for (int k=0; k<NUM_WARPED; k++) {
        vec4 tc       = texelFetch(warped_uv[k], ivec2(gl_FragCoord), 0);
        vec3 obj      = unproject(tc.xyz, in_inv_proj[k]);
        float angle   = dot(normalize(obj-ncam_pos), normalize(obj-icam_pos[k]));
        float penalty = (tc.w>1e-6 ? max(1e-6,acos(angle)) : 1e6);
        depth   [k]   = project(obj, ncam_proj).z;
        alpha[k]      = (0.0 != texture(input_rgb[k], tc.xy).a);
        penalty       = penalty * pow(5,max(0,k-1));
        nbvalidalpha = (alpha[k])? nbvalidalpha+1: nbvalidalpha;
        samples [k]   = vec4(texture(input_rgb[k], tc.xy).xyz, penalty);
        masks   [k]   = texture(mask_rgb[k], tc.xy);
        gid     [k]   = roundInt(tc.w);
    }
    vec4 color_0 = vec4(0.0,0.0,0.0,1e8); int gid_0 = -1; float depth_0 = -1.0;
    vec4 color_1 = vec4(0.0,0.0,0.0,1e8); int gid_1 = -1; float depth_1 = -1.0;

    for (int k=0; k<NUM_WARPED; k++) {

        if (alpha[k] && samples[k].w<color_0.w) {
            color_1 = color_0;  color_0 = samples[k];
            depth_1 = depth_0;  depth_0 = depth[k];
            gid_1   = gid_0;    gid_0   = gid[k];
  			if (doMasking && masks[k].r < 0.5)  
				color_0 = vec4(0);
        }
        else if (alpha[k] && samples[k].w<color_1.w) {
            color_1 = samples[k];
            depth_1 = depth[k];
            gid_1   = gid[k];
  			if (doMasking && masks[k].r < 0.5)  
				color_1 = vec4(0);
        }
    }
    // used to remove "projected-contour" and masked pixels /
    if (nbvalidalpha < 2 )
      discard;

    vec4 blended_result = blend(color_0, color_1, gid_0, gid_1, depth_0, depth_1);

	out_color    = blended_result;

    gl_FragDepth = blended_result.a;
}

int roundInt(float a) {
    return ((fract(a)>0.5) ? int(ceil(a)) : int(floor(a)));
}

vec3 project(vec3 obj, mat4 proj) {
    vec4 pxl = proj * vec4(obj,1.0);            // project
    pxl   =  pxl / pxl.w;
    pxl.z =  (pxl.z+1.0)/2;
    return pxl.xyz;
}

vec3 unproject(vec3 xyd, mat4 inv_proj) {
    vec4 pxl = vec4(xyd,1.0)*vec4(2.0)-vec4(1.0); // [0,1] -> [-1,1]
    vec4 obj = inv_proj * pxl;                    // unproject
    return (obj.xyz/obj.w);
}

int can_blend(int a_id, int b_id, int a[SPGRAPH_NEIGHBORS], int b[SPGRAPH_NEIGHBORS]) {
    bool eligible = false;
    for (int i=0; i<SPGRAPH_NEIGHBORS; i++)
        eligible = (eligible || (a_id == b[i]));                      // A and B are neighbors
    if (a[0]==0 && b[0]==0) return 0;                                 // both don't have neighbors
    if (a[0]!=0 && b[0]==0) return 1;                                 // A has neighbors
    if (a[0]==0 && b[0]!=0) return 2;                                 // B has neighbors
    if ((a[0]==-1 && b[0]!=0) || (a[0]!=0 && b[0]==-1) || eligible)   // A and B are neighbors
        return 4;
    return 3;                                                         // not neighbors, have other neighbors
}

vec4 blend(vec4 c0, vec4 c1, int gid0, int gid1, float d0, float d1) {
    float w0 = 1.0 / max(1e-6,c0.w);          // blending weights
    float w1 = 1.0 / max(1e-6,c1.w);

    float isDiff = float(abs(d0-d1)>0.05);
    float isBack = float(d0>d1);
    float m_f    = (1-isDiff)*1.0 + isDiff*blend_f*blend_f;
    float depth_f= (1-isBack)*(1.0/m_f) + isBack*m_f;

    vec4 s0[SPGRAPH_LAYERS],    s1[SPGRAPH_LAYERS];
    int  n0[SPGRAPH_NEIGHBORS], n1[SPGRAPH_NEIGHBORS];
    for (int i=0; i<SPGRAPH_LAYERS; i++) {
        int y0 = gid0>>10; int x0 = gid0-(y0<<10);
        int y1 = gid1>>10; int x1 = gid1-(y1<<10);
        s0[i] = texelFetch(sp_graph[i],ivec2(x0,y0),0);  // spixel correspondences for 1
        s1[i] = texelFetch(sp_graph[i],ivec2(x1,y1),0);  // spixel correspondences for 2
        for (int j=0; j<4; j++) {
            n0[4*i+j] = roundInt(s0[i][j]);
            n1[4*i+j] = roundInt(s1[i][j]);
        }
    }

    int blend_case = can_blend(gid0, gid1, n0, n1);
    switch (blend_case) {
        case 0: w0 *= depth_f; break;  // both contain no MVS points, favour background
        case 1: w0 *= blend_f; break;  // w0 has MVS points, favor it
        case 2: w0 /= blend_f; break;  // w1 has MVS points, favor it
        case 3: w0 *= blend_f; break;  // non corresponding, prefer w0
        case 4: break;                 // corresponding, angle weights
    }

    float ws  = w0 + w1;
    float nz  = float(ws > 0.01);    // avoid divide by 0 error

    vec3  blended_color = nz*(c0.rgb*w0 + c1.rgb*w1)/ws + (1.0-nz)*vec3(0);
    float blended_depth = nz*(d0*w0 + d1*w1)/ws + (1.0-nz)*0.999;

    return vec4(blended_color, blended_depth);
}
