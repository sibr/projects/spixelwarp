/*
 * Copyright (C) 2020, Inria
 * GRAPHDECO research group, https://team.inria.fr/graphdeco
 * All rights reserved.
 *
 * This software is free for non-commercial, research and evaluation use 
 * under the terms of the LICENSE.md file.
 *
 * For inquiries contact sibr@inria.fr and/or George.Drettakis@inria.fr
 */


#version 420

#define USE_CUSTOM_DEPTH (1)

layout(binding = 0) uniform sampler2D image;
layout(binding = 1) uniform sampler2D spimg;
#if USE_CUSTOM_DEPTH == 1
layout(binding = 2) uniform sampler2D customDepth;
#endif
layout(binding 	= 3) uniform sampler2D mask;

layout(location= 0) out vec4 out_data;
layout(location= 1) out vec4 out_color;

in vec4 texcoord;
uniform bool doMasking;	// do masking

void main(void) {
    vec4 rgb   = texture(image, texcoord.xy);
    vec4 sp_id = texture(spimg, texcoord.xy);
    float sp_gid_1 = texcoord.w;

#if USE_CUSTOM_DEPTH == 1
  float depth   = texture(customDepth, texcoord.xy).x; //<== Use depth from the image
#else
  float depth   = texcoord.z;  //<== Use depth from the drawn triangle
#endif

	if( doMasking && (texture(mask, texcoord.xy).r < 0.5))
		out_color = vec4(0);
    else if (  abs(sp_gid_1-sp_id.x) < 0.5     // same superpixel
            || abs(sp_gid_1-sp_id.y) < 0.5     // HACK #1: same depth neighbor
            || abs(sp_gid_1-sp_id.z) < 0.5     // HACK #1: same depth neighbor
  			|| abs(sp_gid_1-sp_id.w) < 0.5 )  // HACK #1: same depth neighbor
    {
        gl_FragDepth= depth;
        out_data = vec4(texcoord.xy, depth, sp_gid_1);
        out_color= rgb;
    }
    else discard;
}
